<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Taiwan Specialty - Culture & Heritage | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="thingslevel2">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                       <!--<img alt="Background Image" class="background-image" src="img/taiwanSpecialtyHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="#main">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                       <!--<img alt="Background Image" class="background-image" src="img/nightMarketHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">    <p class="lead">
                                    <a class="btn btn-sm mt104" href="night-market.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                       <!--<img alt="Background Image" class="background-image" src="img/fineDiningHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="fine-dining.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>


                <!-- level1輪播 -->
                <!--<li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 4" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/culinaryHero.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="culinary.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
        </section>

        <section class="bg-primary" id="main" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold" v-html="thingslevel2.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="thingslevel2.description">
                        <!--<h2>TAIWANESE</h2>
                            <p>Grazing is the recommended approach to food in Taiwan, lest you be overwhelmed by choices. Every town, county, and region have its special gastronomic delights. Roughly speaking, Taipei is associated with the beef noodles, the hearty hotpots and stews, and the buns, dumplings, and pancakes of the post-WWII Chinese migrants, and of course, authentic Japanese and Western cuisines. Eastern Taiwan evokes barbecued boar, sticky rice in bamboo tubes, red quinoa and millet wine – the foods of the indigenous people. Kinmen Island shares the passion for oyster croquettes of the Fujianese across the water. ‘Aiyu’ fig jelly was born in Chiayi.  Kaohsiung, Pingtung and Penghu Island take pride in their seafood; Taichung, their traditional Chinese pastries, including the quintessential souvenir, pineapple cake. </p>
                            <p>But above all, Tainan, Taiwan’s oldest city, is where many Taiwanese dishes originated. Tainan was the center of politics, trade, and culture on the island for almost three centuries, from the start of the Dutch period (early 17th century) to Taipei’s rise to prominence (late 19th century). During that time, the city flourished, and people from neighboring regions moved there in search of opportunities. They brought their influences to bear upon the food, and in the ensuing decades, these dishes drifted to dining tables all over the nation, where they continued to evolve. Native Taiwanese chow includes the ubiquitous oyster omelet and slack-season “danzai” noodles, which started out as noodles sold by fishermen during the typhoon season when it was too dangerous to be out at sea. </p>
                            <p>Some other common Taiwanese foods are braised pork belly over rice and Chiayi’s version with shredded turkey. Sticky rice is common and comes mixed with pig’s blood and steamed, in a large dumpling wrapped in lotus leaves, or cooked with roe-heavy mud crab. The Taiwanese also love desserts which can be a popsicle made with the juice of local limes, or an elaborate pouf of shaved ice, piled high with fresh fruit, sweetened pulses, syrup, and confectionary. </p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">

            <div class="container">
                <div><h3>Features</h3></div>
                <div class="row">
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li class="feature-pad" >
                                <a href="taiwan-specialty.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="night-market.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="fine-dining.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">
                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project" >
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 1">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 2">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 3">
                            <a href="temples.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 4">
                            <a href="culinary.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 5">
                            <a href="outdoor.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 6">
                            <a href="wellness.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 7">
                            <a href="ecotourism.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end of Things to do  -->

        <?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <!--         <div class="modal-strip bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 overflow-hidden">
                                <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                                <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                                <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });

        var vm = new Vue({
            el: "#thingslevel2",
            data: {
                things: [],
                things2: [],
                search_key: ''
            },
            methods: {
                edit: function (id) {
                    alert(id);
                },
                Get: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'things_to_do_level1',
                        },
                        success: function (res2) {
                            console.log(res2);
                            res2 = JSON.parse(res2);

                            if (res2.success) {
                                vm.things2 = res2.data;

                                //settimeout show img
                                setTimeout(function () {
                                    console.log('ReFresh masonry');
                                    $('.masonry').masonry();

                                }, 1000);
                            }
                        },
                        error: function (res2) {
                            console.log(res2);
                        }
                    });
                }
            },
            computed: {
                searched_table_data() {
                    let clone = JSON.parse(JSON.stringify(this.things));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                },
                searched_table_data2() {
                    let clone = JSON.parse(JSON.stringify(this.things2));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                }
            },
            created: function () {
                $.ajax({
                    type: "POST",
                    url: $.config.WebApiUrl,
                    data: {
                        'global_oTable': 'things_to_do_level2'
                    },
                    success: function (res) {
                        console.log(res);
                        res = JSON.parse(res);

                        if (res.success) {
                            vm.things = res.data;
                            vm.Get();

                        } else {
                            alert(res.msg);
                            //show_remind( res.msg , "error" );
                        }
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            }
        });
    </script>
</body>

</html>
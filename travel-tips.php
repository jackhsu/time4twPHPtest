<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>TRAVEL TIPS - Before You Go| Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="beforeyougo">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <!--<li class="vid-bg image-bg overlay">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
                    </div>
                    <div class="fs-vid-background">
                        <video muted="" loop="">
                            <source src="video/video.webm" type="video/webm">
                            <source src="video/video.mp4" type="video/mp4">
                            <source src="video/video.ogv" type="video/ogg">
                        </video>
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 2" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + beforeyougo.image + ')' }">
                   <!--<img alt="Background Image" class="background-image" src="img/travelTipsHero.jpg">-->
                    </div>

                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/getreadyLogo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>
        <section class="bg-primary" v-for="beforeyougo in searched_table_data"  v-show="beforeyougo.id == 2">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold italic" v-html="beforeyougo.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="beforeyougo.description">
                        <!-- <p>Taiwan’s convenience stores, open 24 hours, are much more than places for cheap food and drinks. You can also pay utility bills, buy transportation and concert tickets, pay for and pick up online purchases, or have goods delivered at convenience stores throughout Taiwan. Some stores also have dining areas, restrooms and ATMs.</p><p>
                        The recycling rate in Taiwan is a high 55%. There are almost no trash cans on the streets but you’ll likely find recycle bins in the larger train stations and some public areas. The country’s music-playing garbage truck usually swings by neighborhoods twice a day, five days a week. That’s when people bring out their sorted garbage from their homes.  </p><p>
                        Vegetarians eat very well in Taiwan. There is at least one vegetarian buffet in every neighborhood. High quality and an impressive variety define these buffets. Western-style vegan eateries are also becoming increasingly popular in the larger cities. </p><p>
                        Taiwan’s public restrooms are very clean and usually provide toilet paper. Most public restrooms, especially those in parks, temples, and gas stations, have squat style facilities, but there are usually a couple of sit-down stalls as well. </p><p>
                        Some mobile phone providers may require foreigners to show two forms of photo ID to register for a SIM card. </p><p>
                        Keep in mind that earthquakes and typhoons affect Taiwan. Be sure to check flights and ferry times during typhoon season, as cancellations are frequent. It is best to avoid mountainous areas after earthquakes and typhoons.  </p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Before you go -->
        <section class="pt0">

            <div class="container">

                <!--     <div class="row masonry-loader">
                        <div class="col-sm-12 text-center">
                            <div class="spinner"></div>
                        </div>
                    </div> -->
                <div class="row ">

                    <div class="col-md-4 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 2">
                            <a href="travel-tips.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 3">
                            <a href="getting-there.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="row masonry">
                    <div class="col-md-4 col-sm-6 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 4">
                            <a href="getting-around.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 5">
                            <a href="visas.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 6">
                            <a href="language.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                </div>

            </div>
        </section><!-- end of Before you go -->

        <?php include_once("incl/footer.php"); ?>

    </div>

    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        
        $(document).ready(function() {

            var vm = new Vue({
                el: "#beforeyougo",
                data: {
                    imgPath: $.config.imgPath,
                    before: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.before));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'before_you_go'
                        },
                        success: function (res) {
                            console.log(res);
                            res = JSON.parse(res);

                            if (res.success) {
                                vm.before = res.data;
                                vm.imgPath = vm.imgPath;

                                js_initail();

                            } else {
                                alert(res.msg);
                                //show_remind( res.msg , "error" );
                            }
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                }
            });
        
        });
    </script>
</body>

</html>
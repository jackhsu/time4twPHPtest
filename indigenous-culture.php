<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Indigenous Culture - Culture & Heritage | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="thingslevel1">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 2" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" src="img/indigenousCultureHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="#main">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                
                <!-- level1輪播 -->
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 3" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="temples.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 4" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="culinary.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 5" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="outdoor.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 6" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="wellness.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 7" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="ecotourism.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 1" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="culture-and-heritage.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                
                
                <!-- level2輪播 -->
                <!--<li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data2" v-show="thingslevel2.id == 4" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/lanyuIslandHero.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="lanyu-island.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data2" v-show="thingslevel2.id == 5" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/eastCoastHero.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="east-coast.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                    
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data2" v-show="thingslevel2.id == 6" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/indigenousMuseumsHero.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="museums-for-indigenous-art-and-history.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li> -->
            </ul>
        </section>

        <section class="bg-primary" id="main">
            <div class="container" v-for="thingslevel1 in searched_table_data"  v-show="thingslevel1.id == 2">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold" v-html="thingslevel1.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="thingslevel1.description">
                        <!--                        <p>There are 16 recognized tribes in Taiwan, with a population of over half a million or two percent of the total population. Their ancestors had lived here from between 7,000 to 2,000 years ago, and belong to the Austronesian ethnic group. </p>
                                                <p>The recent years have seen a revival of indigenous practices in the villages. Younger villagers are regaining pride in their roots, and a growing number are striving to build a sustainable tourism industry around them. This means more ethnic homestays, guided tours, and workshops promoting agriculture, handicraft, language, music, and restaurants serving indigenous fare. </p>
                                                <strong>The villages, which you can visit, are roughly dispersed over: </strong>
                                                <br><br>
                                                <ul>
                                                    <li>Hualien, Taitung, and Taroko Gorge (East coast) – Amis, Bunun, Truku  </li>
                                                    <li>The central and northern mountainous regions – Atayal  </li>
                                                    <li>The central and southern mountains – Bunun  </li>
                                                    <li>Kaohsiung and Pingtung (South) – Paiwan and Rukai </li>
                                                    <li>Nantou and Chiayi, including Alishan National Park (Central Southern) – Tsou   </li>
                                                    <li>Lanyu Island – Tao  </li>
                                                </ul>
                                                <p>The Amis are the most populous, followed by the Paiwan and the Atayal. Most indigenous people speak at least some Mandarin. The tribes share similarities, yet they are also different. For example, all except the Tao engage in the ritualistic drinking of millet wine. The Paiwan and Rukai make sophisticated woodcarvings of animals, ancestors, and human heads, the last a nod to the custom of headhunting. The Tao share this figurative wood carving tradition, but the pieces are simpler and often sport a maritime motif. The Atayal and Seediq are known for their weaving and the Bunun for their hunting and their sophisticated eight-part harmonic singing. Facial tattooing was practiced by both genders of the Atayal and the Truku to honor achievements in hunting and weaving, and ensure their ancestors in the afterlife would recognize the deceased.</p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">

            <div class="container">
                <div><h3>Features</h3></div>
                <div class="row">
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li class="feature-pad" >
                                <a href="lanyu-island.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 4" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 4" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="east-coast.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 5" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 5" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="museums-for-indigenous-art-and-history.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 6" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 6" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">
                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project" >
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 1">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 2">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 3">
                            <a href="temples.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 4">
                            <a href="culinary.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 5">
                            <a href="outdoor.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 6">
                            <a href="wellness.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data" v-show="thingslevel1.id == 7">
                            <a href="ecotourism.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end of Things to do  -->

        <?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <!--         <div class="modal-strip bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 overflow-hidden">
                                <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                                <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                                <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });

        var vm = new Vue({
            el: "#thingslevel1",
            data: {
                things: [],
                things2: [],
                search_key: ''
            },
            methods: {
                edit: function (id) {
                    alert(id);
                },
                Get: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'things_to_do_level2',
                        },
                        success: function (res2) {
                            console.log(res2);
                            res2 = JSON.parse(res2);

                            if (res2.success) {
                                vm.things2 = res2.data;

                                //settimeout show img
                                setTimeout(function () {
                                    console.log('ReFresh masonry');
                                    $('.masonry').masonry();

                                }, 1000);
                            }
                        },
                        error: function (res2) {
                            console.log(res2);
                        }
                    });
                }
            },
            computed: {
                searched_table_data() {
                    let clone = JSON.parse(JSON.stringify(this.things));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                },
                searched_table_data2() {
                    let clone = JSON.parse(JSON.stringify(this.things2));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                }
            },
            created: function () {
                $.ajax({
                    type: "POST",
                    url: $.config.WebApiUrl,
                    data: {
                        'global_oTable': 'things_to_do_level1'
                    },
                    success: function (res) {
                        console.log(res);
                        res = JSON.parse(res);

                        if (res.success) {
                            vm.things = res.data;
                            vm.Get();

                        } else {
                            alert(res.msg);
                            //show_remind( res.msg , "error" );
                        }
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            }
        });

    </script>
</body>

</html>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Adventure/Outdoor | Time for Taiwan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include_once("incl/nav.php"); ?>

    <ul class="share hidden-sm hidden-xs">
        <li class="">
        <a href="#">
        <span class="ti-facebook"></span>
        </a>
        </li>
        <li class="">
        <a href="#">
        <span class="ti-twitter-alt"></span>
        </a>
        </li>
        </li>
    </ul>

    <div class="main-container">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
                <ul class="slides">
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/outdoorHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="#main">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                    
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/cyclingHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="cycling.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                   
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/waterSportsHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">    <p class="lead">
                                        <a class="btn btn-sm mt104" href="water-sports.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                   
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/hikingHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                     <p class="lead">
                                        <a class="btn btn-sm mt104" href="hiking.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </section>

        <section class="bg-primary" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold">ADVENTURE/<br>OUTDOOR
                         </h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8">
<p>Blessed with a picture-perfect terrain Taiwan makes for a fabulous outdoor adventure. Luxuriant forests carpet two-thirds of the island, many belonging to a handful of mountain ranges that soar to peaks just shy of 4,000 meters, and retreat to lowland landscapes of rolling plains, terraced flatlands, and meandering rivers. Towns are connected via a vast network of roads to entry points in the mountains. And embracing all of this is a shimmering shoreline of over 1,500 kilometers, gusty Penghu Islands’ included. This is why despite its compact size, Taiwan is one of Asia’s top destinations for cycling, hiking, and windsurfing. </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">
                
                <div class="container">
                    <div><h3>Features</h3></div>
                    <div class="row">
                        <!-- Place somewhere in the <body> of your page -->
                        <div class="flexslider">
                          <ul class="slides">
                            <li class="feature-pad">
<!-- <a href="national-palace-museum.php"> -->
                              <img src="img/waterSportsFeatures.jpg" /><p class="flex-caption">Water Sports</p>
                           </li>
                            <li class="feature-pad">
                              <img src="img/hikingFeatures.jpg" /><p class="flex-caption">Hiking</p>
                            </li>
                            <li class="feature-pad">
                              <img src="img/cyclingFeatures.jpg" /><p class="flex-caption">Cycling</p>
                            </li>                      <li class="feature-pad">
                              <img src="img/hikingFeatures.jpg" /><p class="flex-caption">Hiking</p>
                            </li>
                            <li class="feature-pad">
                              <img src="img/cyclingFeatures.jpg" /><p class="flex-caption">Cycling</p>
                            </li>       
                                            
                          </ul>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">

                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" src="img/things2do-1.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Culture and Heritage</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" src="img/things2do-2.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Indigenous Culture</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="temples.php">
                                <img alt="Pic" src="img/things2do-3.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Temples</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="culinary.php">
                                <img alt="Pic" src="img/things2do-4.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Culinary</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="outdoor.php">
                                <img alt="Pic" src="img/things2do-5.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Adventure/Outdoor</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="wellness.php">
                                <img alt="Pic" src="img/things2do-6.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Wellness</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="ecotourism.php">
                                <img alt="Pic" src="img/things2do-7.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Ecotourism</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- end of Things to do  -->

<?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
<!--         <div class="modal-strip bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow-hidden">
                        <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                        <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                        <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                    </div>
                </div>
            </div>
        </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>
    
    <script>
        $(window).load(function() {
          $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 210,
            itemMargin: 5,
            minItems: 2,
            maxItems: 4
          });
        });
    </script>
</body>

</html>
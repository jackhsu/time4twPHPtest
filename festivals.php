<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Festivals | Time for Taiwan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include_once("incl/nav.php"); ?>

    <ul class="share hidden-sm hidden-xs">
		<li class="">
		<a href="#">
		<span class="ti-facebook"></span>
		</a>
		</li>
		<li class="">
		<a href="#">
		<span class="ti-twitter-alt"></span>
		</a>
		</li>
		</li>
	</ul>

    <div class="main-container">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
                <ul class="slides">
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/festivalsHero-1.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="#main">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                       
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/festivalsHero-2.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="#main">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                    
                </ul>
        </section>

        <section class="bg-primary" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                    	<span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold">CULTRURE
                        <br>&
                         <br>HERITAGE</h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8">

                        <h2>Festivals</h2>

                        <p>Taiwan’s cultural calendar bristles with festivals celebrating everything from a god’s birthday to jazz. The country is undoubtedly one of the best places in the world to experience folk and religious festivals – wonderful to behold and involving everyone. A surprising number of young people help out at religious parades. Onlookers are welcome, sometimes even to take part. There’s always a sense of inclusion regardless of faith and never a sense that the proceedings are too sacred for photography. Taiwan is also one of Asia’s cultural capitals. The host of festivals held in the major cities throw light on what the country’s cultural heavyweights are up to.</p>

                        <h3>Mazu Pilgrimage</h3>

                        <p>This sensational nine-day pilgrimage in March or April celebrates the birthday of Mazu. Covering four counties, 60 temples, and a distance of 400 km, it is the country’s largest religious and folk activity. A palanquin carrying a statue of the goddess travels from Zhenlan Temple (Taichung) to Fengtian Temple (Chiayi) and back, escorted by hundreds of thousands of pilgrims and spectators from Taiwan and abroad. Some believers make the full journey, though most do not. Everywhere the goddess goes, people throng to touch her sedan chair for luck or prostrate themselves so she can pass over them. Participating temples and volunteers provide water, meals, and lodging along the way. 
                        </p><p>                
                        Mazu, also known as goddess of the sea, or Tianhou (Empress of Heaven), is the patron goddess of sailors and seafarers. Fishing communities all over southern China and Asia, with local variations, worship her. Mazu – meaning “grandmother” – was a young girl from Fujian named Lin Moniang (A.D. 960), who is said to have used magical powers to save her brothers (or her father, according to some versions) from drowning. She died shortly afterward. Her widespread popularity may have to do with the fact that early Chinese emigrants traveled to other places by sea, as they did to Taiwan.         Mazu is also known as Tianhou or Empress of Heaven. She is the patron goddess of sailors and seafarers, worshipped by fishing communities all over southern China and Asia. Mazu is believed to have been a young girl from Fujian named Lin Mo-niang (ca A.D. 960), who used divine powers to save her brothers (or her father, according to some versions,) from drowning. She died shortly afterwards. </p>

                        <p>See <a class="link" target="_blank" href="http://www.dajiamazu.org.tw">www.dajiamazu.org.tw</a> for details of each year’s event.</p>

                        <h3>Burning of the Wang Ye Boats</h3>
                        <p>This spectacular folk festival is celebrated in fishing settlements along the southwestern coast. The most extravagant celebrations are in Pingtung county, famously at the vibrant Donglong Temple, a center of folk faith in southern Taiwan.
                        </p><p>
                        Festivities last eight days and see tens of thousands in attendance with their cameras, especially on the first and the final days. Celebrations involve a plethora of rituals. These include beckoning of the gods by mediums, parading the boat through town to collect wicked spirits, and feasting the gods. In the wee hours of the last day, the vessel – ornately furnished and crewed by effigies – is moved to the beach. It is loaded with goods fit for a voyage and the deities are invited to board. Then just before dawn, it is set alight. The festival happens in October every three years, in the Years of the Bull, Dragon, Goat, and Dog. The next one is in 2018. 
                        </p><p>
                        Wang Ye is not royalty or a sage named Wang, as the term might suggest. Rather it refers to any of a host of plague gods of over a hundred names. While ancient boat-burning rituals were specifically concerned with disease prevention, today’s practices seek general protection and good fortune.
                        </p>

                        <h3>Yanshui Beehive Fireworks Festival</h3>
                        <p>Don your motorcycle helmet and your best non-flammable clothing. And don’t forget thick gloves, earplugs, and goggles. Make sure no flesh is exposed. This fireworks festival in the town of Yanshui in Tainan is not for the faint-hearted. Thousands of people in what looks like hazmat suits take to the streets and put themselves willingly in the way of exploding skyrockets. It’s a riot – smoky, ear-splitting, and heart-stoppingly fun.   
                        </p><p>
                        The festival takes place on the 14th and 15th days of the Lunar New Year. An image of Guandi, the God of War, is paraded through the streets from the Martial Temple amidst much fiery fanfare. The highlight is the beehive fireworks – large cages filled with bottle rockets that are set off into the crowd, like a swarm of angry bees. You can go right up to the cages and let the rockets pop before your (protected) eyes or enjoy the proceedings from a safe distance, as many do.  
                        </p><p>
                        The tradition originated in the 19th century when the town was plagued by sickness and the people begged Guandi to end the plague. Miraculously, Guandi paid a visit to Yanshui. Throngs of townsfolk gathered for his arrival, setting off firecrackers as they wound through every street, path, and alley. By dawn, the plague was history. 
                        </p>

                        <h3>Outdoor Music Festivals</h3>

                        <p>Outdoor concerts are fun, all-in experiences merging live music, nature, and local life. Taiwan has all kinds of outdoor music festivals, though mostly indie, rock, and pop, with more joining the party each year.
                        </p><p>
                        The granddaddy of Taiwan’s music festivals, Spring Scream, is known for its cool underground vibe. For several days in April, dozens of parties pop up in Kending around this time and over 200 acts from Taiwan, Asia, and beyond play to thousands in Kending’s grassy Eluanbi National Park.
                        </p><p>
                        Megaport Festival, a two-day extravaganza held at Kaohsiung’s Pier 2 Art District in March, features dozens of rock, punk, electro and metal bands from Asia. Also happening in March but in Taipei is the T-Fest, which has an indie lineup with a strong international component. 
                        </p><p>
                        The three-day Gongliao Hohaiyan Rock Festival on Fulong Beach (New Taipei City) sees participants combining music with water sports. Genres are mostly rock, rap, indie, and folk. You can camp there too. For an even more intense experience, the wild Beastie Rock Festival in Tamsui (New Taipei City) sees rock musicians from Taiwan and Asia whipping the crowd into a frenzy. At Taichung’s Heart Town Festival, the audience screams and slamdances to heavy metal. 
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php include_once("incl/gridThingsToDo.php"); ?>
        <?php include_once("incl/footer.php"); ?>
 
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.min.js"></script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/masonry.min.js"></script>
    <script src="js/twitterfetcher.min.js"></script>
    <script src="js/spectragram.min.js"></script>
    <script src="js/ytplayer.min.js"></script>
    <script src="js/countdown.min.js"></script>
    <script src="js/smooth-scroll.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/scripts.js"></script>
    <script>
		$(window).load(function() {
		  $('.flexslider').flexslider({
		    animation: "slide",
		    animationLoop: false,
		    itemWidth: 210,
		    itemMargin: 5,
		    minItems: 2,
		    maxItems: 4
		  });
		});
    </script>
</body>

</html>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>General Info - Things to do | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="thingstodo" ></div>


    <script>
        $(window).load(function () {

            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });

        });

        $(document).ready(function () {

            function level0() {
                var vm = new Vue({
                    el: "#thingstodo",
                    data: {
                        imgPath: $.config.imgPath,
                        things: [],
                        things2: [],
                        search_key: ''
                    },
                    methods: {
                        edit: function (id) {
                            alert(id);
                        },
                        Get: function () {
                            $.ajax({
                                type: "POST",
                                url: $.config.WebApiUrl,
                                data: {
                                    'global_oTable': 'things_to_do_level1',
                                },
                                success: function (res2) {
                                    console.log(res2);
                                    res2 = JSON.parse(res2);

                                    if (res2.success) {

                                        vm.imgPath = vm.imgPath;
                                        vm.things2 = res2.data;

                                        js_initail();

                                    }
                                },
                                error: function (res2) {
                                    console.log(res2);
                                }
                            });
                        }
                    },
                    computed: {
                        searched_table_data() {
                            let clone = JSON.parse(JSON.stringify(this.things));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp;
                        },
                        searched_table_data_level1() {
                            let clone = JSON.parse(JSON.stringify(this.things2));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp;
                        }
                    },
                    created: function () {
                        $.ajax({
                            type: "POST",
                            url: $.config.WebApiUrl,
                            data: {
                                'global_oTable': 'things_to_do',
                            },
                            success: function (res) {
                                console.log(res);
                                res = JSON.parse(res);

                                if (res.success) {
                                    vm.things = res.data;
                                    vm.imgPath = vm.imgPath;
                                    vm.Get();


                                } else {
                                    alert(res.msg);
                                    //show_remind( res.msg , "error" );
                                }
                            },
                            error: function (res) {
                                console.log(res);
                            }
                        });
                    }
                });
            }



            function level1()
            {
                var vm1 = new Vue({
                    el: "#thingstodo",
                    data: {
                        imgPath: $.config.imgPath,
                        things: {},
                        things1: [],
                        things2: [],
                        thingsfeatures: [],
                        search_key: ''
                    },
                    methods: {
                        edit: function (id) {
                            alert(id);
                        }
                    },
                    computed: {
                        searched_table_data_level1() {
                            let clone = JSON.parse(JSON.stringify(this.things1));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp
                        },
                        searched_table_data_level1feature() {
                            let clone = JSON.parse(JSON.stringify(this.thingsfeatures));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp;
                        },
                        searched_table_data_level2() {
                            let clone = JSON.parse(JSON.stringify(this.things2));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp
                        }
                    },
                    created: function () {
                        var ajax1 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level1'}});
                        var ajax2 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level2'}});
                        $.when(ajax1, ajax2).then(function (res1, res2) {

                            //本頁大圖/標題/內文
                            $.each(JSON.parse(res1[0]).data, function (k, v) {
                                var tmp_page = v.title2.replace(/ /g, "-").toLowerCase();
                                if (tmp_page == getParameterByName("page")) {
                                    vm1.things = v;
                                }
                            });

                            //大圖輪播
                            console.log("level1-Hero");
//                            var tmp_arr1 = [];
//                            $.each(JSON.parse(res1[0]).data, function (k, v) {
//                                var tmp_page = v.title2.replace(/ /g, "-").toLowerCase();
//                                if (tmp_page != getParameterByName("page")) {
//                                    v.href = "things-to-do.php?page=" + tmp_arr_level1[ v.id - 1 ];
//                                    tmp_arr1[ tmp_arr1.length ] = v;
//                                }
//                            });
//                            vm1.things1 = tmp_arr1;
//                            console.log(vm1.things1);

                            //things_to_do_grid
                            var tmp_arr2 = [];
                            $.each(JSON.parse(res1[0]).data, function (k, v) {
                                //console.log(v);
                                v.href = "things-to-do.php?page=" + tmp_arr_level1[ v.id - 1 ];
                                tmp_arr2[ tmp_arr2.length ] = v;
                            });
                            vm1.thingsfeatures = tmp_arr2;

                            //feature
                            var tmp_arr3 = [];
                            $.each(JSON.parse(res1[0]).data, function (k, v) {
                                //console.log(v);
                                var tmp_page = v.title2.replace(/ /g, "-").toLowerCase();
                                if (tmp_page == getParameterByName("page")) {
                                    var tmp_level_id = v.id;
                                    //console.log(tmp_level_id);
                                    $.each(JSON.parse(res2[0]).data, function (k, v) {
                                        //console.log(v);
                                        if (tmp_level_id == v.level1_id)
                                        {
                                            v.href = "things-to-do.php?page=" + tmp_arr_level2[ v.id - 1 ];
                                            tmp_arr3[ tmp_arr3.length ] = v;
                                        }
                                    });
                                    vm1.things2 = tmp_arr3;
                                    //console.log(vm1.things2);
                                }
                            });

                            vm1.imgPath = vm1.imgPath;

                            js_initail();

                        });
                    }
                });

            }

            function level2()
            {

                var vm2 = new Vue({
                    el: "#thingstodo",
                    data: {
                        imgPath: $.config.imgPath,
                        things: {},
                        things1: [],
                        things2: [],
                        thingsfeatures: [],
                        search_key: ''
                    },
                    methods: {
                        edit: function (id) {
                            alert(id);
                        }
                    },
                    computed: {
                        searched_table_data_level1() {
                            let clone = JSON.parse(JSON.stringify(this.things1));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp
                        },
                        searched_table_data_level1feature() {
                            let clone = JSON.parse(JSON.stringify(this.thingsfeatures));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp
                        },
                        searched_table_data_level2() {
                            let clone = JSON.parse(JSON.stringify(this.things2));
                            let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                            return temp
                        }
                    },
                    created: function () {
                        var ajax1 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level1'}});
                        var ajax2 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level2'}});
                        $.when(ajax1, ajax2).then(function (res1, res2) {

                            //本頁大圖/標題/內文
                            $.each(JSON.parse(res2[0]).data, function (k, v) {
                                var tmp_page = v.title2.replace(/ /g, "-").toLowerCase();
                                if (tmp_page == getParameterByName("page")) {
                                    vm2.things = v;
                                }
                            });

                            //大圖輪播
                            console.log("level2-Hero");
                            /*var tmp_arr1 = [];
                            $.each(JSON.parse(res2[0]).data, function (k, v) {
                                var tmp_page = v.title2.replace(/ /g, "-").toLowerCase();
                                if (tmp_page != getParameterByName("page")) {
                                    v.href = "things-to-do.php?page=" + tmp_arr_level2[ v.id - 1 ];
                                    tmp_arr1[ tmp_arr1.length ] = v;
                                }
                            });
                            vm2.things1 = tmp_arr1;
                            console.log(vm2.things1);*/

                            //things_to_do_grid
                            var tmp_arr2 = [];
                            $.each(JSON.parse(res1[0]).data, function (k, v) {
                                //console.log(v);
                                v.href = "things-to-do.php?page=" + tmp_arr_level1[ v.id - 1 ];
                                tmp_arr2[ tmp_arr2.length ] = v;
                            });
                            vm2.thingsfeatures = tmp_arr2;

                            //feature
                            var tmp_arr3 = [];
                            $.each(JSON.parse(res1[0]).data, function (k, v) {
                                //console.log(v);
                                var tmp_level_id = v.id;
                                $.each(JSON.parse(res2[0]).data, function (k, v) {
                                    //console.log(v);
                                    if (tmp_level_id == v.level1_id)
                                    {
                                        v.href = "things-to-do.php?page=" + tmp_arr_level2[ v.id - 1 ];
                                        tmp_arr3[ tmp_arr3.length ] = v;
                                    }
                                });
                                vm2.things2 = tmp_arr3;
                                //console.log(vm1.things2);
                            });
                            vm2.imgPath = vm2.imgPath;

                            js_initail();

                        });

                    }
                });

            }

            var tmp_arr_level1 = ["culture-and-heritage", "indigenous-culture", "temples", "cuisine", "adventure/outdoor", "wellness", "ecotourism", "festivals", "test"];
            var tmp_arr_level2 = ["museums", "national-taiwan-museum-of-fine-arts", "national-museum-of-taiwan-literature", "national-palace-museum",
                "lanyu-(orchid)-island", "east-coast", "museums-for-indigenous-art-and-history",
                "baoan-temple", "lugang-longshan-temple", "tainan-confucius-temple", "penghu-tianhou-temple",
                "taiwan-specialty", "night-market", "fine-dining",
                "cycling", "water-sports", "hiking",
                "spa", "hot-spring",
                "butterfly-watching", "bird-watching",
                "mazu-pilgrimage", "burning-of-the-wangye-boats", "yanshui-beehive-fireworks-festival", "outdoor-music-festivals",
                "test"];

            if (tmp_arr_level1.indexOf(getParameterByName("page")) != "-1")
            {
                var tmp_load_html = "things-to-do-level1";
            } else if (tmp_arr_level2.indexOf(getParameterByName("page")) != "-1")
            {
                var tmp_load_html = "things-to-do-level2";
            } else
            {
                var tmp_load_html = "things-to-do-level0";
            }
            console.log(tmp_load_html);

            load_html(
                    "page_queue",
                    "html/" + tmp_load_html + ".php",
                    {},
                    {
                        focus: ".main-container"
                    },
                    function (data)
                    {
                        $(this.data2.cb_focus).show();

                        $(".main-container").html(data);
                        $(".main-container").show();


                        if( tmp_load_html == "things-to-do-level1" )
                        {
                                level1();
                        } 
                        else if( tmp_load_html == "things-to-do-level2" )
                        {
                                level2();
                        }
                        else
                        {
				level0();
                        }


                    },
                    function (data) {
                        console.log(data);
                    }
            );

        });
    </script>
</body>

</html>
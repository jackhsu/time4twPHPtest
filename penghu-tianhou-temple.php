<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Penghu Tianhou Temple - Culture & Heritage | Time for Taiwan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
    <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
</head>

<body>

<?php include_once("incl/nav.php"); ?>

    <ul class="share hidden-sm hidden-xs">
		<li class="">
		<a href="#">
		<span class="ti-facebook"></span>
		</a>
		</li>
		<li class="">
		<a href="#">
		<span class="ti-twitter-alt"></span>
		</a>
		</li>
		</li>
	</ul>

    <div class="main-container">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
                <ul class="slides">
                   
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/tianhouHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="#main">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                     
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/baoanHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="baoan-temple.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                   
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/lugangHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">    <p class="lead">
                                        <a class="btn btn-sm mt104" href="lugang-longshan-temple.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>                   
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/tainanCofuciusHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                	<img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                     <p class="lead">
                                        <a class="btn btn-sm mt104" href="tainan-confucius-temple.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="image-bg">
                        <div class="background-image-holder">
                            <img alt="Background Image" class="background-image" src="img/templesHero.jpg">
                        </div>
                        <div class="container v-align-transform">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                    <p class="lead">
                                        <a class="btn btn-sm mt104" href="temples.php">MORE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li> 
                </ul>
        </section>

        <section class="bg-primary" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                    	<span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold">CULTRURE
                        <br>&
                         <br>HERITAGE</h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8">
                        <h2>Penghu Tianhou Temple</h2>
                        <p>Maze-like alleys in Magong’s historic quarter lead up to the courtyard of Penghu’s most famous temple. Not only is Penghu Tianhou Temple utterly charming, some say it’s Taiwan’s oldest Mazu temple. A monument (displayed in the back hall) presented by the Ming court to a Chinese general who drove the Dutch out of Penghu in 1604 lends confirmation to this claim. This temple has no shortage of enthralling details to affirm its uniqueness.
                        </p><p>
                        The current look and layout of this temple was shaped by a master from Chaozhou in China in 1922. This explains its modest grace and the slight departures from the southern Fujian-style of Taiwan’s other Mazu temples. You may notice the polygonal (rather than rectangular or curved) entrance steps and sweeping swallowtail eaves that appear unusually high. The walls were built from local coral stones. Inside, the temple is an Aladdin’s Cave of art by Taiwanese and Cantonese craftsmen. There are wood and stone carvings, colored paintings, even gold powder paintings – the last of the Cantonese practice of creating images by applying gold powder on damp black paint. 
                        </p><p>
                        A gold-faced Mazu sits in the main hall, protected from typhoons by a set of eight beautiful folding doors installed in the 1940s. The rear hall was once a gathering place for scholars. It is now a small gallery for relics, including the monument mentioned above and a map of Penghu port drawn by the Dutch.
                        </p><p>
                        One of the loveliest things about this temple is the presence of Oriental Skylarks. In spring and summer, their shrill and musical song fills the courtyards as they dart about, sometimes hovering motionless in mid-air.
                        </p><p>
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <?php include_once("incl/featuresTemples.php"); ?>
        <?php include_once("incl/gridThingsToDo.php"); ?>
        <?php include_once("incl/footer.php"); ?>
 
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/flexslider.min.js"></script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/masonry.min.js"></script>
    <script src="js/twitterfetcher.min.js"></script>
    <script src="js/spectragram.min.js"></script>
    <script src="js/ytplayer.min.js"></script>
    <script src="js/countdown.min.js"></script>
    <script src="js/smooth-scroll.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/scripts.js"></script>
    <script>
		$(window).load(function() {
		  $('.flexslider').flexslider({
		    animation: "slide",
		    animationLoop: false,
		    itemWidth: 210,
		    itemMargin: 5,
		    minItems: 2,
		    maxItems: 4
		  });
		});
    </script>
</body>

</html>
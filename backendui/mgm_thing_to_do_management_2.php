<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <title>Management Of THINGS TO DO Level II | go2taiwan</title>


        <link rel="shortcut icon" href="img/favicon.ico"/>
        <link type="text/css" href="css/global.css" rel="stylesheet"/>
        <link href="js/summernote/summernote.css" rel="stylesheet">
        <link href="js/dropzone/dropzone.css" rel="stylesheet">

        <?php include_once("js/all_js.php"); ?>


    </head>
    <body class="skin-default">

        <div class="preloader">
            <div class="loader_img"><img src="img/loader.gif" alt="loading..." height="64" width="64"></div>
        </div>

        <?php include( "html/mgm_header.php"); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">

            <?php include( "html/mgm_sidebar.php"); ?>

            <aside class="right-side">
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="index.html"><i class="fa fa-fw ti-home"></i> Dashboard</a>
                        </li>
                        <li class="active">THINGS TO DO Level II</li>
                    </ol>
                </section>

                <section id="app" class="content">
                    <div class="panel">
                        <div class="panel-body">

                            <h3>THINGS TO DO Level II</h3>

                            <!--sample datatable plugin-->
                            <div class="form-group">
                                <div class="col-sm-6 p-0" style="padding: 0 0 20px;">
                                    <label>Show</label>
                                    <select name="count" class="form-control" style="width:100px;display: inline;">
                                        <option value="0">1</option>
                                        <option value="1">2</option>
                                        <option value="2">3</option>
                                        <option value="3">4</option>
                                        <option value="4">5</option>
                                    </select>
                                    <label>entries</label>
                                </div>

                                <div class="col-sm-6 pull-right" style="padding: 0;">
                                    <input v-model="search_key" style="width: 150px;" class="form-control pull-right" type="text">
                                    <label for="input-text" class="pull-right" style="margin-top: 6px;">Search：</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 ">
                                    <button class="btn btn-info" @click="edit(null)" data-toggle="modal" data-target="#form_modal">Add</button>
                                </div>
                            </div>

                            <!--sample code-->
                            <div class="table-grid">
                                <table class="table table-bordered" width=100%>
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <input name="select_all" value="1" type="checkbox">
                                            </th>
                                            <th class="text-center">Level1_id</th>
                                            <th class="text-center">Title</th>
                                            <th class="text-center">Title2</th>
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Published</th>
                                            <th class="text-center">Recent push time</th>
                                            <th class="text-center">operating</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="place in searched_table_data">
                                            <td class="text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="text-center" v-html="place.level1_id "></td>
                                            <td class="text-center" v-html="place.title "></td>
                                            <td class="text-center" v-html="place.title2 "></td>
                                            <td class="text-center"><img alt="image" class="" v-bind:src="imgPath + place.image">
                                            </td>
                                            <td class="" v-html="place.description"></td>
                                            <td class="text-center">{{ place.published_time }}</td>
                                            <td class="text-center">{{ place.recent_push_time }}</td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-icon btn-default m-r-20" @click="edit(place)" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <modal id="form_modal" class="modal fade animated" role="dialog" v-bind:formdata="formdata"> </modal>
                </section>

                <!--modify dialog-->
                <template id="modal-template" >
                    <div >
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <form  id="formEdit" role="form" @submit.prevent="Save"  @reset.prevent="Reset" >
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Title</label>
                                                    <input class="form-control" placeholder="NORTHERN TAIWAN" v-model="formdata.title"  type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Title2</label>
                                                    <input class="form-control" placeholder="NORTHERN TAIWAN" v-model="formdata.title2"  type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Level1_id</label>
<!--                                                    <input class="form-control" placeholder="" v-model="formdata.level1_id"  type="text">-->
                                                    </select>
                                                    <select v-model="formdata.level1_id">
                                                        <option v-for="option in FaPlaces" v-bind:value="option.id">
                                                            {{ option.id }}
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 m-t-10">
                                                <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <div id="summernote-fa">

                                                        <div id="summernote">

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="col-sm-12 m-t-10">
                                                <div class="form-group">
                                                    <label class="control-label">Upload Image</label>
                                                </div>
                                                <div class='form-group'>
                                                    <img class="img-rounded" style='width:40%;height: auto;' v-bind:src="formdata.imgPath + formdata.image" />
                                                </div>
                                                <div class='form-group'>
                                                    <form action="" method="post" class="dropzone" id="my-awesome-dropzone"></form>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 m-t-10">
                                                <div class="form-group">
                                                    <label class="control-label">Upload Image2</label>


                                                </div>
                                                <div class='form-group' >
                                                    <img class="img-rounded" style='width:40%;height: auto;' v-bind:src="formdata.imgPath + formdata.image2" />
                                                </div>
                                                <div class='form-group'>
                                                    <form action="" method="post" class="dropzone" id="my-awesome-dropzone2"></form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success" >Submit</button>
                                        <button type="reset" class="btn btn-default" >
                                            Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </template>
                <div >

                </div>
            </aside>
        </div>
        <script>
            checkLogin();
            $(document).ready(function () {
                var myAwesomeDropzone = new Dropzone("#my-awesome-dropzone",
                        {
                            url: $.config.uploadImgUrl,
                            maxFilesize: 2,
                            addRemoveLinks: true,
                            dictResponseError: 'Server not Configured',
                            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                            init: function () {
                                var self = this;
                                self.options.addRemoveLinks = true;
                                self.options.dictRemoveFile = "Delete";
                                self.on("complete", function (file) {
                                    this.removeAllFiles(true);
                                });
                                self.on("success", function (file, response) {
                                    console.log('success');
                                    console.log(file);
                                    var data = JSON.parse(response).data;
                                    var nameArray = data.path.split('/');
                                    var fullFileName = nameArray[nameArray.length - 3] + '/' + nameArray[nameArray.length - 2] + '/' + data.file;
                                    console.log(fullFileName);
                                    vm.formdata.image = fullFileName;

                                });
                                //New file added
                                self.on("addedfile", function (file) {
                                    console.log(this.files);
                                    if (this.files.length > 1) {
                                        this.removeFile(this.files[0]);
                                    }
                                    console.log('new file added ', file);
                                });
                                // Send file starts
                                self.on("sending", function (file) {
                                    console.log('upload started', file);
                                    $('.meter').show();
                                });
                                // File upload Progress
                                self.on("totaluploadprogress", function (progress) {
                                    console.log("progress ", progress);
                                    $('.roller').width(progress + '%');
                                });
                                self.on("queuecomplete", function (progress) {
                                    $('.meter').delay(999).slideUp(999);
                                });
                                // On removing file
                                self.on("removedfile", function (file) {
                                    console.log(file);
                                });
                            }
                        });

                var myAwesomeDropzone2 = new Dropzone("#my-awesome-dropzone2",
                        {
                            url: $.config.uploadImgUrl,
                            maxFilesize: 2,
                            addRemoveLinks: true,
                            dictResponseError: 'Server not Configured',
                            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
                            init: function () {
                                var self = this;
                                self.options.addRemoveLinks = true;
                                self.options.dictRemoveFile = "Delete";
                                self.on("complete", function (file) {
                                    this.removeAllFiles(true);
                                });
                                self.on("success", function (file, response) {
                                    var data = JSON.parse(response).data;
                                    var nameArray = data.path.split('/');
                                    var fullFileName = nameArray[nameArray.length - 3] + '/' + nameArray[nameArray.length - 2] + '/' + data.file;

                                    vm.formdata.image2 = fullFileName;

                                });
                                //New file added
                                self.on("addedfile", function (file) {
                                    console.log(this.files);
                                    if (this.files.length > 1) {
                                        this.removeFile(this.files[0]);
                                    }
                                    console.log('new file added ', file);
                                });
                                // Send file starts
                                self.on("sending", function (file) {
                                    console.log('upload started', file);
                                    $('.meter').show();
                                });
                                // File upload Progress
                                self.on("totaluploadprogress", function (progress) {
                                    console.log("progress ", progress);
                                    $('.roller').width(progress + '%');
                                });
                                self.on("queuecomplete", function (progress) {
                                    $('.meter').delay(999).slideUp(999);
                                });
                                // On removing file
                                self.on("removedfile", function (file) {
                                    console.log(file);
                                });
                            }
                        });

            });

            var vm = new Vue({
                el: "#app",
                data: {
                    imgPath: $.config.imgPath,
                    places: [],
                    search_key: '',
                    formdata: {}
                },

                methods: {

                    re: function () {
                        console.log(this.$data.places);
                        Object.assign(this.$data, this.$options.data());
                    },
                    edit: function (place) {
                        if (place == null) {
                            place = {};
                            place.id = null;
                            place.title = '';
                            place.title2 = '';
                            place.level1_id = '';
                            place.description = '';
                            place.image = '';
                            place.image2 = '';
                        }
                        vm.formdata = place;
                        vm.formdata.imgPath = vm.imgPath;
                        $('#summernote').summernote('code', place.description);

                    },
                    Get: function () {

                        $.ajax({
                            type: "POST",
                            url: $.config.WebApiUrl,
                            //"../../restapilaravel/public/service/users/listGlobalValue"
                            data: {
                                'global_oTable': 'things_to_do_level2'
                            },
                            success: function (res) {

                                res = JSON.parse(res);
                                if (res.success) {
                                    vm.places = res.data;

                                } else {
                                    alert(res.msg);
                                    //show_remind( res.msg , "error" );
                                }

                            },
                            error: function (res) {
                                console.log(res);
                            }
                        });
                    }

                },
                components: {
                    'modal': {
                        template: '#modal-template',
                        props: ['formdata'],
                        data: {
                            FaPlaces: []

                        },
                        methods: {

                            Save: function () {
                                this.formdata.description = $('#summernote').summernote('code');

                                var global_value = {};
                                if (this.formdata.title)
                                    global_value.title = this.formdata.title;
                                if (this.formdata.title2)
                                    global_value.title2 = this.formdata.title2;
                                global_value.level1_id = this.formdata.level1_id;
                                if (this.formdata.description)
                                    global_value.description = this.formdata.description;
                                if (this.formdata.image)
                                    global_value.image = this.formdata.image;
                                if (this.formdata.image2)
                                    global_value.image2 = this.formdata.image2;


                                var data = {};
                                data.global_oTable = "things_to_do_level2";
                                if (this.formdata.id)
                                    data.global_oTableKeyValue = this.formdata.id;
                                data.global_value = global_value;

                                console.log(JSON.stringify(data));

                                $.ajax({
                                    type: "POST",
                                    dataType: "text",
                                    url: $.config.setGlobalValue,
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: data,
                                    success: function (data) {

                                        console.log(data);
                                        data = JSON.parse(data);
                                        if (data.success) {
                                            // 成功時此部分執行
                                            $('#form_modal').modal('hide');
                                             vm.Get();
                                        } else
                                        {
                                            // 失敗時此部分執行
                                            show_remind(data.msg, "error");
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        console.log(xhr.status);
                                        console.log(thrownError);
                                    }
                                });

                            },
                            Reset: function () {
                                $('#form_modal').modal('hide');
                                vm.Get();
                            }

                        },
                        mounted: function () {
                            var mod = this;
                            $.ajax({
                                type: "POST",
                                url: $.config.WebApiUrl,
                                data: {
                                    'global_oTable': 'things_to_do_level1'
                                },
                                success: function (res) {
                                    res = JSON.parse(res);
                                    if (res.success) {
                                        mod.FaPlaces = res.data;
                                        console.log('Fa');
                                        console.log(mod.FaPlaces);

                                    } else {
                                        alert(res.msg);
                                        //show_remind( res.msg , "error" );
                                    }

                                },
                                error: function (res) {
                                    console.log(res);
                                }
                            });

                        }
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.places));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    this.Get();
                }
            });
        </script>
    </body>
</html>

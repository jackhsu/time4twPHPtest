<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-fw ti-home"></i> Dashboard</a></li>
        <li class="active">THINGS TO DO Level I</li>
    </ol>
</section>

<section class="content">
    <div class="panel">
        <div class="panel-body">
            
            <h3>THINGS TO DO Level I</h3>
            
            <!--sample datatable plugin-->
            <div class="form-group">
                <div class="col-sm-6 p-0" style="padding: 0 0 20px;">
                    <label>Show</label>
                    <select name="count" class="form-control" style="width:100px;display: inline;">
                        <option value="0">1</option>
                        <option value="1">2</option>
                        <option value="2">3</option>
                        <option value="3">4</option>
                        <option value="4">5</option>
                    </select>
                    <label>entries</label>
                </div>

                <div class="col-sm-6 pull-right" style="padding: 0;">
                    <input style="width: 150px;" class="form-control pull-right" type="text">
                    <label for="input-text" class="pull-right" style="margin-top: 6px;">Search：</label>
                </div>
            </div>

            <!--sample code-->
            <div class="table-grid">
                <table class="table table-bordered" width=100%>
                    <thead>
                        <tr>
                            <th class="text-center"><input name="select_all" value="1" type="checkbox"></th>
                            <th class="text-center">Title</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Published</th>
                            <th class="text-center">Recent push time</th>
                            <th class="text-center">operating</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">CULTRURE &amp; HERITAGE</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">Indigenous Culture</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">Temples</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">CULINARY</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">Adventure/Outdoor</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">WELLNESS</td>
                            <td class="text-center"><img alt="image" class="" src="../img/northern-tw.jpg"></td>
                            <td class="">The capital Taipei is the gateway to northern Taiwan. It is also the political, financial and cultural heart of the nation, a city known for its world-class museums, massive culinary repertoire, and fascinating culture. A train-ride of under an hour or two away from Taipei, another world awaits. New Taipei City is a mosaic of towns – some celebrated for their ceramics, some for sculptures, hot springs or a unique mining heritage, others for their indigenous roots, their Dutch and Spanish connections, their night markets or tea fields. In addition, national parks and geoparks offer a range of hiking options that can be easily combined with sightseeing, birdwatching and hot springing. A clever system of cycling routes hugs the coastline, whizzes through rural townships and over major highways. If you just want to chill, there are sandy beaches with sweet surfable waves and buoyant music festivals. </td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</section>


<!--modify dialog-->
<div id="form_modal" class="modal fade animated" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <form role="form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input class="form-control" placeholder="NORTHERN TAIWAN" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12 m-t-10">
                            <div class="form-group">
                                <label class="control-label">Description</label>
                                <div class="summernote">
                                    <h3>Lorem Ipsum is simply</h3>
                                    dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                                    when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                                    typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                                    <br/>
                                    <br/>
                                    <ul>
                                        <li>Remaining essentially unchanged</li>
                                        <li>Make a type specimen book</li>
                                        <li>Unknown printer</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 m-t-10">
                            <div class="form-group">
                                <label class="control-label">Image</label>
                                <div id="dropz" class="dropzone col-sm-12 no-padding">
<!--                                    <div class="am-text-success dz-message">
                                        <span class="icon-cloud-outline fa-lg"></span>
                                        Drop files here or <a>Browse</a> to select.
                                    </div>-->
                                </div>
<!--                                <form action="/file-upload"
                                        class="dropzone"
                                        id="my-awesome-dropzone"></form>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


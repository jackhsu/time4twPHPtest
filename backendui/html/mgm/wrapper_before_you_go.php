<style>
    /*breadcrumb*/
    .right-side > .content-header {
        height: auto;
    }
    .right-side > .content-header > .breadcrumb {
        padding: 8px 15px;
    }
    /*table*/
    .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td {
        vertical-align: middle;
    }
    .table > thead > tr > th {
        vertical-align: middle;
    }
</style>


<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-fw ti-home"></i> Dashboard</a></li>
        <li class="active">BEFORE YOU GO</li>
    </ol>
</section>

<section class="content">
    <div class="panel">
        <div class="panel-body">
            
            <h3 style="margin-bottom:30px;">BEFORE YOU GO</h3>
            
            <!--sample datatable plugin-->
            <div class="form-group">
                <div class="col-sm-6 p-0" style="padding: 0 0 20px;">
                    <label>Show</label>
                    <select name="count" class="form-control" style="width:100px;display: inline;">
                        <option value="0">1</option>
                        <option value="1">2</option>
                        <option value="2">3</option>
                        <option value="3">4</option>
                        <option value="4">5</option>
                    </select>
                    <label>entries</label>
                </div>

                <div class="col-sm-6 pull-right" style="padding: 0;">
                    <input style="width: 150px;" class="form-control pull-right" type="text">
                    <label for="input-text" class="pull-right" style="margin-top: 6px;">Search：</label>
                </div>
            </div>

            <!--sample code-->
            <div class="table-grid">
                <table class="table table-bordered" width=100%>
                    <thead>
                        <tr>
                            <th class="text-center"><input name="select_all" value="1" type="checkbox"></th>
                            <th class="text-center">Title</th>
                            <th class="text-center">share</th>
                            <th class="text-center">Published</th>
                            <th class="text-center">Recent push time</th>
                            <th class="text-center">The number of push</th>
                            <th class="text-center">operating</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">Getting There</td>
                            <td class="text-center">50</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">1000</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center"><input type="checkbox"></td>
                            <td class="text-center">Getting There</td>
                            <td class="text-center">50</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">2017-08-10 10:25:31</td>
                            <td class="text-center">1000</td>
                            <td class="text-center">
                                <button type="button" class="btn btn-icon btn-default m-r-20"><i class="icon fa fa-fw ti-pencil-alt"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</section>
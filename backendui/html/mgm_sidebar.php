<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar-->
    <section class="sidebar">
        <div id="menu" role="navigation">
            <div class="nav_profile">
                <div class="media profile-left">
                    <a class="pull-left profile-thumb" href="#">
                        <img src="img/authors/avatar1.jpg" class="img-circle" alt="User Image">
                    </a>
                    <div class="content-profile">
                        <h4 class="media-heading">Addison</h4>
                    </div>
                </div>
            </div>
            <ul class="navigation">
                <li>
                    <a href="mgm_place_to_go_management.php" h_target="mgm_place_to_go_management">
                        <i class="menu-icon ti-desktop"></i>
                        <span class="mm-text ">PLACE TO GO</span>
                    </a>
                </li>
                
                <li class="menu-dropdown">
                    <a href="#">
                        <i class="menu-icon ti-desktop"></i>
                        <span>THINGS TO DO</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="mgm_thing_to_do_management.php" h_target="mgm_thing_to_do_management">
                                <i class="fa fa-fw ti-plug"></i> Level 0
                            </a>
                        </li>
                        <li>
                            <a href="mgm_thing_to_do_management_1.php" h_target="mgm_thing_to_do_management_1">
                                <i class="fa fa-fw ti-plug"></i> Level I
                            </a>
                        </li>
                        <li>
                            <a href="mgm_thing_to_do_management_2.php" h_target="mgm_thing_to_do_management_2">
                                <i class="fa fa-fw ti-plug"></i> Level II
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="mgm_before_you_go_management.php" h_target="mgm_before_you_go_management">
                        <i class="fa fa-fw ti-bell"></i> 
                        <span class="mm-text ">BEFORE YOU GO</span>
                    </a>
                </li>
                <li>
                    <a href="mgm_whats_happening_management.php" h_target="mgm_whats_happening_management">
                        <i class="fa fa-fw ti-star"></i> 
                        <span class="mm-text ">WHATS HAPPENING</span>
                    </a>
                </li>
                <li>
                    <a href="mgm_deals_management.php" h_target="mgm_deals_management">
                        <i class="fa fa-fw ti-tag"></i> 
                        <span class="mm-text ">DEALS</span>
                    </a>
                </li>
            </ul>
            <!-- / .navigation -->
        </div>
        <!-- menu -->
    </section>
    <!-- /.sidebar -->
</aside>


<script type="text/javascript">
        var html = location.href.split("/")[ location.href.split("/").length-1 ];
        html = html.split( "?" )[0].split( "#" )[0].split( ".php" )[0];
        $( "#menu a[h_target=" + html + "]" ).parents( "li" ).addClass( "active" );
</script>
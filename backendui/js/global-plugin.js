
            function fn_bLazy( tmp_focus ){
                
                        if( $.bLazy )
                        $.bLazy.destroy();

                        $.bLazy = new Blazy({
                                    container: tmp_focus ,// Default is window
                                    offset: 100 ,// Loads images 100px before they're visible ,
                                    success: function(ele){
                                                
                                                // Image has loaded
                                                // Do your business here
                                                // console.log( ele );

                                    },
                                    error: function(ele, msg){

                                                // console.log( ele );
                                                $( ele ).css( "background-image" , "url('./images/image-loader.gif')" );

                                                if(msg === 'missing'){
                                                    // Data-src is missing
                                                }
                                                else if(msg === 'invalid'){
                                                    // Data-src is invalid
                                                }  
                                    }
                        });
            }

            function load_html( tmp_queue , tmp_url , data , data2 , success_back , error_back )
            {           
                        if( data2.focus ) 
                        {
                                    $( data2.focus ).hide();
                                    loading_ajax_show();
                        }
                        
                        data2.cb_focus = function( data )
                        {
                                    var tmp_focus = $( this.focus );
                                                tmp_focus.show();
                                                
                                    setTimeout(function() {
                                                loading_ajax_hide();
                                    }, 2000 );
                        };
                        
                        
                        $.Ajaxq( tmp_queue , "POST" ,  tmp_url , data , data2 , success_back , error_back );
            }
            
            function load_css( tmp_queue , tmp_url , data , data2 , success_back , error_back )
            {
                
                        $.Ajaxq( tmp_queue , "POST" ,  tmp_url , data , data2 , success_back , error_back );
                        
            }
            

<script type="text/javascript" src="js/jquery.min.js"></script>

<!-- global js -->
<script src="js/app.js" type="text/javascript"></script>

<!-- babel cdn -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.js"></script>

<!-- vue cdn -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js"></script>

<!-- SUMMERNOTE -->
<script src="js/summernote/summernote.min.js"></script>
<script src="js/dropzone/dropzone.js"></script>
<script src="js/config.js"></script>
<script src="js/global.js" type="text/javascript"></script>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <title>Management Of PLACE TO GO | go2taiwan</title>

        <link rel="shortcut icon" href="img/favicon.ico"/>
        <!-- global css -->
        <link type="text/css" href="css/global.css" rel="stylesheet"/>
        <link type="text/css" href="css/app.css" rel="stylesheet" />
        <link type="text/css" href="vendors/themify/css/themify-icons.css" rel="stylesheet"/>

        <script type="text/javascript" src="js/jquery.min.js"></script>

        <!-- global js -->
        <script src="js/app.js" type="text/javascript"></script>

        <!-- babel cdn -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.js"></script>

        <!-- vue cdn -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.js"></script>

        <!-- page level js -->
        <!--script type="text/javascript" src="vendors/datatables/js/jquery.dataTables.js"></script>
        <script type="text/javascript" src="js/custom_js/simple-table.js"></script-->

        <!-- SUMMERNOTE -->
        <script src="js/summernote/summernote.min.js"></script>
        <link href="js/summernote/summernote.css" rel="stylesheet">

        <!-- DROPZONE -->
        <script src="js/dropzone/dropzone.js"></script>
        <link href="js/dropzone/dropzone.css" rel="stylesheet">
<script src="js/global.js"></script>
        <script>
            $(document).ready(function () {

                //$('.summernote').summernote();
                //event: Dropzone AL 20170605
                $("#dropz").dropzone({
                    url: "handle-upload.php",
                    maxFiles: 10,
                    maxFilesize: 512,
                    acceptedFiles: ".js,.obj,.dae"
                });
            });
        </script>
    </head>
    <body class="skin-default">

        <div class="preloader">
            <div class="loader_img"><img src="img/loader.gif" alt="loading..." height="64" width="64"></div>
        </div>

        <?php include( "html/mgm_header.php"); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">

            <?php include( "html/mgm_sidebar.php"); ?>

            <aside class="right-side">
                <section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-fw ti-home"></i> Dashboard</a>
                        </li>
                        <li class="active">PLACE TO GO</li>
                    </ol>
                </section>

                <section id="app" class="content">
                    <div class="panel">
                        <div class="panel-body">

                            <h3>PLACE TO GO</h3>

                            <!--sample datatable plugin-->
                            <div class="form-group">
                                <div class="col-sm-6 p-0" style="padding: 0 0 20px;">
                                    <label>Show</label>
                                    <select name="count" class="form-control" style="width:100px;display: inline;">
                                        <option value="0">1</option>
                                        <option value="1">2</option>
                                        <option value="2">3</option>
                                        <option value="3">4</option>
                                        <option value="4">5</option>
                                    </select>
                                    <label>entries</label>
                                </div>

                                <div class="col-sm-6 pull-right" style="padding: 0;">
                                   <span class="option-search pull-right hidden-xs">
                                        <span class="search-wrapper">
                                            <input type="text" placeholder="Search here"><i class="ti-search"></i>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <!--sample code-->
                            <div class="table-grid">
                                <table class="table table-bordered" width=100%>
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <input name="select_all" value="1" type="checkbox">
                                            </th>
                                            <th class="text-center">Title</th>
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Published</th>
                                            <th class="text-center">Recent push time</th>
                                            <th class="text-center">operating</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="place in searched_table_data">
                                            <td class="text-center">
                                                <input type="checkbox">
                                            </td>
                                            <td class="text-center" v-html="place.title "></td>
                                            <td class="text-center"><img alt="image" class="" src="../img/things-to-do-Main-Hero.jpg">
                                            </td>
                                            <td class="" v-html="place.description"></td>
                                            <td class="text-center">{{ place.published_time }}</td>
                                            <td class="text-center">{{ place.recent_push_time }}</td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-icon btn-default m-r-20" @click="edit(place)" data-toggle="modal" data-target="#form_modal"><i class="icon fa fa-fw ti-pencil-alt"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <modal id="form_modal" class="modal fade animated" role="dialog" v-bind:formdata="formdata"> </modal>
                </section>

                <!--modify dialog-->
                <template id="modal-template" >
                    <div >
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <form  id="formEdit" role="form" @submit.prevent="Save"  @reset.prevent="Reset" >
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Title</label>
                                                    <input class="form-control" placeholder="NORTHERN TAIWAN" v-model="formdata.title"  type="text">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 m-t-10">
                                                <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <div id="summernote-fa">

                                                        <div id="summernote">

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <div class="col-sm-12 m-t-10">
                                                <div class="form-group">
                                                    <label class="control-label">Image</label>
                                                    <div id="dropz" class="dropzone col-sm-12 no-padding">
                                                        <div class="am-text-success dz-message">
                                                            <span class="icon-cloud-outline fa-lg"></span> Drop files here or <a>Browse</a> to select.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success" >Submit</button>
                                        <button type="reset" class="btn btn-default" >
                                            Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </template>
                <div >

                </div>
            </aside>
        </div>
        <script>
            checkLogin();
            var vm = new Vue({
                el: "#app",
                data: {
                    places: [],
                    search_key: '',
                    formdata: {}
                },

                methods: {
                    re: function () {
                        console.log(this.$data.places);
                        Object.assign(this.$data, this.$options.data());
                    },
                    edit: function (place) {
                        vm.formdata = place;
                        $('#summernote').summernote('code', place.description);

                    },
                    Get: function () {
                        $.ajax({
                            type: "POST",
                            url: "./restapilaravel/public/service/users/listGlobalValue",
                            //url: "http://www.ggyyggy.com/go2taiwan/restapilaravel/public/service/users/listGlobalValue",
                            data: {
                                'global_oTable': 'place_to_go'
                            },
                            success: function (res) {
                                console.log(url);
                                console.log(res);
                                res = JSON.parse(res);
                                if (res.success) {
                                    vm.places = res.data;
                                } else {
                                    alert(res.msg);
                                    //show_remind( res.msg , "error" );
                                }

                            },
                            error: function (res) {
                                console.log(res);
                            }
                        });
                    }
                },
                components: {
                    'modal': {
                        template: '#modal-template',
                        props: ['formdata'],
                        methods: {
                            Save: function () {
                                console.log(this.formdata.id);
                                console.log($('#summernote').summernote('code'));
                                this.formdata.description = $('#summernote').summernote('code');
                                $.ajax({
                                    type: "POST",
                                    dataType: "text",
                                    url: "../restapilaravel/public/service/users/setGlobalValue",
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    data: {
                                        "global_oTableKeyValue": this.formdata.id, // 資料表主鍵值 ex: b_id = 96 (修改b_id=96的那筆資料)
                                        "global_oTable": "place_to_go", // 資料表名稱
                                        "global_value": {"title": this.formdata.title, "description": this.formdata.description}  // 欲修改的欄位值(單個欄位與多個都可)，只需輸入欲修改的欄位，無修改請不用輸入。
                                    },
                                    success: function (data) {

                                        console.log(data);
                                        data = JSON.parse(data);
                                        if (data.success) {
                                            // 成功時此部分執行
                                            $('#form_modal').modal('hide');
                                        } else
                                        {
                                            // 失敗時此部分執行
                                            show_remind(data.msg, "error");
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        console.log(xhr.status);
                                        console.log(thrownError);
                                    }
                                });

                            },
                            Reset: function () {
                                $('#form_modal').modal('hide');
                                vm.Get();
                            }
                        }
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.places));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    this.Get();
                }
            });
        </script>
    </body>
</html>

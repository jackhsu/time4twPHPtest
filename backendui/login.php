<!DOCTYPE html>
<html>

    <head>
        <title>::Backend Login::</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="img/favicon.ico"/>
        <!-- Bootstrap -->
        <link href="css/plugins/bootstrap.min.css" rel="stylesheet">
        <!-- end of bootstrap -->
        <!--page level css -->
        <link type="text/css" href="vendors/themify/css/themify-icons.css" rel="stylesheet"/>
        <link href="vendors/iCheck/css/all.css" rel="stylesheet">
        <!-- <link href="vendors/bootstrapvalidator/css/bootstrapValidator.min.css" rel="stylesheet"/> -->
        <link href="css/login.css" rel="stylesheet">
        <!--end page level css-->
    </head>

    <body id="sign-in">
        <div class="preloader">
            <div class="loader_img"><img src="img/loader.gif" alt="loading..." height="64" width="64"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-form">
                    <div class="panel-header">
                        <h2 class="text-center">
                            <img src="img/pages/clear_black.png" alt="Logo">
                        </h2>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form  id="authentication" class="login_validator" @submit.prevent="Login">
                                    <div class="form-group">
                                        <label for="email" class="sr-only"> E-mail</label>
                                        <input type="text" class="form-control  form-control-lg" id="inputLoginUserName" v-model="email" placeholder="E-mail">
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="sr-only">Password</label>
                                        <input type="password" class="form-control form-control-lg" id="inputLoginUserPassword" v-model="password" placeholder="Password">
                                    </div>
                                    <!-- <div class="form-group checkbox">
                                        <label for="remember">
                                            <input type="checkbox" name="remember" id="remember">&nbsp; Remember Me
                                        </label>
                                    </div> -->
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-primary btn-block" >
                                    </div>
                                    <!-- <a href="forgot_password.html" id="forgot" class="forgot"> Forgot Password ? </a>
        
                                    <span class="pull-right sign-up">New ? <a href="register.html">Sign Up</a></span> -->
                                </form>
                            </div>
                        </div>
                        <!--  <div class="row text-center social">
                             <div class="col-xs-12">
                                 <p class="alter">Sign in with</p>
                             </div>
                             <div class="row">
                                 <div class="col-sm-8 col-sm-offset-2">
                                     <div class="col-xs-4">
                                         <a href="#" class="btn btn-lg btn-facebook">
                                             <i class="ti-facebook"></i>
                                         </a>
                                     </div>
                                     <div class="col-xs-4">
                                         <a href="#" class="btn btn-lg btn-twitter">
                                             <i class="ti-twitter-alt"></i>
                                         </a>
                                     </div>
                                     <div class="col-xs-4">
                                         <a href="#" class="btn btn-lg btn-google">
                                             <i class="ti-google"></i>
                                         </a>
                                     </div>
                                 </div>
                             </div>
                         </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- global js -->
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- end of global js -->
        <!-- page level js -->
        <!-- <script type="text/javascript" src="vendors/iCheck/js/icheck.js"></script> -->
        <!-- <script src="vendors/bootstrapvalidator/js/bootstrapValidator.min.js" type="text/javascript"></script> -->
        <script type="text/javascript" src="js/custom_js/login.js"></script>
        <script type="text/javascript" src="js/md5.js"></script>
        <!-- end of page level js -->

        <?php include( "js/all_js.php"); ?>

        <script>
            var vm = new Vue({
                el: "#authentication",
                data: {
                    email: "AdministratorUserName",
                    password: "520520op"
                },
                methods: {
                    Login: function () {
                        console.log(vm.email);
                         console.log(vm.password);

                        //$('#login').unbind("touchend").bind('touchstart', function (event) {

                        $.ajax({
                            type: "POST",
                            dataType: "text",
                            url: "http://ec2-54-69-68-125.us-west-2.compute.amazonaws.com/time4twPHP/APIServices/slim3/public/users/login",
                            headers: {
                                'Authorization': "Basic " + btoa(md5(vm.password))
                            },
                            data: {
                                "userinfo_UserName": vm.email
                            },
                            success: function (data) {
                                console.log(data);
                                data = JSON.parse(data);
                                if (data.success) {
                                    //show_remind("登入成功", "error");
                                    setCookie("scs_cookie", data.data);
                                    window.open('index.php','_self');
                                } else {
                                    show_remind(data.msg, "error");
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr.status);
                                console.log(thrownError);
                            }
                        });

                        //});

                    }
                },
                created: function () {

                    //$("#login").bind("click", function () {});

                }

            });
        </script>

    </body>
</html>

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>GETTING AROUND - Before You Go| Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="beforeyougo">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <!--<li class="vid-bg image-bg overlay">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
                    </div>
                    <div class="fs-vid-background">
                        <video muted="" loop="">
                            <source src="video/video.webm" type="video/webm">
                            <source src="video/video.mp4" type="video/mp4">
                            <source src="video/video.ogv" type="video/ogg">
                        </video>
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 4" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + beforeyougo.image + ')' }">
                    <!--<img alt="Background Image" class="background-image" src="img/gettingAroundHero.jpg">-->
                    </div>

                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/getreadyLogo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>
        <section class="bg-primary" v-for="beforeyougo in searched_table_data"  v-show="beforeyougo.id == 4">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold italic" v-html="beforeyougo.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="beforeyougo.description">
                        <!--<h2>AIR</h2>
                        <p>Domestic flights from Taipei leave from Songshan Airport. Flights to offshore islands can sometimes be cancelled due to weather, especially on the east coast. Domestic airlines include:</p>
                        <li><a href="http://www.dailyair.com.tw" target="_blank">Daily Air Corporation</a></li>
                        <li><a href="http://www.fat.com.tw" target="_blank">Far Eastern Air Transport (FAT)</a></li>
                        <li><a href="http://www.mandarin-airlines.com" target="_blank">Mandarin Airlines</a></li>
                        <li><a href="http://www.uniair.com.tw" target="_blank">Uni Air</a></li><br>
                        <h2>TRAIN</h2>
                        <p><a href="http://www.railway.gov.tw" target="_blank">The TRA, Taiwan Railways Administration</a>, has an extensive system of trains running along both the east and west coasts, and connecting all major cities on the island.</p> 
                        <p>Tickets can be booked 14 days in advance on the TRA website with a passport number. If you plan on taking an express train, especially on weekends or holidays, buy your tickets well in advance. Please refer to the website for pricing and timetables. Most urban train stations have visitor information centers with helpful English-speaking staff.</p><br>
                        <h2>HIGH SPEED RAIL</h2>
                        <p>The HSR, High Speed Rail, services major cities on the west coast and takes passengers from north to south in less than half the time ordinary trains could. Please refer to the HSR website for stations, timetables, and pricing.</p>
                        <p>Advance booking is advised. Note that you will need your passport number to book in advance. Reservations can be made at any HSR station, or online on the HSR website. All HSR stations have visitor information centers with English-speaking staff to help with bus transfers, hotel bookings, and car rentals.</p><br>
                        <h2>REGIONAL BRANCH LINES</h2>
                        <p>There are a number of small regional branch lines to destinations like Alishan, Jiji, and Pingxi.</p><br>
                        <h2>BUS</h2>
                        <p>Taiwan’s buses are cheap, convenient, and reliable. See <a href="http://www.taiwanbus.tw/" target="_blank">Taiwan Bus</a> for routes and operators.</p>
                        <p>Urban bus fare depends on the number of zones passed during a trip. Payment is accepted differently on different buses. Sometimes you pay when you board and sometimes you pay when you alight. As a rule of thumb, follow the passengers ahead of you or ask the driver! </p>
                        <p>Tickets for long-distance buses can be bought at bus stations or convenience stores. Advance reservations are recommended on weekends and holidays.</p>
                        <p>A comprehensive network of buses operates between larger cities. There are routes running in the north (Taipei-Fulong), and from north to south (Taipei-Kending). Services from the west coast to the east coast (Taichung-Hualien, Kaohsiung-Taitung) and along the east coast (Hualien-Taitung) are less frequent. </p><br>
                        <h2>TOURIST SHUTTLE</h2>
                        <p>Taiwan has an excellent system of small shuttle buses that link major stations with tourist sites. Visit <a href="http://www.taiwantrip.com.tw" target="_blank">www.taiwantrip.com.tw</a> for details.</p><br>
                        <h2>METRO</h2>
                        <p><a href="http://english.metro.taipei/" target="_blank">Taipei</a> and <a href="http://www.krtco.com.tw/en/" target="_blank">Kaohsiung</a> have metro systems. Taichung’s metro is also coming soon. Kaohsiung also has a circular light-rail system.</p><br>
                        <h2>TAXIS</h2>
                        <p>Taxis are common in all Taiwan cities. Fees may apply for luggage and advance reservations. Outside of urban areas, taxi drivers will either use meters or ask for a flat rate. In small towns or remote areas, it's a good idea to get your hotel to call first and keep the driver's number for subsequent rides. Some convenience stores may also help you call a cab. </p><br>
                        <h2>FERRY</h2>
                        <p>Ferries run regularly between the main island and the Penghu Archipelago, Lanyu (Orchid Island), and Green Island, and Little Liuqiu Island. </p><br>
                        <h2>PUBLIC BIKE</h2>
                        <p>Taiwan’s major cities have growing public bike networks with parking stations close to metro stations and tourist destinations. Please refer to these websites for details of these bike sharing programs. <a href="https://taipei.youbike.com.tw/en/" target="_blank">Taipei YouBike</a>, <a href="www.c-bike.com.tw" target="_blank">Kaohsiung C-Bike</a>, <a href="http://pbike.pthg.gov.tw" target="_blank">Pingtung PBike</a>, and <a href="http://i.youbike.com.tw" target="_blank">Taichung iBike</a>.</p><br>
                        <h2>Cars & Scooters</h2>
                        <p>Having your own vehicle is useful on the east coast and in mountainous areas. Riding a scooter (your own or rented by the pier) is handy on the offshore islands and in Kending. <a href="http://iff.immigration.gov.tw" target="_blank">The Information for Foreigners website</a> has further details. </p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Before you go -->
        <section class="pt0">

            <div class="container">

                <!--     <div class="row masonry-loader">
                        <div class="col-sm-12 text-center">
                            <div class="spinner"></div>
                        </div>
                    </div> -->
                <div class="row ">

                    <div class="col-md-4 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 2">
                            <a href="travel-tips.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 3">
                            <a href="getting-there.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                </div>
                <div class="row masonry">
                    <div class="col-md-4 col-sm-6 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 4">
                            <a href="getting-around.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 5">
                            <a href="visas.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 masonry-item project b4go">
                        <div class="image-tile inner-title zoomin" v-for="beforeyougo in searched_table_data" v-show="beforeyougo.id == 6">
                            <a href="language.php">
                                <img alt="Pic" v-bind:src="imgPath + beforeyougo.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="beforeyougo.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                </div>

            </div>
        </section><!-- end of Before you go -->

        <?php include_once("incl/footer.php"); ?>

    </div>

    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        
        $(document).ready(function() {
        
            var vm = new Vue({
                el: "#beforeyougo",
                data: {
                    imgPath: $.config.imgPath,
                    before: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.before));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'before_you_go'
                        },
                        success: function (res) {
                            console.log(res);
                            res = JSON.parse(res);

                            if (res.success) {
                                vm.before = res.data;
                                vm.imgPath = vm.imgPath;

                                js_initail();

                            } else {
                                alert(res.msg);
                                //show_remind( res.msg , "error" );
                            }
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                }
            });
        
        });
    </script>
</body>

</html>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>National Palace Museum - Culture & Heritage | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="thingslevel2">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 1" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" src="img/thingsHero-2.1.1.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="#main">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 2" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" src="img/thingsHero-2.1.2.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="national-taiwan-museum-of-fine-arts.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>  
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 3" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" src="img/thingsHero-2.1.3.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="national-museum-of-taiwan-literature.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>


                <!-- level1輪播 -->
                <!--<li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 1" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/thingsHero-2.1.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="culture-and-heritage.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
        </section>

        <section class="bg-primary" id="main" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 1">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold" v-html="thingslevel2.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="thingslevel2.description">
                        <!--<h2>NATIONAL PALACE MUSEUM</h2>
                            <p>The museum illuminates the trajectory of Taiwanese literature with original manuscripts, narrative (some bilingual), readings, video footage, and relics. It all began with the fascinating folktales of the indigenous peoples, passed down via song and by word of mouth. From the 17th century, there was a blossoming of Chinese-language literature when a formidable pirate Koxinga conquered Dutch-ruled Tainan and triggered immigration from China. In the 20th century, Japanese rule and the influx of Chinese migrants post-WWII saw Taiwanese writers succumbing to the competing influences of Japanese, Chinese, and even American culture. This eclectic heritage has produced a deliciously heterogeneous body of literature, with writings in Japanese and Chinese, that embrace one or the other culture, or both, and writings that do not. Modern Taiwanese Literature reflects this colorful legacy, yet fathoms local history, dialects and folk traditions for inspiration. A large hall at the museum displays a rich array of ‘mother tongue literary works’ – poetry, novels, and songs written in Hakka, Hoklo, and indigenous dialects. </p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">

            <div class="container">
                <div><h3>Features</h3></div>
                <div class="row">
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li class="feature-pad" >
                                <a href="national-palace-museum.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 1" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 1" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="national-taiwan-museum-of-fine-arts.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 2" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 2" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="national-museum-of-taiwan-literature.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 3" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 3" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">
                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project" >
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 1">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 2">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 3">
                            <a href="temples.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 4">
                            <a href="culinary.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 5">
                            <a href="outdoor.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 6">
                            <a href="wellness.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 7">
                            <a href="ecotourism.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end of Things to do  -->

        <?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <!--         <div class="modal-strip bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 overflow-hidden">
                                <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                                <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                                <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });

        var vm = new Vue({
            el: "#thingslevel2",
            data: {
                things: [],
                things2: [],
                search_key: ''
            },
            methods: {
                edit: function (id) {
                    alert(id);
                },
                Get: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'things_to_do_level1',
                        },
                        success: function (res2) {
                            console.log(res2);
                            res2 = JSON.parse(res2);

                            if (res2.success) {
                                vm.things2 = res2.data;

                                //settimeout show img
                                setTimeout(function () {
                                    console.log('ReFresh masonry');
                                    $('.masonry').masonry();

                                }, 1000);
                            }
                        },
                        error: function (res2) {
                            console.log(res2);
                        }
                    });
                }
            },
            computed: {
                searched_table_data() {
                    let clone = JSON.parse(JSON.stringify(this.things));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                },
                searched_table_data2() {
                    let clone = JSON.parse(JSON.stringify(this.things2));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                }
            },
            created: function () {
                $.ajax({
                    type: "POST",
                    url: $.config.WebApiUrl,
                    data: {
                        'global_oTable': 'things_to_do_level2'
                    },
                    success: function (res) {
                        console.log(res);
                        res = JSON.parse(res);

                        if (res.success) {
                            vm.things = res.data;
                            vm.Get();

                        } else {
                            alert(res.msg);
                            //show_remind( res.msg , "error" );
                        }
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            }
        });
    </script>
</body>

</html>
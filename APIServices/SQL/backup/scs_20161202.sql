-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 2016 年 12 月 02 日 05:37
-- 伺服器版本: 5.5.39
-- PHP 版本： 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫： `scs`
--

-- --------------------------------------------------------

--
-- 資料表結構 `address`
--

CREATE TABLE IF NOT EXISTS `address` (
  `userinfo_ID` varchar(64) NOT NULL,
  `address_Address` varchar(128) DEFAULT NULL COMMENT '地址',
  `address_City` varchar(64) DEFAULT NULL COMMENT '城市',
  `address_Province` varchar(32) DEFAULT NULL COMMENT '省份',
  `address_Country` varchar(32) DEFAULT NULL COMMENT '國家',
  `address_PostalCode` varchar(16) DEFAULT NULL COMMENT '郵遞區號',
  `address_Longitude` float(10,6) DEFAULT NULL COMMENT '經度',
  `address_Latitude` float(10,6) DEFAULT NULL COMMENT '緯度',
  `address_CreateDateTime` datetime DEFAULT NULL,
  `address_UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `address`
--

INSERT INTO `address` (`userinfo_ID`, `address_Address`, `address_City`, `address_Province`, `address_Country`, `address_PostalCode`, `address_Longitude`, `address_Latitude`, `address_CreateDateTime`, `address_UpdateDateTime`) VALUES
('1', '123', '123', '123', '123', '123', 123.000000, 123.000000, '2016-12-01 00:00:00', '2016-12-01 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `contactinfo`
--

CREATE TABLE IF NOT EXISTS `contactinfo` (
  `contactinfo_ID` varchar(64) NOT NULL,
  `contactinfo_PrimaryTable` varchar(64) NOT NULL COMMENT '關聯表',
  `contactinfo_PrimaryID` varchar(64) NOT NULL COMMENT '關聯表ID',
  `contactinfo_Name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `contactinfo_Title` varchar(64) DEFAULT NULL COMMENT '職稱',
  `contactinfo_PhoneNumber` varchar(64) DEFAULT NULL COMMENT '電話',
  `contactinfo_Extension` varchar(64) DEFAULT NULL COMMENT '分機'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `contactinfo`
--

INSERT INTO `contactinfo` (`contactinfo_ID`, `contactinfo_PrimaryTable`, `contactinfo_PrimaryID`, `contactinfo_Name`, `contactinfo_Title`, `contactinfo_PhoneNumber`, `contactinfo_Extension`) VALUES
('1', '123', '123', '123', '123', '123', '123');

-- --------------------------------------------------------

--
-- 資料表結構 `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL COMMENT '序列碼',
  `customer_name` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '雇主',
  `customer_contact` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '連絡方式(名字加電話)',
  `customer_address` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '地址',
  `customer_email` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '信箱',
  `customer_fax` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '傳真',
  `customer_pay_method` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '付款方式',
  `customer_terms` datetime NOT NULL COMMENT '期限',
  `customer_status` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT 'inactive' COMMENT '狀態(active:,inactive:)',
  `customer_remark` varchar(5000) CHARACTER SET utf8 NOT NULL COMMENT '註記',
  `customer_creator` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '創立者(u_id)',
  `customer_createmployee_date` datetime NOT NULL COMMENT '創建日期',
  `customer_last_updatemployee_date` datetime NOT NULL COMMENT '最後更新日期'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- 資料表結構 `customerinfo`
--

CREATE TABLE IF NOT EXISTS `customerinfo` (
  `customerinfo_ID` varchar(64) NOT NULL,
  `customerinfo_SystemGenerateNumber` int(11) DEFAULT NULL,
  `customerinfo_CompanyName` varchar(64) DEFAULT NULL,
  `customerinfo_AddressID` varchar(64) DEFAULT NULL,
  `customerinfo_StartDate` date DEFAULT NULL,
  `customerinfo_PayTerm` varchar(64) DEFAULT NULL,
  `customerinfo_ContactID` varchar(64) DEFAULT NULL,
  `customerinfo_FaxNumber` varchar(64) DEFAULT NULL,
  `customerinfo_EMail` varchar(512) DEFAULT NULL,
  `customerinfo_TaxNumber` varchar(64) DEFAULT NULL,
  `customerinfo_CreateDateTime` datetime DEFAULT NULL,
  `customerinfo_UpdateDateTime` datetime DEFAULT NULL,
  `customerinfo_Status` varchar(64) DEFAULT NULL,
  `customerinfo_Remarks` varchar(1024) DEFAULT NULL,
  `customerinfo_CreateBy` varchar(64) DEFAULT NULL,
  `customerinfo_CreateByID` varchar(64) DEFAULT NULL,
  `customerinfo_UpdateBy` varchar(64) DEFAULT NULL,
  `customerinfo_UpdateByID` varchar(64) DEFAULT NULL,
  `customerinfo_PaymentMethod` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `customerinfo`
--

INSERT INTO `customerinfo` (`customerinfo_ID`, `customerinfo_SystemGenerateNumber`, `customerinfo_CompanyName`, `customerinfo_AddressID`, `customerinfo_StartDate`, `customerinfo_PayTerm`, `customerinfo_ContactID`, `customerinfo_FaxNumber`, `customerinfo_EMail`, `customerinfo_TaxNumber`, `customerinfo_CreateDateTime`, `customerinfo_UpdateDateTime`, `customerinfo_Status`, `customerinfo_Remarks`, `customerinfo_CreateBy`, `customerinfo_CreateByID`, `customerinfo_UpdateBy`, `customerinfo_UpdateByID`, `customerinfo_PaymentMethod`) VALUES
('1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `employee_ID` int(11) NOT NULL COMMENT '序列碼',
  `employee_firstName` varchar(250) CHARACTER SET utf8 NOT NULL,
  `employee_middleName` varchar(250) CHARACTER SET utf8 NOT NULL,
  `employee_lastName` varchar(250) CHARACTER SET utf8 NOT NULL,
  `employee_gender` varchar(10) CHARACTER SET utf8 NOT NULL COMMENT '性別',
  `employee_birthday` date NOT NULL COMMENT '生日',
  `employee_phone` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '手機',
  `employee_address` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '地址',
  `employee_email` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '信箱',
  `employee_home` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '住家電話',
  `employee_emergency` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '緊急電話',
  `employee_position` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '職務',
  `employee_SIN` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '識別證號',
  `employee_wage` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT 'monthly' COMMENT '報酬(monthly: 每月,)',
  `employee_rate` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '稅率？',
  `employee_start_date` datetime NOT NULL COMMENT '雇用日期',
  `employee_status` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT 'inactive' COMMENT '狀態(active:,inactive:)',
  `employee_remark` varchar(250) CHARACTER SET utf8 NOT NULL COMMENT '註記',
  `employee_created` int(11) NOT NULL COMMENT '創立者(u_id)',
  `employee_createDate` datetime NOT NULL COMMENT '創建日期',
  `employee_lastUpdateDate` datetime NOT NULL COMMENT '最後更新日期'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `employee`
--

INSERT INTO `employee` (`employee_ID`, `employee_firstName`, `employee_middleName`, `employee_lastName`, `employee_gender`, `employee_birthday`, `employee_phone`, `employee_address`, `employee_email`, `employee_home`, `employee_emergency`, `employee_position`, `employee_SIN`, `employee_wage`, `employee_rate`, `employee_start_date`, `employee_status`, `employee_remark`, `employee_created`, `employee_createDate`, `employee_lastUpdateDate`) VALUES
(1, 'test', 'test', 'test', 'test', '2016-12-22', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', '2016-12-22 00:00:00', 'test', 'test', 1, '2016-12-22 00:00:00', '2016-12-22 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `employeeinfo`
--

CREATE TABLE IF NOT EXISTS `employeeinfo` (
  `employeeinfo_ID` varchar(64) NOT NULL,
  `employeeinfo_SystemGenerateNumber` int(11) DEFAULT NULL,
  `employeeinfo_FirstName` varchar(64) DEFAULT NULL COMMENT '名',
  `employeeinfo_MiddleName` varchar(64) DEFAULT NULL COMMENT '中間名',
  `employeeinfo_LastName` varchar(64) DEFAULT NULL COMMENT '姓',
  `employeeinfo_Gender` varchar(8) DEFAULT NULL COMMENT '性別',
  `employeeinfo_HomeNumber` varchar(64) DEFAULT NULL COMMENT '家裡電話',
  `employeeinfo_PhoneNumber` varchar(64) DEFAULT NULL COMMENT '行動電話',
  `employeeinfo_Birthday` date DEFAULT NULL COMMENT '出生日',
  `employeeinfo_EmergencyContactName` varchar(64) DEFAULT NULL COMMENT '緊急聯絡人姓名',
  `employeeinfo_EmergencyContactNumber` varchar(64) DEFAULT NULL COMMENT '緊急聯絡人電話',
  `employeeinfo_SIN` varchar(64) DEFAULT NULL COMMENT '工卡(類似身分證字號)',
  `employeeinfo_Position` varchar(64) DEFAULT NULL COMMENT '職稱',
  `employeeinfo_WageSalary` varchar(64) DEFAULT NULL COMMENT '薪水模式(月薪,\r\n時薪)',
  `employeeinfo_PayRate` decimal(10,0) DEFAULT NULL COMMENT '薪水',
  `employeeinfo_DLNumber` varchar(16) DEFAULT NULL COMMENT '駕照號碼',
  `employeeinfo_EMail` varchar(512) DEFAULT NULL,
  `employeeinfo_AddressID` varchar(64) DEFAULT NULL COMMENT '居住地址ID(關聯AddressInfo)',
  `employeeinfo_StartDate` date DEFAULT NULL COMMENT '開始上班日',
  `employeeinfo_CreateDateTime` datetime DEFAULT NULL,
  `employeeinfo_UpdateDateTime` datetime DEFAULT NULL,
  `employeeinfo_Status` varchar(64) DEFAULT NULL COMMENT '狀態',
  `employeeinfo_Remarks` varchar(1024) DEFAULT NULL COMMENT '備註',
  `employeeinfo_CreateBy` varchar(64) DEFAULT NULL,
  `employeeinfo_CreateByID` varchar(64) DEFAULT NULL,
  `employeeinfo_UpdateBy` varchar(64) DEFAULT NULL,
  `employeeinfo_UpdateByID` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `employeeinfo`
--

INSERT INTO `employeeinfo` (`employeeinfo_ID`, `employeeinfo_SystemGenerateNumber`, `employeeinfo_FirstName`, `employeeinfo_MiddleName`, `employeeinfo_LastName`, `employeeinfo_Gender`, `employeeinfo_HomeNumber`, `employeeinfo_PhoneNumber`, `employeeinfo_Birthday`, `employeeinfo_EmergencyContactName`, `employeeinfo_EmergencyContactNumber`, `employeeinfo_SIN`, `employeeinfo_Position`, `employeeinfo_WageSalary`, `employeeinfo_PayRate`, `employeeinfo_DLNumber`, `employeeinfo_EMail`, `employeeinfo_AddressID`, `employeeinfo_StartDate`, `employeeinfo_CreateDateTime`, `employeeinfo_UpdateDateTime`, `employeeinfo_Status`, `employeeinfo_Remarks`, `employeeinfo_CreateBy`, `employeeinfo_CreateByID`, `employeeinfo_UpdateBy`, `employeeinfo_UpdateByID`) VALUES
('1', 1, '1', '1', '1', '1', '1', '1', '2016-11-30', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2016-11-30', '2016-11-30 00:00:00', '2016-11-30 00:00:00', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- 資料表結構 `jobsite`
--

CREATE TABLE IF NOT EXISTS `jobsite` (
  `jobsite_join_id` varchar(250) NOT NULL COMMENT 'Customer的No加J的No ex:C1J1',
  `jobsite_id` int(11) NOT NULL COMMENT '序列碼',
  `jobsite_cid` int(11) NOT NULL COMMENT 'customer_id(關聯用)',
  `jobsite_location` varchar(250) NOT NULL COMMENT '詳細地址',
  `jobsite_address` varchar(250) NOT NULL COMMENT '地址',
  `jobsite_city` varchar(250) NOT NULL COMMENT '城市',
  `jobsite_emergency` varchar(250) NOT NULL COMMENT '緊急電話',
  `jobsite_supervisor` varchar(250) NOT NULL COMMENT '監督人電話',
  `jobsite_superintend` varchar(250) NOT NULL COMMENT '主管電話',
  `jobsite_start_date` datetime NOT NULL COMMENT '建立日期',
  `jobsite_typemployee_of_works` varchar(250) NOT NULL COMMENT '工作類別(複選)',
  `jobsite_scope` varchar(250) NOT NULL COMMENT '工作領域、範圍',
  `jobsite_internal_note` varchar(250) NOT NULL,
  `jobsite_company` varchar(250) NOT NULL COMMENT '公司',
  `jobsite_uid` int(11) NOT NULL,
  `jobsite_createmployee_date` datetime NOT NULL COMMENT '創建日期',
  `jobsite_last_updatemployee_date` datetime NOT NULL COMMENT '最後更新日期'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `jobsite`
--

INSERT INTO `jobsite` (`jobsite_join_id`, `jobsite_id`, `jobsite_cid`, `jobsite_location`, `jobsite_address`, `jobsite_city`, `jobsite_emergency`, `jobsite_supervisor`, `jobsite_superintend`, `jobsite_start_date`, `jobsite_typemployee_of_works`, `jobsite_scope`, `jobsite_internal_note`, `jobsite_company`, `jobsite_uid`, `jobsite_createmployee_date`, `jobsite_last_updatemployee_date`) VALUES
('1', 0, 0, '', '', '', '', '', '', '0000-00-00 00:00:00', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `project_ID` varchar(64) NOT NULL,
  `customer_ID` varchar(64) DEFAULT NULL,
  `project_Customer` varchar(128) DEFAULT NULL,
  `project_ContractNo` varchar(64) DEFAULT NULL,
  `project_JobSiteName` varchar(128) DEFAULT NULL,
  `project_AddressID` varchar(64) DEFAULT NULL,
  `project_Scope` varchar(1024) DEFAULT NULL,
  `project_SiteSupervisorContactID` varchar(64) DEFAULT NULL,
  `project_SuperintendContactID` varchar(45) DEFAULT NULL,
  `project_ForemanContactID` varchar(45) DEFAULT NULL,
  `project_EmergencyNo` varchar(64) DEFAULT NULL,
  `project_ContactID` varchar(64) DEFAULT NULL,
  `project_CreateDateTime` datetime DEFAULT NULL,
  `project_UpdateDateTime` datetime DEFAULT NULL,
  `project_CreateBy` varchar(64) DEFAULT NULL,
  `project_CreateByID` varchar(64) DEFAULT NULL,
  `project_UpdateBy` varchar(64) DEFAULT NULL,
  `project_UpdateByID` varchar(64) DEFAULT NULL,
  `project_Terms` varchar(64) DEFAULT NULL,
  `project_JobStartDateTime` datetime DEFAULT NULL,
  `project_Frequency` int(11) DEFAULT NULL,
  `project_Remarks` varchar(256) DEFAULT NULL,
  `project_Status` varchar(64) DEFAULT NULL,
  `project_InternalNote` varchar(2048) DEFAULT NULL,
  `project_ChargingMethod` varchar(64) DEFAULT NULL COMMENT 'Hourly, Regular, Contract',
  `project_Rate` decimal(10,0) DEFAULT NULL,
  `project_ProjectStatus` varchar(64) DEFAULT NULL COMMENT 'Pending, In Progress, Complete, Cancel',
  `project_ProjectNumber` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `project`
--

INSERT INTO `project` (`project_ID`, `customer_ID`, `project_Customer`, `project_ContractNo`, `project_JobSiteName`, `project_AddressID`, `project_Scope`, `project_SiteSupervisorContactID`, `project_SuperintendContactID`, `project_ForemanContactID`, `project_EmergencyNo`, `project_ContactID`, `project_CreateDateTime`, `project_UpdateDateTime`, `project_CreateBy`, `project_CreateByID`, `project_UpdateBy`, `project_UpdateByID`, `project_Terms`, `project_JobStartDateTime`, `project_Frequency`, `project_Remarks`, `project_Status`, `project_InternalNote`, `project_ChargingMethod`, `project_Rate`, `project_ProjectStatus`, `project_ProjectNumber`) VALUES
('1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `userinfo`
--

CREATE TABLE IF NOT EXISTS `userinfo` (
  `userinfo_ID` varchar(64) NOT NULL,
  `userinfo_InExUser` varchar(64) NOT NULL COMMENT '是否為內部使用者(admin/normal)',
  `employee_ID` varchar(64) DEFAULT NULL COMMENT '關聯至 employee table',
  `userinfo_UserName` varchar(64) DEFAULT NULL COMMENT '使用者帳號',
  `userinfo_Password` varchar(64) DEFAULT NULL COMMENT '使用者密碼',
  `userinfo_CreateDateTime` datetime DEFAULT NULL COMMENT '建立這筆資料的時間',
  `userinfo_UpdateDateTime` datetime DEFAULT NULL COMMENT '最後一次更新這筆資料的時間',
  `userinfo_Status` varchar(64) DEFAULT NULL COMMENT '狀態',
  `userinfo_SystemLocation` varchar(64) DEFAULT NULL,
  `userinfo_Remarks` varchar(512) DEFAULT NULL COMMENT '備註',
  `userinfo_token` varchar(250) NOT NULL DEFAULT '[]',
  `userinfo_CreateBy` varchar(64) DEFAULT NULL COMMENT '建立這筆資料的使用者帳號',
  `userinfo_CreateByID` varchar(64) DEFAULT NULL COMMENT '建立這筆資料的使用者ID',
  `userinfo_UpdateBy` varchar(64) DEFAULT NULL COMMENT '更新這筆資料的使用者帳號',
  `userinfo_UpdateByID` varchar(64) DEFAULT NULL COMMENT '更新這筆資料的使用者ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `userinfo`
--

INSERT INTO `userinfo` (`userinfo_ID`, `userinfo_InExUser`, `employee_ID`, `userinfo_UserName`, `userinfo_Password`, `userinfo_CreateDateTime`, `userinfo_UpdateDateTime`, `userinfo_Status`, `userinfo_SystemLocation`, `userinfo_Remarks`, `userinfo_token`, `userinfo_CreateBy`, `userinfo_CreateByID`, `userinfo_UpdateBy`, `userinfo_UpdateByID`) VALUES
('1', 'admin', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '[]', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `workorder`
--

CREATE TABLE IF NOT EXISTS `workorder` (
  `workorder_ID` varchar(64) NOT NULL,
  `project_ID` varchar(64) NOT NULL,
  `workorder_SystemGenerateNumber` int(11) NOT NULL,
  `workorder_WorkOrderNumber` varchar(64) DEFAULT NULL,
  `workorder_WorkOrderStatus` varchar(64) DEFAULT NULL COMMENT 'Stage: Pending->Dispatched->In Progress->Complete (Pending can switch to cancel, Dispatched can back to Pending in dispatching view)',
  `workorder_CreateDateTime` datetime DEFAULT NULL,
  `workorder_UpdateDateTime` datetime DEFAULT NULL,
  `workorder_CreateBy` varchar(64) DEFAULT NULL,
  `workorder_CreateByID` varchar(64) DEFAULT NULL,
  `workorder_UpdateBy` varchar(64) DEFAULT NULL,
  `workorder_UpdateByID` varchar(64) DEFAULT NULL,
  `workorder_TypeOfWork` varchar(128) DEFAULT NULL COMMENT 'Creator attribute, List based on project',
  `workorder_HelperRequire` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Creator attribute',
  `workorder_EquipmentTools` varchar(512) DEFAULT NULL,
  `workorder_MaterialSupplies` varchar(512) DEFAULT NULL COMMENT 'Creator attribute',
  `workorder_EstStartDateTime` datetime DEFAULT NULL COMMENT 'Creator attribute',
  `workorder_EstHours` decimal(10,0) DEFAULT '0' COMMENT 'Creator attribute',
  `workorder_BillingHours` decimal(10,0) DEFAULT '0' COMMENT 'Creator attribute',
  `workorder_NoCharge` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Creator attribute',
  `workorder_OperatorNote` varchar(1024) DEFAULT NULL COMMENT 'Creator attribute',
  `workorder_Driver` varchar(128) DEFAULT NULL COMMENT 'Creator attribute',
  `workorder_Helper` varchar(128) DEFAULT NULL COMMENT 'Creator attribute',
  `workorder_DayOfOperation` int(11) DEFAULT NULL COMMENT 'Operator attribute',
  `workorder_StartDateTime` datetime DEFAULT NULL COMMENT 'Operator attribute',
  `workorder_FinishDateTime` datetime DEFAULT NULL COMMENT 'Operator attribute',
  `workorder_Dumping` int(11) DEFAULT NULL COMMENT 'Operator attribute',
  `workorder_BackToYard` datetime DEFAULT NULL COMMENT 'Operator attribute',
  `workorder_TailgateMeetingNote` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `workorder`
--

INSERT INTO `workorder` (`workorder_ID`, `project_ID`, `workorder_SystemGenerateNumber`, `workorder_WorkOrderNumber`, `workorder_WorkOrderStatus`, `workorder_CreateDateTime`, `workorder_UpdateDateTime`, `workorder_CreateBy`, `workorder_CreateByID`, `workorder_UpdateBy`, `workorder_UpdateByID`, `workorder_TypeOfWork`, `workorder_HelperRequire`, `workorder_EquipmentTools`, `workorder_MaterialSupplies`, `workorder_EstStartDateTime`, `workorder_EstHours`, `workorder_BillingHours`, `workorder_NoCharge`, `workorder_OperatorNote`, `workorder_Driver`, `workorder_Helper`, `workorder_DayOfOperation`, `workorder_StartDateTime`, `workorder_FinishDateTime`, `workorder_Dumping`, `workorder_BackToYard`, `workorder_TailgateMeetingNote`) VALUES
('1', '1', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '0', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `workorderattendant`
--

CREATE TABLE IF NOT EXISTS `workorderattendant` (
  `workorderattendant_ID` varchar(64) NOT NULL,
  `workorder_ID` varchar(64) NOT NULL,
  `workorderattendant_Name` varchar(64) DEFAULT NULL,
  `workorderattendant_PicturePath` varchar(64) DEFAULT NULL,
  `workorderattendant_SignDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `workorderattendant`
--

INSERT INTO `workorderattendant` (`workorderattendant_ID`, `workorder_ID`, `workorderattendant_Name`, `workorderattendant_PicturePath`, `workorderattendant_SignDateTime`) VALUES
('1', '1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `workordercrewmember`
--

CREATE TABLE IF NOT EXISTS `workordercrewmember` (
  `workordercrewmember_ID` varchar(64) NOT NULL,
  `workorder_ID` varchar(64) NOT NULL,
  `workordercrewmember_Name` varchar(64) DEFAULT NULL,
  `workordercrewmember_Position` varchar(64) DEFAULT NULL,
  `workordercrewmember_ArrivalTime` datetime DEFAULT NULL,
  `workordercrewmember_CreateDateTime` datetime DEFAULT NULL,
  `workordercrewmember_UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `workordercrewmember`
--

INSERT INTO `workordercrewmember` (`workordercrewmember_ID`, `workorder_ID`, `workordercrewmember_Name`, `workordercrewmember_Position`, `workordercrewmember_ArrivalTime`, `workordercrewmember_CreateDateTime`, `workordercrewmember_UpdateDateTime`) VALUES
('1', '1', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `workorderreport`
--

CREATE TABLE IF NOT EXISTS `workorderreport` (
  `workorderreport_ID` varchar(64) NOT NULL,
  `workorder_ID` varchar(64) NOT NULL,
  `workorderreport_VehicleName` varchar(64) DEFAULT NULL,
  `workorderreport_VehicleID` varchar(64) DEFAULT NULL,
  `workorderreport_PicturePath` varchar(64) DEFAULT NULL,
  `workorderreport_VehicleTask` varchar(512) DEFAULT NULL,
  `workorderreport_CreateDateTime` datetime DEFAULT NULL,
  `workorderreport_UpdateDateTime` datetime DEFAULT NULL,
  `workorderreport_CreateBy` varchar(64) DEFAULT NULL,
  `workorderreport_CreateByID` varchar(64) DEFAULT NULL,
  `workorderreport_UpdateBy` varchar(64) DEFAULT NULL,
  `workorderreport_UpdateByID` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `workorderreport`
--

INSERT INTO `workorderreport` (`workorderreport_ID`, `workorder_ID`, `workorderreport_VehicleName`, `workorderreport_VehicleID`, `workorderreport_PicturePath`, `workorderreport_VehicleTask`, `workorderreport_CreateDateTime`, `workorderreport_UpdateDateTime`, `workorderreport_CreateBy`, `workorderreport_CreateByID`, `workorderreport_UpdateBy`, `workorderreport_UpdateByID`) VALUES
('1', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `address`
--
ALTER TABLE `address`
 ADD PRIMARY KEY (`userinfo_ID`);

--
-- 資料表索引 `contactinfo`
--
ALTER TABLE `contactinfo`
 ADD PRIMARY KEY (`contactinfo_ID`);

--
-- 資料表索引 `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`customer_id`);

--
-- 資料表索引 `customerinfo`
--
ALTER TABLE `customerinfo`
 ADD PRIMARY KEY (`customerinfo_ID`);

--
-- 資料表索引 `jobsite`
--
ALTER TABLE `jobsite`
 ADD PRIMARY KEY (`jobsite_id`);

--
-- 資料表索引 `project`
--
ALTER TABLE `project`
 ADD PRIMARY KEY (`project_ID`);

--
-- 資料表索引 `userinfo`
--
ALTER TABLE `userinfo`
 ADD PRIMARY KEY (`userinfo_ID`);

--
-- 資料表索引 `workorderattendant`
--
ALTER TABLE `workorderattendant`
 ADD PRIMARY KEY (`workorderattendant_ID`);

--
-- 資料表索引 `workordercrewmember`
--
ALTER TABLE `workordercrewmember`
 ADD PRIMARY KEY (`workordercrewmember_ID`);

--
-- 資料表索引 `workorderreport`
--
ALTER TABLE `workorderreport`
 ADD PRIMARY KEY (`workorderreport_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

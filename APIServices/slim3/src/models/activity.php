<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/user.php");

class Activity extends BasicModel {

    protected $ci;
    protected $db;
    private $userM;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
        $this->userM = new User($ci);
    }

    /**
     * 取得Activity資料 By Id
     * @param string $id 編號
     * @return object Activity資料
     */
    public function getByEmployeeIdandDate($employee_id, $date) {
        //select資料
        $sql = " select  DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                  ." , CONCAT(COALESCE(e.employeeinfo_FirstName, ''), ' ', COALESCE(e.employeeinfo_LastName, '')) as operators "
                  ." , p.*, e.employeeinfo_ID as employeeinfo_ID "
                  ." , DATE_FORMAT(p.punch_start_time, '%h:%i%p') as punch_start_time "
                  ." , DATE_FORMAT(p.punch_end_time, '%h:%i%p') as punch_end_time "
                  ." , count(w.workorder_ID) as workorders "
                  ." , count(case w.workorder_Status when 'Dispatched' then 1 else null end) as Dispached "
                  ." , count(case w.workorder_Status when 'In Progress' then 1 else null end) as InProgress "
                  ." , count(case w.workorder_Status when 'Complete' then 1 else null end) as Complete "
                  ." , DATE_FORMAT(p.punch_CreateDateTime, '%Y-%m-%d %H:%i') as punch_CreateDateTime "
                  ." , DATE_FORMAT(p.punch_UpdateDateTime, '%Y-%m-%d %H:%i') as punch_UpdateDateTime "
              ." from employeeinfo as e "
              ." inner join workorder as w on w.workorder_OperatorFromEmployeeID = e.employeeinfo_ID "
                         ." and workorder_Status != 'Pending' and  DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') = '".$date."' "
              ." left join punch as p on p.employeeinfo_ID = e.employeeinfo_ID and DATE_FORMAT(p.punch_start_time, '%Y-%m-%d') = DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') "
              ." where e.employeeinfo_ID = '".$employee_id."' "
              ." group by DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') ";

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = (array)$stmt->fetchAll(PDO::FETCH_OBJ);
        }
        if($r) {
            $r = (array)$r[0];
            if(isset($r['punch_CreateByID'])) {
                $creater = $this->userM->getById($r['punch_CreateByID']);
                if($creater['success']) {
                    $r['creater'] = $creater['data']['userinfo_UserName'];
                }
            }
            if(isset($r['punch_UpdateByID'])) {
                $updater = $this->userM->getById($r['punch_UpdateByID']);
                if($updater['success']) {
                    $r['updater'] = $updater['data']['userinfo_UserName'];
                }
            }
        }

        //select workorder資料
        $sql = " select w.* "
                ." ,DATE_FORMAT(w.workorder_StartTime, '%h:%i%p') as workorder_StartTime "
                ." ,DATE_FORMAT(w.workorder_CompleteTime, '%h:%i%p') as workorder_CompleteTime "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d %H:%i') as workorder_EstStartDateTime "
              ." from workorder as w "
              ." where w.workorder_Status != 'Pending' "
              ." and w.workorder_OperatorFromEmployeeID = '".$employee_id."' "
              ." and DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') = '".$date."' ";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r['workorder_list'] = $stmt->fetchAll(PDO::FETCH_OBJ);
        }

        return $r;
    }
}
?>

<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Supplier extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得supplier資料 By Id
     * @param string $id 編號
     * @return object supplier資料
     */
    public function getById($id) {
        $sql ="select "
                ." *, "
                ." DATE_FORMAT(s.Since, '%Y-%m-%d') as Since, "
                ." CONCAT( COALESCE(a.address_PostalCode, ''), ' ', COALESCE(a.address_Address, ''), ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Latitude, ''), ' ', COALESCE(a.address_Longitude, '')) as address, "
                ." a.address_ID as supplier_Address_address_ID, a.address_Address as supplier_Address_address_Address, a.address_City as supplier_Address_address_City, a.address_Province as supplier_Address_address_Province, a.address_PostalCode as supplier_Address_address_PostalCode, "
                ." a.address_Latitude as supplier_Address_address_Latitude, a.address_Longitude as supplier_Address_address_Longitude,"
                ." creater.userinfo_UserName as creater, "
                ." DATE_FORMAT(s.supplier_CreateDateTime, '%Y-%m-%d %H:%i') as supplier_CreateDateTime, "
                ." DATE_FORMAT(s.supplier_UpdateDateTime, '%Y-%m-%d %H:%i') as supplier_UpdateDateTime "
            ." from supplier as s "
            ." left join address as a on s.supplier_AddressID = a.address_ID "
            ." left join userinfo as creater on s.supplier_CreateByID = creater.userinfo_ID "
            ." where s.SupplierID = '".$id."'";

        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            $result = (array)$stmt->fetch(PDO::FETCH_OBJ);
            if($result['address'] == '    ') {
                $result['address'] = "";
            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 建立supplier資料
     * @param object $supplierData ex:array("欄位名稱"=> "欄位值")
     * @return object supplier資料
     */
    public function create($supplierData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("supplier", $supplierData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $supplierId = $dbh->lastInsertId();

            $addressId = $this->createAddress($relateData["supplier_Address"]);
            $updatesupplierData = array(
                "supplier_AddressID" => $addressId
            );

            $updateSQL = $this->prepareUpdate("supplier", $updatesupplierData, "`SupplierID`='".$supplierId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($supplierId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 修改supplier資料
     * @param object $supplierData ex:array("欄位名稱"=> "欄位值")
     * @return object supplier資料
     */
    public function updateById($supplierData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $supplierId = $supplierData["SupplierID"];
            /*
            //更新通訊錄(舊系統資料更新時才會新增contact資料)
            $contactKey = ["supplier_Emergency", "supplier_Supervisor", "supplier_Superintend", "supplier_Foreman"];
            for($i=0;$i<count($contactKey);$i++) {
                $key = $contactKey[$i];
                if($relateData[$key]["contactinfo_ID"] != "") {
                    $this->updateContact($relateData[$key], "`contactinfo_ID`='".$relateData[$key]["contactinfo_ID"]."'");
                } else {
                    $supplierData[$key."ContactID"] = $this->createContact("supplier", $supplierId, $relateData[$key]);
                }
            }
            */
            //更新地址(舊系統資料更新時才會新增address資料)
            if($relateData["supplier_Address"]["address_ID"]) {
                $this->updateAddress($relateData["supplier_Address"], "`address_ID`='".$relateData["supplier_Address"]["address_ID"]."'");
            } else {
                unset($relateData["supplier_Address"]["address_ID"]);
                $supplierData["supplier_AddressID"] = $this->createAddress($relateData["supplier_Address"]);
            }
            $prepare = $this->prepareUpdate("supplier", $supplierData, "`SupplierID`='".$supplierId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($supplierId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    public function createContact($tableName, $pk, $contactData) {
        $dbh = $this->ci->db;
        $contactData["contactinfo_PrimaryTable"] = $tableName;
        $contactData["contactinfo_PrimaryID"] = $pk;
        $prepare = $this->prepareInsertSQL("contactinfo", $contactData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateContact($contactData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("contactinfo", $contactData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }

    public function createAddress($addressData) {
        $dbh = $this->ci->db;
        $prepare = $this->prepareInsertSQL("address", $addressData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateAddress($addressData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("address", $addressData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }
}

?>

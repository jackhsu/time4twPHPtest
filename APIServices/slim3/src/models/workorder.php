<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class WorkOrder extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得workorder資料 By Id
     * @param string $id 編號
     * @return object workorder資料
     */
    public function getById($id) {
        $sql ="select "
                ." w.*, creater.userinfo_UserName as creater "
                ." ,CONCAT(COALESCE(operator.employeeinfo_FirstName, '') , IF(operator.employeeinfo_FirstName is null or operator.employeeinfo_FirstName = '', '', ' '), COALESCE(operator.employeeinfo_LastName, '')) as workorder_OperatorFromEmployeeName "
                ." ,CONCAT(COALESCE(helper.employeeinfo_FirstName, '') , IF(helper.employeeinfo_FirstName is null or helper.employeeinfo_FirstName = '', '', ' '), COALESCE(helper.employeeinfo_LastName, '')) as workorder_HelperFromEmployeeName "
                ." ,IF(u.unit_LicensePlate is null, u.unit_access_ID, u.unit_LicensePlate) as workorder_UnitFromUnitName "
                ." ,date(w.workorder_InvoiceDate) as workorder_InvoiceDate "
                ." ,CONCAT(COALESCE(purchaser.employeeinfo_FirstName, '') , IF(purchaser.employeeinfo_FirstName is null or purchaser.employeeinfo_FirstName = '', '', ' '), COALESCE(purchaser.employeeinfo_LastName, '')) as workorder_Purchaser_Name "
                ." ,DATE_FORMAT(w.workorder_StartTime, '%h:%i%p') as workorder_StartTime "
                ." ,DATE_FORMAT(w.workorder_CompleteTime, '%h:%i%p') as workorder_CompleteTime "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date  "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as workorder_EstStartDate  "
                ." ,DATE_FORMAT(w.workorder_EstStartDateTime, '%h:%i%p') as workorder_EstStartTime  "
                ." ,DATE_FORMAT(w.workorder_CreateDateTime, '%Y-%m-%d %H:%i') as workorder_CreateDateTime "
                ." ,DATE_FORMAT(w.workorder_UpdateDateTime, '%Y-%m-%d %H:%i') as workorder_UpdateDateTime "
            ." from workorder as w"
            ." inner join userinfo as creater on w.workorder_CreateByID = creater.userinfo_ID "
            ." left join employeeinfo as operator on w.workorder_OperatorFromEmployeeID = operator.employeeinfo_ID "
            ." left join employeeinfo as helper on w.workorder_HelperFromEmployeeID = helper.employeeinfo_ID "
            ." left join employeeinfo as purchaser on w.workorder_Purchaser = purchaser.employeeinfo_ID "
            ." left join unit as u on w.workorder_UnitFromUnitID = u.unit_ID "
            ." where w.workorder_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            return (array)$stmt->fetch(PDO::FETCH_OBJ);
        } else {
            return false;
        }
    }

    /**
     * 取得dispatching資料,dataTable格式
     * ["Operator", "Helper"]使用
     * @param string $searchKey 搜尋的關鍵字
     * @return object workorder資料
     */
    public function dispatching($params) {
        //search keyWord
        if(isset($params['searchKey'])) {
            $key = explode(" ", $params['searchKey']);
            $condition = " where e.employeeinfo_FirstName like '%".$key[0]."%' ";
            if(isset($key[1]) && $key[1]!="") {
                $condition = $condition." and e.employeeinfo_LastName  like '%".$key[1]."%' ";
            }
        } else {
            $condition = "";
        }
        //select資料
        $orderColumn = array(
            " name "
            , " jobsite "
            , " worktime "
            , " unit "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = " select SQL_CALC_FOUND_ROWS "
                      ."CONCAT(e.employeeinfo_ID, ',', COALESCE(e.employeeinfo_FirstName, '') , ' ', COALESCE(e.employeeinfo_LastName, '')) as name "
                      ." , j.jobsite_Name as jobsite, w.workorder_EstStartDateTime as worktime, u.unit_LicensePlate as unit  "
              ." from employeeinfo as e "
              ." left join workorder as w on (w.workorder_OperatorFromEmployeeID = e.employeeinfo_ID or w.workorder_HelperFromEmployeeID = e.employeeinfo_ID) and w.workorder_EstStartDateTime < (current_date() + INTERVAL 1 DAY ) and  w.workorder_EstStartDateTime > current_date() "
              ." left join jobsite as j on j.jobsite_ID = w.jobsite_ID "
              ." left join unit as u on u.unit_ID = w.workorder_UnitFromUnitID "
              .$condition
              .$orderBy.$limit;
        $r = array();

        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = (int)$recordsTotal;
            $r["recordsFiltered"] = (int)$recordsTotal;
            return $r;
        } else {
            return false;
        }
    }

    /**
     * 取得dispatching資料,dataTable格式
     * "Unit" 使用
     * @param string $searchKey 搜尋的關鍵字
     * @return object workorder資料
     */
    public function dispatchingUnit($params) {
        //search keyWord
        if(isset($params['searchKey'])) {
            $condition = " and u.unit_LicensePlate like '%".$params['searchKey']."%' ";
        } else {
            $condition = "";
        }
        //select資料
        $orderColumn = array(
            " unit "
            , " name "
            , " jobsite "
            , " time "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = " select SQL_CALC_FOUND_ROWS "
                    ." CONCAT(unit_ID, ',', IF(u.unit_LicensePlate is null, u.unit_access_ID, u.unit_LicensePlate)) as unit "
                    ." , CONCAT(COALESCE(e.employeeinfo_FirstName, '') , IF(e.employeeinfo_FirstName is null or e.employeeinfo_FirstName = '', '', ' '), COALESCE(e.employeeinfo_LastName, '')) as name "
                    ." , j.jobsite_Name as jobsite "
                    ." , unit_LastDispatchedDateTime as time "
              ." from unit as u "
              ." left join workorder as w on w.workorder_UnitFromUnitID = u.unit_ID "
              ." left join jobsite as j on j.jobsite_ID = w.jobsite_ID "
              ." left join employeeinfo as e on e.employeeinfo_ID = w.workorder_OperatorFromEmployeeID "
              ." where u.unit_Status = 'Active' "
              .$condition
              ." group by u.unit_ID "
              .$orderBy.$limit;
        $r = array();

        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = (int)$recordsTotal;
            $r["recordsFiltered"] = (int)$recordsTotal;
            return $r;
        } else {
            return false;
        }
    }

    /**
     * 建立workorder資料
     * @param object $data ex:array("欄位名稱"=> "欄位值")
     * @return object workorder資料
     */
    public function create($data) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("workorder", $data);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $workorderId = $dbh->lastInsertId();

            $updateWorkorderData = array(
                "workorder_JoinID" => $data["workorder_JoinID"]."W$workorderId"
            );

            $updateSQL = $this->prepareUpdate("workorder", $updateWorkorderData, "`workorder_ID`='".$workorderId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($workorderId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 修改workorder資料
     * @param object $jobstieData ex:array("欄位名稱"=> "欄位值")
     * @return object jobstie資料
     */
    public function updateById($data, $id) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $prepare = $this->prepareUpdate("workorder", $data, "`workorder_ID`='".$id."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();

            //更新unit的unit_LastDispatchedDateTime
            if(isset($data['workorder_UnitFromUnitID'])) {
                $unit_data = array();
                $unit_data['unit_LastDispatchedDateTime'] = 'now()';
                $prepare = $this->prepareUpdate("unit", $unit_data, "`unit_ID`='".$data['workorder_UnitFromUnitID']."'");
                $stmt = $dbh->prepare($prepare);
                $stmt->execute();
            }

            $dbh->commit();
            return $this->getById($id);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 取消workorder(Pending、Dispatched狀態才可取消)
     * @param object $data "workorder_UpdateByID" => 更新者id
                           ,"workorder_UpdateDateTime" => 時間
     * @param object $id workorder_id
     * @return object 
     */
    public function cancel($data, $id) {
        $dbh = $this->ci->db;
        $d = $this->getById($id);
        if($d['workorder_Status'] != 'Pending' && $d['workorder_Status'] !='Dispatched') {
            return false;
        }
        $dbh->beginTransaction();
        try {
            $data['workorder_Status'] = 'Cancel';
            $prepare = $this->prepareUpdate("workorder", $data, "`workorder_ID`='".$id."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($id);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }
}

?>

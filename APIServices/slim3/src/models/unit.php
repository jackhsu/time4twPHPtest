<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Unit extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得unit資料, 轉成dataTable格式
     * @param object $params ex:array("length"=> "換頁條件", "order"=> "換頁條件","search"=> "換頁條件","start"=> "換頁條件","searchKey"=> "搜尋條件")
     * @return object unit資料表資料
     */
    public function toDatatable($params) {
        $r = array(
                "data" => array()
                , "recordsTotal" => 0
                , "recordsFiltered" => 0
            );

        //search keyWord
        $search = array();
        if(isset($params['searchKey'])) {
            $search[] = " (unit_access_ID like '%".$params['searchKey']."%' " 
                       ." or unit_ID = '".(int)$params['searchKey']."' "
                       ." or unit_LicensePlate like '%".$params['searchKey']."%' "
                       ." or unit_Status like '%".$params['searchKey']."%' "
                       ." or unit_LastDispatchedDateTime like '%".$params['searchKey']."%') ";
        }
        if(isset($params['advanceSearchId'])) {
            $search[] = " unit_ID = '".(int)$params['advanceSearchId']."'";
        }
        if(isset($params['advanceSearchAccessId'])) {
            $search[] = " unit_access_ID = '".$params['advanceSearchAccessId']."'";
        }
        $condition = "";
        $condition = implode(" and ",$search);
        $condition = strlen($condition)?" where ".$condition:"";
        
        $orderColumn = array(
            " CAST(unit_access_ID as SIGNED) "
            , " CAST(unit_ID as SIGNED) "
            , " unit_LicensePlate "
            , " unit_Status "
            , " unit_LastDispatchedDateTime "
            , " CAST(unit_ID as SIGNED) "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "SELECT SQL_CALC_FOUND_ROWS unit_access_ID, unit_ID, unit_LicensePlate, unit_Status, DATE_FORMAT(unit_LastDispatchedDateTime, '%Y-%m-%d %H:%i') as unit_LastDispatchedDateTime, unit_ID "
              ." FROM unit "
              .$condition
              .$orderBy.$limit;

        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }
        
        return $r;
    }

    /**
     * 取得unit資料 By Id
     * @param string $id 編號
     * @return object unit資料
     */
    public function getById($id) {
        $sql = " select *, DATE_FORMAT(unit_LastDispatchedDateTime, '%Y-%m-%d %H:%i') as unit_LastDispatchedDateTime from unit where unit_ID = '".$id."' ";
        
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return array("success"=>true, "data" => $data[0]);
        } else {
            return array("success"=>false,"msg"=>"unit_ID不存在");
        }
    }

    /**
     * 取得unit資料 By Id
     * @param object $filterPostData ex:array("欄位名稱"=> "欄位值")
     * @return object bool
     */
    public function create($filterPostData) {
        $sql = "INSERT INTO unit (unit_access_ID, unit_unit, unit_vin, unit_model, unit_LicensePlate, unit_Status, unit_CreateByID, unit_CreateDateTime) 
                          VALUES (:unit_access_ID, :unit_unit, :unit_vin, :unit_model, :unit_LicensePlate, :unit_Status, :unit_CreateByID, now())";
        
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute($filterPostData)) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
        }
        return $callback;
    }
    
    /**
     * 修改jobsite資料
     * @param object $unitData ex:array("欄位名稱"=> "欄位值")
     * @return object unit資料
     */
    public function updateById($unitData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $unitId = $unitData["unit_ID"];
            $prepare = $this->prepareUpdate("unit", $unitData, "`unit_ID`='".$unitId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($unitId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }
}

?>

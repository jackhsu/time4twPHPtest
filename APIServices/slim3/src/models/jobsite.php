<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class Jobsite extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得jobsite資料 By Id
     * @param string $id 編號
     * @return object jobsite資料
     */
    public function getById($id) {
        $sql ="select "
                ." CONCAT( COALESCE(customerContact.contactinfo_Name, ''), ' ', COALESCE(customerContact.contactinfo_PhoneNumber, '')) as createContact, "
                ." CONCAT( COALESCE(a.address_PostalCode, ''), ' ', COALESCE(a.address_Address, ''), ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Latitude, ''), ' ', COALESCE(a.address_Longitude, '')) as address, "
                ." a.address_ID as jobsite_Address_address_ID, a.address_Address as jobsite_Address_address_Address, a.address_City as jobsite_Address_address_City, a.address_Province as jobsite_Address_address_Province, a.address_PostalCode as jobsite_Address_address_PostalCode, "
                ." a.address_Latitude as jobsite_Address_address_Latitude, a.address_Longitude as jobsite_Address_address_Longitude,"
                ." CONCAT(  IF(emergency.contactinfo_Name is null, '', CONCAT(emergency.contactinfo_Name, ' ')), emergency.contactinfo_PhoneNumber,  IF(emergency.contactinfo_Extension is null, '', CONCAT(' #',emergency.contactinfo_Extension))) as emergency, "
                ." CONCAT(  IF(foreman.contactinfo_Name is null, '', CONCAT(foreman.contactinfo_Name, ' ')), foreman.contactinfo_PhoneNumber,  IF(foreman.contactinfo_Extension is null, '', CONCAT(' #',foreman.contactinfo_Extension))) as foreman, "                
                ." CONCAT(  IF(supervisor.contactinfo_Name is null, '', CONCAT(supervisor.contactinfo_Name, ' ')), supervisor.contactinfo_PhoneNumber,  IF(supervisor.contactinfo_Extension is null, '', CONCAT(' #',supervisor.contactinfo_Extension))) as supervisor, "
                ." CONCAT(  IF(superintend.contactinfo_Name is null, '', CONCAT(superintend.contactinfo_Name, ' ')), superintend.contactinfo_PhoneNumber,  IF(superintend.contactinfo_Extension is null, '', CONCAT(' #',superintend.contactinfo_Extension))) as superintend, "
                ." job.*, c.*, "
                ." emergency.contactinfo_ID as Emergency_contactinfo_ID, emergency.contactinfo_Name as Emergency_contactinfo_Name, emergency.contactinfo_Title as Emergency_contactinfo_Title, emergency.contactinfo_PhoneNumber as Emergency_contactinfo_PhoneNumber, emergency.contactinfo_Extension as Emergency_contactinfo_Extension, "
                ." foreman.contactinfo_ID as Foreman_contactinfo_ID, foreman.contactinfo_Name as Foreman_contactinfo_Name, foreman.contactinfo_Title as Foreman_contactinfo_Title, foreman.contactinfo_PhoneNumber as Foreman_contactinfo_PhoneNumber, foreman.contactinfo_Extension as Foreman_contactinfo_Extension, "
                ." supervisor.contactinfo_ID as Supervisor_contactinfo_ID, supervisor.contactinfo_Name as Supervisor_contactinfo_Name, supervisor.contactinfo_Title as Supervisor_contactinfo_Title, supervisor.contactinfo_PhoneNumber as Supervisor_contactinfo_PhoneNumber, supervisor.contactinfo_Extension as Supervisor_contactinfo_Extension, "
                ." superintend.contactinfo_ID as Superintend_contactinfo_ID, superintend.contactinfo_Name as Superintend_contactinfo_Name, superintend.contactinfo_Title as Superintend_contactinfo_Title, superintend.contactinfo_PhoneNumber as Superintend_contactinfo_PhoneNumber, superintend.contactinfo_Extension as Superintend_contactinfo_Extension, "
                ." creater.userinfo_UserName as creater, updater.userinfo_UserName as updater, "
                ." COALESCE(DATE_FORMAT(job.jobsite_StartDateTime, '%Y-%m-%d %H:%i'), '') as jobsite_StartDateTime, "
                ." DATE_FORMAT(job.jobsite_CreateDateTime, '%Y-%m-%d %H:%i') as jobsite_CreateDateTime, "
                ." DATE_FORMAT(job.jobsite_UpdateDateTime, '%Y-%m-%d %H:%i') as jobsite_UpdateDateTime "
            ." from jobsite as job "
            ." left join customerinfo as c on job.jobsite_CustomerID = c.customerInfo_ID "
            ." left join contactinfo as customerContact on c.customerInfo_ContactID = customerContact.contactinfo_ID and customerContact.contactinfo_PrimaryTable = 'customerinfo' "
            ." left join address as a on job.jobsite_AddressID = a.address_ID "
            ." left join contactinfo as emergency on job.jobsite_EmergencyContactID = emergency.contactinfo_ID and emergency.contactinfo_PrimaryTable = 'jobsite' "
            ." left join contactinfo as foreman on job.jobsite_ForemanContactID = foreman.contactinfo_ID and foreman.contactinfo_PrimaryTable = 'jobsite' "
            ." left join contactinfo as supervisor on job.jobsite_SupervisorContactID = supervisor.contactinfo_ID and supervisor.contactinfo_PrimaryTable = 'jobsite' "
            ." left join contactinfo as superintend on job.jobsite_SuperintendContactID = superintend.contactinfo_ID and superintend.contactinfo_PrimaryTable = 'jobsite' "
            ." inner join userinfo as creater on job.jobsite_CreateByID = creater.userinfo_ID "
            ." left join userinfo as updater on job.jobsite_UpdateByID = updater.userinfo_ID "
            ." where job.jobsite_ID = '".$id."'";
        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            $result = (array)$stmt->fetch(PDO::FETCH_OBJ);
            if($result['address'] == '    ') {
                $result['address'] = "";
            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 建立jobsite資料
     * @param object $jobstieData ex:array("欄位名稱"=> "欄位值")
     * @return object jobstie資料
     */
    public function create($jobstieData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("jobsite", $jobstieData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $jobsiteId = $dbh->lastInsertId();

            $addressId = $this->createAddress($relateData["jobsite_Address"]);
            $updateJobsiteData = array(
                "jobsite_JoinID" => "C".$jobstieData["jobsite_CustomerID"]."J$jobsiteId"
                ,"jobsite_AddressID" => $addressId
            );

            if(isset($relateData["jobsite_Emergency"])) {
                $updateJobsiteData["jobsite_EmergencyContactID"] = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Emergency"]);
            }
            if(isset($relateData["jobsite_Supervisor"])) {
                $updateJobsiteData["jobsite_SupervisorContactID"] = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Supervisor"]);
            }
            if(isset($relateData["jobsite_Superintend"])) {
                $updateJobsiteData["jobsite_SuperintendContactID"] = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Superintend"]);
            }
            if(isset($relateData["jobsite_Foreman"])) {
                $updateJobsiteData["jobsite_ForemanContactID"] = $this->createContact("jobsite", $jobsiteId, $relateData["jobsite_Foreman"]);
            }

            $updateSQL = $this->prepareUpdate("jobsite", $updateJobsiteData, "`jobsite_ID`='".$jobsiteId."'");
            $stmt = $dbh->prepare($updateSQL);
            $stmt->execute();
            $dbh->commit(); 

            return $this->getById($jobsiteId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 修改jobsite資料
     * @param object $jobstieData ex:array("欄位名稱"=> "欄位值")
     * @return object jobstie資料
     */
    public function updateById($jobstieData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $jobsiteId = $jobstieData["jobsite_ID"];
            //更新通訊錄(舊系統資料更新時才會新增contact資料)
            $contactKey = ["jobsite_Emergency", "jobsite_Supervisor", "jobsite_Superintend", "jobsite_Foreman"];
            for($i=0;$i<count($contactKey);$i++) {
                $key = $contactKey[$i];
                if($relateData[$key]["contactinfo_ID"] != "") {
                    $this->updateContact($relateData[$key], "`contactinfo_ID`='".$relateData[$key]["contactinfo_ID"]."'");
                } else if($relateData[$key]["contactinfo_PhoneNumber"]) {
                    unset($relateData[$key]["contactinfo_ID"]);
                    $jobstieData[$key."ContactID"] = $this->createContact("jobsite", $jobsiteId, $relateData[$key]);
                }
            }
            //更新地址(舊系統資料更新時才會新增address資料)
            if($relateData["jobsite_Address"]["address_ID"] != "") {
                $this->updateAddress($relateData["jobsite_Address"], "`address_ID`='".$relateData["jobsite_Address"]["address_ID"]."'");
            } else {
                unset($relateData["jobsite_Address"]["address_ID"]);
                $jobstieData["jobsite_AddressID"] = $this->createAddress($relateData["jobsite_Address"]);
            }
            $prepare = $this->prepareUpdate("jobsite", $jobstieData, "`jobsite_ID`='".$jobsiteId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($jobsiteId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    public function createContact($tableName, $pk, $contactData) {
        $dbh = $this->ci->db;
        $contactData["contactinfo_PrimaryTable"] = $tableName;
        $contactData["contactinfo_PrimaryID"] = $pk;
        $prepare = $this->prepareInsertSQL("contactinfo", $contactData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateContact($contactData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("contactinfo", $contactData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }

    public function createAddress($addressData) {
        $dbh = $this->ci->db;
        $prepare = $this->prepareInsertSQL("address", $addressData);
        $stmt = $dbh->prepare($prepare["sql"]);
        $stmt->execute($prepare["val"]);
        return $dbh->lastInsertId();
    }

    public function updateAddress($addressData, $condition) {
        $dbh = $this->ci->db;
        $updateSQL = $this->prepareUpdate("address", $addressData, $condition);
        $stmt = $dbh->prepare($updateSQL);
        $stmt->execute();
    }
}

?>

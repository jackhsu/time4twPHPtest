<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

class PoNumber extends BasicModel {

    protected $ci;
    protected $db;

    function __construct(ContainerInterface $ci) {
        parent::__construct($ci);
        $this->db = $ci->db;
    }

    /**
     * 取得poNumber資料 By Id
     * @param string $id 編號
     * @return object poNumber資料
     */
    public function getById($id) {
        $sql ="select po.*
                    , creater.userinfo_UserName as creater 
                    , IFNULL(DATE_FORMAT(po.PromiseDate, '%Y-%m-%d'), DATE_FORMAT(po.PromiseTime, '%Y-%m-%d')) as PromiseDate 
                    , IFNULL(DATE_FORMAT(po.PromiseTime, '%h:%i%p'), DATE_FORMAT(po.PromiseDate, '%h:%i%p')) as PromiseTime 
                    , DATE_FORMAT(po.poNumber_CreateDateTime, '%Y-%m-%d %H:%i') as poNumber_CreateDateTime
               from `po number` as po 
               left join userinfo as creater on po.poNumber_CreateByID = creater.userinfo_ID
               where po.po_ID = '".$id."'";

        $stmt = $this->db->prepare($sql);
        if ($stmt->execute()) {
            $result = (array)$stmt->fetch(PDO::FETCH_OBJ);
            return $result;
        } else {
            return false;
        }
    }

    /**
     * 建立poNumber資料
     * @param object $poNumberData ex:array("欄位名稱"=> "欄位值")
     * @return object poNumber資料
     */
    public function create($poNumberData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction(); 
        try { 
            $prepare = $this->prepareInsertSQL("`po number`", $poNumberData);
            $stmt = $dbh->prepare($prepare["sql"]);
            $stmt->execute($prepare["val"]);
            $poNumberId = $dbh->lastInsertId();

            return $this->getById($poNumberId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }

    /**
     * 修改poNumber資料
     * @param object $poNumberData ex:array("欄位名稱"=> "欄位值")
     * @return object poNumber資料
     */
    public function updateById($poNumberData, $relateData) {
        $dbh = $this->ci->db;
        $dbh->beginTransaction();
        try {
            $poNumberId = $poNumberData["po_ID"];
            $prepare = $this->prepareUpdate("po number", $poNumberData, "`po_ID`='".$poNumberId."'");
            $stmt = $dbh->prepare($prepare);
            $stmt->execute();
            $dbh->commit();
            return $this->getById($poNumberId);
        } catch(PDOExecption $e) { 
            $dbh->rollback();
            return $e->getMessage();
        }
    }
}

?>

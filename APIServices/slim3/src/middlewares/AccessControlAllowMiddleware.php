<?php

/*
 * Access Control Allow domain
 */

class AccessControlAllowMiddleware {

    /**
     * Veryify Token is valid or not
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        /*
        multiple ex: if ($http_origin == "http://server1.com" ||
                    $http_origin == "http://server2.com" ||
                    $http_origin == "http://www.server3.com" ||
                    $http_origin == "http://www.server4.com")
                {
                    header("Access-Control-Allow-Origin: $http_origin");
                }
        */
        $response = $next($requestWithAuthInfo, $response)
                    ->withHeader('Access-Control-Allow-Origin', 'http://mysite')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
}

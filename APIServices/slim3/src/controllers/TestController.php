<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/activity.php");
require_once ( __DIR__ . "/../models/punch.php");
require_once ( __DIR__ . "/../models/user.php");
require_once ( __DIR__ . "/../models/jobsite.php");

class TestController extends BasicController
{
    private $db;
    private $activityM;
    private $punchM;
    private $userM;
    private $jobsiteM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("workorder", $ci);
        $this->db = $ci->db;
        $this->jobsiteM = new Jobsite($ci);
    }

    /**
     * 資料庫轉換, 將customer old data的資料新增至address table, 並update customerInfo_AddressID
     */
    public function dbTransformCustomerAddress($request, $response, $args) {
        //select資料
        $sql = " select * from customerinfo";

        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_OBJ, PDO::FETCH_ORI_NEXT)) {
                if($row->customerInfo_AddressID == null) {
                    //add Address
                    $oldData = (array)json_decode($row->customerInfo_OldData);
                    $addressData = array(
                        "address_Address" => $oldData["BillingAddress"]
                        , "address_City" => $oldData["City"]
                        , "address_Province" => $oldData["Province"]
                        , "address_PostalCode" => $oldData["PostalCode"]
                        , "address_CreateDateTime" => date("Y-m-d H:i:s", time())
                    );
                    $addressDataId = $this->jobsiteM->createAddress($addressData);

                    //update Customer Address
                    $updateData = array(
                        "customerInfo_AddressID" => $addressDataId
                    );
                    $updateSQL = $this->jobsiteM->prepareUpdate("customerInfo", $updateData, "`customerInfo_ID`='".$row->customerInfo_ID."'");
                    $stmt2 = $this->ci->db->prepare($updateSQL);
                    $stmt2->execute();
                }
            }
        }
        return $this->jsonResponse($response, $r);
    }
}   
?>
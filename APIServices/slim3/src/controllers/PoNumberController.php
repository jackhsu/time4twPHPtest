<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/poNumber.php");

class PoNumberController extends BasicController {

    private $db;
    //supplier model
    private $supplierM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("po number", $ci);
        $this->poNumberM = new PoNumber($ci);
    }

    /**
     * 取得所有po number資料(DataTable格式)
     */
    public function collection($request, $response, $args) {
        $sql = "select from `po number`";
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_NUM);
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有po number資料(DataTable格式)
     */
    public function toDatatable($request, $response, $args) {    
        $params = $request->getQueryParams();
        
        //search keyWord
        $search = array();

        if(isset($params['searchKey'])) {
            $search[] = " (poId = '".(int)$params['searchKey']."'"
                        ." or r.workorder like '%".$params['searchKey']."%' "
                        ." or r.po_Supplier like '%".$params['searchKey']."%' "
                        ." or r.sup_Supplier like '%".$params['searchKey']."%' "
                        ." or r.PromiseDate like '%".$params['searchKey']."%' "
                        ." or r.PromiseTime like '%".$params['searchKey']."%') ";
        }

        $condition = "";
        $condition = implode(" and ",$search);
        $condition = strlen($condition)?" where ".$condition:"";

        //select資料
        $orderColumn = array(
            " CAST(poId as SIGNED) "
            , " workOrder "
            , " Supplier "
            , " PromiseDate "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select
                    r.poId as poId
                    , IF(isnull(workorder_ID), '', r.workorder) as workOrder
                    , IF(isnull(po_SupplierID), r.po_Supplier, r.sup_Supplier) as Supplier
                    , CONCAT(COALESCE(r.PromiseDate, ''), ' ', COALESCE(r.PromiseTime, '')) as PromiseDate
                    , r.*
                from (
                        select po.po_ID as poId
                        , w.workorder_ID as workorder_ID
                        , CONCAT(COALESCE(cm.customerInfo_companyName, '') , ' (', COALESCE(w.workorder_JoinID, ''), ')') as workorder
                        , sup.Supplier as sup_Supplier
                        , po.supplier_ID as po_SupplierID
                        , po.Supplier as po_Supplier
                        , IFNULL(DATE_FORMAT(po.PromiseDate, '%Y-%m-%d'), DATE_FORMAT(po.PromiseTime, '%Y-%m-%d')) as PromiseDate 
                        , IFNULL(DATE_FORMAT(po.PromiseTime, '%h:%i%p'), DATE_FORMAT(po.PromiseDate, '%h:%i%p')) as PromiseTime 
                        from
                        `po number` as po
                        left join supplier as sup on po.supplier_ID = sup.SupplierID
                        left join employeeinfo as takeBy on po.takenBy_employeeinfo_ID = takeBy.employeeinfo_ID
                        left join employeeinfo as pickBy on po.pickUpBy_employeeinfo_ID = pickBy.employeeinfo_ID
                        left join workorder as w on po.workorder_ID = w.workorder_ID
                        left join customerinfo as cm on cm.customerInfo_ID = w.customer_ID
                ) as r "
             .$condition
             .$orderBy.$limit;
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 新增po number
     */
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        //poNumber info
        $poNumberColumn = ["workorder_ID", "supplier_ID", "takenBy_employeeinfo_ID", "ShipTo"
        , "pickUpBy_employeeinfo_ID", "RequestBy", "Memo", "PromiseDate", "PromiseTime"
        , "FreightCost", "CustomClearing", "Paid", "Method", "CardNo"];
        $relateColumn = [];
        $poNumberData = $this->getNeedKeyByObject($poNumberColumn, $data);
        
        isset($poNumberData['PromiseDate'])?$poNumberData['PromiseDate'] = $data['PromiseDate']." ".date("H:i:s", strtotime($data['PromiseTime'])):"";
        isset($poNumberData['PromiseTime'])?$poNumberData['PromiseTime'] = $data['PromiseDate']." ".date("H:i:s", strtotime($data['PromiseTime'])):"";
        $poNumberData["poNumber_CreateByID"] = $creater["userinfo_ID"];

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);
        $d = $this->poNumberM->create($poNumberData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 修改po number by id
     */ 
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        //poNumber 
        $poNumberColumn = ["po_ID", "workorder_ID", "supplier_ID", "takenBy_employeeinfo_ID", "ShipTo"
        , "pickUpBy_employeeinfo_ID", "RequestBy", "Memo", "PromiseDate", "PromiseTime"
        , "FreightCost", "CustomClearing", "Paid", "Method", "CardNo"];
        $relateColumn = [];

        $poNumberData = $this->getNeedKeyByObject($poNumberColumn, $data);
        $poNumberData["workorder_ID"]=='null'?$poNumberData["workorder_ID"]=null:'';
        $poNumberData["supplier_ID"]=='null'?$poNumberData["supplier_ID"]=null:'';
        $poNumberData["pickUpBy_employeeinfo_ID"]=='null'?$poNumberData["pickUpBy_employeeinfo_ID"]=null:'';
        $poNumberData["takenBy_employeeinfo_ID"]=='null'?$poNumberData["takenBy_employeeinfo_ID"]=null:'';
        $poNumberData['PromiseDate'] = $data['PromiseDate']." ".date("H:i:s", strtotime($data['PromiseTime']));
        $poNumberData['PromiseTime'] = $data['PromiseDate']." ".date("H:i:s", strtotime($data['PromiseTime']));
        $poNumberData["poNumber_UpdateByID"] = $updater["userinfo_ID"];
        $poNumberData["poNumber_UpdateDateTime"] = 'now()';

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);

        $d = $this->poNumberM->updateById($poNumberData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得po number by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->poNumberM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }
}   
?>
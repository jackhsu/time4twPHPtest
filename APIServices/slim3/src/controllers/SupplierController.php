<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/supplier.php");

class SupplierController extends BasicController {

    private $db;
    //supplier model
    private $supplierM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("supplier", $ci);
        $this->supplierM = new Supplier($ci);
    }

    /**
     * 取得所有supplier資料(DataTable格式)
     */
    public function collection($request, $response, $args) {
        $sql = "select from supplier";
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_NUM);
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有客戶資料(select2格式)
     */
    public function toSelect2($request, $response, $args) {
        $r = array();
        $sql = " select SupplierID as id, Supplier as text from supplier ";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = 0;
        }
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有supplier資料(DataTable格式)
     */
    public function toDatatable($request, $response, $args) {    
        $params = $request->getQueryParams();
        
        //search keyWord
        $search = array();

        if(isset($params['searchKey'])) {
            $search[] = " (s.SupplierID = '".(int)$params['searchKey']."'"
                        ." or s.Supplier like '%".$params['searchKey']."%' "
                        ." or a.address_Address like '%".$params['searchKey']."%' "
                        ." or a.address_City like '%".$params['searchKey']."%' "
                        ." or a.address_Province like '%".$params['searchKey']."%' "
                        ." or s.SupplierType like '%".$params['searchKey']."%') ";
        }
        /*
        if(isset($params['advanceSearchId'])) {
            $search[] = " job.supplier_ID = '".(int)$params['advanceSearchId']."'";
        }
        if(isset($params['advanceSearchAccessId'])) {
            $search[] = " job.supplier_AccessID = '".$params['advanceSearchAccessId']."'";
        }
        */
        $condition = "";
        $condition = implode(" and ",$search);
        $condition = strlen($condition)?" where ".$condition:"";

        //select資料
        $orderColumn = array(
            " CAST(SupplierID as SIGNED) "
            , " Supplier "
            , " Address "
            , " SupplierType "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select SQL_CALC_FOUND_ROWS "
                    ." s.SupplierID as SupplierID, "
                    ." s.Supplier as Supplier, "
                    ." CONCAT( a.address_Address, ' ', COALESCE(a.address_City, ''), ' ', COALESCE(a.address_Province, '')) as address, "
                    ." s.SupplierType as SupplierType "
             ." from supplier as s"
             ." left join address as a on s.supplier_AddressID = a.address_ID"
             .$condition
             .$orderBy.$limit;
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    /**
     * 新增supplier
     */
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        //supplierinfo
        $supplierColumn = ["Supplier", "Address", "City", "Province", "PostalCode", "Phone1", "Phone2"
        , "Fax", "AccountNo", "Term", "SupplierType", "OrderContact", "Payable", "CreditLimit", "Note", "Since"];
        $relateColumn = ["supplier_Address"];
        $supplierData = $this->getNeedKeyByObject($supplierColumn, $data);
        $supplierData["supplier_CreateByID"] = $creater["userinfo_ID"];

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);
        $d = $this->supplierM->create($supplierData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 修改supplier by id
     */ 
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);
        //supplierinfo
        $supplierColumn = ["SupplierID", "Supplier", "Address", "City", "Province", "PostalCode", "Phone1", "Phone2"
        , "Fax", "AccountNo", "Term", "SupplierType", "OrderContact", "Payable", "CreditLimit", "Note", "Since"];
        $relateColumn = ["supplier_Address"];

        $supplierData = $this->getNeedKeyByObject($supplierColumn, $data);
        $supplierData["supplier_UpdateByID"] = $updater["userinfo_ID"];
        $supplierData["supplier_UpdateDateTime"] = 'now()';

        $relateData = $this->getNeedKeyByObject($relateColumn, $data);

        $d = $this->supplierM->updateById($supplierData, $relateData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得supplier by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->supplierM->getById($args["id"]);
        return $this->jsonResponse($response, $r);
    }
}   
?>
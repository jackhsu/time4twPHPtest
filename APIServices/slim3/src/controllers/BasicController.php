<?php
/**
 * Description of BasicController
 *
 * @author Jim Yu <jimbo626@gmail.com>
 */
use \Interop\Container\ContainerInterface as ContainerInterface;
require_once ( __DIR__ . "/../lib/DatabaseHelper.php");

class BasicController {

    protected $tableName = "";
    protected $ci;

    //Constructor
    public function __construct($tableName, ContainerInterface $ci) {
        $this->ci = $ci;
        $this->tableName = $tableName;
    }

    public function getTableData() {
        $r = array();
        $sql = "SELECT * FROM $this->tableName";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = 0;
        }
        return $r;
    }

    /**
     * 取得資料表資料並轉成dataTable格式
     * @return [[rowData], [rowData]...] 資料表資料
     */
    public function getTableDataToDataTable($filterData,$column='*',$table='') {
        $r = array();
        $table = empty($table)?$this->tableName:$table;
//        $order_Arr = array("Customer_id","Customer_EMail","Customer_Name","Customer_Update_Date","Customer_Create_Date","Customer_Update_Ip","operation_html");
        $order_str = " ORDER BY CAST(".((int)$filterData["order"][0]["column"]+1)." as SIGNED) ".$filterData["order"][0]["dir"];

        $search_str = isset($filterData['searchKey'])?$filterData['searchKey']:"";
        
        $sql = "SELECT SQL_CALC_FOUND_ROWS $column FROM $table"." $search_str "."$order_str LIMIT ".$filterData["start"].", ".$filterData["length"];// 
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
//            $sql = "SELECT FOUND_ROWS()";
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
//            $stmt = $this->ci->db->prepare($sql);
            if ($stmt->execute()) {
                $r["recordsTotal"] = $recordsTotal;
                $r["recordsFiltered"] = $recordsTotal;
            } else {
                $r["data"] = array();
                $r["recordsTotal"] = 0;
                $r["recordsFiltered"] = 0;
            }
        } else {
            $r["data"] = array();
            $r["recordsTotal"] = 0;
            $r["recordsFiltered"] = 0;
        }
        return $r;
    }

    /**
     * Give response, add JSON header and encode data to Json format
     *
     * @param  response
     * @param  data
     *
     * @return Response
     */
    public function jsonResponse($response, $data) {
        return $response->withHeader('Content-Type', 'application/json')
                        ->write(json_encode($data));
    }

    public function parameterErrorResponse($response) {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        return $this->jsonResponse($response, $callback);
    }

    public function checkEmpty($requestData,$value) {
        $return = true;
        if( gettype($value) === "array" ) {
            foreach ($value as $key => $value2) {
                if( !isset($requestData[$value2]) || empty($requestData[$value2]) ) {
                    return false;
                }
            }
        }
        else if( gettype($value) === "string" ) {
            if( isset($requestData[$value2]) && !empty($requestData[$value2]) ) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
        return $return;
    }

    public function checkIsset($requestData,$value) {
        $return = true;
        if( gettype($value) === "array" ) {
            foreach ($value as $key => $value2) {
                if( !isset($requestData[$value2]) ) {
                    return false;
                }
            }
        }
        else if( gettype($value) === "string" ) {
            if( isset($requestData[$value2]) ) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
        return $return;
    }
    
    /**
     * 取得登入者資料
     * @param  request
     * return object login user data
     */
    public function getLoginUser($request) {
        $authString = $request->getHeaderLine("Authorization");
        $pos = strpos($authString, "Token");
        if ( $pos === false ) {
            return false;
        }        
        $token = trim(substr($authString, $pos+5));

        $sql = "select * from authentication as a "
              ." inner join userinfo as u on a.userinfo_ID = u.userinfo_ID "
              ." where a.authentication_token = '$token'";
        $dbh = $this->ci->db;
        $stmt = $dbh->prepare($sql);
        if($stmt->execute()) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 回傳所需的key val
     * @param array $key objectKey的名稱
     * @param object $data objectVal
     * @return object data
     */
    public function getNeedKeyByObject($key, $data) {
        $r = array();
        for($i=0;$i<count($key);$i++) {
            if(isset($data[$key[$i]])) {
                $r[$key[$i]] = $data[$key[$i]];
            }
        }
        return $r;
    }
}

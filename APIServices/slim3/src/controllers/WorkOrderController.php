<?php
use \Interop\Container\ContainerInterface as ContainerInterface;

require_once ( __DIR__ . "/../models/workorder.php");
require_once ( __DIR__ . "/../models/jobsite.php");

class WorkOrderController extends BasicController
{
    private $db;
    //jobsite model
    private $jobsiteM;
    //workorder model
    private $workorderM;

    //Constructor
    public function __construct(ContainerInterface $ci) {
        parent::__construct("workorder", $ci);
        $this->workorderM = new WorkOrder($ci);
        $this->jobsiteM = new Jobsite($ci);
    } 
    
    public function create($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $creater = $this->getLoginUser($request);
        $jobsite = $this->jobsiteM->getById($data["jobsite_ID"]);

        $workorderColumn = ["workorder_JoinID", "workorder_ID", "customer_ID", "jobsite_ID", "workorder_EstStartDateTime"
                        , "workorder_EstHours", "workorder_TypeOfWork", "workorder_DayOfOperation", "workorder_MaterialSupplies"
                        , "workorder_EquipmentTools", "workorder_InternalNotes", "workorder_OperatorFromEmployeeID"
                        , "workorder_HelperFromEmployeeID", "workorder_UnitFromUnitID", "workorder_OperatorNote", "workorder_Rate"
                        , "workorder_BillingHours", "workorder_PO", "workorder_TicketID", "workorder_PaymentTerm"];
        $workorderData = $this->getNeedKeyByObject($workorderColumn, $data);
        $workorderData['workorder_EstStartDateTime'] = $data['workorder_EstStartDate']." ".date("H:i:s", strtotime($data['workorder_EstStartTime']));
        $workorderData['workorder_JoinID'] = $jobsite["jobsite_JoinID"];
        $workorderData['customer_ID'] = $jobsite["jobsite_CustomerID"];
        $workorderData['workorder_Status'] = "Pending";
        $workorderData['workorder_CreateByID'] = $creater["userinfo_ID"];
        //型態轉換
        $workorderData['workorder_BillingHours'] = floatval($workorderData['workorder_BillingHours']);
        $workorderData['workorder_DayOfOperation'] = intval($workorderData['workorder_DayOfOperation']);

        $d = $this->workorderM->create($workorderData);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }
    
    public function updateById($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);

        $workorderColumn = ["workorder_AccessID", "workorder_JoinID", "customer_ID", "jobsite_ID", "workorder_EstStartDateTime"
                        , "workorder_EstHours", "workorder_TypeOfWork", "workorder_DayOfOperation", "workorder_MaterialSupplies"
                        , "workorder_EquipmentTools", "workorder_InternalNotes", "workorder_OperatorFromEmployeeID"
                        , "workorder_HelperFromEmployeeID", "workorder_UnitFromUnitID", "workorder_OperatorNote"
                        , "workorder_StartTime", "workorder_CompleteTime", "workorder_Feedback"
                        , "workorder_Rate", "workorder_BillingHours", "workorder_PO", "workorder_TicketID", "workorder_PaymentTerm"
                        , "workorder_Status"
                        , "workorder_InvoiceID", "workorder_InvoiceDate", "workorder_Purchaser", "workorder_RequestFromPurchaser"
                        ];

        $workorderData = $this->getNeedKeyByObject($workorderColumn, $data);
        if(isset($data['workorder_EstStartDate']) && isset($data['workorder_EstStartTime'])) {
            $workorderData['workorder_EstStartDateTime'] = $data['workorder_EstStartDate']." ".date("H:i:s", strtotime($data['workorder_EstStartTime']));
        }
        if(isset($data["jobsite_ID"])) {
            $jobsite = $this->jobsiteM->getById($data["jobsite_ID"]);
            $workorderData['workorder_JoinID'] = $jobsite["jobsite_JoinID"]."W".$data["workorder_ID"];
            $workorderData['customer_ID'] = $jobsite["jobsite_CustomerID"];
        }
        //型態轉換
        if(isset($data["workorder_BillingHours"])) {
            $workorderData['workorder_BillingHours'] = floatval($workorderData['workorder_BillingHours']);
        }
        if(isset($data["workorder_DayOfOperation"])) {
            $workorderData['workorder_DayOfOperation'] = intval($workorderData['workorder_DayOfOperation']);
        }

        //設定狀態
        $workorder = $this->workorderM->getById($data["workorder_ID"]);
        /*
        //檢視dispatch: Helper 是否有設定過
        if($workorder['workorder_HelperFromEmployeeID'] != null && !isset($workorderData["workorder_HelperFromEmployeeID"])) {
            $workorderData["workorder_HelperFromEmployeeID"] = null;
        }
        */

        //punch time 觸發
        if(isset($workorderData["workorder_StartTime"])) {
            if($workorderData["workorder_StartTime"] == "") {
                $workorderData["workorder_StartTime"] = null;
            } else {
                $workorderData["workorder_StartTime"] = $workorder['Date']." ".date("H:i:s", strtotime($workorderData['workorder_StartTime']));
            }
        }
        if(isset($workorderData["workorder_CompleteTime"])) {
            if($workorderData["workorder_CompleteTime"] == "") {
                $workorderData["workorder_CompleteTime"] = null;
            } else {
                $workorderData["workorder_CompleteTime"] = $workorder['Date']." ".date("H:i:s", strtotime($workorderData['workorder_CompleteTime']));
            }
        }
        //依據資料欄位判別目前workorder應有的狀態
        if(!isset($workorderData['workorder_Status'])) {
            //Pending -> Dispatched
            if($workorder['workorder_Status'] == "Pending" && isset($workorderData["workorder_OperatorFromEmployeeID"]) && isset($workorderData["workorder_UnitFromUnitID"])) {
                $workorderData['workorder_Status'] = "Dispatched";
            }//Dispatched -> In Progress
            else if($workorder['workorder_Status'] == "Dispatched" && isset($workorderData["workorder_StartTime"])) {
                $workorderData["workorder_StartTime"] = 'now()';
                $workorderData['workorder_Status'] = "In Progress";
            }//Dispatched -> Pending
            else if($workorder['workorder_Status'] == "Dispatched" && ($workorderData["workorder_OperatorFromEmployeeID"] == null || $workorderData["workorder_UnitFromUnitID"] == null)) {
                $workorderData["workorder_StartTime"] = null;
                $workorderData["workorder_CompleteTime"] = null;
                $workorderData['workorder_Status'] = "Pending";
            }//In Progress -> Complete
            else if($workorder['workorder_Status'] == "In Progress" && isset($workorderData["workorder_CompleteTime"])) {
                $workorderData["workorder_CompleteTime"] = 'now()';
                $workorderData['workorder_Status'] = "Complete";
            }//Complete -> Invoiced
            else if($workorder['workorder_Status'] == "Complete" && isset($workorderData["workorder_InvoiceID"]) && isset($workorderData["workorder_InvoiceDate"]) && isset($workorderData["workorder_Purchaser"]) && isset($workorderData["workorder_RequestFromPurchaser"])) {
                $workorderData['workorder_Status'] = "Invoiced";
            }
        }
        $workorderData["workorder_UpdateDateTime"] = 'now()';
        if(count($workorderData) > 0) {
            $d = $this->workorderM->updateById($workorderData, $data["workorder_ID"]);
            $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
            return $this->jsonResponse($response, $r);
        } else {
            $r = array("success"=> true, "msg"=> "nothing to update", "result"=> $this->workorderM->getById($data["workorder_ID"]));
            return $this->jsonResponse($response, $r);
        }
    }

    /**
     * 取得workorder by id
     */
    public function getByID($request, $response, $args) {
        $r = $this->workorderM->getById($args["id"]);
        if(isset($r["workorder_OldData"])) {
            $r["workorder_OldData"] = str_replace( array( "\n", "\r" ), array( "<br>", "\\r" ), $r["workorder_OldData"]);
        }
        return $this->jsonResponse($response, $r);
    }

    /**
     * 取得所有訂單資料(select2格式)
     */
    public function toSelect2($request, $response, $args) {
        $r = array();
        $sql = "select 
                    w.workorder_ID as id
                    , CONCAT(COALESCE(cm.customerInfo_companyName, '') , ' (', COALESCE(w.workorder_JoinID, ''), ')') as text
                    , CONCAT(COALESCE(e.employeeinfo_FirstName, '') , ' ', COALESCE(e.employeeinfo_LastName, '')) as orderBy
                    , CONCAT(COALESCE(operator.employeeinfo_FirstName, '') , ' ', COALESCE(operator.employeeinfo_LastName, '')) as requestBy
                    , CONCAT(DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d'), ' ', DATE_FORMAT(w.workorder_EstStartDateTime, '%h:%i %p')) as orderDate
                    , unit.unit_LicensePlate as unitNo
                from workorder as w
                left join userinfo as u on w.workorder_CreateByID = u.userinfo_ID
                left join employeeinfo as operator on w.workorder_OperatorFromEmployeeID = operator.employeeinfo_ID
                left join employeeinfo as e on u.employee_ID = e.employeeinfo_ID
                left join unit as unit on w.workorder_UnitFromUnitID = unit.unit_ID
                left join customerinfo as cm on cm.customerInfo_ID = w.customer_ID
                where w.workorder_EstStartDateTime >= DATE_FORMAT(DATE_ADD(NOW(),INTERVAL -1 YEAR),'%Y-%m-01') ";
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $r = 0;
        }
        return $this->jsonResponse($response, $r);
    }

    public function toDatatable($request, $response, $args) {
        $params = $request->getQueryParams();
        
        //search keyWord
        $search = array();
        if(isset($params['searchKey'])) {
            $search[] = " (w.workorder_AccessID like '%".$params['searchKey']."%'"
                       ." or w.workorder_ID = '".(int)$params['searchKey']."'"
                       ." or w.workorder_JoinID like '%".$params['searchKey']."%' "
                       ." or c.customerInfo_companyName like '%".$params['searchKey']."%' "
                       ." or j.jobsite_Name like '%".$params['searchKey']."%' "
                       ." or w.workorder_TypeOfWork like '%".$params['searchKey']."%' "
                       ." or u.unit_LicensePlate like '%".$params['searchKey']."%' "
                       ." or w.workorder_EquipmentTools like '%".$params['searchKey']."%' "

                       ." or operator.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                       ." or operator.employeeinfo_LastName like '%".$params['searchKey']."%' "
                       ." or helper.employeeinfo_FirstName like '%".$params['searchKey']."%' "
                       ." or helper.employeeinfo_LastName like '%".$params['searchKey']."%' "
                       ." or w.workorder_Status like '%".$params['searchKey']."%' "
                       ." or w.workorder_TicketID like '%".$params['searchKey']."%') ";
        }
        if(isset($params['startDate']) && isset($params['endDate'])) {
            $search[] = " w.workorder_EstStartDateTime >= '".$params['startDate']. " 00:00:00' ";
            $search[] = " w.workorder_EstStartDateTime <= '".$params['endDate']." 23:59:59' ";
        }
        if(isset($params['advanceSearchId'])) {
            $search[] = " w.workorder_ID = '".(int)$params['advanceSearchId']."'";
        }
        if(isset($params['advanceSearchAccessId'])) {
            $search[] = " w.workorder_AccessID = '".$params['advanceSearchAccessId']."'";
        }
        $condition = "";
        $condition = implode(" and ",$search);
        $condition = strlen($condition)?" where ".$condition:"";

        //select資料
        $orderColumn = array(
            " CAST(workorder_AccessID as SIGNED) "
            , " CAST(w.workorder_ID as SIGNED) "
            , " w.workorder_EstStartDateTime "
            , " Time "
            , " customer "
            , " jobsite "
            , " TypeOfWork "
            , " Unit "
            , " w.workorder_EquipmentTools "
            , " operators "
            , " helpers "
            , " Status "
            , " Ticket "
        );
        $orderBy = " ORDER BY ".$orderColumn[$params["order"][0]["column"]].$params["order"][0]["dir"];
        $limit = " LIMIT ".$params["start"].", ".$params["length"];
        $sql = "select  SQL_CALC_FOUND_ROWS "
                    ." w.workorder_AccessID as workorder_AccessID "
                    ." , CONCAT(w.workorder_ID, ',', w.workorder_JoinID) as No "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%Y-%m-%d') as Date "
                    ." , DATE_FORMAT(w.workorder_EstStartDateTime, '%h:%i %p') as Time "
                    ." , c.customerInfo_companyName as customer "
                    ." , j.jobsite_Name as jobsite "
                    ." , w.workorder_TypeOfWork as TypeOfWork "
                    ." , u.unit_LicensePlate as Unit "
                    ." , REPLACE(w.workorder_EquipmentTools, ',', '<br>') as equipment "
                    ." , CONCAT(COALESCE(operator.employeeinfo_FirstName, '') , ' ', COALESCE(operator.employeeinfo_LastName, '')) as operators "
                    ." , CONCAT(COALESCE(helper.employeeinfo_FirstName, '') , ' ', COALESCE(helper.employeeinfo_LastName, '')) as helpers "
                    ." , w.workorder_Status as Status "
                    ." , w.workorder_TicketID as Ticket "
                ." from workorder as w "
                ." inner join customerinfo as c on c.customerInfo_ID = w.customer_ID "
                ." inner join jobsite as j on j.jobsite_ID = w.jobsite_ID "
                ." left join unit as u on u.unit_ID = w.workorder_UnitFromUnitID "
                ." left join employeeinfo as operator on operator.employeeinfo_ID = w.workorder_OperatorFromEmployeeID "
                ." left join employeeinfo as helper on helper.employeeinfo_ID = w.workorder_HelperFromEmployeeID "
                .$condition
                .$orderBy.$limit;
        $this->ci->logger->info($sql);
        $r = array();
        $stmt = $this->ci->db->prepare($sql);
        if ($stmt->execute()) {
            $r["data"] = $stmt->fetchAll(PDO::FETCH_NUM);
            $recordsTotal = $this->ci->db->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
            $r["recordsTotal"] = $recordsTotal;
            $r["recordsFiltered"] = $recordsTotal;
        }

        return $this->jsonResponse($response, $r);
    }

    public function dispatching($request, $response, $args) {
        $params = $request->getQueryParams();

        $d = $this->workorderM->dispatching($params);
        $r = $d?array_merge(array("success"=> true), $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function dispatchingUnit($request, $response, $args) {
        $params = $request->getQueryParams();

        $d = $this->workorderM->dispatchingUnit($params);
        $r = $d?array_merge(array("success"=> true), $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }

    public function cancel($request, $response, $args) {
        $data = $request->getParsedBody();
        //取得登入者的資料
        $updater = $this->getLoginUser($request);

        $cancelData["workorder_UpdateByID"] = $updater["userinfo_ID"];
        $cancelData["workorder_UpdateDateTime"] = 'now()';

        $d = $this->workorderM->cancel($cancelData, $data["workorder_ID"]);
        $r = $d?array("success"=> true, "result"=> $d):array("success"=> false, "result"=> $d);
        return $this->jsonResponse($response, $r);
    }
}   
?>
﻿<?php

use models\Dashboard\Dashboard;

$app->get('/dashboard', function () use ($app) {
	echo "dashboard";
});

$app->get('/dashboard/getById', function () use ($app) {
	echo "dashboard getById";
});

$app->post('/dashboard/add', function () use ($app) {
	echo "dashboard add";
});

$app->put('/dashboard', function () {
	echo 'dashboard PUT route';
});

$app->delete('/dashboard', function () {
	echo 'dashboard DELETE route';
});
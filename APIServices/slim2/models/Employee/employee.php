<?php
/**
 * This is an example of Employee Class using PDO
 */
namespace models\Employee;
use lib\Core;
use PDO;

class Employee {
	protected $core;
	function __construct() {
		$this->core = Core::getInstance();
	}
	
	public function insertEmployee($data) {
            
	}

	public function getEmployeeByToken($token) {
        
	}
        
	/**
	 *
	 * @retirm [object, object...] 
	 */
	public function getEmployees() {
		$r = array();		
		$sql = "SELECT * FROM employee";
		$stmt = $this->core->dbh->prepare($sql);
		//$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		if ($stmt->execute()) {
			$r = $stmt->fetchAll(PDO::FETCH_ASSOC);		   	
		} else {
			$r = 0;
		}		
		return $r;
	}
	// Update the data of an employee
	public function updateEmployee($data) {
		
	}
	// Delete employee
	public function deleteEmployee($id) {
		
	}
}

?>
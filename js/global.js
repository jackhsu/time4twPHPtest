

if ($.login == undefined)
{
    $.login = {};
    $.login.fn = {};
}


if ($.global == undefined)
{
    $.global = {};
    $.global.fn = {};
}

$.global.test = function (data) {

    // alert( 'checkauth' );

};


// other



function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
            ((expires) ? "; expires=" + expires.toGMTString() : "") +
            ((path) ? "; path=" + path : "") + //you having wrong quote here
            ((domain) ? "; domain=" + domain : "") +
            ((secure) ? "; secure" : "");
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}

function delete_cookie(name, path, domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
                ((path) ? ";path=" + path : "") +
                ((domain) ? ";domain=" + domain : "") +
                ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }
}

$.global.ajax = function (type, url, data, data2, success_back, error_back)
{
    if (error_back == "") {
        error_back = function (e) {
            console.log(e);
        };
    }
    $.ajax({
        type: type,
        dataType: 'json',
        url: url,
        async: true,
        data: data,
        data2: data2,
        success: success_back,
        error: error_back
    });
};

$.Ajaxq = function (list, type, url, data, data2, success_back, error_back)
{
    if (error_back == "") {
        error_back = function (e) {
            console.log(e);
        };
    }
    $.ajaxq(list, {
        type: type,
        url: url,
        async: true,
        data: data,
        data2: data2,
        success: success_back,
        error: error_back
    });
}

function containsAllAscii(str) {
    return  /^[\000-\177]*$/.test(str);
}

function PutBT(num) {
    // show_remind( num );
}

function getStrLength(str) {

    return str.replace(/[^\x00-\xff]/g, "xx").length;

}

function getInterceptedStr(sSource, iLen)
{
    if (sSource.replace(/[^\x00-\xff]/g, "xx").length <= iLen)
    {
        return sSource;
    }

    var str = "";
    var l = 0;
    var schar;
    for (var i = 0; schar = sSource.charAt(i); i++)
    {
        str += schar;
        l += (schar.match(/[^\x00-\xff]/) != null ? 2 : 1);
        if (l >= iLen)
        {
            break;
        }
    }

    return str;
}


function notification_cb(msg)
{
    //show_remind( "os="+msg.os+ " ,app="+msg.app+ " ,udid="+msg.udid+ " ,token="+msg.token );
    $.noti_token = msg.token;
    var data = {
        os: msg.os,
        app: msg.app,
        udid: msg.udid,
        token: msg.token
    };
    var success_back = function (data) {

        //show_remind( data );

    };
    var error_back = function (data) {
        console.log(data);
    };
    $.Ajax("GET", "php/mobile_API/insertToken.php", data, "", success_back, error_back);

}

function loading_ajax_show()
{
    $("#loading").css("z-index", "9999");
    $("body").css("overflow", "hidden");
//        $( "body" ).css( "display" , "none" );
    $("#loading").show();
}
function loading_ajax_hide()
{
    $("#loading").css("z-index", "-999");
    $("body").css("overflow", "auto");
//        $( "body" ).css( "display" , "block" );
    $("#loading").hide();
}

function show_remind(msg, state) {
    state = state || "success";
    if ($.notify) {
        if (state === "success" || state === "error") {
            $.notify(msg, {className: state, position: "middle center"});//bottom center
        } else {
            $.notify(msg, {className: "success", position: "middle center"});//bottom center
        }
    } else
    {
        alert(msg);
    }
}

function difference_now(time) {

    var callback;
    time = time.split(" ");
    var time1 = time[0].split("-");
    var time2 = time[1].split(":");
    var old_time = new Date(parseInt(time1[0]), parseInt(time1[1]) - 1, parseInt(time1[2]), parseInt(time2[0]), parseInt(time2[1]), parseInt(time2[2]));
    var now_time = new Date();
    var difference = now_time.getTime() - old_time.getTime();
    var difference_sec = parseInt(difference / 1000);
    //callback["same_m"] = ( old_time.getMonth() === now_time.getMonth() && old_time.getFullYear() === now_time.getFullYear() ) ? true : old_time.getFullYear() + "-" + ( old_time.getMonth() + 1 );
    if (difference_sec < 60) {
        callback = difference_sec + " 秒前";
    } else if (difference_sec < 60 * 60) {
        callback = parseInt(difference_sec / 60) + " 分前";
    } else if (difference_sec < 24 * 60 * 60) {
        callback = parseInt(difference_sec / 60 / 60) + " 小時前";
    } else if (difference_sec < 30 * 24 * 60 * 60) {
        callback = parseInt(difference_sec / 60 / 60 / 24) + " 天前";
    } else if (difference_sec < 12 * 30 * 24 * 60 * 60) {
        callback = parseInt(difference_sec / 30 / 60 / 60 / 24) + " 個月前";
    } else {
        callback = parseInt(difference_sec / 12 / 30 / 60 / 60 / 24) + " 年前";
    }
    return callback;
}

function create_page(value) {

    var p_tag = value.p_tag[0] ? "#" + value.p_tag[0] : "";
    var title = getStrLength(value.p_title) <= 70 ? value.p_title : getInterceptedStr(value.p_title, 70 - 3) + "...";
    return '<li class="showing">' +
            '        <dl>' +
            '                <dt>' +
            '                        <div class="category" cate="' + value.p_category_id + '">' + value.cate_name + '</div>' +
            '                        <a href="v_article_info.php?p=' + value.page_id + '">' +
            '                                <div class="res_div">' +
            '                                        <div style="background-image:url(\'' + value.p_icon + '\')"></div>' +
            '                                </div>' +
            '                        </a></dt>' +
            '                <dd>' +
            '                        <h5><a href="v_article_info.php?p=' + value.page_id + '" title="' + value.p_title + '">' + title + '</a></h5>' +
            '                        <p><b style="opacity: 1;">觀看次數 ' + value.p_click_num + ' / ' + difference_now(value.p_date) + '</b><span style="display: none; opacity: 1;">' + value.p_date + '</span></p>' +
            '                        <nav ch="' + value.ch_id + '">' + value.ch_name + '</nav>' +
            '                        <div class="list-sub">' + p_tag + '</div>' +
            '                        <div class="fb" style="display: none; opacity: 1;"><a href="v_article_info.php"><img alt="" src="template/images/fb.png"></a></div>' +
            '                </dd>' +
            '        </dl>' +
            '</li>';

}

function create_new_page(value) {

    //var p_tag = value.p_tag[0] ? "#"+value.p_tag[0] : "" ;
    var title = getStrLength(value.p_title) <= 70 ? value.p_title : getInterceptedStr(value.p_title, 70 - 3) + "...";
    return '<li class="showing">' +
            '        <dl>' +
            '                <dt>' +
            '                        <div class="category" cate="' + value.cate_id + '">' + value.cate_name + '</div>' +
            '                        <a href="v_article_info.php?p=' + value.page_id + '">' +
            '                                <div class="res_div">' +
            '                                        <div style="background-image:url(\'' + value.p_icon + '\')"></div>' +
            '                                </div>' +
            '                        </a></dt>' +
            '                <dd>' +
            '                        <h5><a href="v_article_info.php?p=' + value.page_id + '" title="' + value.p_title + '">' + title + '</a></h5>' +
            '                        <p><b style="opacity: 1;">觀看次數 ' + value.p_click_num + ' / ' + difference_now(value.p_date) + '</b><span style="display: none; opacity: 1;">' + value.p_date + '</span></p>' +
            '                        <nav ch="' + value.ch_id + '">' + value.ch_name + '</nav>' +
            '                        <div class="list-sub" cate="' + value.sub_cate_id + '">' + value.sub_cate_name + '</div>' +
            '                        <div class="fb" style="display: none; opacity: 1;"><a href="v_article_info.php"><img alt="" src="template/images/fb.png"></a></div>' +
            '                </dd>' +
            '        </dl>' +
            '</li>';

}

function clear_input() {

    $("input:not([type=button])").val("");
    $("textarea").val("");

}


function scrollto(pos, time) {
    time = time || 1000;
    $('html, body').animate({
        scrollTop: pos.offset().top - $(window).height() / 2
    }, time);
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function GetDecimals(num, decimals) {
    //無條件捨去
    var tmp = Math.pow(10, decimals);
    return Math.floor(parseFloat(num) * tmp) / tmp;
}

function GetReadNumber(num) {

    var _tmp;
    var tmp = Math.floor(parseFloat(num));
    var length = String(tmp).length;
    if (length >= 7) {
        _tmp = length - 7 > 3 ? 3 : length - 7;
        return Math.floor(tmp / Math.pow(10, 3 + _tmp)) / Math.pow(10, 3 - _tmp) + "M";
    } else if (length >= 4) {
        _tmp = length - 4 > 3 ? 3 : length - 4;
        return Math.floor(tmp / Math.pow(10, 0 + _tmp)) / Math.pow(10, 3 - _tmp) + "K";
    } else {
        return GetDecimals(num, 1);
    }
}

function NumConverteChinese(num) {

    if (typeof num === "number") {
        String(num).length;
    }
    return false;
}

function NumGetDigit(num) {


}

function YearFunction(sel)
{
    var tmp = "";
    if ($("[type=month]").val() === "02")
    {
        if (parseInt($(sel).val()) % 4 === 0)
            var days = 29;
        else
            var days = 28;
        for (var i = 1; i <= days; i++)
        {
            if (i < 10)
                tmp += '<option value=0' + i + '>' + i + '日</option>';
            else
                tmp += '<option value=' + i + '>' + i + '日</option>';
        }
        $("[type=day]").html(tmp);
    }
}
function MonthFunction(sel)
{
    var tmp = "";
    switch ($(sel).val())
    {
        case "01":
        case "03":
        case "05":
        case "07":
        case "08":
        case "10":
        case "12":
            var days = 31;
            break;
        case "04":
        case "06":
        case "09":
        case "11":
            var days = 30;
            break;
        case "02":
            if (parseInt($("[type=year]").val()) % 4 === 0)
                var days = 29;
            else
                var days = 28;
            break;
    }
    for (var i = 1; i <= days; i++)
    {
        if (i < 10)
            tmp += '<option value=0' + i + '>' + i + '日</option>';
        else
            tmp += '<option value=' + i + '>' + i + '日</option>';
    }
    $("[type=day]").html(tmp);
}

/*
 例如:你要在45前面補到字傳長度為5的字串，你就要呼叫paddingLeft( 45 , 5 )
 得到的結果就會是00045
 */
function paddingLeft(str, lenght) {
    if (str != undefined)
    {
        if (typeof str == "number")
            str = str.toString();

        if (str.length >= lenght)
            return str;
        else
            return paddingLeft("0" + str, lenght);
    } else
    {
        return "";
    }
}
function paddingRight(str, lenght) {

    if (typeof str == "number")
        str = str.toString();

    if (str.length >= lenght)
        return str;
    else
        return paddingRight(str + "0", lenght);
}



$("document").ready(function () {

    //$("form").submit(false);
    /*
     //disable select, onmousedown部分功能
     //參考網址: https://sites.google.com/site/ocianacafe/
     
     //form tags to omit in NS6+:
     var omitformtags=["input", "textarea", "select"];
     function disableselect(e){
     if ($.inArray(e.target.tagName.toLowerCase(),omitformtags)==-1)
     {
     return false
     }
     }
     function reEnable(){
     return true
     }
     if (typeof document.onselectstart!="undefined")
     document.onselectstart=new Function ("return false")
     else{
     document.onmousedown=disableselect
     document.onmouseup=reEnable
     }
     */
});


/** 數字前面補0
 * @param string/int str 個數  
 * @param int cnt 個數  
 */
$.addZero = function (str, cnt) {
    var length = str.length;
    for (var i = length; i < cnt; i++) {
        str = "0" + str;
    }
    return str;
};

/** 數字每三位數加","
 * @param string/int str 個數  
 */
$.strToMoneyNumber = function (str) {
    str = str.toString().split(".");
    var val = str[0].replace(/\,/g, "").split("").reverse();
    var money = "";
    for (var i = 0; i < val.length; i++) {
        if ((i) % 3 == 0 && i != 0) {
            money = "," + money;
        }
        money = val[i] + money;
    }
    if (str[1] != undefined) {
        money += "." + str[1];
    }
    return money;
};

/** 檢查input是否為空
 * @param object valid {
 bool: true無錯誤/false有錯誤
 ,msg: string錯誤訊息
 }
 * @param array id jquery select string
 * @param string str replace String
 * @return valid
 */
$.isValidRequiredField = function (valid, requiredField, str) {
    var tag = {}, errorMsg = "";
    for (var i = 0; i < requiredField.length; i++) {
        tag = $(requiredField[i]);
        if (["INPUT", "TEXTAREA", "SELECT"].indexOf(tag[0].tagName) != -1) {
            if (tag.val() === "" || tag.val() === null) {
                tag.closest(".form-group").addClass("has-error").removeClass("has-success");
                valid["bool"] = false;
                if (valid["msg"] === "") {
                    errorMsg = "Please input " + requiredField[i].replace(str, "");
                } else {
                    errorMsg = "、input " + requiredField[i].replace(str, "");
                }
                valid["msg"] += errorMsg;
            }
        } else {
            if (tag.html() == "" || tag.data("data") == undefined) {
                tag.closest(".form-group").addClass("has-error").removeClass("has-success");
                valid["bool"] = false;
                if (valid["msg"] === "") {
                    errorMsg = "Please input " + requiredField[i].replace(str, "");
                } else {
                    errorMsg = "、input " + requiredField[i].replace(str, "");
                }
                valid["msg"] += errorMsg;
            }
        }
    }

    return valid;
};

/** 檢查email格式
 * @param object valid {
 bool: true無錯誤/false有錯誤
 ,msg: string錯誤訊息
 }
 * @param array id jquery select string
 * @param string str replace String
 * 參考網址:http://stackoverflow.com/questions/2855865/jquery-regex-validation-of-e-mail-address
 * @return valid
 */
$.isValidEmailAddress = function (valid, requiredField, str) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var input = {}, errorMsg = "";
    for (var i = 0; i < requiredField.length; i++) {
        input = $(requiredField[i]);
        if (input.val() != "" && !pattern.test(input.val())) {
            input.closest(".form-group").addClass("has-error").removeClass("has-success");
            valid["bool"] = false;
            if (valid["msg"] === "") {
                errorMsg = "Malformed email addresses";
            } else {
                errorMsg = "、Malformed email addresses";
            }
            valid["msg"] += errorMsg;
        }
    }
    return valid;
};

/** 限制input只能輸入數字
 * 限制input輸入長度
 * @param string id jquery select string
 * @param int length 輸入的長度
 * 參考網址:http://stackoverflow.com/questions/995183/how-to-allow-only-numeric-0-9-in-html-inputbox-using-jquery
 */
$.inputOnlyNumber = function (id, length) {
    $(id).keydown(function (e) {
        var val = $(this).val();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right, down, up
                                (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
                if (length && val.length > length) {
                    e.preventDefault();
                }
            });
}

/** 輸入電話號碼時input自動補"-"符號
 * @param string id jquery select string
 */
$.inputKeyTurnPhoneNumber = function (id) {
    $(id).keyup(function (e) {
        var val = $(this).val().replace(/\-/g, "").split("").reverse();
        var number = "";
        for (var i = 0; i < val.length; i++) {
            if (i == 4 || i == 7) {
                number = "-" + number;
            }
            number = val[i] + number;
        }
        $(this).val(number);
    });
}

/** 輸入數字時input自動補","符號
 * @param string id jquery select string
 */
$.inputKeyTurnMoneyNumber = function (id) {
    $(id).keyup(function (e) {
        var val = $(this).val()
        val = $.strToMoneyNumber(val);
        $(this).val(val);
    });
};


//浮點數相加
function FloatAdd(arg1, arg2)
{
    var r1, r2, m;
    try {
        r1 = arg1.toString().split(".")[1].length;
    } catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    } catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return (FloatMul(arg1, m) + FloatMul(arg2, m)) / m;
}
//浮點數相減
function FloatSubtraction(arg1, arg2)
{
    var r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = arg2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2));
    n = (r1 >= r2) ? r1 : r2;
    return ((arg1 * m - arg2 * m) / m).toFixed(n);
}
//浮點數相乘
function FloatMul(arg1, arg2)
{
    var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
    try {
        m += s1.split(".")[1].length;
    } catch (e) {
    }
    try {
        m += s2.split(".")[1].length;
    } catch (e) {
    }
    return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m);
}
//浮點數相除
function FloatDiv(arg1, arg2)
{
    var t1 = 0, t2 = 0, r1, r2;
    try {
        t1 = arg1.toString().split(".")[1].length
    } catch (e) {
    }
    try {
        t2 = arg2.toString().split(".")[1].length
    } catch (e) {
    }
    with (Math)
    {
        r1 = Number(arg1.toString().replace(".", ""))
        r2 = Number(arg2.toString().replace(".", ""))
        return (r1 / r2) * pow(10, t2 - t1);
    }
}

// 萬位
function Num4Zero(value)
{
    value = value.replace(/萬/g, "");
    var tmp_length = value.split(".")[0].length;
    if (tmp_length > 4)
    {
        if (value.substr(tmp_length - 4) == "0000")
        {
            value = value.substr(0, tmp_length - 4) + "萬";
        } else {
            value = value.substr(0, tmp_length - 4) + "萬" + parseFloat(value.substr(tmp_length - 4)).toString();
        }
    }
    return value;
}

// 萬位
function Num4Zero2(value)
{
    value = value.replace(/萬/g, "");
    var tmp_length = value.split(".")[0].length;
    if (tmp_length > 4)
    {
        if (value.substr(tmp_length - 4) == "0000")
        {
            return value.substr(0, tmp_length - 4) + ",";
        } else {
            return value.substr(0, tmp_length - 4) + "," + parseFloat(value.substr(tmp_length - 4)).toString();
        }
    }
    return "," + value;
}


// 萬位
function Num4Zero1(value)
{
    var tmp_value_0 = value.split(".")[0];
    var tmp_length_0 = tmp_value_0.length;
    var tmp_value_1 = value.split(".")[1];
    var tmp_length_1 = tmp_value_1 == undefined ? 0 : tmp_value_1.length;

    if (tmp_length_0 > 4)
        if (tmp_length_1 == 0)
        {
            value = '<label class="number">' + value.substr(0, tmp_length_0 - 4) + '</label><label class="body_gray">萬</label><label class="number">' + value.substr(tmp_length_0 - 4) + '</label>';
        } else {
            value = '<label class="number">' + value.substr(0, tmp_length_0 - 4) + '</label><label class="body_gray">萬</label><label class="number">' + value.substr(tmp_length_0 - 4) + '</label>';
        }
    return value;
}

// 1050710 -> 105/07/01

function Date4Type1(value)
{

    if (value.length == 7)
    {
        value = value.substr(0, 3) + '/' + value.substr(3, 2) + '/' + value.substr(5, 2);
    }

    return value;
}


function getImgSize(imgSrc)
{
    var newImg = new Image();
    newImg.src = imgSrc;
    var height = newImg.height;
    var width = newImg.width;
    show_remind('The image size is ' + width + 'X' + height);

}
//產生Guid
//function NewGuid() {
//    function s4() {
//        return Math.floor((1 + Math.random()) * 0x10000)
//                .toString(16)
//                .substring(1);
//    }
//    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
//            s4() + '-' + s4() + s4() + s4();
//}
function NewGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
function NewFormatDate() {

    var date = new Date();
    return (date.getFullYear().toString() + $.addZero((date.getMonth() + 1).toString(), 2) + $.addZero(date.getDate().toString(), 2));

}
function NewFormatTime() {

    var date = new Date();
    return ($.addZero(date.getHours().toString(), 2) + $.addZero(date.getMinutes().toString(), 2) + $.addZero(date.getSeconds().toString(), 2));

}


function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function load_html(tmp_queue, tmp_url, data, data2, success_back, error_back)
{
    if (data2.focus)
    {
        $(data2.focus).hide();
        loading_ajax_show();
    }

    data2.cb_focus = function (data)
    {
        var tmp_focus = $(this.focus);
        tmp_focus.show();

        setTimeout(function () {
            loading_ajax_hide();
        }, 2000);
    };

    if (tmp_url === "html/footer.php") {
        var success_back2 = function (data)
        {
            success_back(data);
            if (getCookie("aroflyTranslate")) {
                $("#language").val(getCookie("aroflyTranslate")).trigger("change");
            }
        };
    }
//                        else if( tmp_url === "html/header.php" ){// || tmp_url === "html/header_index.php" || tmp_url === "html/mgm_header.php"
////                            var success_back2 = success_back + function(data)
////                            {
////                                if($.login.userInfo.userinfo_avator){
////                                    $("#emptyAvator").hide();
////                                    $("#imgHeaderAvator").attr("src","http://"+location.host+"/arofly/storage/account/"+$.login.userInfo.userinfo_ID+"/"+$.login.userInfo.userinfo_avator).show();
////                                }
////                            };
//                            success_back = function (data){
//                                
//                                success_back2(data);
//                                if($.login.userInfo.userinfo_avator){
//                                    $("#emptyAvator").hide();
//                                    $("#imgHeaderAvator").attr("src","http://"+location.host+"/arofly/storage/account/"+$.login.userInfo.userinfo_ID+"/"+$.login.userInfo.userinfo_avator).show();
//                                }
//                            }
//                        }
    else {
        var success_back2 = success_back;
    }

    $.Ajaxq(tmp_queue, "POST", tmp_url, data, data2, success_back2, error_back);
}




function js_initail()
{
                                
            $('.background-image-holder').each(function() {
                $(this).addClass('fadeIn');
            });

            //settimeout show img
            setTimeout(function () {
                console.log('ReFresh masonry');
                $('.masonry').masonry();
            }, 5000);
    
}
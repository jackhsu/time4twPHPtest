<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Search Results | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?>
        <?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <div class="nav-container">

        <?php include_once("incl/nav.php"); ?>

        </div>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="result">

        <section class="bg-primary">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold italic">
                            SEARCH RESULTS</h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8">
                        <h2 >SEARCH RESULTS FOR: {{search}}</h2>
                    </div>
                </div>
            </div>
        </section>

        <!-- Deals -->
        <section class="main-pad ">
            <div class="container pb88">
                <div class="columns" v-for="result in searched_table_data">
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a v-bind:href="result.href" target="_blank">
                                <img class="bookHover" alt="Pic" v-bind:src="imgPath + result.image2">
                                <div class="bookNow fixedH" >
                                    <h5 class="bookNowTitle" v-html="result.title2"></h5>
                                    <p class="bookNowText" v-html="result.description"></p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!--<div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/taiwan-depth/" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-2.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Taiwan in Depth</h5><p class="bookNowText">During this 8 day tour of Taiwan, you will see Taipei, Sun Moon Lake, Kaohsiung and the Spring and Autumn Pavilion, and the Eternal Spring Shrine inside Taroko National Park.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/taste-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-3.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Taste of Taiwan</h5><p class="bookNowText">On this 7-day Cuisine tour of Taiwan, You will take a cooking class and sample foods at a famous Taiwanese night market. You will also ride Taiwan's high-speed bullet train to Sun Moon Lake, Taiwan’s largest body of water.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/zjsthU" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-4.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">8 Days Taiwan Island Classic Tour</h5><p class="bookNowText">Start this 8-day tour in Taipei at the National Palace Museum and one of Taiwan's famous night markets! On this tour, you will visit Sun Moon Lake, Alishan National Scenic Area, lugang Town, and have extra free time in Taipei!</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.supervaluetours.com/type_tourtype.do?tid=50&fr=supusa" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-5.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Taiwan Island Tour 10 days</h5><p class="bookNowText">This island tour will feature the best of Taiwan! From soup dumplings to the largest collection of Chinese artifacts in Taipei to contemplating a rose-gold sunset at Sun Moon Lake, Taiwan is sure to surprise with this tour!</p>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw8" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-6.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">8 Days Enchanting Taiwan Tour</h5><p class="bookNowText">On this 8-day tour around Taiwan, you will be visiting Taipei, Sun Moon Lake, Fo Guang Shan Buddhist Monastery, Kaohsiung, Kenting National Park, East Coast & Taroko National Park.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw5" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-7.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">5 Days Around Taiwan Tour</h5><p class="bookNowText">During this 5-day tour, you will be visiting Sun Moon Lake, Kaohsiung, Kenting, Taitung, Hualien, and the Taroko Gorge.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.intrepidtravel.com/us/taiwan/explore-taiwan-100826" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-8.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">10% OFF Intrepid Travel's Explore Taiwan tour</h5><p class="bookNowText">This small group tour will see Taiwan's exciting food culture, hike, ride and kayak around Sun Moon Lake and visit both the Qingshui Cliffs and Taroko Gorge. Book until August 20th with promo code "50146" for 10% off your booking.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/LXzALk" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-9.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Vancouver to Hong Kong, plus 3-day stopover in Taipei</h5></h6><p class="bookNowText">This direct flight from Vancouver to Hong Kong includes an optional 3-day stopover in Taipei! Enjoy 3 nights stay in Taipei with breakfast included. Subway cards for Taipei and Hong Kong will also be provided.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.bicycleadventures.com/destinations/taiwan-bike-tours/Taiwan-Bike-Tour" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-10.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Taiwan Bicycle Tour with Silks Spa Experience</h5></h6><p class="bookNowText">This 11-day bike tour offers days of riding through Taiwan's sparsely populated south and east, filled with national parks, rice paddies, palm trees, aboriginal villages, colorful temples and incredibly friendly people.</p>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://campbelltravel.bc.ca/en/" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-11.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Taipei Free and Easy </h5></h6><p class="bookNowText">3 nights stopover in Taipei with breakfast, include round trip air to one of the following destinations: Hong Kong, Seoul, Osaka or Nagoya. </p>
                                </div>
                            </a>
                        </div>
                    </div>                     
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.panagoetour.com/colourful-north-date-and-price" target="_blank">
                                <img class="bookHover" alt="Pic" src="img/deals-12.jpg">
                                <div class="bookNow fixedH">

                                    <h5 class="bookNowTitle">Colorful North</h5></h6><p class="bookNowText">This 4-day package tours Northern Taiwan and features extinct volcanoes, hot springs, and extraordinary rock formations along the coast! Explore the nature of Taiwan on this short but exciting trip!</p>
                                </div>
                            </a>
                        </div>
                    </div>-->

                </div>
            </div>
        </section><!-- end of Deals -->

        <?php include_once("incl/footer.php"); ?>

    </div>

    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        
        $(document).ready(function() {

            var vm = new Vue({
                el: "#result",
                data: {
                    imgPath: $.config.imgPath,
                    detail: [],
                    search: "",
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.detail));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {

                    $("#search").unbind("magic").bind("magic", function (event) {

                        if (getParameterByName("search") != "")
                        {
                            $('input[name="text1"]').val(getParameterByName("search"));
                        }

                        var tmp_href =
                                [
                                    ["things-to-do.php"],
                                    ["culture-and-heritage.php", "indigenous-culture.php", "temples.php", "cuisine.php", "outdoor.php", "wellness.php", "ecotourism.php", "festivals.php"],
                                    ["museums.php", "national-taiwan-museum-of-fine-arts.php", "national-museum-of-taiwan-literature.php", "national-palace-museum.php",
                                        "lanyu-island.php", "east-coast.php", "museums-for-indigenous-art-and-history.php",
                                        "baoan-temple.php", "lugang-longshan-temple.php", "tainan-confucius-temple.php", "penghu-tianhou-temple.php",
                                        "taiwan-specialty.php", "night-market.php", "fine-dining.php",
                                        "cycling.php", "water-sports.php", "hiking.php",
                                        "spa.php", "hot-spring.php",
                                        "butterfly-watching.php", "bird-watching.php",
                                        "mazu-pilgrimage.php", "wangye-boats.php", "yanshui-festival.php", "outdoor-music.php"],
                                    ["before-you-go.php", "getting-there.php", "getting-around.php", "visas.php", "language.php"]
                                ];


                        var tmp_search = $('input[name="text1"]').val();
                        var ajax1 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do'}});
                        var ajax2 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level1'}});
                        var ajax3 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level2'}});
                        var ajax4 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'before_you_go'}});
                        $.when(ajax1, ajax2, ajax3, ajax4).then(function (res1, res2, res3, res4) {
                            //console.log(res1);
                            var res = [res1, res2, res3, res4];
                            console.log(res);
                            var tmp_arr = [];

                            $.each(res, function (k, v) {

                                var tmp_res = k;
                                $.each(JSON.parse(v[0]).data, function (k, v) {

                                    if (v.title.search(tmp_search) != -1 || v.description.search(tmp_search) != -1)
                                    {
                                        v.href = tmp_href[ tmp_res ][ v.id - 1 ];
                                        if (v.image2 == "")
                                            v.image2 = v.image;
                                        tmp_arr[ tmp_arr.length ] = v;
                                    }
                                });
                            });

                            vm.detail = tmp_arr;
                            console.log(vm.detail);
                            vm.imgPath = vm.imgPath;
                            vm.search = tmp_search;
                        }).fail(function () {
                            alert("fail");
                        });
                    });

                    $("#search").trigger("magic");
                }
            });
        
        });
//        window.addEventListener("keydown", function (event) {
//            console.log(event.keyCode);
//            if (event.keyCode == 13)
//            {
//                $("#search").trigger("magic");
//                // Suppress "double action" if event handled
//                event.preventDefault();
//            }
//
//        }, true);

    </script>
</body>

</html>
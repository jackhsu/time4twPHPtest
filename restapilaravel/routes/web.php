<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;

Route::middleware(['cors'])->group(function () {
    
        //Restful API
        Route::get('service/api/clearProjectChatroom', ['middleware' => 'checkAuth', 'uses' => 'APIController@clearProjectChatroom']);


        Route::post('service/users/login', ['middleware' => 'checkBaseAuth', 'uses' => 'UsersController@nothing']);
        Route::post('service/users/register', ['uses' => 'UsersController@register']);
        Route::post('service/users/usersColumnList', ['uses' => 'UsersController@usersColumnList']); // Vincent
        Route::post('service/users/getUserinfoValue', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getUserinfoValue']);
        Route::post('service/users/getUserinfoByID', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getUserinfoByID']);
        Route::post('service/users/getGlobalValue', ['uses' => 'UsersController@getGlobalValue']);      // Vincent
        Route::post('service/users/setGlobalValue', ['uses' => 'UsersController@setGlobalValue']);      // Vincent
        Route::post('service/users/listGlobalValue', ['uses' => 'UsersController@listGlobalValue']);    // Vincent
        Route::post('service/users/removeGlobalValue', ['uses' => 'UsersController@removeGlobalValue']);  //Jerry  
        Route::post('service/users/userLogout', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@userLogout']);
        Route::get('service/users/checkToken', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@checkToken']);
        //, function(Request $request){
        //        
        //        echo json_encode(array("success"=>true,"data"=>$request["token_info"][0]));
        ////        exit;  
        //}]);
        Route::post('service/users/getPermissions', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getPermissions']);
        Route::post('service/users/editMyName', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@editMyName']);
        Route::post('service/users/editMyEmail', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@editMyEmail']);
        Route::post('service/users/editMyPassword', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@editMyPassword']);
        Route::post('service/users/reSendEmailForAuth', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@reSendEmailForAuth']);
        Route::post('service/users/getUsersTableColumn', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getUsersTableColumn']);
        Route::post('service/users/usersList', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@usersList']);
        Route::post('service/users/addMember', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@addMember']);
        Route::post('service/users/usersColumnListByOrder', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@usersColumnListByOrder']);
        Route::post('service/users/updateUsersColumnOrder', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@updateUsersColumnOrder']);
        Route::post('service/users/editUsersColumnInfo', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@editUsersColumnInfo']);
        Route::post('service/users/getUsersListAndColumn', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getUsersListAndColumn']);
        Route::post('service/users/searchUserData', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@searchUserData']);
        Route::post('service/users/searchUserDataDatatableFormat', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@searchUserDataDatatableFormat']);
        Route::post('service/users/setUserinfoValue', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@setUserinfoValue']);

        Route::post('service/users/getBusinessList', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getBusinessList']);
        Route::post('service/users/editUsersBusinessName', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@editUsersBusinessName']);
        Route::post('service/users/addBusiness', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@addBusiness']);

        Route::post('service/users/forgotPassword', ['uses' => 'UsersController@forgotPassword']);
        Route::post('service/users/checkForgotToken', ['uses' => 'UsersController@checkForgotToken']);
        Route::post('service/users/editPasswordByForgotToken', ['uses' => 'UsersController@editPasswordByForgotToken']);

        Route::post('service/users/gerCityDistrict', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@gerCityDistrict']); //justin 20170608
        Route::post('service/users/uploadImg', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@uploadImg']); //justin 20170609
        Route::post('service/users/releaseImg', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@releaseImg']); //justin 20170609
        Route::post('service/users/removeTmpImg', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@removeTmpImg']); //justin 20170610
        Route::post('service/users/getUserinfoImage', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@getUserinfoImage']);//justin 20170611

        Route::post('service/users/setCollection', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@setCollection']);  //Jerry 20170703
        Route::post('service/users/delCollection', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@delCollection']);  //Jerry 20170703
        Route::post('service/users/delHistory', ['middleware' => 'checkAuth' , 'uses' => 'UsersController@delHistory']);  //Jerry 20170704


        //chat API
        Route::get('service/chat/getAllBorrowerByInvestor', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getAllBorrowerByInvestor']);
        Route::get('service/chat/getInvestorByBorrower', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getInvestorByBorrower']);
        Route::get('service/chat/getChatroomID', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getChatroomID']);
        Route::get('service/chat/getTalkContent', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getTalkContent']);
        Route::get('service/chat/getTalkContentWithCustomerService', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getTalkContentWithCustomerService']);
        Route::post('service/chat/sendMessagetToRoom', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@sendMessagetToRoom']);
        Route::post('service/chat/sendFile', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@sendFile']);
        Route::post('service/chat/borrowerResChat', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@borrowerResChat']);
        Route::post('service/chat/investorResChat', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@investorResChat']);
        Route::get('service/chat/getMyChatCommand', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getMyChatCommand']);
        Route::get('service/chat/returnAPIState', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@returnAPIState']);
        Route::post('service/chat/pushAPI', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@pushAPI']);
        Route::get('service/chat/getBorrowInfoByBorrowIdFromInvestor', ['middleware' => 'checkAuth' , 'uses' => 'ChatController@getBorrowInfoByBorrowIdFromInvestor']);


        //transaction API
        Route::post('service/transaction/buyPoint', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@buyPoint']);
        Route::post('service/transaction/buyAdPublish', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@buyAdPublish']);
        Route::post('service/transaction/sendMoneyAsk', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@sendMoneyAsk']);
        Route::post('service/transaction/sendTransactionRes', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@sendTransactionRes']);

        Route::post('service/transaction/borrowNewBid', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@borrowNewBid']);
        Route::post('service/transaction/borrowbidList', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@borrowbidList']);
        Route::post('service/transaction/borrowbidListVersion2', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@borrowbidListVersion2']);
        Route::post('service/transaction/bidListbyBorrowId', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@bidListbyBorrowId']);
        Route::post('service/transaction/InvestorBid', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@InvestorBid']);
        Route::get('service/transaction/scheduleChangeStatus', ['uses' => 'TransactionController@scheduleChangeStatus']);

        Route::post('service/transaction/getAllFinishByInvestor', ['middleware' => 'checkAuth' , 'uses' => 'TransactionController@getAllFinishByInvestor']);

        //Companys API
        Route::get('service/company/getAllMyAccountListByCompany', ['middleware' => 'checkAuth' , 'uses' => 'CompanysController@getAllMyAccountListByCompany']);
        Route::post('service/company/turnOutPoint', ['middleware' => 'checkAuth' , 'uses' => 'CompanysController@turnOutPoint']);

        
});

        Route::post('service/upload/tempFile', ['uses' => 'UploadController@tempFile']);
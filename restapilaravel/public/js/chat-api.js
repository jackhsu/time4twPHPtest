
function chatroomHtmlEvent(){
        
        //talk
        var $talk = $('#talk'),
                $talkToggle = $talk.find('.toggle'),
                $talkWindow = $talk.find('.window'),
                $talkClose = $talk.find('.close');
        
        $talkWindow.css('bottom',$talkWindow.height() * -1);

        $talkToggle.click(function(e) {
                $talkWindow.animate({bottom:0},300);
                $("#talk > a").removeClass("new-msg");
        });

        $talkClose.click(function(e) {
                $talkWindow.animate({bottom:$talkWindow.height() * -1},300);
        });

        // 側欄下拉
        $('.top_btn').click(function() {
            $(this).toggleClass('active').siblings('.top').slideToggle();
        });
        $('.bot_btn').click(function() {
            $(this).toggleClass('active').siblings('.bot').slideToggle();
        });
        $('.bid_btn').click(function() {
            $(this).toggleClass('active').siblings('.bid').slideToggle();
        });
        $('.agree_btn').click(function() {
            $(this).toggleClass('active').siblings('.agree').slideToggle();
        });     
        $('.ask_btn').click(function() {
            $(this).toggleClass('active').siblings('.ask').slideToggle();
        });   
        $('.all_btn').click(function() {
            $(this).toggleClass('active').siblings('.all').slideToggle();
        });      

        // 問候語
        $('#textListBtn').unbind('click').bind('click',function(){
            $('#textList').toggle();
        });           
        // 貼圖
        $('#illustrationListBtn').unbind('click').bind('click',function(){
            $('#illustrationList').toggle();
        });


        $('#ModalBtn').click(function() {
            $('#Modal').modal();
        });

        $('#ChatEdit input[target-edit],#btnChatEditComplete, #btnChatEditCancel,#btnChatEditAdd').hide();


        //切換成編輯模式
        $('#btnChatEdit').unbind('click').bind('click',function(){

            $('#ChatEdit p[target-view],#btnChatEdit').hide();
            $('#ChatEdit input[target-edit], #btnChatEditComplete, #btnChatEditCancel').show();

        });
        
}

function getChatroomID(Investor,Borrower){
        console.log("getChatroomIDgetChatroomIDgetChatroomIDgetChatroomIDgetChatroomIDgetChatroomID")
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getChatroomID",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    Investor : Investor ,
                    Borrower : Borrower
                },
                success: function(data){
//                    console.log(data);
                    data = JSON.parse(data);
//                    console.log(data);
                    if( data.success ) 
                    {
//                        if($.switchPolling&&$.pollingPara["Investor"]===Investor&&$.pollingPara["Borrower"]===Borrower){
                            
                            if( $.member.userinfo_InExUser==="Borrower" ){
                                $("#btnGoToBid").hide();
                                $("#btnBorrowInfo").hide();
                            }
                            else if( $.member.userinfo_InExUser==="Investor" && data.data["cr_status"]===3 ){
                                $("#btnGoToBid").show();
                                $("#btnBorrowInfo").show().attr("borrowID",Borrower);
                            }
                            else if( $.member.userinfo_InExUser==="Investor" ){
                                $("#btnGoToBid").show();
                                $("#btnBorrowInfo").hide();
                            }
                            else{
                                $("#btnGoToBid").hide();
                                $("#btnBorrowInfo").hide();
                            }
                            
                            if( data.data["cr_status"]===0&&$.member["userinfo_InExUser"]==="Investor" ){
                                    $("#inputMsg").attr("disabled",true).attr("placeholder","同意後才可開始聊天").val("");
                                    var html = '<div>\n\
                                                <p id="LabelProfileAccount" class="form-control-static" target-view="">如果想與此借款人聊天，請前往此借方競標。</p>\n\
                                            </div>';
                                    $("#talk .msg").html(html);
                            }
                            else if( (data.data["cr_status"]===1||data.data["cr_status"]===2)&&$.member["userinfo_InExUser"]==="Investor" ){
                                    $("#inputMsg").attr("disabled",true).attr("placeholder","同意後才可開始聊天").val("");
                                    var html = '<div>\n\
                                                <p id="LabelProfileAccount" class="form-control-static" target-view="">這位借款人想跟您聊聊，是否同意他觀看您廣告詳細資訊？</p>\n\
                                                <div class="form-group text-center">\n\
                                                    <a id="btnAgreeComun" class="btn btn-w-m btn-success btn-w-m">同意</a>\n\
                                                    <a id="btnRejectComun" class="btn btn-w-m btn-outline btn-default  btn-w-m ">拒絕且消失在詢問列表</a>\n\
                                                </div>\n\
                                            </div>';
                                    $("#talk .msg").html(html);
                            }
                            else if( ($.member.userinfo_Status==="pause"||$.member.userinfo_Status==="pause2")&&$.member["userinfo_InExUser"]==="Borrower" ){
                                    $("#inputMsg").attr("disabled",true).attr("placeholder","您現在已被暫停使用聊天系統").val("");
                            }
                            else{
                                    $.switchPolling = true;
                                    $.cr_convid = data.data["cr_convid"];
                            }
                            
                            
                            $("#btnGoToBid").attr("href","mgi_bid_record?borrow_id="+$.pollingPara["Borrower"]);
//                            getTalkContentCallback(data.data["message"],data.data["cr_status"],$.pollingPara["Borrower"]);
                            
//                        }
                            
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
//        var request = new XMLHttpRequest();
//
//        request.open('GET', 'https://api.leancloud.cn/1.1/rtm/messages/history?convid=591d49395c497d006935f940');
//
//        request.setRequestHeader('Content-Type', 'application/json');
//        request.setRequestHeader('X-LC-Id', 'MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz');
//        request.setRequestHeader('X-LC-Key', 'VaIUCWxS03vG3wX707yfbYxg,master');
//
//        request.onreadystatechange = function () {
//            console.log(this.readyState);
//            if (this.readyState === 4) {
//              console.log('Status:', this.status);
//              console.log('Headers:', this.getAllResponseHeaders());
//              console.log('Body:', this.responseText);
//              testAPI_getTalkContentCallback(this.responseText);
//            }
//        };

        

//        var body = {};//reversed:true
//        request.send(JSON.stringify(body));
}

function testAPI_getTalkContent2(Investor,Borrower){
    
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        if( Investor===null && Borrower===null ){
//            getTalkContentWithCustomerService2();
        }
        else
            getTalkContentCommon2(Investor,Borrower );
          
}

function getTalkContentCommon2(Investor,Borrower){
    
        var request = new XMLHttpRequest();

        request.open('GET', 'https://api.leancloud.cn/1.1/rtm/messages/history?convid='+$.cr_convid);

        request.setRequestHeader('Content-Type', 'application/json');
        request.setRequestHeader('X-LC-Id', 'MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz');
        request.setRequestHeader('X-LC-Key', 'VaIUCWxS03vG3wX707yfbYxg,master');

        request.onreadystatechange = function () {
            console.log(this.readyState);
            if (this.readyState === 4) {
//                console.log('Status:', this.status);
//                console.log('Headers:', this.getAllResponseHeaders());
//                console.log('Body:', this.responseText);
//              testAPI_getTalkContentCallback(this.responseText);
                if($.switchPolling&&$.pollingPara["Investor"]===Investor&&$.pollingPara["Borrower"]===Borrower){
                    getTalkContentCallback2(this.responseText);
                }
                
            }
        };

        

        var body = {};//reversed:true
        request.send(JSON.stringify(body));
}

function testAPI_getTalkContent(Investor,Borrower){
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        if( Investor===null && Borrower===null )
            getTalkContentWithCustomerService();
        else
            getTalkContentCommon(Investor,Borrower );
//        var request = new XMLHttpRequest();
//
//        request.open('GET', 'https://api.leancloud.cn/1.1/rtm/messages/history?convid=591d49395c497d006935f940');
//
//        request.setRequestHeader('Content-Type', 'application/json');
//        request.setRequestHeader('X-LC-Id', 'MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz');
//        request.setRequestHeader('X-LC-Key', 'VaIUCWxS03vG3wX707yfbYxg,master');
//
//        request.onreadystatechange = function () {
//            console.log(this.readyState);
//            if (this.readyState === 4) {
//              console.log('Status:', this.status);
//              console.log('Headers:', this.getAllResponseHeaders());
//              console.log('Body:', this.responseText);
//              testAPI_getTalkContentCallback(this.responseText);
//            }
//        };

        

//        var body = {};//reversed:true
//        request.send(JSON.stringify(body));
}

function getTalkContentCommon(Investor,Borrower){
    
        $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getTalkContent",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    Investor : Investor ,
                    Borrower : Borrower
                },
                success: function(data){
//                    console.log(data);
                    data = JSON.parse(data);
//                    console.log(data);
                    if( data.success ) 
                    {
                        if($.switchPolling&&$.pollingPara["Investor"]===Investor&&$.pollingPara["Borrower"]===Borrower){
                            
                            if( $.member.userinfo_InExUser==="Investor" && data.data["cr_ask_money"]!==0 ){
                                $("[id=askMoneyValue]").html(data.data["cr_ask_money"]);
                                $("#btnBuildTransaction").hide();
                                $("#btnSubmitTransaction").hide();
                                $("#waitTransaction").show();
                                $("#btnGoToBid").show();
                            }
                            else if( $.member.userinfo_InExUser==="Borrower" && data.data["cr_ask_money"]!==0 ){
                                $("[id=askMoneyValue]").html(data.data["cr_ask_money"]);
                                $("#btnBuildTransaction").hide();
                                $("#waitTransaction").hide();
                                $("#btnSubmitTransaction").show();
                                $("#btnGoToBid").hide();
                            }
                            else if( $.member.userinfo_InExUser==="Investor" && data.data["cr_status"]===3 ){
                                $("#waitTransaction").hide();
                                $("#btnSubmitTransaction").hide();
                                $("#btnBuildTransaction").show();
                                $("#btnGoToBid").show();
                            }
                            else if( $.member.userinfo_InExUser==="Investor" ){
                                $("#btnBuildTransaction").hide();
                                $("#btnSubmitTransaction").hide();
                                $("#waitTransaction").hide();
                                $("#btnGoToBid").show();
                            }
                            else{
                                $("#btnBuildTransaction").hide();
                                $("#btnSubmitTransaction").hide();
                                $("#waitTransaction").hide();
                                $("#btnGoToBid").hide();
                            }
                            $("#btnGoToBid").attr("href","mgi_bid_record?borrow_id="+$.pollingPara["Borrower"]);
                            getTalkContentCallback(data.data["message"],data.data["cr_status"],$.pollingPara["Borrower"]);
                            
                        }
                            
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

function getTalkContentWithCustomerService(){
        
        $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getTalkContentWithCustomerService",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {},
                success: function(data){
//                    console.log(data);
                    data = JSON.parse(data);
//                    console.log(data);
                    if( data.success ) 
                    { 
                            $("#btnBuildTransaction").hide();
                            $("#btnSubmitTransaction").hide();
                            $("#waitTransaction").hide();
                            $("#btnGoToBid").hide();
                            getTalkContentCallback(data.data["message"]);
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

function getTalkContentCallback2(responseText){
        
        responseText = JSON.parse(responseText);
        $.responseText = responseText;
        console.log(responseText);
        var date;
        responseText = responseText.reverse();
//        console.log(responseText);
        var html = '';
        $.each(responseText,function(k,v){
//           console.log(v);
           date = new Date(v["timestamp"]);
           html += pushMessageHtml(v["from"],
                    date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate(),
                    date.getHours()+":"+date.getMinutes()+":"+date.getSeconds(),
                    v["data"]);
        });
        
//        if( (cr_status===1||cr_status===2)&&$.member["userinfo_InExUser"]==="Investor" ){
//                $("#inputMsg").attr("disabled",true).attr("placeholder","同意後才可開始聊天").val("");
//                html += '<div>\n\
//                            <p id="LabelProfileAccount" class="form-control-static" target-view="">這位借款人想跟您聊聊，是否同意他觀看您廣告詳細資訊？</p>\n\
//                            <div class="form-group text-center">\n\
//                                <a id="btnAgreeComun" class="btn btn-w-m btn-success btn-w-m">同意</a>\n\
//                                <a id="btnRejectComun" class="btn btn-w-m btn-outline btn-default  btn-w-m ">拒絕且消失在詢問列表</a>\n\
//                            </div>\n\
//                        </div>';
//        }
//        else{
                $("#inputMsg").attr("disabled",false).attr("placeholder","請輸入文字");
//        }
        $("#talk .msg").html(html);
        $("#talk .msg")[0].scrollTop = $("#talk .msg")[0].scrollHeight;
}

function getTalkContentCallback(responseText,cr_status){
//        $(".chat-discussion").html("");
        cr_status = cr_status || 3;
        console.log("cr_status="+cr_status);
        var date;
        responseText = responseText.reverse();
//        console.log(responseText);
        var html = '';
        $.each(responseText,function(k,v){
//           console.log(v);
           date = new Date(v["timestamp"]);
           html += pushMessageHtml(v["from"],
                    date.getFullYear()+"/"+(date.getMonth()+1)+"/"+date.getDate(),
                    date.getHours()+":"+date.getMinutes()+":"+date.getSeconds(),
                    v["data"]);
        });
        
//        if( (cr_status===1||cr_status===2)&&$.member["userinfo_InExUser"]==="Borrower" ){
//                $("#inputMsg").attr("disabled",true).attr("placeholder","同意後才可開始聊天").val("");
//                html += '<div>\n\
//                            <p id="LabelProfileAccount" class="form-control-static" target-view="">這位投資人想跟您聊聊，是否同意他觀看您廣告詳細資訊？</p>\n\
//                            <div class="form-group text-center">\n\
//                                <a id="btnAgreeComun" class="btn btn-w-m btn-success btn-w-m">同意</a>\n\
//                                <a id="btnRejectComun" class="btn btn-w-m btn-outline btn-default  btn-w-m ">拒絕且消失在詢問列表</a>\n\
//                            </div>\n\
//                        </div>';
//        }
        if( (cr_status===1||cr_status===2)&&$.member["userinfo_InExUser"]==="Investor" ){
                $("#inputMsg").attr("disabled",true).attr("placeholder","同意後才可開始聊天").val("");
                html += '<div>\n\
                            <p id="LabelProfileAccount" class="form-control-static" target-view="">這位借款人想跟您聊聊，是否同意他觀看您廣告詳細資訊？</p>\n\
                            <div class="form-group text-center">\n\
                                <a id="btnAgreeComun" class="btn btn-w-m btn-success btn-w-m">同意</a>\n\
                                <a id="btnRejectComun" class="btn btn-w-m btn-outline btn-default  btn-w-m ">拒絕且消失在詢問列表</a>\n\
                            </div>\n\
                        </div>';
        }
        else if( ($.member.userinfo_Status==="pause"||$.member.userinfo_Status==="pause2")&&$.member["userinfo_InExUser"]==="Borrower" ){
                $("#inputMsg").attr("disabled",true).attr("placeholder","您現在已被暫停使用聊天系統").val("");
        }
        else{
                $("#inputMsg").attr("disabled",false).attr("placeholder","請輸入文字");
        }
        $("#talk .msg").html(html);
        $("#talk .msg")[0].scrollTop = $("#talk .msg")[0].scrollHeight;
}

function pushMessageHtml(name,date,time,content){
    
//    var classType = $.member.userinfo_UserName===name ? "right" : "left";
//    content = content.replace( /</g , " &#60; " ) ;
//    content = content.replace( /&/g , " &amp; " ) ;
//    content = content.replace( />/g , " &gt; " ) ;
    
//    return '<div class="chat-message '+classType+'">\n\
//                                    <img class="message-avatar" src="img/a4.jpg" alt="" >\n\
//                                    <div class="message">\n\
//                                        <a class="message-author" href="#"> '+name+' </a>\n\
//                                        <span class="message-date">  '+date+' - '+time+' </span>\n\
//                                        <span class="message-content">'
//                                                +content+
//                                        '</span>\n\
//                                    </div>\n\
//                                </div>';
    
    var classType = $.member.userinfo_UserName===name ? "sent" : "recieve";
    
    return '<dl class="'+classType+'">\n\
                <dt>'+content+'</dt>\n\
                <dd>'+date+' '+time+'</dd>\n\
            </dl>';
    
}

function testAPI_sendMessagetToRoom(content,Investor,Borrower){
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/chat/sendMessagetToRoom",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    content : content ,
                    Investor : Investor ,
                    Borrower : Borrower
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            console.log(data["result"]);
                            console.log(data["result"]["msg-id"]);
                            console.log(data["result"]["timestamp"]);
                            show_remind("傳送成功","success");
                            if(data["status"]===0||data["status"]===1){
//                                 $(".chat-discussion")[0].scrollTop = $(".chat-discussion")[0].scrollHeight;
//                                 swal( "請先付款，才可以持續與對方聊天" );
                            }
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
//        var request = new XMLHttpRequest();
//
//        request.open('POST', 'https://api.leancloud.cn/1.1/rtm/messages');
//
//        request.setRequestHeader('Content-Type', 'application/json');
//        request.setRequestHeader('X-LC-Id', 'MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz');
//        request.setRequestHeader('X-LC-Key', 'VaIUCWxS03vG3wX707yfbYxg,master');
//
//        request.onreadystatechange = function () {
//            console.log(this.readyState);
//            if (this.readyState === 4) {
//              console.log('Status:', this.status);
//              console.log('Headers:', this.getAllResponseHeaders());
//              console.log('Body:', this.responseText);
//              testAPI_getTalkContent();
//            }
//        };
//
//        var body = {"from_peer": $.member.userinfo_UserName, 
//            "message": content, 
//            "conv_id": "591d49395c497d006935f940", 
//            "transient": false
//        };
//        request.send(JSON.stringify(body));
}

function sendMoneyAsk(money,Investor,Borrower){
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/transaction/sendMoneyAsk",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    money : money ,
                    Investor : Investor ,
                    Borrower : Borrower
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            show_remind("請求成功","success");
                            $("#ModalBuildTransaction").modal("hide");
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}

function sendTransactionRes(Investor,Borrower){
        Investor = Investor || null;
        Borrower = Borrower || null;
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/transaction/sendTransactionRes",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    Investor : Investor ,
                    Borrower : Borrower
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            show_remind("已接受成功，請至交易紀錄查看此筆","success");
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}
  
function testAPI_OpenTalkRoom(){
        var request = new XMLHttpRequest();

        request.open('POST', 'https://api.leancloud.cn/1.1/classes/_Conversation');

        request.setRequestHeader('Content-Type', 'application/json');
        request.setRequestHeader('X-LC-Id', 'MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz');
        request.setRequestHeader('X-LC-Key', 'qevnXOAHcJQo0wBTkzxV6KDH');

        request.onreadystatechange = function () {
            console.log(this.readyState);
            if (this.readyState === 4) {
              console.log('Status:', this.status);
              console.log('Headers:', this.getAllResponseHeaders());
              console.log('Body:', this.responseText);
            }
        };

        var body = {"name":"My Private Room",
            "m": ["BillGates", "SteveJobs"],
            "tr": true,
            "sys": true};
//                    {
//                      'title': '工程师周会',
//                      'content': '每周工程师会议，周一下午2点'
//                    };

        request.send(JSON.stringify(body));
}

function getAllBorrowerByInvestor(initChatUser){
    
    $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getAllBorrowerByInvestor",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {},
                success: function(data){
//                    console.log(data);
                    data = JSON.parse(data);
//                    console.log(data);
                    if( data.success ) 
                    {
                        $.test = data.data;
                        var html ="";
                        $.each(data.data["biding"],function(k,v){
                            //text-navy 上線class
//                            html+='<div class="chat-user" u_id="'+v["userinfo_ID"]+'">\n\
//                                    <i class="fa fa-circle text-muted m-r-sm m-t-sm float-left"></i>\n\
//                                    <img class="chat-avatar" src="img/a4.jpg" alt="" >\n\
//                                    <div class="chat-user-name">\n\
//                                        <a href="#">'+v["userinfo_UserName"]+'</a><span class="m-l-sm">'+v["userinfo_UserIdentity"]+'</span><span class="m-l-xs">'+v["userinfo_Area"]+'</span>\n\
//                                    </div>\n\
//                                </div>';
                            html+='<li class="B" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatBorrowList .list:eq(2)").html(html);
                        html="";
                        $.each(data.data["AgreeBorrowerList"],function(k,v){
                            html+='<li class="B" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatBorrowList .list:eq(3)").html(html);
                        html="";
                        $.each(data.data["askBorrowerList"],function(k,v){
                            html+='<li class="B" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatBorrowList .list:eq(4)").html(html);
                        html="";
                        $.each(data.data["allBorrowerList"],function(k,v){
                            html+='<li class="B" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatBorrowList .list:eq(5)").html(html);
                        
                        chatUserEvent();
                        
                        console.log( "initChatUser="+initChatUser )
                        if( initChatUser ){
                            $("#chatBorrowList .list [u_id='"+initChatUser+"']").trigger("click");
                        }
                        
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
    
}

function getInvestorByBorrower( initChatUser ){
    
    $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getInvestorByBorrower",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {},
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                        $.test = data.data;
                        var html ="";
                        $.each(data.data["AgreeBorrowerList"],function(k,v){
                            //text-navy 上線class
                            //AL 20170720 個金icon=>class="L"  企業icon=>class="V" 
                            html+='<li class="L" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatinvestorList .list:eq(2)").html(html);
                        html="";
                        $.each(data.data["allBorrowerList"],function(k,v){
                            html+='<li class="L" u_id="'+v["userinfo_ID"]+'">\n\
                                    <div>\n\
                                        <p>'+v["userinfo_UserName"]+' '+v["userinfo_ID"]+'</p><p>'+v["userinfo_Area"]+'</p>\n\
                                    </div>\n\
                                </li>';
                        });
                        $("#chatinvestorList .list:eq(3)").html(html);
                        chatUserEvent();
                        
                        console.log( "initChatUser="+initChatUser )
                        if( initChatUser ){
                            $("#chatinvestorList .list [u_id='"+initChatUser+"']").trigger("click");
                        }
                        
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
    
}
                
function chatUserEvent(){
        
        if($.member.userinfo_InExUser==="Investor"){
            $("#talk #chatBorrowList .list:not('.bot') li").unbind("click").bind("click",function(){
                $("#talk .list li.active").removeClass("active");
                $(this).addClass("active").removeClass("new-msg-list");
                var u_id = $(this).attr("u_id");
                console.log(u_id)
                $("#talk .msg").html("");
                $.pollingPara = {"Investor":null,"Borrower": u_id };
                getUserInfo(u_id);
//                testAPI_getTalkContent(null,u_id);
//                $.switchPolling = true;
                $.switchPolling = false;
                getChatroomID(null,u_id);
            });
        }
        else if($.member.userinfo_InExUser==="Borrower"){
            $("#talk #chatinvestorList .list:not('.bot') li").unbind("click").bind("click",function(){
                $("#talk .list li.active").removeClass("active");
                $(this).addClass("active").removeClass("new-msg-list");
                var u_id = $(this).attr("u_id");
                console.log(u_id)
                $("#talk .msg").html("");
                $.pollingPara = {"Investor":u_id,"Borrower": null };
                getUserInfo(u_id);
//                testAPI_getTalkContent(u_id,null);
//                $.switchPolling = true;
                $.switchPolling = false;
                getChatroomID(u_id,null);
            });
        }
        $("#talk .list.bot li").unbind("click").bind("click",function(){
            $("#talk .list li.active").removeClass("active");
            $(this).addClass("active");
//            var u_id = $(this).attr("u_id");
//            console.log(u_id)
            $("#talk .msg").html("");
            $.pollingPara = {"Investor":null,"Borrower": null };
//            getUserInfo(u_id);
            $("#talk .id c:eq(0)").html( "正在跟" );
            $("#talk .id c:eq(1)").html( "客服" );
            $("#talk .id c:eq(2)").html( "聊聊中" );
//            getTalkContentWithCustomerService();
            $.switchPolling = true;
        });
        
        
}

function borrowerResChat(Investor,response){
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/chat/borrowerResChat",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    Investor:Investor ,
                    response:response
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            if(response){
                                resetChat();
                                show_remind("回應成功，可開始聊天","success");
                            }
                            else{
                                resetChat();
                                show_remind("已成功拒絕","success");
                            }
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

function investorResChat(Borrower,response){
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/chat/investorResChat",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    Borrower:Borrower ,
                    response:response
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            if(response){
                                chatList();
                                show_remind("已扣除點數，可開始聊天","success");
                            }
                            else{
                                resetChat();
                                show_remind("已成功拒絕","success");
                            }
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

function chatInit(){
        
        //AL 20170720 借方icon=>images/B.svg 個金icon=>images/L.svg  企業icon=>images/V.svg
        $.chatInit = true;
        resetChat();
        $("#talk .msg").delegate("[id='btnAgreeComun']","click",function(){
//                                show_remind("btnAgreeComun");
//                        borrowerResChat($.pollingPara["Investor"],true);
                investorResChat($.pollingPara["Borrower"],true);
        });
        $("#talk .msg").delegate("[id='btnRejectComun']","click",function(){
//                        borrowerResChat($.pollingPara["Investor"],false);
                investorResChat($.pollingPara["Borrower"],false);
        });
        $('#inputMsg').unbind( "keypress" ).bind( "keypress" , function(e){
                if( e.which === 13 ){
                    $('#btnSendMsg').trigger('click');
                }
        });
        $("#btnSendMsg").unbind("click").bind("click",function(){
                if($("#inputMsg").attr("disabled")=="disabled"){
                    return false;
                }

                if($("#inputMsg").val()==""){
                    show_remind("請輸入您的訊息","error");
                }
                else{
                    if($.switchPolling)
                        testAPI_sendMessagetToRoom($("#inputMsg").val(),$.pollingPara["Investor"],$.pollingPara["Borrower"]);
                    else{

                    }

                    $("#inputMsg").val("");
                }
        });
        $('#btnBuildTransaction').unbind( "click" ).bind( "click" , function(e){
                $("#ModalBuildTransaction").modal("show");
        });
        $('#ModalBuildTransactionYes').unbind( "click" ).bind( "click" , function(e){
                console.log("sendMoneyAsk");
                if($("#inputModalBuildTransactionMoney").val()!==""){
                        sendMoneyAsk($("#inputModalBuildTransactionMoney").val(),$.pollingPara["Investor"],$.pollingPara["Borrower"]);
                }
        });
        $('#btnSubmitTransaction').unbind( "click" ).bind( "click" , function(e){
                sendTransactionRes($.pollingPara["Investor"],$.pollingPara["Borrower"]);
        });

        $(".btnBorrowInfo").unbind("click").bind("click",function(){
        //                         data-toggle="modal" data-target="#"
                var borrowID = $(this).attr("borrowID");
                
                console.log(borrowID);
                $.ajax({
                    type:"GET",
                    dataType: "text",
                    url: "service/chat/getBorrowInfoByBorrowIdFromInvestor",
                    headers: {
                               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                               'Authorization': "Token "+getCookie("lnbCookie")
                    },
                    data: {
                            borrowID : borrowID
                    },
                    success: function(data){
                                data = JSON.parse(data);
                                console.log(data);
                                if( data.success ) {

                                        $("#myModalBorrowInfo #customerInfo_ID").html(data.data[0].userinfo_ID);
                                        $("#myModalBorrowInfo [id=customerInfo_username]").html(data.data[0].userinfo_UserName);
                                        $("#myModalBorrowInfo [id=customerInfo_address]").html(data.data[0].userinfo_Address);
                                        $("#myModalBorrowInfo [id=customerInfo_job]").html("");
                                        $("#myModalBorrowInfo [id=customerInfo_hoptax]").html("");
                                        $("#myModalBorrowInfo [id=customerInfo_reason]").html("");

                                        $("#myModalBorrowInfo [id=customerInfo_phone]").html(data.data[0].userinfo_UserPhone);
                                        $("#myModalBorrowInfo [id=customerInfo_birthday]").html(data.data[0].userinfo_UserAge);
                                        $("#myModalBorrowInfo [id=customerInfo_gender]").html("");
                                        $("#myModalBorrowInfo [id=customerInfo_place]").html("");
                                        $("#myModalBorrowInfo [id=customerInfo_identity]").html("");


                                        $("#myModalBorrowInfo").modal("show");
                                }
                                else {
                                        show_remind(data.msg,"error");
                                }
                    },
                    error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                    }
                });

        });

        //tab-block
        $('.tab-block').each(function(index, element) {
                $(this).find('.tabs a').eq(0).addClass('active');
                $(this).find('.tab-content > li').eq(0).show();
        });

        $('.tab-block .tabs a').unbind('click').bind('click',function(){
                var $tabIndex = $(this).index();
                $(this).addClass('active').siblings().removeClass('active');
                $(this).parent().siblings('.tab-content').find('> li').eq($tabIndex).show().siblings().hide();
        });
        
        chatRoomPolling();
}

function resetChat( initChatUser ){
        $("#talk .id c:eq(0)").html( "請選擇聊天對象" );
        $("#talk .id c:eq(1)").html( "" );
        $("#talk .id c:eq(2)").html( "" );
        $("#talk .msg").html("");
        $("#btnGoToBid").hide();
        $("#btnBorrowInfo").hide();
        $("#inputMsg").attr("disabled",true).attr("placeholder","請選擇聊天對象").val("");
        $.pollingPara = {"Investor":null,"Borrower": null };
        $.switchPolling = false;
        chatList( initChatUser );
}
function chatList( initChatUser ){
        $("#BorrowerList .chat-user:not('.chat-user-title')").remove();
        $("#InvestorList .chat-user:not('.chat-user-title')").remove();
        if($.member){
            console.log("login");
            if($.member.userinfo_InExUser==="Investor"){
                $("#talk .user").html( "<img src='images/V.svg' class='img-lg m-r-sm' style='width: 30px;'>Hi ~ " + $.member.userinfo_UserName + " 您好" );
                $("#inputChatObjectSearch").attr("placeholder","搜尋借款人姓名或地址");
                getAllBorrowerByInvestor( initChatUser );
                $("#chatBorrowList").show();
                $('#inputChatObjectSearch').unbind( "keypress" ).bind( "keypress" , function(e){
                        if( e.which === 13 ){
                            if( $('#inputChatObjectSearch').val() !== "" ){
                                $("#chatBorrowList .list:not('.bot') li").hide();
                                $( "#chatBorrowList .list:not('.bot') li div:contains('"+$('#inputChatObjectSearch').val()+"')" ).parent().show();
                            }
                            else{
                                $("#chatBorrowList .list li").show();
                            } 
                        }
                });
            }
            else if($.member.userinfo_InExUser==="Borrower"){
                $("#talk .user").html( "<img src='images/V.svg' class='img-lg m-r-sm' style='width: 30px;'>Hi ~ " + $.member.userinfo_UserName + " 您好" );
                $("#inputChatObjectSearch").attr("placeholder","搜尋投資人姓名或地址");
                getInvestorByBorrower( initChatUser );
                $("#chatinvestorList").show();
                $('#inputChatObjectSearch').unbind( "keypress" ).bind( "keypress" , function(e){
                        if( e.which === 13 ){
                            if( $('#inputChatObjectSearch').val() !== "" ){
                                $("#chatinvestorList .list:not('.bot') li").hide();
                                $( "#chatinvestorList .list:not('.bot') li div:contains('"+$('#inputChatObjectSearch').val()+"')" ).parent().show();
                            }
                            else{
                                $("#chatinvestorList .list li").show();
                            } 
                        }
                });
            }
        }
        else{
            console.log("unlogin");
            $("#talk .user").html( "<img src='images/V.svg' class='img-lg m-r-sm' style='width: 30px;'>Hi ~ 遊客56988 您好" );
            $("#visitorList").show();
        }
}

function chatRoomPolling() {
    setTimeout(function () {

        if($.switchPolling){
//            testAPI_getTalkContent($.pollingPara["Investor"],$.pollingPara["Borrower"]);
            testAPI_getTalkContent2($.pollingPara["Investor"],$.pollingPara["Borrower"]);
        }

        chatRoomPolling();
    }, 1000);
}

function greetingsInit() {

    var userinfo_id = $.member.userinfo_ID;
    var userinfo_oTableColumn = 'userinfo_greetings.userinfo_Greetings';

    $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getUserinfoValue",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    "userinfo_id": userinfo_id,
                    "userinfo_oTableColumn": userinfo_oTableColumn,
                },
                success: function(data){

                    console.log(data);
                    var data = JSON.parse(data);

                    if( data.success ) {

                        var modal = '';
                        var textList = '';
                        var id = $.member.userinfo_ID;
                        var greetings_data = data.data.userinfo_value.replace(/\\"/g, '"');

                        $.each( eval(greetings_data) , function(index, value){

                            index = index + 1 ;
                            modal += '<div class="form-group col-sm-12 col-xs-12">'+
                                        '<label class="col-sm-2 col-xs-2 control-label text-left">' +index+ '.</label>'+
                                        '<div class="col-sm-10">'+
                                        '<p class="form-control-static" target-view>' +value+ '</p>'+
                                        '</div>'+
                                        '<div class="col-sm-9 col-xs-8">'+
                                            '<input name="LabelGreeting" placeholder="' +value+ '" value="' +value+ '" class="form-control" type="text" target-edit style="display: none;">'+
                                        '</div>'+
                                        '<div class="col-sm-1 col-xs-2">'+
                                            '<button class="btnProfileRemove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'+
                                        '</div>'+
                                    '</div>';

                            if(index <= 3) {
                                textList += '<li class="cursor"><a>' +value+ '</a></li>';
                            }

                        });

                        $("#Modal .modal-body").html(modal);
                        $(".list-unstyled li:eq(0)").nextAll().remove();
                        $(textList).insertAfter(".list-unstyled li:eq(0)");
                        $(".btnProfileRemove").css({'border': 'none', 'background-color': 'transparent', 'vertical-align': '-webkit-baseline-middle', 'display': 'none'});
//                                bindBtnProfileRemove();
                        bindtextList();

                    } else {

                        console.log( data.msg );
                        $(".list-unstyled li:eq(0)").nextAll().remove();

                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
    });

}

function bindtextList() {
    $("#textList .list-unstyled li").slice(1).unbind('click').bind('click',function(e){
        var greeting = $(this).children('a').text();
        console.warn( greeting );
        $("#inputMsg").val( greeting );
    });
}

function getUserInfo(u_id){
        console.log("getUserInfo");
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getUserinfoByID",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    userinfo_id : u_id
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            $("#talk .id c:eq(0)").html( "正在跟" );
                            $("#talk .id c:eq(1)").html( data.data[0].userinfo_UserName );
                            $("#talk .id c:eq(2)").html( "聊聊中" );
                            $(".ibox-title")
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}




chatRoomCommandPolling();

function chatRoomCommandPolling() {
    setTimeout(function () {

        if($.chatInit)
            getMyChatCommand();
        else
            chatRoomCommandPolling();
    }, 5000);
}

function getMyChatCommand(){
    
        $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/getMyChatCommand",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {},
                success: function(data){
//                    console.log(data);
                    data = JSON.parse(data);
//                    console.log(data);
                    if( data.success ) 
                    {
                           if( data.data[0] ) {
                               $.each( data.data , function(k,v){ 
                                    doAPI( v.cca_command_id , v.cca_command_user , v.cca_id );
                               });
                               console.log("doAPI")
                           }
                           else{
                               console.log("noAPI")
                           }
                            
                            
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                    chatRoomCommandPolling();
                    
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                    chatRoomCommandPolling();
                }
        });
    
}

function pushAPI( user , command_id , command_user ){
        
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/chat/pushAPI",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    user : user ,
                    command_id : command_id ,
                    command_user : command_user
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                            
                    }
                    else{
                            show_remind(data.msg,"error");
                    }
                    
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

function doAPI( command_id , command_user , id ){
        
        switch(command_id){
            case 1://聊天室閃並置頂
                $("#talk > a").addClass("new-msg");
                if( $("#talk [u_id='"+command_user+"']")[0] ){
                    var html = $("#talk [u_id='"+command_user+"']").html();
                    html = '<li u_id="'+command_user+'">' + html + '</li>';
                    console.log(html);
                    if($.member.userinfo_InExUser==="Investor"){
                        $("#chatBorrowList .list:eq(0)").html(html);
                    }
                    else if($.member.userinfo_InExUser==="Borrower"){
                        $("#chatinvestorList .list:eq(0)").html(html);
                    }
                    chatUserEvent();
                    $("#talk [u_id='"+command_user+"']").addClass("new-msg-list");
                }
                returnAPIState( id );
                break;
            case 2://聊天室閃爍，那個位置至頂
                $("#talk > a").addClass("new-msg");
                if( $("#talk [u_id='"+command_user+"']")[0] ){
                    var html,parentPos;
                    $.each($("#talk [u_id='"+command_user+"']"),function(k,v){
                        html = $(v).html();
                        parentPos = $(v).parent();
                        $(v).remove();
                        html = '<li u_id="'+command_user+'">' + html + '</li>';
                        parentPos.prepend(html);
                    });
                    chatUserEvent();
                    $("#talk [u_id='"+command_user+"']").addClass("new-msg-list");
                }
                returnAPIState( id );
                break;
            case 3://reload聊天室
                resetChat();
                returnAPIState( id );
                break;
            case 4:
                if(typeof bidListbyBorrowId === "function"){
                    bidListbyBorrowId();
                    returnAPIState( id );
                }
                break;
            case 5://reload再閃爍，那個位置至頂
                resetChat();
                setTimeout(function () {
                    $("#talk > a").addClass("new-msg");
                    if( $("#talk [u_id='"+command_user+"']")[0] ){
                        var html,parentPos;
                        $.each($("#talk [u_id='"+command_user+"']"),function(k,v){
                            html = $(v).html();
                            parentPos = $(v).parent();
                            $(v).remove();
                            html = '<li u_id="'+command_user+'">' + html + '</li>';
                            parentPos.prepend(html);
                        });
                        chatUserEvent();
                        $("#talk [u_id='"+command_user+"']").addClass("new-msg-list");
                    }
                }, 3000);
                returnAPIState( id );
                break;
            case 6://reload聊天室，再focus
                resetChat( command_user );
                returnAPIState( id );
                break;
                
                
                
        }
        
        returnAPIState();
        
}

function returnAPIState( id ){
        
        $.ajax({
                type:"GET",
                dataType: "text",
                url: "service/chat/returnAPIState",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                data: {
                    cca_id : id
                },
                success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    console.log(data);
                    if( data.success ) 
                    {
                        
                    }
                    else{
//                            show_remind(data.msg,"error");
                    }
                    
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
        
}

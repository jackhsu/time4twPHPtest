
window.fbAsyncInit = function() {
    FB.init({
            appId      : '732063513556699',
            cookie     : true,  // enable cookies to allow the server to access 
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.2' // use version 2.2
    });
    $( "[id=fb_login_btn]" ).show();
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/zh_TW/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function FB_login() {
            FB.login(function(response) {
                    checkLoginState();
            }, {
                scope: 'email', 
                return_scopes: true
            });
};
function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    if (response.status === 'connected') {
            // Logged into your app and Facebook.
            FB_connected_callback( function(response) {
                    if( typeof facebook_data_connected_callback_init != "undefined" )
                            facebook_data_connected_callback_init( response );
                              
            });
    } else if (response.status === 'not_authorized') {
            
    } else {
            
    }
}

function FB_connected_callback( cbsuccess ) {
      FB.api('/me', cbsuccess
      );
}


$("document").ready(function() {
    
        if( typeof init_connect_callback != "undefined" )
                init_connect_callback();
        
        //check_login(); Vincent 2017/7/6
        check_remember();
	//大版登入登出切換
//	$('.top ul li.login #login-block input.button:not(".button-g")').bind( "click" , function(e) {
//                
//                var bool = true;
//                var msg;
//                if( !$('.top ul li.login #login-block input[type=email]').val() ){ bool = false; msg = "請輸入您的Email及密碼!!";}
//                else if( !$('.top ul li.login #login-block input[type=password]').val() ){bool = false; msg = "請輸入您的Email及密碼!!";}
//                else if( !$('.top ul li.login #login-block input[type=captcha]').val() ){bool = false; msg = "請輸入驗證碼!!";}
//
//                if( bool ) {
//                    loading_ajax_show();
//                    login_func( $('.top ul li.login #login-block input[type=email]').val() , 
//                                $('.top ul li.login #login-block input[type=password]').val() , 
//                                $('.top ul li.login #login-block input[type=captcha]').val() , 
//                                $('.top ul li.login #login-block [id=remember_account]').is( ":checked" ) );
//                                
//                }
//                else {
//                    show_remind( msg , "error" );
//                }
//        });
//        
//	$('#login-block input.button-g').bind( "click" , function(e) {
//                
//                var pos = $( this ).parents( "#login-block" );
//                pos.find('input[type=email]').val( 'funbook19.test@gmail.com' );
//                pos.find('input[type=password]').val( 'funbook19' );
//                pos.find('[id=remember_account]')[0].checked = true ;
//                var data = {};
//                var success_back = function( data ) {
//                        pos.find('input[type=captcha]').val( data );
//                }
//                var error_back = function( data ) {
//                        
//                }
//                $.Ajax( "GET" , "php/verification_g.php" , data , "" , success_back , error_back);
//                
//        });
//        
//        $('.top ul li.login #logout-block .logout').click(function(e) {
//                logout_func();
//        });
//	//小版登入登出切換
//	$('#mobi-member #login-block input.button:not(".button-g")').click(function(e) {
//                var bool = true;
//                var msg;
//                if( !$('#mobi-member #login-block input[type=email]').val() ){ bool = false; msg = "請輸入您的Email及密碼!!";}
//                else if( !$('#mobi-member #login-block input[type=password]').val() ){bool = false; msg = "請輸入您的Email及密碼!!";}
//                else if( !$('#mobi-member #login-block input[type=captcha]').val() ){bool = false; msg = "請輸入驗證碼!!";}
//                
//                if( bool ) {
//                    loading_ajax_show();
//                    login_func( $('#mobi-member #login-block input[type=email]').val() , 
//                                $('#mobi-member #login-block input[type=password]').val() , 
//                                $('#mobi-member #login-block input[type=captcha]').val() ,
//                                $('#mobi-member #login-block [id=remember_account]').is( ":checked" ));
//                }
//                else {
//                    show_remind( msg , "error" );
//                }
//        });
//	
//	$('#mobi-member #logout-block .logout').click(function(e) {
//                    logout_func();
//        });
	
         
});

function re_captcha() {
        $( ".captcha img" ).attr( "src" , "php/verification.php?" + Math.random() );
}

function check_login() {
    
    if( window.Web2App ) {
            
            window.Web2App.get_cookie( "lnbCookie" );
            
    }
    else if ( getCookie("lnbCookie") ) {
            
            get_cookie_cb( getCookie("lnbCookie") );
            
    }
    else
    {
            logout_layout();
            if( typeof unconnected_callback != "undefined" )
                unconnected_callback();
    }
    
}

function check_remember() {
    
    if ( localStorage.getItem( "funbook19_account" ) ) {
            
            $( "#login-block [type=email]" ).val( localStorage.getItem( "funbook19_account" ) );
            $( "#login-block [type=password]" ).val( localStorage.getItem( "funbook19_password" ) );
            $.each( $( "[id=remember_account]" ) , function( index , value ){
                    if( !$( value ).is( ":checked" ) ) {
                        $( value ).click();
                    }
            });
            
    }
    
}

function get_cookie_cb( callback ) {
    
    $.lnbCookie = callback;
    if( $.lnbCookie !== null && $.lnbCookie !== "" ) {
        
            $.ajax({
                type:"GET",
//                dataType: "text",
                url: "service/users/checkToken",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                    'Authorization': "Token "+$.lnbCookie
                },
                data: {},
                success: function(data){
                    
                    console.log(data);
                    data = JSON.parse(data);
                    if( data.success ) {
                        login_layout( data.data );

                        if( $.login == undefined )
                            $.login = {} ;
                        $.login.userInfo = data.data;
//                        $( "[id=sidebar_usericon]" ).css( "background-image" , "url('" + data.data.ch_icon + "')" );
                        //$( "[id=sidebar_name]" ).html( data.data.ch_name + '<span>'+ data.data.a_kind_name +'</span>' );
                        //$( "[id=sidebar_name]" ).html( data.data.userinfo_UserName + '<span>'+ data.data.userinfo_InExUser +'</span>' );
                        $( "[id=sidebar_userinfo_UserName]" ).html( data.data.userinfo_UserName );

//                        var tmp_LabelProfileState = "" ;
//                        $.each( eval( data.data.userinfo_InExUser ) , function( index , value ) {
//                                if( index == 0 )
//                                tmp_LabelProfileState += value ;
//                                else
//                                tmp_LabelProfileState += " & "  + value ;
//                        });
                        $( "[id=sidebar_userinfo_InExUser]" ).attr( "title" , data.data.userinfo_InExUser );
                        $( "[id=sidebar_userinfo_InExUser]" ).html( data.data.userinfo_InExUser + '<b class="caret"></b>' );//AL 20170318新增下拉箭頭
                        
                        // ++ jack
                        $.loginmsg = {} ;
                        $.loginmsg.userinfo = data.data ;

                        $( "[userinfo_inexuser]" ).hide();
                        $( "[userinfo_inexuser=" + $.loginmsg.userinfo.userinfo_InExUser + "]" ).show();
                        
                        // -- jack
                        
                        

                        if( typeof connected_callback != "undefined" )
                            connected_callback( data.data );
                    }
                    else {
                        show_remind( data.msg , "error" );
                        delete_cookie( "lnbCookie" );// , "/" , ".oort.com.tw"
                        logout_layout();
                        if( typeof unconnected_callback != "undefined" )
                            unconnected_callback();
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
            });
            
    }
    else {
            delete_cookie( "lnbCookie" );// , "/" , ".oort.com.tw"
            logout_layout();
            if( typeof unconnected_callback != "undefined" )
                unconnected_callback();
    }
    
}

function login_func( account , password , authentication , remember , login_time , pos ) {
        
        login_time = login_time || "one_month";
        if( remember ) {
            localStorage.setItem( "funbook19_account" , account );
            localStorage.setItem( "funbook19_password" , password );
        }
        else {
            localStorage.removeItem( "funbook19_account" );
            localStorage.removeItem( "funbook19_password" );
        }
        
        var data = {
                    account:      account ,
                    password:   md5( password ) ,
                    authentication: authentication ,
                    login_time: login_time
        };
        var success_back = function( data ) {
                
                data = JSON.parse( data );
                loading_ajax_hide();
                if( data.success ) {
                    setCookie("lnbCookie", data.data, "", "/", ".oort.com.tw");
                    check_login();
                }
                else {
                    show_remind( data.msg , "error" );
                    $( ".captcha img" ).attr( "src" , "php/verification.php?" + Math.random() );
                    if( data.action === "show_box" ){
                        $( "#alert-msg" ).show();
                    }
                    else if( data.action === "captcha" && pos ){
                        pos.parent().find(".alert").hide();
                        pos.parent().find(".alert.error").show();
                        scrollto( $( "#input_captcha2" ) );
                    }
                }
                
        }
        var error_back = function( data ) {
                
        }
        $.Ajax( "GET" , "php/member.php?func=login" , data , "" , success_back , error_back);

}

function logout_func() {
        
        var data = {
                token:   getCookie( "lnbCookie" )
        };
        var success_back = function( data ) {
        
                data = JSON.parse( data );
                if( data.success ) {
                    delete_cookie( "lnbCookie" );// , "/" , ".oort.com.tw"
                    logout_layout();
                    if( typeof unconnected_callback != "undefined" )
                        unconnected_callback();
                }
                else {
                    show_remind( data.msg , "error" );
                }

        }
        var error_back = function( data ) {
                
        }
        $.Ajax( "GET" , "php/member.php?func=logout" , data , "" , success_back , error_back);
        
}

function login_layout( data ) {
        //有登入
        $( ".login-block" ).hide();
        $( ".logout-block" ).show();
        
        $('.top ul li.login').addClass('active');
        // mobile and PC hide 登入box
        $('[id=login-block]').stop(true, false).animate({top:40, opacity:0}).hide().css( "z-index" , "" );
        // mobile show 已登入操作box
        $( "#mobi-member [id=logout-block]" ).show().css( "opacity" , "1" ).css( "z-index" , "" );
        // PC hide 已登入操作box
        $('.top > ul > li.login [id=logout-block]').stop(true, false).animate({top:40, opacity:0}).hide().css( "z-index" , "" );
        $('#cover2').hide();
        
        if( location.pathname.search("mgm_") !== -1 ){
                var data = {
                        token:     getCookie("lnbCookie"),
                        a_id:      data.a_id
                };
                var success_back = function( data ) {

                        var tmp = "";
                        data = JSON.parse( data );
                        if (data.success) {
                                var ms_json = JSON.parse( data.data['ms_json'] );
                                $("#sidebar_setting [h_target]").parent().hide();
                                if (ms_json) {
                                        $.each( ms_json , function( k , v ){
                                                $("#sidebar_setting [h_target="+v+"]").parent().show();
                                        });
                                        
                                }
                                else{
                                        $("#sidebar_setting [h_target]").parent().show();
                                }

                        } else {
                                $("#sidebar_setting [h_target]").parent().show();
                        }

                };
                var error_back = function( data ) {
                        $( ".list input[type=checkbox]" ).prop('checked', false);
                };
                $.Ajax( "POST" , "php/json_mgm_sidebar.php?func=read_mgm_sidebar_json" , data , "" , success_back , error_back);

        }
        
}

function logout_layout() {
        //未登入
        $( ".logout-block" ).hide();
        $( ".login-block" ).show();
        
        $('.top ul li.login.active').removeClass('active');
        
        // mobile show 登入box
        $( "#mobi-member [id=login-block]" ).show().css( "opacity" , "1" ).css( "z-index" , "" );
        // mobile and PC hide 已登入操作box
        $('[id=logout-block]').stop(true, false).animate({top:40, opacity:0}).hide().css( "z-index" , "" );
        $('#cover2').hide();
}

function remind_login() {
        //check header現在要trigger哪個
        show_remind( "請先登入" );
        if (window.matchMedia('(max-width: 768px)').matches) {
            $('#mobi-rbtn , #header .container #mobi-rbtn').trigger( "click" );
        } else {
            $('.top > ul > li.login').trigger( "mouseenter" );
        }
        
}

function logoutBtnEvent(){
        
        $("[id=logoutBtn]").unbind("click").bind("click",function(){
                
                $.ajax({
                    type:"POST",
                    dataType: "text",
                    url: "service/users/userLogout",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                        'Authorization': "Token "+getCookie( "lnbCookie" )
                    },
                    data: {
                    },
                    success: function(data){
                        console.log(data);
                        data = JSON.parse(data);
                        if( data.success ) 
                        {
                                delete_cookie( "lnbCookie" );
                                location.href = "login" ;
                        }
                        else{
                                location.href = "login" ;
                        }
                    },
                    error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                    }
                });
                
        });
        
}
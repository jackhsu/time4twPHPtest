/* 
 * Dear all,
 * please copy and paste the following code into your parts
 * there're three function, setGlobalValue, getGlobalValue and listGlobalValue.
 * thanks!
 */

// 直接從 ajax 複製即可，無須理會function。

/*
 * setGlobalValue
 * 此方法無法存取任何以 userinfo 有關之資料表
 * 此方法無法存取無主鍵之資料表(利用主鍵來更改)
 * 此方法無法更改資料表主索引鍵之值
 * 回傳值: {"success":true} (成功) / {"success":false, "msg": "Update data fail"} (失敗)
 */
function setGlobalValue() {
    
    $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/setGlobalValue",
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                        "global_oTableKeyValue": 96,        // 資料表主鍵值 ex: b_id = 96 (修改b_id=96的那筆資料)
                        "global_oTable": "board",           // 資料表名稱
                        "global_value": {"b_title": "0805_標題", "b_content": "0805_內容"}  // 欲修改的欄位值(單個欄位與多個都可)，只需輸入欲修改的欄位，無修改請不用輸入。
            },
            success: function(data){
                
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                        // 成功時此部分執行
                }
                else 
                {
                        // 失敗時此部分執行
                        show_remind( data.msg , "error" );
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError);

            }
    });
    
}

function setGlobalValue_add() {
    
    $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/setGlobalValue",
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                        "global_oTableKeyValue": null,        // 資料表主鍵值 ex: b_id = 96 (修改b_id=96的那筆資料)
                        "global_oTable": "board",           // 資料表名稱
                        "global_value": {"b_title": "0805_標題", "b_content": "0805_內容"}  // 欲修改的欄位值(單個欄位與多個都可)，只需輸入欲修改的欄位，無修改請不用輸入。
            },
            success: function(data){
                
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                        // 成功時此部分執行
                }
                else 
                {
                        // 失敗時此部分執行
                        show_remind( data.msg , "error" );
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError);

            }
    });
    
}


/*
 * getGlobalValue
 * 此方法無法存取任何以 userinfo 有關之資料表
 * 此方法無法存取無主鍵之資料表(利用主鍵來更改)
 * 回傳值: {"data":[{"a_id":"....","b_id":96,...],"success":true} (成功) / {"msg":"no result","success":false} (失敗)
 */
function getGlobalValue() {
    
    $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/getGlobalValue",
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                        "global_oTableKeyValue": 96,    // 資料表主鍵值 ex: b_id = 96 (修改b_id=96的那筆資料)
                        "global_oTable": "board",       // 資料表名稱
            },
            success: function(data){
                
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                        // 成功時此部分執行
                }
                else 
                {
                        // 失敗時此部分執行
                        show_remind( data.msg , "error" );
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError);

            }
    });
    
}


/*
 * listGlobalValue
 * 此方法無法存取任何以 userinfo 有關之資料表
 * 此方法無法存取不存在資料庫之資料表 
 * 回傳值: {data: [{a_id: "....", b_id: 55, b_title: "....",...},...], success: true} (成功) / {"msg":"no result","success":false} (失敗)
 */
function listGlobalValue() {
    
    $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/listGlobalValue",
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                        "global_oTable": "board"       // 資料表名稱
            },
            success: function(data){
                
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                        // 成功時此部分執行
                }
                else 
                {
                        // 失敗時此部分執行
                        show_remind( data.msg , "error" );
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError);

            }
    });
    
}


/*
 * 補充修改 getUserinfoValue
 * 根據 userinfo_InExUser 權限的不同來進行不同的操作
 * 除了 Administrator(管理員) 之外其他使用者只能存取自己的資料
 * 管理員可針對需要的資料使用 userinfo_id 進行資料存取
 */
function getUserinfoValue() {
    
     $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/getUserinfoValue",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': "Token "+getCookie( "lnbCookie" )
            },
            data: {
                "userinfo_id": 159, // 如不是 Administrator，此處會自動抓取 token，如是 Administrator 不指定此處則自動抓取自己的資料
                "userinfo_oTableColumn": "userinfo.userinfo_Area"
            },
            success: function(data){
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                      
                }
                else {
                      swal("ERROR", data.msg, "error");
                    
                }
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });
        
}


/*
 * 補充修改 setUserinfoValue
 * 根據 userinfo_InExUser 權限的不同來進行不同的操作
 * 除了 Administrator(管理員) 之外其他使用者只能修改自己的資料
 * 管理員可針對需要的資料使用 userinfo_id 進行資料修改
 */
function setUserinfoValue() {
    
     $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/setUserinfoValue",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': "Token "+getCookie( "lnbCookie" )
            },
            data: {
                "userinfo_id": 159, // 如不是 Administrator，此處會自動抓取 token，如是 Administrator 不指定此處則自動抓取自己的資料
                "userinfo_oTableColumn": "userinfo.userinfo_Area",
                "userinfo_value": "北北基"
            },
            success: function(data){
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                      
                }
                else {
                      swal("ERROR", data.msg, "error");
                    
                }
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });
        
}

/*
 * 
 * 
 * 
 * 
 */
function pushAPICommand1() {
    
    //聊天室閃並置頂API
    pushAPI( 1 , 1 , 18 );
//    "user": 1, // 推播給的user_id  BorrowerTest1@gmail.com
//    "command_id": 1,// 推播給的command_id
//    "command_user": 18// 事件對象的user_id InvestorTest1@gmail.com
        
}
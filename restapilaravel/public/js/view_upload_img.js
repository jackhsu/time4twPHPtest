$(document).ready(function() {
        
        upload_event();
        
});

function allow_subname( val ) {
        
        var subname;
        val = val || "all";
        
        switch( val ){
            case "jpg":
                subname = [ "jpg" ];
                break;
            case "images":
                subname = [ "jpg" , "jpeg" , "png" , "gif" ];
                break;
            case "all":
                subname = [ "chm" , "pdf" , "zip" , "rar" , "tar" ,
                            "gz" , "bzip2" , "gif" , "jpg" , "jpeg" ,
                            "png" , "torrent" , "bmp" , "txt" , "doc" , "docx" ,
                            "swf" , "7z" , "mp3" , "dat" , "avi" ,
                            "mpg" , "mpeg" , "rmvb" , "rm" , "wmv" ,
                            "wma" , "flv" , "mp4" , "asf" , "asx" ,
                            "mov" , "qt" , "wav" , "wax" , "3gp" ,
                            "vob" , "mkv" , "iso" , "tgz" ];
                break;
            case "php":
                subname = [ "php" ];
                break;
        }
        return subname;
}

function allow_size( val ) {
        
        var object;
        
        switch( val ){
            case "type1":
                object = {"width":323,"height":400};
                break;
            case "type2":
                object = {"width":323,"height":400};
                break;
            case "type3":
                object = {"width":1920,"height":1080};
                break;
            case "type4":
                object = {"width":950,"height":1467};
                break;
            case "banner1":
                object = {"width":1146,"height":556};
                break;
            case "banner2":
                object = {"width":825,"height":400};
                break;
            case "banner3":
                object = {"width":825,"height":499};
                break;
            case "banner4":
                object = {"width":256,"height":355};
                break;
        }
        return object;
}

function upload_event()
{
            
            $("[id=transient_file]").unbind('change').bind('change', function(e) {
                        
                        var JudgeFilesType = e.originalEvent.currentTarget.files[0].name.split(".");
                        JudgeFilesType = JudgeFilesType[JudgeFilesType.length-1];
                        JudgeFilesType = JudgeFilesType.toLowerCase();
                        
                        var allow = $( this ).attr( "allow" );
                        
                        if( typeof allow === "undefined" ){
                            
                        }
                        else{
                            allow = allow_subname( allow );
                            if( $.inArray( JudgeFilesType , allow ) === -1 ) {
                                show_remind( "格式錯誤，請上傳" + allow.join( "、" ) + "等格式之檔案" , "error" );
                            }
                        }
                            
                        
//                        if( $.inArray( JudgeFilesType , allow ) === -1 ) {
//                            show_remind( "格式錯誤，請上傳" + allow.join( "、" ) + "等格式之檔案" , "error" );
//                        }
//                        else {
                            var files = e.originalEvent.currentTarget.files;
                            
                            if( $( this ).attr( "size" ) ){
                                var size2 = $( this ).attr( "size" );
                                // check upload img width height
                                var _URL = window.URL || window.webkitURL;
                                var file, img;
                                if ((file = this.files[0])) {
                                    img = new Image();
//                                    img.setAttribute("size", size2);
                                    img.onload = function onload() {
//                                        var size = this.getAttribute("size");
                                        var size = allow_size( size2 );
                                        if( size.width !== this.width || size.height !== this.height ){
                                                show_remind( "正確尺寸寬:"+size.width+"，長"+size.height+"的圖片檔" , "error");
                                                return false;
                                        }
                                        handleFileUpload( files , $( "[id='bar'][target='"+$(e.target).attr("target")+"']" ) , "transient" , $(e.target).attr("target") );
                                    };
                                    img.src = _URL.createObjectURL(file);
                                }
                                
                            }
                            else{
                                handleFileUpload( files , $( "[id='bar'][target='"+$(e.target).attr("target")+"']" ) , "transient" , $(e.target).attr("target") );
                            }
//                        }
                        e.preventDefault();
            });
            
            $( ".clear_upload" ).unbind('click').bind('click', function(e) {
                    var target = $( this ).attr( "target" );
                    if( $("#"+target).prop("tagName")==="DIV" )
                        $( "#" + target ).css( "background-image" , "url('"+website_no_img_url+"')" );
                    else if( $("#"+target).prop("tagName")==="IMG" )
                        $( "#" + target ).attr( "src" , website_no_img_url );
                    $.upload_file[ target ] = "CLEAR";
            });
}

function handleFileUpload(files,obj,func,event)
{
        GetAllGamesModels(files,obj,func,event);
}

function GetAllGamesModels(files,obj,func,event){
        
        for (var tmp = 0; tmp < files.length; tmp++) 
        {
                
                console.log(this);
                var fd = new FormData();
                fd.append('file', files[tmp]);

                var status = new createStatusbar(obj); //Using this we can set progress.
                status.setFileNameSize(files[tmp].name,files[tmp].size);
                
                sendHtmlToServer(fd,files[tmp],status,func,event);//["1036_vincent_5-46","1037_vincent_5-47","1038_vincent_5-48"]
                
        }

}

var rowCount=0;
function createStatusbar(obj)
{
            rowCount++;
            var row="odd";
            if(rowCount %2 ===0) row ="even";
            this.statusbar = $("<div class='statusbar "+row+"'></div>");
            this.filename = $("<div class='filename'></div>").appendTo(this.statusbar);
            this.size = $("<div class='filesize'></div>").appendTo(this.statusbar);
            this.progressBar = $("<div class='progressBar'><div></div></div>").appendTo(this.statusbar);
            this.abort = $("<div class='abort'>Abort</div>").appendTo(this.statusbar);
            this.statusbar.attr("class","")
            obj.append(this.statusbar);

           this.setFileNameSize = function(name,size)
           {
                        var sizeStr="";
                        var sizeKB = size/1024;
                        if(parseInt(sizeKB) > 1024)
                        {
                                var sizeMB = sizeKB/1024;
                                sizeStr = sizeMB.toFixed(2)+" MB";
                        }
                        else
                        {
                                sizeStr = sizeKB.toFixed(2)+" KB";
                        }

                        this.filename.html(name);
                        this.size.html(sizeStr);
           };
           this.setProgress = function(progress)
           {
                        var progressBarWidth =progress*this.progressBar.width()/ 100;  
                        this.progressBar.find('div').animate({ width: progressBarWidth }, 10).html(progress + "% ");
                        if(parseInt(progress) >= 100)
                        {
                                this.abort.hide();
                                //bohan
                                this.abort.parent().prev().prev().show();
                                //this.abort.parent().next().children().attr( "src" , "" );
                                //this.abort.parent().next().show();
                                this.abort.parent().remove();
                        }
           };
           this.setAbort = function(jqxhr)
           {
                        var sb = this.statusbar;
                        this.abort.click(function()
                        {
                                jqxhr.abort();
                                if( $.upload_file != undefined && $.upload_file.upload == "transient_file" ) {
                                        $("#usericon").css("background-image",'url("template/assets/img/icon_uplaod-02.png")');
                                        $("#usericon").attr("img", "" );
                                        $("#cooperate_icon").css("background-image",'url("template/assets/img/icon_uplaod-02.png")');
                                        $("#cooperate_icon").attr("img", "" );
                                }
                                sb.hide();
                        });
           };
}

function sendHtmlToServer( formData , file , status , func , event , backup_name )
{
            var uploadURL;
            
            uploadURL ='service/chat/sendFile?'
                    + 'token=' + getCookie("hf_cookie")
                    + '&func=' + func
                    + '&Investor=' + $.pollingPara["Investor"]
                    + '&Borrower=' + $.pollingPara["Borrower"];
//                    + '&Image_name=' + $("#Add_Image_name").val()
//                    + '&Artist=' + $("#Add_Artist").val()
//                    + '&Type=' + $("#Add_Type").val()
//                    + '&Medium=' + $("#Add_Medium").val()
//                    + '&Style=' + $("#Add_Style").val()
//                    + '&AskingPrice=' + $("#Add_AskingPrice").val()
            
            
            var extraData ={};
            var jqXHR=$.ajaxq("aaa",{
                        xhr: function() {
                                    var xhrobj = $.ajaxSettings.xhr();
                                    if (xhrobj.upload) {
                                                xhrobj.upload.addEventListener('progress', function(event) {
                                                            var percent = 0;
                                                            var position = event.loaded || event.position;
                                                            var total = event.total;
                                                            if (event.lengthComputable) {
                                                                                    percent = Math.ceil(position / total * 100);
                                                            }
                                                            //Set progress
                                                            status.setProgress(percent);
                                                }, false);
                                    }
                                    return xhrobj;
                        },
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                    'Authorization': "Token "+getCookie("lnbCookie")
                        },
                        url: uploadURL,
                        type: "POST",
                        contentType:false,
                        processData: false,
                        cache: false,
                        data: formData,
                        data2 : event,
                        //dataType: "json",
                        success: function(data){
                                console.log(data);
                                var data = JSON.parse( data );
                                console.log(data);
                                if( data.success ){
                                    console.log("傳送成功");
                                }
                                else{
                                    show_remind( data.msg , "error" );
                                }
                                
                        }
            });

            status.setAbort(jqXHR);
}

var delete_transient_file = function( filename ) {
        $.ajax({
                    type: "POST",
                    url: "php/user_upload_image.php?func=del_transient",
                    data : {
                        transient_file : filename
                    },
                    success: function( data ) { return null; } ,
                    error: function( data ) {  }
        });
}
$(window).on('beforeunload', function(){
        $.each( $.upload_file_transient , function(index, value) {
                delete_transient_file( value );
        });
});
$(window).unload( function(){
        $.each( $.upload_file_transient , function(index, value) {
                delete_transient_file( value );
        });
});




function datatable_event()
{
            var tmp_Date = new Date().toLocaleDateString() ;

            $('#StartDate').unbind( 'keyup' ).bind( 'keyup' , function () {
                        var tmp_val = $(this).val().replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 5 )
                        {
                                $(this).val( tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 2 ) + "-" + tmp_val.substr( 6 , 2 ) );
                        }
            });

            $('#StartDate').val( tmp_Date.split( "/" )[0] + paddingLeft( tmp_Date.split( "/" )[1] , 2 ) + "01" );
            $('#StartDate').trigger( 'keyup' );


            $('#EndDate').unbind( 'keyup' ).bind( 'keyup' , function () {
                        var tmp_val = $(this).val().replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 5 )
                        {
                                $(this).val( tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 2 ) + "-" + tmp_val.substr( 6 , 2 ) );
                        }
            });
            $('#EndDate').val( tmp_Date.split( "/" )[0] + paddingLeft( tmp_Date.split( "/" )[1] , 2 ) + "30" );
            $('#EndDate').trigger( 'keyup' );


            $('#notificationBtn').unbind( 'keyup' ).bind( 'click' , function () {
                        sub_datatable_notification();
            });

            $('#normalSearchBtn').unbind( 'click' ).bind( 'click' , function () {
                        datatable_ad_search();
            });


            // $.dataDD
            var data = [] ;
            var ii = 0 ;
            $.each( $.login.dataDD , function(k,v){ 
                        if( v.userinfo_UserPhone != "" && v.userinfo_UserPhone != null )
                        {
                                    data[ii] = {};
                                    data[ii].id     = v.userinfo_ID ;
                                    data[ii].text   = v.userinfo_UserName + "@" + v.userinfo_UserPhone.toString() ;
                                    ii ++ ;
                        }
            });
            // var data = [{ id: 0, text: 'enhancement' }, { id: 1, text: 'bug' }, { id: 2, text: 'duplicate' }, { id: 3, text: 'invalid' }, { id: 4, text: 'wontfix' }];

            $.inputAddPhoneSearch = $("#inputAddPhoneSearch").select2({
                        // data: select.concat(data)
                        data: data 
            });

            $("#inputAddPhoneSearch").change(function(e) {
                        var data = $(this).select2('data')[0];
                        console.log( data );
                        var contact = data.text.split( "@" )[1];
                        // $( this ).html( data.text );
                        var tmp_val = contact.replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 9 )
                        {   
                                tmp_val = tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 3 ) + "-" + tmp_val.substr( 7 , 3 ) ;
                        }
                        $( "#inputAddPhone" ).val( tmp_val );
            });

//                            $("#inputAddPhone").change(function(e) {
//                                        var data = $(this).select2('data')[0];
//                                        var contact = data['contactinfo_Name'] + " " + data['contactinfo_PhoneNumber'];
//                                        $("#createContact").html(contact);
//                            });


            $('#inputAddPhone').on( 'keyup', function () {
                        var tmp_val = $(this).val().replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 9 )
                        {   
                                $(this).val( tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 3 ) + "-" + tmp_val.substr( 7 , 3 ) );
                        }

//                                        $( "#inputAddPhoneSearch" ).select2("open");
//                                        $( ".select2-search__field" ).val( $(this).val() ).trigger( "keyup" ); 

            });


            // Add event listener for opening and closing details
            $('#example1 tbody td.details-control0').unbind( 'click' ).bind( 'click' , function () {
                        var tr = $(this).closest('tr');
                        var row = $.initDatatable_member.row( tr );

                        if ( row.child.isShown() ) {
                                    // This row is already open - close it
                                    $('div.details-table,div.details-table1', row.child() ).slideUp( function () {
                                                row.child.hide();
                                                tr.removeClass('shown');
                                    });
                        }
                        else
                        {
                                    // Open this row
                                    row.child( format2( row.data() ), 'details-table' ).show();
                                    tr.addClass('shown');

                                    $( 'div.details-table,div.details-table1' , row.child() ).slideDown();

                                    datatable_select();


                                    sub_datatable_event0( tr.next() , $( this ) );

                        }


            });

            $('#example1 tbody td.details-control7').unbind( 'click' ).bind( 'click' , function () {
                        var tr = $(this).closest('tr');
                        var row = $.initDatatable_member.row( tr );

                        if ( row.child.isShown() ) {

                                $('div.details-table2', row.child()).slideUp( function () {
                                        row.child.hide();
                                        tr.removeClass('shown');
                                });

                        }
                        else
                        {
                                row.child( format3(row.data()), 'details-table2' ).show();
                                tr.addClass('shown');

                                $('div.details-table2', row.child() ).slideDown();

                                sub_datatable_event7( tr.next() , $( this ) );

                        }
            });

            $('#example1 tbody td').unbind( "dblclick" ).bind("dblclick",function(){
            // $('#example1 tbody').on('dblclick', 'td', function () {

                        var tr      = $(this).closest('tr');
                        var row     = $.initDatatable_member.row( tr );
                        var data    = row.data();

                        var tmp_focus = $(this).children( "div" ) ;
                        if( tmp_focus.length > 0 )
                        {
                                    var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                                    var tmp_focus_oinput        = tmp_focus.attr( "target-oinput" ) ;
                                    var tmp_focus_otype         = tmp_focus.attr( "target-otype" ) ;
                                    var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;

                                    $("[target-btn=cancel]").trigger('click');

                                    

                                    if( !( $.login.GetLoginPermissions == "Business" && tmp_focus_otablecolumn == "userinfo.userinfo_business" ) )
                                    if( tmp_focus.attr( "target-oInput" ) == 1 && tmp_focus.attr( "target-oType" ).split( "," ).length == 2 )
                                    {
                                                var tmp_value = tmp_focus.attr( "target-data" ) ;

                                                var tmp_html = '<form style="width: 300px !important;" target-value class="full-width" >' ;
                                                console.log( tmp_value );
                                                $.each( tmp_focus.attr( "target-oType" ).split( "," ) , function(k,v){ 
                                                            if( v == tmp_value )
                                                            tmp_html += "<input type='radio' name='location' value=" + v + " checked=checked >" + v + "" ;
                                                            else
                                                            tmp_html += "<input type='radio' name='location' value=" + v + " >" + v + "" ;
                                                });
                                                tmp_html += "</form>" ;


                                                $( this ).html(
                                                            tmp_html + 
                                                            '<button target-btn="save"      type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Save</button>' + 
                                                            '<button target-btn="cancel"    type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Cancel</button>'
                                                );

                                                $("[target-btn=save]").unbind('click').bind('click', function() {

                                                            tmp_value = $( this ).prev().children( "[name=location]:checked" ).val();
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );




                                                            show_remind( tmp_value + "儲存成功" , "success" );
                                                            // 送出參數
                                                            $.ajax({
                                                                    type:"POST",
                                                                    // dataType: "text",
                                                                    url: "service/users/setUserinfoValue",
                                                                    headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                                'Authorization': "Token "+getCookie("lnbCookie")
                                                                    },
                                                                    data: {
                                                                                userinfo_id : tmp_focus_id ,
                                                                                userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                                userinfo_value : tmp_value
                                                                    },
                                                                    success: function(data){

                                                                                console.log(data);
                                                                                data = JSON.parse(data);
                                                                                if( data.success ) {
                                                                                            show_remind( data.msg , "success" );
                                                                                }
                                                                                else 
                                                                                {
                                                                                            show_remind( data.msg , "error" );
                                                                                }
                                                                    },
                                                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                                        console.log(xhr.status); 
                                                                        console.log(thrownError); 
                                                                    }
                                                            });

                                                            console.log( "bohan save value" );
                                                            datatable_event();
                                                });

                                                $("[target-btn=cancel]").unbind('click').bind('click', function() {

                                                            // var tmp_value = $( "[target-value]" ).attr( "target-value" ) ;
                                                            // tmp_value = $( this ).prev().val();
                                                            
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );
                                                            console.log( "bohan cancel value" );
                                                            datatable_event();

                                                });

                                    }
                                    else if( tmp_focus.attr( "target-oInput" ) == 1 )
                                    {
                                                var tmp_value = tmp_focus.attr( "target-data" ) ;

                                                var tmp_html = '<select style="width: 100%;" target-value class="full-width" >' ;
                                                $.each( tmp_focus.attr( "target-oType" ).split( "," ) , function(k,v){ 
                                                            if( v == tmp_value )
                                                            tmp_html += "<option value=" + v + " selected=selected >" + v + "</option>" ;
                                                            else
                                                            tmp_html += "<option value=" + v + " >" + v + "</option>" ;
                                                });
                                                tmp_html += "</select>" ;


                                                $( this ).html(
                                                            tmp_html + 
                                                            '<button target-btn="save"      type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Save</button>' + 
                                                            '<button target-btn="cancel"    type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Cancel</button>'
                                                );

                                                $("[target-btn=save]").unbind('click').bind('click', function() {

                                                            tmp_value = $( this ).prev().val();
                                                            
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );


                                                            show_remind( tmp_value + "儲存成功" , "success" );
                                                            // 送出參數
                                                            $.ajax({
                                                                    type:"POST",
                                                                    // dataType: "text",
                                                                    url: "service/users/setUserinfoValue",
                                                                    headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                                'Authorization': "Token "+getCookie("lnbCookie")
                                                                    },
                                                                    data: {
                                                                                userinfo_id : tmp_focus_id ,
                                                                                userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                                userinfo_value : tmp_value
                                                                    },
                                                                    success: function(data){

                                                                                console.log(data);
                                                                                data = JSON.parse(data);
                                                                                if( data.success ) {
                                                                                            show_remind( data.msg , "success" );
                                                                                }
                                                                                else 
                                                                                {
                                                                                            show_remind( data.msg , "error" );
                                                                                }
                                                                    },
                                                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                                        console.log(xhr.status); 
                                                                        console.log(thrownError); 
                                                                    }
                                                            });

                                                            console.log( "bohan save value" );
                                                            datatable_event();
                                                });

                                                $("[target-btn=cancel]").unbind('click').bind('click', function() {

                                                            // var tmp_value = $( "[target-value]" ).attr( "target-value" ) ;
                                                            // tmp_value = $( this ).prev().val();
                                                            
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );
                                                            console.log( "bohan cancel value" );
                                                            datatable_event();

                                                });

                                    }
                                    else if( tmp_focus.attr( "target-oInput" ) == 0 )
                                    {
                                                var tmp_value = tmp_focus.attr( "target-data" ) ;
                                                
                                                
                                                if( tmp_focus_otablecolumn == "userinfo.userinfo_UserAge" )
                                                {
                                                        show_remind( "請輸入民國年" );
                                                        if( tmp_value.split( "-" ).length == 3 )
                                                        tmp_value = ( parseInt( tmp_value.split( "-" )[0] ) - 1911 ).toString() + "-" + tmp_value.split( "-" )[1] + "-" + tmp_value.split( "-" )[2] ;
                                                        else
                                                        tmp_value = "" ;
                                                }
                                                
                                                var tmp_html = '<textarea style="min-width: 300px; width: 100%; height: 300px;"  target-value="' + tmp_value + '"  class="form-control" placeholder="輸入文字" required="" type="text" >' + tmp_value + '</textarea>' ;


                                                $( this ).html(
                                                            tmp_html +
                                                            '<button target-btn="save"      type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Save</button>' + 
                                                            '<button target-btn="cancel"    type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Cancel</button>'

                                                );

                                                $("[target-btn=save]").unbind('click').bind('click', function() {

                                                            tmp_value = $( this ).prev().val();
                                                            
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );

                                                            
                                                            if( tmp_focus_otablecolumn == "userinfo.userinfo_UserAge" )
                                                            {
                                                                    tmp_value = ( parseInt( tmp_value.split( "-" )[0] ) + 1911 ).toString() + "-" + tmp_value.split( "-" )[1] + "-" + tmp_value.split( "-" )[2] ;
                                                            }

                                                            show_remind( tmp_value + "儲存成功" , "success" );
                                                            // 送出參數
                                                            $.ajax({
                                                                    type:"POST",
                                                                    // dataType: "text",
                                                                    url: "service/users/setUserinfoValue",
                                                                    headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                                'Authorization': "Token "+getCookie("lnbCookie")
                                                                    },
                                                                    data: {
                                                                                userinfo_id : tmp_focus_id ,
                                                                                userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                                userinfo_value : tmp_value
                                                                    },
                                                                    success: function(data){

                                                                                console.log(data);
                                                                                data = JSON.parse(data);
                                                                                if( data.success ) {
                                                                                            show_remind( data.msg , "success" );
                                                                                }
                                                                                else 
                                                                                {
                                                                                            show_remind( data.msg , "error" );
                                                                                }
                                                                    },
                                                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                                        console.log(xhr.status); 
                                                                        console.log(thrownError); 
                                                                    }
                                                            });

                                                            console.log( "bohan save value" );
                                                            datatable_event();
                                                });

                                                $("[target-btn=cancel]").unbind('click').bind('click', function() {

                                                            // var tmp_value = $( "[target-value]" ).attr( "target-value" ) ;
                                                            
                                                            sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );
                                                            console.log( "bohan cancel value" );
                                                            datatable_event();

                                                });
                                    }

                        }

            });

};



function sub_datatable_event0( focus_tr , focus_this )
{
            var focus = focus_tr ;
            var focus_save = focus.find( "[target-btn=sub-save]" );
            var focus_save_focus = $( "[target-id=" + focus_this.children( "div" ).attr( "target-id" ) + "][target-otablecolumn='userinfo_project.userinfo_project']" );
            focus_save.attr( "target-otablecolumn" , focus_save_focus.attr( "target-otablecolumn" ) );
            focus_save.attr( "target-id" , focus_save_focus.attr( "target-id" ) );
            focus_save.attr( "target-data" , focus_save_focus.attr( "target-data" ) );
            focus_save.attr( "target-otype" , focus_save_focus.attr( "target-otype" ) );
            
            var tmp_focus_table = focus.find( ".details-table1" ).find( "tbody" ).eq(2) ;
            if( focus_save_focus.attr( "target-data" ) != "" 
                    && focus_save_focus.attr( "target-data" ) != "null" 
                    && focus_save_focus.attr( "target-data" ) != undefined
                    )
            {
                    var tmp_focus_table_html = "" ;
                    var tmp_focus_table_arr = eval( eval( '"' + focus_save_focus.attr( "target-data" ) + '"' ) );
                    $.each( tmp_focus_table_arr , function(k,v){ 
                            tmp_focus_table_html = 
                            '<tr>' +
                                '<td>' + v[0] + '</td>' +
                                '<td>' + v[1] + '</td>' +
                                '<td>' + v[2] + '</td>' +
                                '<td>' + v[3] + '</td>' +
                                '<td>' + v[4] + '</td>' +
                                '<td>' + v[5] + '</td>' +
                                '<td>' + v[6] + '</td>' +
                            '</tr>' + tmp_focus_table_html  ;
                    });
                    tmp_focus_table.html( tmp_focus_table_html );
            }else{
                    tmp_focus_table.html( "" );
            }

                                      
            
    
    
    
            focus_save.eq(0).unbind( "click" ).bind("click",function(){

                        var tmp_focus = $(this) ;
                        var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                        var tmp_focus_table  = tmp_focus.parent().parent().parent().parent() ;

                        var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;

    
                        // 送出參數
                        $.ajax({
                                    type:"POST",
                                    // dataType: "text",
                                    url: "service/users/getUserinfoValue",
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                'Authorization': "Token "+getCookie("lnbCookie")
                                    },
                                    data: {
                                                userinfo_id : tmp_focus_id ,
                                                userinfo_oTableColumn : tmp_focus_otablecolumn 
                                    },
                                    success: function(data){

                                                console.log(data);
                                                data = JSON.parse(data);

                                                if( data.data == undefined )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value != "" 
                                                        && data.data.userinfo_value != "null" 
                                                        && data.data.userinfo_value != undefined
                                                        )
                                                var tmp_focus_data  = eval( eval( '"' + data.data.userinfo_value + '"' ) ) ;
                                                else
                                                var tmp_focus_data  = [] ;

                                                var tmp = [] ;
                                                tmp[0] = tmp_focus_table.find( "[target-btn=sub-date]" ).val() ;
                                                tmp[1] = tmp_focus_table.find( "[target-btn=sub-project]" ).val() ;
                                                tmp[2] = "" ;
                                                tmp[3] = "" ;
                                                tmp[4] = "" ;
                                                tmp[5] = "" ;
                                                tmp[6] = tmp_focus_table.find( "[target-btn=sub-business]" ).val() ;
                                                tmp_focus_data[ tmp_focus_data.length ] = tmp ;

                                                console.log( tmp_focus_data );

                                                tmp_focus_table.next().next().children( "tbody" ).prepend(
                                                        '<tr>' + 
                                                                '<td>' + tmp[0] + '</td>' + 
                                                                '<td>' + tmp[1] + '</td>' + 
                                                                '<td>' + tmp[2] + '</td>' + 
                                                                '<td>' + tmp[3] + '</td>' + 
                                                                '<td>' + tmp[4] + '</td>' + 
                                                                '<td>' + tmp[5] + '</td>' + 
                                                                '<td>' + tmp[6] + '</td>' + 
                                                        '</tr>'
                                                );

                                                $( "#example1 a[target-id=" + tmp_focus_id + "][target-otablecolumn='" + tmp_focus_otablecolumn + "']" )
                                                .html( tmp[0] + ' ' + tmp[6] + ' ' + tmp[1] );


                                                // 送出參數
                                                $.ajax({
                                                        type:"POST",
                                                        // dataType: "text",
                                                        url: "service/users/setUserinfoValue",
                                                        headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                    'Authorization': "Token "+getCookie("lnbCookie")
                                                        },
                                                        data: {
                                                                    userinfo_id : tmp_focus_id ,
                                                                    userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                    userinfo_value : JSON.stringify( tmp_focus_data )
                                                        },
                                                        success: function(data){

                                                                    console.log(data);
                                                                    data = JSON.parse(data);
                                                                    if( data.success ) {
                                                                                show_remind( data.msg , "success" );
                                                                    }
                                                                    else 
                                                                    {
                                                                                show_remind( data.msg , "error" );
                                                                    }
                                                        },
                                                        error:function(xhr, ajaxOptions, thrownError){ 
                                                                    console.log(xhr.status); 
                                                                    console.log(thrownError); 
                                                        }
                                                });
                                    },
                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                console.log(xhr.status); 
                                                console.log(thrownError); 
                                    }
                        });
                
            });
            
                                                
            focus_save.eq(1).unbind( "click" ).bind("click",function(){

                        var tmp_focus = $(this) ;
                        var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                        var tmp_focus_table  = tmp_focus.parent().parent().parent().parent() ;

                        var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;

    
                        // 送出參數
                        $.ajax({
                                    type:"POST",
                                    // dataType: "text",
                                    url: "service/users/getUserinfoValue",
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                'Authorization': "Token "+getCookie("lnbCookie")
                                    },
                                    data: {
                                                userinfo_id : tmp_focus_id ,
                                                userinfo_oTableColumn : tmp_focus_otablecolumn 
                                    },
                                    success: function(data){

                                                console.log(data);
                                                data = JSON.parse(data);

                
                                                if( data.data == undefined )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value != "" 
                                                        && data.data.userinfo_value != "null" 
                                                        && data.data.userinfo_value != undefined
                                                        )
                                                var tmp_focus_data  = eval( eval( '"' + data.data.userinfo_value + '"' ) ) ;
                                                else
                                                var tmp_focus_data  = [] ;

                                                var tmp = [] ;
                                                tmp[0] = tmp_focus_table.find( "[target-btn=sub-date]" ).val() ;
                                                tmp[1] = tmp_focus_table.find( "[target-btn=sub-project]" ).val() ;
                                                tmp[2] = tmp_focus_table.find( "[target-btn=sub-project-name]" ).val() ;
                                                tmp[3] = tmp_focus_table.find( "[target-btn=sub-project-content]" ).val() ;
                                                tmp[4] = tmp_focus_table.find( "[target-btn=sub-project-cost]" ).val() ;
                                                tmp[5] = tmp_focus_table.find( "[target-btn=sub-project-time]" ).val() ;
                                                tmp[6] = tmp_focus_table.find( "[target-btn=sub-business]" ).val() ;
                                                tmp_focus_data[ tmp_focus_data.length ] = tmp ;

                                                console.log( tmp_focus_data );

                                                tmp_focus_table.next().children( "tbody" ).prepend(
                                                        '<tr>' + 
                                                                '<td>' + tmp[0] + '</td>' + 
                                                                '<td>' + tmp[1] + '</td>' + 
                                                                '<td>' + tmp[2] + '</td>' + 
                                                                '<td>' + tmp[3] + '</td>' + 
                                                                '<td>' + tmp[4] + '</td>' + 
                                                                '<td>' + tmp[5] + '</td>' + 
                                                                '<td>' + tmp[6] + '</td>' + 
                                                        '</tr>'
                                                );

                                                $( "#example1 a[target-id=" + tmp_focus_id + "][target-otablecolumn='" + tmp_focus_otablecolumn + "']" )
                                                .html( tmp[0] + ' ' + tmp[6] + ' ' + tmp[1] );


                                                // 送出參數
                                                $.ajax({
                                                        type:"POST",
                                                        // dataType: "text",
                                                        url: "service/users/setUserinfoValue",
                                                        headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                    'Authorization': "Token "+getCookie("lnbCookie")
                                                        },
                                                        data: {
                                                                    userinfo_id : tmp_focus_id ,
                                                                    userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                    userinfo_value : JSON.stringify( tmp_focus_data )
                                                        },
                                                        success: function(data){

                                                                    console.log(data);
                                                                    data = JSON.parse(data);
                                                                    if( data.success ) {
                                                                                show_remind( data.msg , "success" );
                                                                    }
                                                                    else 
                                                                    {
                                                                                show_remind( data.msg , "error" );
                                                                    }
                                                        },
                                                        error:function(xhr, ajaxOptions, thrownError){ 
                                                                    console.log(xhr.status); 
                                                                    console.log(thrownError); 
                                                        }
                                                });
                                    },
                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                console.log(xhr.status); 
                                                console.log(thrownError); 
                                    }
                        });

            });
             
    
            var tmp_focus_table = focus.find( ".details-table" ).find( "tbody" ) ;
            
            
            
            tmp_focus_table.find( "td" ).unbind( 'autoinput' ).bind( 'autoinput' , function () {
                
                        var tmp_focus = $(this).children( "div" ) ;
                        if( tmp_focus.length > 0 )
                        {
                                    var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                                    var tmp_focus_oinput        = tmp_focus.attr( "target-oinput" ) ;
                                    var tmp_focus_otype         = tmp_focus.attr( "target-otype" ) ;
                                    var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;
                                    
                                    // $("[target-btn=cancel]").trigger('click');

                                    if( !( $.login.GetLoginPermissions == "Business" && tmp_focus_otablecolumn == "userinfo.userinfo_business" ) )
                                    if( tmp_focus.attr( "target-oInput" ) == 1 )
                                    {
                                                var tmp_value = tmp_focus.attr( "target-data" ) ;

                                                var tmp_html = '<select style="width: 100%; height: 70px;" target-value class="full-width" >' ;
                                                $.each( tmp_focus.attr( "target-oType" ).split( "," ) , function(k,v){ 
                                                            if( v == tmp_value )
                                                            tmp_html += "<option value=" + v + " selected=selected >" + v + "</option>" ;
                                                            else
                                                            tmp_html += "<option value=" + v + " >" + v + "</option>" ;
                                                });
                                                tmp_html += "</select>" ;


                                                $( this ).html(
                                                            tmp_html + 
                                                            '<button target-btn="save"      target-otablecolumn="' + tmp_focus_otablecolumn + '" target-oinput="' + tmp_focus_oinput + '" target-otype="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 90%;">Save</button>'
                                                            // '<button target-btn="cancel"    type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Cancel</button>'
                                                );

                                                $("[target-btn=save]").unbind('click').bind('click', function() {

                                                            tmp_value = $( this ).prev().val();
                                                            
                                                            var tmp_focus = $(this) ;
                                                            var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                                                            var tmp_focus_oinput        = tmp_focus.attr( "target-oinput" ) ;
                                                            var tmp_focus_otype         = tmp_focus.attr( "target-otype" ) ;
                                                            var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;
                                                            
                                                            // sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );

                                                            var tmp_row_focus = $.initDatatable_member.row( $( "div[target-id=" + tmp_focus_id + "][target-otablecolumn='userinfo.userinfo_CreateDateTime']" ).parent().parent() ) ;
                                                            console.log( tmp_row_focus );
                                                            tmp_row_focus.data()[ tmp_focus_otablecolumn.split( "." )[1] ] = tmp_value ;
                                                            tmp_row_focus.data( tmp_row_focus.data() );

                                                            show_remind( tmp_value + "儲存成功" , "success" );
                                                            // 送出參數
                                                            $.ajax({
                                                                    type:"POST",
                                                                    // dataType: "text",
                                                                    url: "service/users/setUserinfoValue",
                                                                    headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                                'Authorization': "Token "+getCookie("lnbCookie")
                                                                    },
                                                                    data: {
                                                                                userinfo_id : tmp_focus_id ,
                                                                                userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                                userinfo_value : tmp_value
                                                                    },
                                                                    success: function(data){

                                                                                console.log(data);
                                                                                data = JSON.parse(data);
                                                                                if( data.success ) {
                                                                                            show_remind( data.msg , "success" );
                                                                                }
                                                                                else 
                                                                                {
                                                                                            show_remind( data.msg , "error" );
                                                                                }
                                                                    },
                                                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                                        console.log(xhr.status); 
                                                                        console.log(thrownError); 
                                                                    }
                                                            });

                                                            console.log( "bohan save value" );
                                                });

                                                $("[target-btn=cancel]").unbind('click').bind('click', function() {

                                                            // var tmp_value = $( "[target-value]" ).attr( "target-value" ) ;
                                                            // tmp_value = $( this ).prev().val();
                                                            
                                                            // sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );
                                                            console.log( "bohan cancel value" );

                                                });

                                    }
                                    else if( tmp_focus.attr( "target-oInput" ) == 0 )
                                    {
                                                var tmp_value = tmp_focus.attr( "target-data" ) ;
                                                var tmp_html = '<textarea  target-value="' + tmp_value + '"  class="form-control" placeholder="輸入文字" required="" type="text" >' + tmp_value + '</textarea>' ;

                                                $( this ).html(
                                                            tmp_html +
                                                            '<button target-btn="save"      target-otablecolumn="' + tmp_focus_otablecolumn + '" target-oinput="' + tmp_focus_oinput + '" target-otype="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 90%;">Save</button>'
                                                            // '<button target-btn="cancel"    type="submit" class="btn btn-purple inline-block" style="margin-top: 5px;font-size: 10px;padding: 7px;width: 40%;">Cancel</button>'

                                                );

                                                $("[target-btn=save]").unbind('click').bind('click', function() {

                                                            tmp_value = $( this ).prev().val();
                                                            
                                                            var tmp_focus = $(this) ;
                                                            var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                                                            var tmp_focus_oinput        = tmp_focus.attr( "target-oinput" ) ;
                                                            var tmp_focus_otype         = tmp_focus.attr( "target-otype" ) ;
                                                            var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;
                                                            
                                                            // sub_datatable_layout_data( $( this ) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            // $( this ).parent().html( '<div target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data="' + tmp_value + '" >' + tmp_value + '</div>' );


                                                            var tmp_row_focus = $.initDatatable_member.row( $( "div[target-id=" + tmp_focus_id + "][target-otablecolumn='userinfo.userinfo_CreateDateTime']" ).parent().parent() ) ;
                                                            console.log( tmp_row_focus );
                                                            tmp_row_focus.data()[ tmp_focus_otablecolumn.split( "." )[1] ] = tmp_value ;
                                                            tmp_row_focus.data( tmp_row_focus.data() );
                                                            
                                                            show_remind( tmp_value + "儲存成功" , "success" );
                                                            // 送出參數
                                                            $.ajax({
                                                                    type:"POST",
                                                                    // dataType: "text",
                                                                    url: "service/users/setUserinfoValue",
                                                                    headers: {
                                                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                                'Authorization': "Token "+getCookie("lnbCookie")
                                                                    },
                                                                    data: {
                                                                                userinfo_id : tmp_focus_id ,
                                                                                userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                                userinfo_value : tmp_value
                                                                    },
                                                                    success: function(data){

                                                                                console.log(data);
                                                                                data = JSON.parse(data);
                                                                                if( data.success ) {
                                                                                            show_remind( data.msg , "success" );
                                                                                }
                                                                                else 
                                                                                {
                                                                                            show_remind( data.msg , "error" );
                                                                                }
                                                                    },
                                                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                                        console.log(xhr.status); 
                                                                        console.log(thrownError); 
                                                                    }
                                                            });

                                                            console.log( "bohan save value" );
                                                            datatable_event();
                                                });

                                                $("[target-btn=cancel]").unbind('click').bind('click', function() {

                                                            // var tmp_value = $( "[target-value]" ).attr( "target-value" ) ;
                                                            sub_datatable_layout_data( $(this) , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value );
                                                            console.log( "bohan cancel value" );
                                                            datatable_event();

                                                });



                                    }

                        }
            });
            tmp_focus_table.find( "td" ).trigger( 'autoinput' );
            

            $("[target-btn=sub-date]").unbind( 'keyup' ).bind( 'keyup' , function () {
                        var tmp_val = $(this).val().replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 5 )
                        {
                                $(this).val( tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 2 ) + "-" + tmp_val.substr( 6 , 2 ) );
                        }
            });
            

            $("[target-btn=sub-project-time]").unbind( 'keyup' ).bind( 'keyup' , function () {
                        var tmp_val = $(this).val().replace( /[-]/g , "" ) ;
                        if( tmp_val.length > 5 )
                        {
                                $(this).val( tmp_val.substr( 0 , 4 ) + "-" + tmp_val.substr( 4 , 2 ) + "-" + tmp_val.substr( 6 , 2 ) );
                        }
            });
            
            
            
            
}

function sub_datatable_event7( focus_tr , focus_this )
{
                        
                                                
            var focus = focus_tr ;
            var focus_save = focus.find( ".details-table21" ).find( "[target-btn=sub-save]" );
            var focus_save_focus = focus_this.children( "div" ).children( "a" );
            focus_save.attr( "target-otablecolumn" , focus_save_focus.attr( "target-otablecolumn" ) );
            focus_save.attr( "target-id" , focus_save_focus.attr( "target-id" ) );
            focus_save.attr( "target-data" , focus_save_focus.attr( "target-data" ) );
            focus_save.attr( "target-otype" , focus_save_focus.attr( "target-otype" ) );


            var tmp_focus_table = focus.find( ".details-table22" ).find( "tbody" ) ;
            if( focus_save_focus.attr( "target-data" ) != "" 
                    && focus_save_focus.attr( "target-data" ) != "null" 
                    && focus_save_focus.attr( "target-data" ) != undefined 
                    )
            {
                        var tmp_focus_id = focus_save.attr( "target-id" ) ;
                        var tmp_focus_otablecolumn = focus_save.attr( "target-otablecolumn" ) ;
                        // 送出參數
                        $.ajax({
                                    type:"POST",
                                    // dataType: "text",
                                    url: "service/users/getUserinfoValue",
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                'Authorization': "Token "+getCookie("lnbCookie")
                                    },
                                    data: {
                                                userinfo_id : tmp_focus_id ,
                                                userinfo_oTableColumn : tmp_focus_otablecolumn 
                                    },
                                    success: function(data){

                                                console.log(data);
                                                data = JSON.parse(data);

                                                if( data.data == undefined )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value == "NULL"  )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value != "" 
                                                        && data.data.userinfo_value != "null" 
                                                        && data.data.userinfo_value != undefined
                                                        )
                                                var tmp_focus_data  = eval( eval( '"' + data.data.userinfo_value + '"' ) ) ;
                                                else
                                                var tmp_focus_data  = [] ;

                                                var tmp_focus_table_html = "" ;
                                                var tmp_focus_table_arr = tmp_focus_data ;

                                                $.each( tmp_focus_table_arr , function(k,v){ 
                                                        tmp_focus_table_html = 
                                                        '<tr>' +
                                                            '<td>' + v[0] + '</td>' +
                                                            '<td>' + v[1] + '</td>' +
                                                            '<td>' + v[2] + '</td>' +
                                                            '<td>' + v[3] + '</td>' +
                                                        '</tr>' + tmp_focus_table_html  ;
                                                });
                                                tmp_focus_table.html( tmp_focus_table_html );
                                    },
                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                console.log(xhr.status); 
                                                console.log(thrownError); 
                                    }
                        });
                
            }else{
                    tmp_focus_table.html( "" );
            }

                                                
            focus_save.unbind( "click" ).bind("click",function(){

                        var tmp_focus = $(this) ;
                        var tmp_focus_id            = tmp_focus.attr( "target-id" ) ;
                        var tmp_focus_otablecolumn  = tmp_focus.attr( "target-otablecolumn" ) ;
                        var tmp_focus_table  = tmp_focus.parent().parent().parent().parent() ;

                        // 送出參數
                        $.ajax({
                                    type:"POST",
                                    // dataType: "text",
                                    url: "service/users/getUserinfoValue",
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                'Authorization': "Token "+getCookie("lnbCookie")
                                    },
                                    data: {
                                                userinfo_id : tmp_focus_id ,
                                                userinfo_oTableColumn : tmp_focus_otablecolumn 
                                    },
                                    success: function(data){

                                                console.log(data);
                                                data = JSON.parse(data);

                                                if( data.data == undefined )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value == "NULL"  )
                                                {
                                                var tmp_focus_data  = [] ;
                                                }
                                                else if( data.data.userinfo_value != "" 
                                                        && data.data.userinfo_value != "null" 
                                                        && data.data.userinfo_value != undefined
                                                        )
                                                var tmp_focus_data  = eval( eval( '"' + data.data.userinfo_value + '"' ) ) ;
                                                else
                                                var tmp_focus_data  = [] ;

                                                var tmp = [] ;
                                                tmp[0] = tmp_focus_table.find( "[target-btn=sub-report]" ).val() ;
                                                tmp[1] = tmp_focus_table.find( "[target-btn=sub-schedule]" ).val() ;
                                                tmp[2] = tmp_focus_table.find( "[target-btn=sub-business]" ).val() ;
                                                tmp[3] = new Date().getFullYear() + "-" + ( new Date().getMonth() + 1 ) + "-" + new Date().getDate() ;
                                                tmp_focus_data[ tmp_focus_data.length ] = tmp ;

                                                console.log( tmp_focus_data );

                                                tmp_focus_table.next().children( "tbody" ).prepend(
                                                        '<tr>' + 
                                                                '<td>' + tmp[0] + '</td>' + 
                                                                '<td>' + tmp[1] + '</td>' + 
                                                                '<td>' + tmp[2] + '</td>' + 
                                                                '<td>' + tmp[3] + '</td>' + 
                                                        '</tr>'
                                                );

                                                $( "a[target-id=" + tmp_focus_id + "][target-otablecolumn='" + tmp_focus_otablecolumn + "']" )
                                                .html( tmp[3] + ' ' + tmp[2] + ' ' + tmp[1] );

                                                // 送出參數
                                                $.ajax({
                                                        type:"POST",
                                                        // dataType: "text",
                                                        url: "service/users/setUserinfoValue",
                                                        headers: {
                                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                                                                    'Authorization': "Token "+getCookie("lnbCookie")
                                                        },
                                                        data: {
                                                                    userinfo_id : tmp_focus_id ,
                                                                    userinfo_oTableColumn : tmp_focus_otablecolumn ,
                                                                    userinfo_value : JSON.stringify( tmp_focus_data )
                                                        },
                                                        success: function(data){

                                                                    console.log(data);
                                                                    data = JSON.parse(data);
                                                                    if( data.success ) {
                                                                                show_remind( data.msg , "success" );
                                                                    }
                                                                    else 
                                                                    {
                                                                                show_remind( data.msg , "error" );
                                                                    }
                                                        },
                                                        error:function(xhr, ajaxOptions, thrownError){ 
                                                                    console.log(xhr.status); 
                                                                    console.log(thrownError); 
                                                        }
                                                });
                                    },
                                    error:function(xhr, ajaxOptions, thrownError){ 
                                                console.log(xhr.status); 
                                                console.log(thrownError); 
                                    }
                        });


            });
    
}

function sub_datatable_layout_data( focus , tmp_focus_otablecolumn , tmp_focus_oinput , tmp_focus_otype , tmp_focus_id , tmp_value )
{
            console.log( tmp_focus_otablecolumn );
            if( tmp_focus_otablecolumn == "userinfo.userinfo_UserAge" )
            {
                        if( tmp_value == null )
                                tmp_value = "0000-00-00" ;

                        var tmp_age = tmp_value.split( "-" )[0];
                        if( tmp_age == "0000" )
                            tmp_age = null ;
                        else    
                            tmp_age = $.login.year - 1911 - parseInt( tmp_age );

                        if( tmp_value.split( "-" ).length == 3 )
                        tmp_value = ( parseInt( tmp_value.split( "-" )[0] ) + 1911 ).toString() + "-" + tmp_value.split( "-" )[1] + "-" + tmp_value.split( "-" )[2] ;
                        else
                        tmp_value = "" ;
                    
                        focus.parent().html( '<div style="min-width:70px; max-width: 250px; max-height:60px; overflow:hidden;" target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data=\'' + tmp_value + '\' >' + tmp_age + '</div>' );
            }
            else
            focus.parent().html( '<div style="min-width:70px; max-width: 250px; max-height:60px; overflow:hidden;" target-oTableColumn="' + tmp_focus_otablecolumn + '" target-oInput="' + tmp_focus_oinput + '" target-oType="' + tmp_focus_otype + '" target-id="' + tmp_focus_id + '" target-data=\'' + tmp_value + '\' >' + tmp_value + '</div>' );
}

function sub_datatable_notification()
{
            console.log( "sub_datatable_notification()" );
            
            $.login.dataBB = [] ;
            var ii = 0 ;
            $.each( $.login.dataDD , function( k , v ){
                        if( v.userinfo_UserAge != null )
                        {
                                    var tmp = v.userinfo_UserAge.split( "-" ) ;
                                    if( tmp.length == 3 )
                                    {
                                                // if( tmp[1] == paddingLeft( $.login.month , 2 ) && tmp[0] == $.login.date )
                                                // if( tmp[1] == "10" && tmp[2] == "21" )
                                                if( tmp[1] == "10" )
                                                {
                                                        console.log( v );
                                                        $.login.dataBB[ ii ] = v ;
                                                        ii ++ ;
                                                }
                                    }

                        }

            });
            
            $( "#notificationBody" ).html(
                    
                        '<li>' +
                                '<a>' +
                                        '<div>' +
                                                '<i class="fa fa-envelope fa-fw"></i> 本周有 ' + $.login.dataBB.length + ' 個人生日' +
                                                '<span class="pull-right text-muted small">' + $.login.year + '-' + $.login.month + '-' + $.login.date + '</span>' +
                                        '</div>' +
                                '</a>' +
                        '</li>'
                        // '<li class="divider"></li>'
                                
            );
}

// 借方收藏
function collection_datatable_init() {

        // get collection data
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getGlobalValue",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTable": "collection",     
                },
                success: function(data){

                        console.log(data);
                        data = JSON.parse(data);
                        if( data.success ) { 
                                // render view
                                var totalrawdatatable = data.data;
                                var columnDefs = [] ;
                                
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_date'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return '<ul class="info" >' +
                                                                    '<li>' + 
                                                                        '<span>編號</span>' + 
                                                                        '<span>' + full['userinfo_origin'] + '</span>' +   
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>公司名稱</span>' + 
                                                                        '<span>' + full['userinfo_company'] + '</span>' +  
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>性別</span>' + 
                                                                        '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>地區</span>' + 
                                                                        '<span>' + full['userinfo_area'] + '</span>' +  
                                                                    '</li>' +
                                                                '</ul>' ;
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_expectrate'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_evaluation'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_servicearea'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_usertype'];
                                                }
                                        };  
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return '<div class="control">' + 
                                                                    '<a class="collect btnDelete" data_id="' + full['userinfo_origin'] + '">刪除收藏</a>' +     
                                                                    '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>' + 
                                                                    '<a class="more btnChat" href="#">金主聊聊</a>' + 
                                                                '</div>';
                                                }
                                        };

                                $('#example').DataTable({
                                            data : totalrawdatatable ,
                                            "columnDefs" : columnDefs ,
                                            "dom": 'tlip',
                                            "language": {
                                                "emptyTable": "",
                                                "zeroRecords": ""
                                            },
                                            "scrollX": true,       
                                            "order": [[ 1, "desc" ]]
                                });

                                $('#example tbody tr td .btnDelete').unbind( 'click' ).bind( 'click' , function () {
                                            delCollection( $(this).attr('data_id'), 'mgd' );
                                });
                                
                                $('.btnDetail').unbind('click').bind('click', function() {

                                        var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();
                                        
                                        $('.i_no').text( tab_data['userinfo_ID'] );
                                        $('.i_name').text( tab_data['userinfo_company'] );
                                        $('.i_gender').text( tab_data['userinfo_gender'] );
                                        $('.i_area').text( tab_data['userinfo_area'] );
                                        $('.i_exp').text( tab_data['userinfo_expectrate'] );
                                        $('.i_eval').text( tab_data['userinfo_evaluation'] );
                                        $('.i_serv').text( tab_data['userinfo_servicearea'] );
                                        $('.i_type').text( tab_data['userinfo_usertype'] );
                                });                                     
                        }
                        else {
                               return data.msg;
                        }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}  

// 企、個金主收藏
function mgi_collection_datatable_init() {

        // get collection data
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getGlobalValue",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTable": "collection",     
                },
                success: function(data){

                        console.log(data);
                        data = JSON.parse(data);
                        if( data.success ) { 
                                // render view
                                var totalrawdatatable = data.data;
                                var columnDefs = [] ;
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return '<ul class="info" >' +
                                                                        '<li>' + 
                                                                            '<span>編號</span>' + 
                                                                            '<span>' + full['userinfo_origin'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>性別</span>' + 
                                                                            '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>地區</span>' + 
                                                                            '<span>' + full['userinfo_area'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>職業</span>' + 
                                                                            '<span>' + full['userinfo_job'] + '</span>' +  
                                                                        '</li>' +
                                                                    '</ul>' ;
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanamounts'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanreason'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_evaluation'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_bankstatus'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_privatestatus'];
                                                    }
                                            };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return '<div class="control">' + 
                                                                    '<a class="collect btnDelete" data_id="' + full['userinfo_origin'] + '">刪除收藏</a>' + 
                                                                    '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>' + 
                                                                    '<a class="more btnChat" href="#">金主聊聊</a>' + 
                                                                '</div>';
                                                }
                                        };

                                $('#example').DataTable({
                                            data : totalrawdatatable ,
                                            "columnDefs" : columnDefs ,
                                            "dom": 'tlip',
                                            "language": {
                                                "emptyTable": "",
                                                "zeroRecords": ""
                                            },
                                            "scrollX": true,       
                                            "order": [[ 1, "desc" ]]
                                });

                                $('#example tbody tr td .btnDelete').unbind( 'click' ).bind( 'click' , function () {
                                            delCollection( $(this).attr('data_id'), 'mgi' );
                                });

                                $('.btnDetail').unbind('click').bind('click', function() {
                                        if( typeof $.login !== "undefined" ) {
                                                var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();

                                                    $('.b-no').text( tab_data['userinfo_ID'] );
                                                    $('.b-gender').text( tab_data['userinfo_gender'] );
                                                    $('.b-area').text( tab_data['userinfo_area'] );
                                                    $('.b-job').text( tab_data['userinfo_job'] );
                                                    $('.b-amt').text( tab_data['userinfo_loanamounts'] );
                                                    $('.b-eval').text( tab_data['userinfo_evaluation'] );
                                                    $('.b-reason').text( tab_data['userinfo_loanreason'] );
                                                    $('.b-bank').text( tab_data['userinfo_bankstatus'] );
                                                    $('.b-priv').text( tab_data['userinfo_privatestatus'] );
                                        }                                    
                                });                                  
                        }
                        else {
                               return data.msg;
                        }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}  

// 借方被瀏覽紀錄
function history_datatable_init() {

        // get collection data
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getGlobalValue",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTable": "history",     
                },
                success: function(data){

                        console.log(data);
                        data = JSON.parse(data);
                        if( data.success ) { 
                                // render view
                                var totalrawdatatable = data.data;
                                var columnDefs = [] ;
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return '<ul class="info" >' +
                                                                        '<li>' + 
                                                                            '<span>編號</span>' + 
                                                                            '<span>' + full['userinfo_ID'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>性別</span>' + 
                                                                            '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>地區</span>' + 
                                                                            '<span>' + full['userinfo_area'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>職業</span>' + 
                                                                            '<span>' + full['userinfo_job'] + '</span>' +  
                                                                        '</li>' +
                                                                    '</ul>' ;
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanamounts'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanreason'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_evaluation'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_bankstatus'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_privatestatus'];
                                                    }
                                            };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'className': 'nowrap',
                                                'render': function(data, type, full, meta) {
                                                        return '<div class="control">' + 
                                                                    '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>' + 
                                                                '</div>';
                                                }
                                        };

                                $('#example').DataTable({
                                            data : totalrawdatatable ,
                                            "columnDefs" : columnDefs ,
                                            "dom": 'tlip',
                                            "language": {
                                                "emptyTable": "",
                                                "zeroRecords": ""
                                            },
                                            "scrollX": true,       
                                            "order": [[ 1, "desc" ]]
                                });

                                $('.btnDetail').unbind('click').bind('click', function() {
                                        if( typeof $.login !== "undefined" ) {
                                                var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();

                                                $('.h-no').text( tab_data['userinfo_ID'] );
                                                $('.h-gender').text( tab_data['userinfo_gender'] );
                                                $('.h-area').text( tab_data['userinfo_area'] );
                                                $('.h-job').text( tab_data['userinfo_job'] );
                                                $('.h-amt').text( tab_data['userinfo_loanamounts'] );
                                                $('.h-eval').text( tab_data['userinfo_evaluation'] );
                                                $('.h-reason').text( tab_data['userinfo_loanreason'] );
                                                $('.h-bank').text( tab_data['userinfo_bankstatus'] );
                                                $('.h-priv').text( tab_data['userinfo_privatestatus'] );
                                        }                                    
                                });                                 
                        }
                        else {
                               return data.msg;
                        }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}  

// 企、個金主瀏覽紀錄
function mgi_history_datatable_init() {

        // get collection data
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/getGlobalValue",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': "Token "+getCookie( "lnbCookie" )
                },
                beforeSend: function (xhr) {
                        var token = $('meta[name="csrf_token"]').attr('content');
                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTable": "history",     
                },
                success: function(data){

                        console.log(data);
                        data = JSON.parse(data);
                        if( data.success ) { 
                                // render view
                                var totalrawdatatable = data.data;
                                var columnDefs = [] ;
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return '<ul class="info" >' +
                                                                    '<li>' + 
                                                                        '<span>編號</span>' + 
                                                                        '<span>' + full['userinfo_ID'] + '</span>' +  
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>公司名稱</span>' + 
                                                                        '<span>' + full['userinfo_company'] + '</span>' +  
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>性別</span>' + 
                                                                        '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                    '</li>' +
                                                                    '<li>' + 
                                                                        '<span>地區</span>' + 
                                                                        '<span>' + full['userinfo_area'] + '</span>' +  
                                                                    '</li>' +
                                                                '</ul>' ;
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_expectrate'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_evaluation'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_servicearea'];
                                                }
                                        };
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'render': function(data, type, full, meta) {
                                                        return full['userinfo_usertype'];
                                                }
                                        };  
                                columnDefs[ columnDefs.length ] =
                                        {
                                                'targets': [ columnDefs.length ],
                                                'searchable': true,
                                                'className': 'nowrap',
                                                'render': function(data, type, full, meta) {
                                                        return '<div class="control">' + 
                                                                    '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>' + 
                                                                '</div>';
                                                }
                                        };

                                $('#example').DataTable({
                                            data : totalrawdatatable ,
                                            "columnDefs" : columnDefs ,
                                            "dom": 'tlip',
                                            "language": {
                                                "emptyTable": "",
                                                "zeroRecords": ""
                                            },
                                            "scrollX": true,       
                                            "order": [[ 1, "desc" ]]
                                });
                                
                                $('.btnDetail').unbind('click').bind('click', function() {

                                        var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();

                                        $('.i_no').text( tab_data['userinfo_ID'] );
                                        $('.i_name').text( tab_data['userinfo_company'] );
                                        $('.i_gender').text( tab_data['userinfo_gender'] );
                                        $('.i_area').text( tab_data['userinfo_area'] );
                                        $('.i_exp').text( tab_data['userinfo_expectrate'] );
                                        $('.i_eval').text( tab_data['userinfo_evaluation'] );
                                        $('.i_serv').text( tab_data['userinfo_servicearea'] );
                                        $('.i_type').text( tab_data['userinfo_usertype'] );
                                });                       
                                
                        }
                        else {
                               return data.msg;
                        }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}  

function setCollection(data) {

        var today = new Date();
        var date  = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var insert_data;
        if( $.login.userInfo.userinfo_InExUser.toLowerCase() == "borrower"  ) {
                insert_data = {
                                "userinfo_ID"          : $.login.userInfo['userinfo_ID'],
                                "userinfo_origin"      : data['userinfo_ID'],
                                "userinfo_date"        : date,
                                "userinfo_company"     : data['userinfo_company'],
                                "userinfo_gender"      : data['userinfo_gender'],
                                "userinfo_area"        : data['userinfo_area'],
                                "userinfo_expectrate"  : data['userinfo_expectrate'],
                                "userinfo_evaluation"  : data['userinfo_evaluation'],
                                "userinfo_servicearea" : data['userinfo_servicearea'],
                                "userinfo_usertype"    : data['userinfo_usertype']
                              }  
        } else {
                insert_data = {
                                "userinfo_ID"            : $.login.userInfo['userinfo_ID'],
                                "userinfo_origin"        : data['userinfo_ID'],
                                "userinfo_date"          : date,
                                "userinfo_gender"        : data['userinfo_gender'],
                                "userinfo_area"          : data['userinfo_area'],
                                "userinfo_job"           : data['userinfo_job'],
                                "userinfo_loanamounts"   : data['userinfo_loanamounts'],
                                "userinfo_evaluation"    : data['userinfo_evaluation'],  
                                "userinfo_loanreason"    : data['userinfo_loanreason'],
                                "userinfo_bankstatus"    : data['userinfo_bankstatus'],
                                "userinfo_privatestatus" : data['userinfo_privatestatus']
                              }  
        }

        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/setCollection",
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                           'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],
                        "global_oTable": "collection",           
                        "global_value": insert_data
                },
                success: function(data){
                            console.log(data);  
                            data = JSON.parse(data); 
                            if( data.success ) { 
                                    swal("成功","加入收藏成功", "success");
                            }
                            else {
                                    console.log('Error: '+data.msg);
                            }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}   

function setHistory(data) {

        var today = new Date();
        var date  = today.getFullYear() + '/' + (today.getMonth() + 1) + '/' + today.getDate();
        var insert_data;
        if( $.login.userInfo.userinfo_InExUser.toLowerCase() == "borrower"  ) {
                insert_data = {
                                "userinfo_ID"          : data['userinfo_ID'],
                                "userinfo_origin"      : $.login.userInfo['userinfo_ID'],
                                "userinfo_date"        : date, 
                                "userinfo_company"     : data['userinfo_company'],
                                "userinfo_gender"      : data['userinfo_gender'],
                                "userinfo_area"        : data['userinfo_area'],
                                "userinfo_expectrate"  : data['userinfo_expectrate'],
                                "userinfo_evaluation"  : data['userinfo_evaluation'],
                                "userinfo_servicearea" : data['userinfo_servicearea'],
                                "userinfo_usertype"    : data['userinfo_usertype']
                              }  
        } else {
                insert_data = {
                                "userinfo_ID"            : data['userinfo_ID'],
                                "userinfo_origin"        : $.login.userInfo['userinfo_ID'],
                                "userinfo_date"          : date,
                                "userinfo_gender"        : data['userinfo_gender'],
                                "userinfo_area"          : data['userinfo_area'],
                                "userinfo_job"           : data['userinfo_job'],
                                "userinfo_loanamounts"   : data['userinfo_loanamounts'],
                                "userinfo_evaluation"    : data['userinfo_evaluation'],  
                                "userinfo_loanreason"    : data['userinfo_loanreason'],
                                "userinfo_bankstatus"    : data['userinfo_bankstatus'],
                                "userinfo_privatestatus" : data['userinfo_privatestatus']
                              }  
        }
  
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/setCollection",
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                           'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],
                        "global_oTable": "history",           
                        "global_value": insert_data
                },
                success: function(data){
                        console.log(data);  
                        data = JSON.parse(data); 
                        if( !data.success ) { 
                                console.log('Error: '+data.msg);
                        }

                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}   

function delCollection(seq, src) {

        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/delCollection",
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                           'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTableKeyValue2": seq,  
                        "global_oTable": "collection",     
                },
                success: function(data){
                            console.log(data);  
                            data = JSON.parse(data); 
                            if( data.success ) { 
                                    $('#example').DataTable().clear();
                                    $('#example').DataTable().destroy();
                                    if( src == 'mgd' ) {
                                            collection_datatable_init();
                                    } else {
                                            mgi_collection_datatable_init();
                                    }                                        
                                    swal("成功","刪除收藏名單", "success");
                            }
                            else {
                                console.log('Error: '+data.msg);
                            }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}         

function delHistory(src = null) {

        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/delHistory",
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                        'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                        var token = $('meta[name="csrf-token"]').attr('content');
                        if (token) {
                              return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        }
                },
                data: {
                        "global_oTableKeyValue": $.login.userInfo['userinfo_ID'],  
                        "global_oTable": "history",     
                },
                success: function(data){
                        console.log(data);  
                        data = JSON.parse(data); 
                        if( data.success ) { 
                                $('#example').DataTable().clear().draw();
                                $('#example').DataTable().destroy();
                                if( src == 'mgd' ) {
                                        history_datatable_init();
                                } else {
                                        mgi_history_datatable_init();
                                }   
                                swal("成功","刪除歷史紀錄成功", "success");
                        }
                        else {
                                console.log('Error: '+data.msg);
                        }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                        console.log(xhr.status); 
                        console.log(thrownError); 
                }
        });
}    

//借款人列表
function getBorrowersList() {

        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/listGlobalValue",
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                           'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                },
                data: {
                            "global_oTable" : 'borrowers_list' 
                },
                success: function(data){
                            console.log(data);  
                            data = JSON.parse(data); 
                            if( data.success ) { 
                                    // render view
                                    var totalrawdatatable = data.data;
                                    var columnDefs = [] ;
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return '<ul class="info" >' +
                                                                        '<li>' + 
                                                                            '<span>編號</span>' + 
                                                                            '<span>' + full['userinfo_ID'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>性別</span>' + 
                                                                            '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>地區</span>' + 
                                                                            '<span>' + full['userinfo_area'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>職業</span>' + 
                                                                            '<span>' + full['userinfo_job'] + '</span>' +  
                                                                        '</li>' +
                                                                    '</ul>' ;
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanamounts'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_loanreason'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_evaluation'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_bankstatus'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_privatestatus'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'className': 'nowrap',
                                                    'render': function(data, type, full, meta) {
                                                            
                                                            var button_html = '<div class="control nowrap">';
                                                            
                                                            if( typeof $.login == "undefined" ) {
                                                                    button_html += '<a class="collect">加入收藏</a>';
                                                            } else {
                                                                    if( $.login.userInfo.userinfo_InExUser.toLowerCase() != "borrower"  ) {
                                                                            button_html += '<a class="collect">加入收藏</a>';
                                                                    }
                                                            }
                                                        
                                                            button_html += '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>'
                                                            if( $.login.userInfo.userinfo_InExUser.toLowerCase() != "borrower"  ) {
                                                                    button_html += '<a class="more btnChat">借方聊聊</a>';
                                                            }
                                                            
                                                            button_html += '</div>';
                                                            return  button_html;
                                                    }
                                            };

                                    var tmp_lengthMenu = [[ 5 , 10, 25, 50, -1], [ 5 , 10, 25, 50, "All"]]  ;
                                    $('#example').DataTable({
                                                "pageLength": 5,
                                                "lengthMenu": tmp_lengthMenu ,
                                                "data" : totalrawdatatable ,
                                                "columnDefs" : columnDefs ,
                                                "dom": 'tlip',
                                                "language": {
                                                    "emptyTable": "",
                                                    "zeroRecords": ""
                                                },
                                                "scrollX": true,       
                                                "order": [[ 1, "desc" ]]
                                    });

                                    $('.collect').unbind('click').bind('click', function() {
                                            if( typeof $.login !== "undefined" ) {
                                                    setCollection( $('#example').DataTable().row( $(this).parent().parent().parent() ).data() );
                                            }                                    
                                    });

                                    $('.btnDetail').unbind('click').bind('click', function() {
                                            if( typeof $.login !== "undefined" ) {

                                                    var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();
    
                                                    $('.b-no').text( tab_data['userinfo_ID'] );
                                                    $('.b-gender').text( tab_data['userinfo_gender'] );
                                                    $('.b-area').text( tab_data['userinfo_area'] );
                                                    $('.b-job').text( tab_data['userinfo_job'] );
                                                    $('.b-amt').text( tab_data['userinfo_loanamounts'] );
                                                    $('.b-eval').text( tab_data['userinfo_evaluation'] );
                                                    $('.b-reason').text( tab_data['userinfo_loanreason'] );
                                                    $('.b-bank').text( tab_data['userinfo_bankstatus'] );
                                                    $('.b-priv').text( tab_data['userinfo_privatestatus'] );
                                    
                                                    if( $.login.userInfo.userinfo_InExUser.toLowerCase() != "borrower" ) {
                                                            setHistory( $('#example').DataTable().row( $(this).parent().parent().parent() ).data() );        
                                                    }
                                            }                                    
                                    });                        
                            }
                            else {
                                   return data.msg;
                            }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}   

//企業、投資人列表
function getInvestorList() {
  
        $.ajax({
                type:"POST",
                dataType: "text",
                url: "service/users/listGlobalValue",
                headers: {
                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                           'Authorization': "Token "+getCookie("lnbCookie")
                },
                beforeSend: function (xhr) {
                            var token = $('meta[name="csrf-token"]').attr('content');
                            if (token) {
                                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                            }
                },
                data: {
                            "global_oTable" : 'investor_list' 
                }, 
                success: function(data){
                            console.log(data);  
                            data = JSON.parse(data); 
                            if( data.success ) { 
                                    // render view
                                    var totalrawdatatable = data.data;
                                    var columnDefs = [] ; 
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return '<ul class="info" >' +
                                                                        '<li>' + 
                                                                            '<span>編號</span>' + 
                                                                            '<span>' + full['userinfo_ID'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>公司名稱</span>' + 
                                                                            '<span>' + full['userinfo_company'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>性別</span>' + 
                                                                            '<span>' + full['userinfo_gender'] + '</span>' +  
                                                                        '</li>' +
                                                                        '<li>' + 
                                                                            '<span>地區</span>' + 
                                                                            '<span>' + full['userinfo_area'] + '</span>' +  
                                                                        '</li>' +
                                                                    '</ul>' ;
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_expectrate'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_evaluation'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_servicearea'];
                                                    }
                                            };
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'render': function(data, type, full, meta) {
                                                            return full['userinfo_usertype'];
                                                    }
                                            };                                            
                                    columnDefs[ columnDefs.length ] =
                                            {
                                                    'targets': [ columnDefs.length ],
                                                    'searchable': true,
                                                    'className': 'nowrap',
                                                    'render': function(data, type, full, meta) {
                                                            
                                                            var button_html = '<div class="control nowrap">';
                                                            
                                                            if( typeof $.login == "undefined" ) {
                                                                    button_html += '<a class="collect">加入收藏</a>';
                                                            } else {
                                                                    if( $.login.userInfo.userinfo_InExUser.toLowerCase() == "borrower"  ) {
                                                                            button_html += '<a class="collect">加入收藏</a>';
                                                                    }
                                                            }
                                                        
                                                            button_html += '<a class="more btnDetail" data-toggle="modal" data-target="#myModal">查看內容</a>'

                                                            if( $.login.userInfo.userinfo_InExUser.toLowerCase() == "borrower"  ) {
                                                                    button_html += '<a class="more btnChat">金主聊聊</a>';
                                                            }
                                                            
                                                            button_html += '</div>';
                                                            
                                                            return  button_html;
                                                    }
                                            };

                                    var tmp_lengthMenu = [[ 5 , 10, 25, 50, -1], [ 5 , 10, 25, 50, "All"]]  ;
                                    $('#example').DataTable({
                                                "pageLength": 5,
                                                "lengthMenu": tmp_lengthMenu ,
                                                "data" : totalrawdatatable ,
                                                "columnDefs" : columnDefs ,
                                                "dom": 'tlip',
                                                "language": {
                                                    "emptyTable": "",
                                                    "zeroRecords": ""
                                                },
                                                "scrollX": true,       
                                                "order": [[ 1, "desc" ]]
                                    });

                                    $('.collect').unbind('click').bind('click', function() {
                                            if( typeof $.login !== "undefined" ) {
                                                    setCollection( $('#example').DataTable().row( $(this).parent().parent().parent() ).data() );
                                            }                                    
                                    });

                                    $('.btnDetail').unbind('click').bind('click', function() {

                                            var tab_data = $('#example').DataTable().row( $(this).parent().parent().parent() ).data();

                                            $('.i_no').text( tab_data['userinfo_ID'] );
                                            $('.i_name').text( tab_data['userinfo_company'] );
                                            $('.i_gender').text( tab_data['userinfo_gender'] );
                                            $('.i_area').text( tab_data['userinfo_area'] );
                                            $('.i_exp').text( tab_data['userinfo_expectrate'] );
                                            $('.i_eval').text( tab_data['userinfo_evaluation'] );
                                            $('.i_serv').text( tab_data['userinfo_servicearea'] );
                                            $('.i_type').text( tab_data['userinfo_usertype'] );

                                            if( $.login.userInfo.userinfo_InExUser.toLowerCase() == "borrower" ) {
                                                    setHistory( $('#example').DataTable().row( $(this).parent().parent().parent() ).data() );        
                                            }                                
                                    });                       
                            } 
                            else {
                                    return data.msg;
                            }
                },
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
        });
}   


/*
 * datatable 使用的  dataPicker
 *  auth : jack
 */
function datePickerPlugin_StartDateEndDate( callback )
{
        //Data picker
        $( "#StartDate, #EndDate" ).datepicker({
                todayHighlight: true
        }).on( 'hide' , function(e) {
                $("#dateController button").removeClass('btn-primary').addClass('btn-white');
            
                if( callback != undefined ) {
                        callback();
                }
        });
        
        //預設範圍為當月
        $("#StartDate").datepicker("setDate", moment().startOf('month').format("YYYY-MM-DD"));
        $("#EndDate").datepicker("setDate", moment().endOf('month').format("YYYY-MM-DD"));
    
        //日期按鈕
        $("#dateController button").unbind('click').bind('click', function() {
            
                $("#dateController button").removeClass('btn-primary').addClass('btn-white');
                $(this).removeClass('btn-white').addClass('btn-primary');

                switch($(this).val()) {
                    case "yesterday":
                        $("#StartDate, #EndDate").datepicker("setDate", moment().subtract(1, 'day').format("YYYY-MM-DD"));
                        break;
                    case "today":
                        $("#StartDate, #EndDate").datepicker("setDate", moment().format("YYYY-MM-DD"));
                        break;
                    case "tomorrow":
                        $("#StartDate, #EndDate").datepicker("setDate", moment().add(1, 'day').format("YYYY-MM-DD"));
                        break;
                    default:
                        break;
                }

                if( callback != undefined ) {
                        callback();
                }
        });
        
        if( callback != undefined ) {
                callback();
        }
}
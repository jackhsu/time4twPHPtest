
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}

// init
function googleapis_initMap( tag , focus , lat , lng , zoom ) {
    
        if ($.login.googleMap == undefined)
                $.login.googleMap = {};

        if ($.login.googleMap[tag] == undefined) {
            
                $.login.googleMap[tag] = {} ;

                $.login.googleMap[tag].map 
                        = new google.maps.Map(
                                focus[0], 
                                {
                                        center: {
                                                lat: lat,
                                                lng: lng
                                        },
                                        zoom: zoom,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                }
                        );

                if ( $.login.googleMap.geocoder == undefined)
                $.login.googleMap.geocoder
                        = new google.maps.Geocoder();

                // evnet
                google.maps.event.addListener( $.login.googleMap[tag].map , 'click' , function(event) {

                        console.log(event.latLng);
                        if (this.marker != undefined)
                                this.marker.setMap(null);

                        this.marker = new google.maps.Marker({
                                position: event.latLng ,
                                map     : this ,
                                title   : tag
                        });
                        
                        var tmp_marker = this.marker ;
                        var tmp_map    = this ;

                        // 將經緯度透過 Google map Geocoder API 反查地址
                        $.login.googleMap.geocoder.geocode({
                          'latLng': event.latLng
                        }, function(results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                if (results) {
                                    // 將取得的資訊傳入 marker 訊息泡泡
                                    showAddress( tmp_map , results[0], tmp_marker );
                                }
                            } else {
                                alert("Reverse Geocoding failed because: " + status);
                            }
                        });
                });

                // 設定 marker 的訊息泡泡
                function showAddress( map , result, marker ) {
                    
                        if (  $.login.googleMap.infoWindow != undefined )
                        $.login.googleMap.infoWindow.setMap(null);

                        // $.login.googleMap.popup = new google.maps.InfoWindow({map: map});
                        $.login.googleMap.infoWindow = new google.maps.InfoWindow();

                        map.setCenter( marker.getPosition() );
                        
                        console.log( result );
                        // 顯示傳入的地址資訊
                        $.login.googleMap.infoWindow.setContent( '<a id="infoWindowBtn" target-pos="' + result.geometry.location.lat() + ',' + result.geometry.location.lng() + '" target-address="' +  result.formatted_address + '" class="btn btn-white btn-bitbucket"> Save Address & GPS </a>'  );
                        $.login.googleMap.infoWindow.open( map , marker );
                        
                        $( "#googleMapSearchInput" ).val( result.formatted_address );
                        $( "[id=infoWindowBtn]" ).unbind('click').bind('click',function(){

                                    if (  $.login.googleMap.infoWindow != undefined )
                                    $.login.googleMap.infoWindow.setMap(null);

                                    show_remind( "save success" );
                                    $( "[name='jobsite_Address.address_Latitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[0] );
                                    $( "[name='jobsite_Address.address_Longitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[1] );
                                    $( "[name='jobsite_Address.address_Address']" ).val(  $( this ).attr( "target-address" ) );

                                    $("#myModalMap").data("target").find("span").html(

                                                $( "[name='jobsite_Address.address_PostalCode']" ).val() + " " +
                                                $( "[name='jobsite_Address.address_Address']" ).val() + " " +
                                                $( "[name='jobsite_Address.address_City']" ).val() + " " +
                                                $( "[name='jobsite_Address.address_Latitude']" ).val() + " " +
                                                $( "[name='jobsite_Address.address_Longitude']" ).val() 

                                    );

                        });
                }

                var map = $.login.googleMap[tag].map ;

                // center
                
                
                // Try HTML5 geolocation.
                if( $( "[name='jobsite_Address.address_Address']" ).val() == "" )
                if( navigator.geolocation )
                {
                    
                        navigator.geolocation.getCurrentPosition(function(position) {

                                if (  $.login.googleMap.infoWindow != undefined )
                                $.login.googleMap.infoWindow.setMap(null);
                                $.login.googleMap.infoWindow = new google.maps.InfoWindow({map: map});

                                var pos = {
                                        lat: position.coords.latitude,
                                        lng: position.coords.longitude
                                };

                                $.login.googleMap.infoWindow.setPosition(pos);
                                $.login.googleMap.infoWindow.setContent(  '<a id="infoWindowBtn" target-pos="' + pos.lat + ',' + pos.lng + '" class="btn btn-white btn-bitbucket"> Save GPS </a>' );
                                
                                // map.setCenter(pos);
                                
                                console.log( position.coords.latitude + ' ' + position.coords.longitude );
                                
                                google.maps.event.addListener( $.login.googleMap.infoWindow , 'domready' , function(){
                                    
                                            $( "[id=infoWindowBtn]" ).unbind('click').bind('click',function(){
                                                
                                                        if (  $.login.googleMap.infoWindow != undefined )
                                                        $.login.googleMap.infoWindow.setMap(null);
                                                            
                                                        show_remind( "save success" );
                                                        $( "[name='jobsite_Address.address_Latitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[0] );
                                                        $( "[name='jobsite_Address.address_Longitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[1] );

                                                        $("#myModalMap").data("target").find("span").html(

                                                                    $( "[name='jobsite_Address.address_PostalCode']" ).val() + " " +
                                                                    $( "[name='jobsite_Address.address_Address']" ).val() + " " +
                                                                    $( "[name='jobsite_Address.address_City']" ).val() + " " +
                                                                    $( "[name='jobsite_Address.address_Latitude']" ).val() + " " +
                                                                    $( "[name='jobsite_Address.address_Longitude']" ).val() 

                                                        );
                                                        
                                            });
                                            //code to dynamically load new content to infowindow
                                            //for example:
                                            //    var existing_content = referenceToInfoWindow.getContent();
                                            //    var new_content = "...";
                                            //    referenceToInfoWindow.setContent(existing_content + new_content);
                                }); 
                                
                                
                        }, function() {
                                handleLocationError( true , $.login.googleMap.infoWindow , map.getCenter() );
                        });
                        
                } else {
                        // Browser doesn't support Geolocation
                        handleLocationError( false , $.login.googleMap.infoWindow , map.getCenter() );
                }

                // search
                // This example adds a search box to a map, using the Google Place Autocomplete
                // feature. People can enter geographical searches. The search box will return a
                // pick list containing a mix of places and predicted search terms.
                var input = document.getElementById('googleMapSearchInput'); // $( "[name=address_Address]" )[0] ;
                var searchBox = new google.maps.places.SearchBox( input );
                map.controls[ google.maps.ControlPosition.TOP_LEFT ].push( input );
                // Bias the SearchBox results towards current map's viewport.

//                map.addListener('bounds_changed', function() {
//                    console.log( map.getBounds() );
//                    searchBox.setBounds(map.getBounds());
//                });

                var markers = [];
                
                // [START region_getplaces]
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function() {
                    
                            var places = searchBox.getPlaces();

                            if (places.length == 0) {
                                        return;
                            }

                            // Clear out the old markers.
                            markers.forEach(function(marker) {
                                        marker.setMap(null);
                            });
                            markers = [];

                            // For each place, get the icon, name and location.
                            var bounds = new google.maps.LatLngBounds();
                            places.forEach(function(place) {
                                
                                        console.log( place );
//                                        show_remind( place.formatted_address );
//                                        show_remind( place.geometry.location.lat() + ' ' + place.geometry.location.lng() );


                                        if (  $.login.googleMap.infoWindow != undefined )
                                        $.login.googleMap.infoWindow.setMap(null);
                                        $.login.googleMap.infoWindow = new google.maps.InfoWindow({map: map});

                                        var pos = {
                                                    lat: place.geometry.location.lat(),
                                                    lng: place.geometry.location.lng()
                                        };

                                        $.login.googleMap.infoWindow.setPosition(pos);
                                        $.login.googleMap.infoWindow.setContent(  '<a id="infoWindowBtn" target-pos="' + pos.lat + ',' + pos.lng + '" target-address="' + place.formatted_address + '" class="btn btn-white btn-bitbucket"> Save Address & GPS </a>'  );
                                        
                                        map.setCenter(pos);

                                        google.maps.event.addListener( $.login.googleMap.infoWindow , 'domready' , function(){

                                                    $( "[id=infoWindowBtn]" ).unbind('click').bind('click',function(){
                                                        
                                                                if (  $.login.googleMap.infoWindow != undefined )
                                                                $.login.googleMap.infoWindow.setMap(null);


                                                                show_remind( "save success" );
                                                                console.log(  $( this ).attr( "target-pos" ) ,  $( this ).attr( "target-address" ) );
                                                                $( "[name='jobsite_Address.address_Latitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[0] );
                                                                $( "[name='jobsite_Address.address_Longitude']" ).val( $( this ).attr( "target-pos" ).split( "," )[1] );
                                                                $( "[name='jobsite_Address.address_Address']" ).val(  $( this ).attr( "target-address" ) );
                                                                

                                                                $("#myModalMap").data("target").find("span").html(
                                                                        
                                                                            $( "[name='jobsite_Address.address_PostalCode']" ).val() + " " +
                                                                            $( "[name='jobsite_Address.address_Address']" ).val() + " " +
                                                                            $( "[name='jobsite_Address.address_City']" ).val() + " " +
                                                                            $( "[name='jobsite_Address.address_Latitude']" ).val() + " " +
                                                                            $( "[name='jobsite_Address.address_Longitude']" ).val() 
                                                                            
                                                                );

                                                    });
                                        }); 
                                
//                                var icon = {
//                                      url: place.icon,
//                                      size: new google.maps.Size(71, 71),
//                                      origin: new google.maps.Point(0, 0),
//                                      anchor: new google.maps.Point(17, 34),
//                                      scaledSize: new google.maps.Size(25, 25)
//                                };

                                // Create a marker for each place.
//                                markers.push( new google.maps.Marker({
//                                      map: map,
//                                      icon: icon,
//                                      title: place.name,
//                                      position: place.geometry.location
//                                }));
//
//                                if (place.geometry.viewport) {
//                                      // Only geocodes hav
                            });
                            
                            // map.fitBounds(bounds);
                });
                // [END region_getplaces]
        

        }
}



// search
// https://developers.google.com/maps/documentation/javascript/examples/places-searchbox?hl=zh-tw
function googleapis_search( tag , places ) {

}


// panTo
// https://developers.google.com/maps/documentation/javascript/examples/event-simple?hl=zh-tw
function googleapis_panTo( tag , getPosition) {

        if ( $.login.googleMap[ tag ].marker != undefined)
                console.log($.login.googleMap[ tag ].marker.position.position.lat() + $.login.googleMap[ tag ].marker.position.position.lng());

        $.login.googleMap[ tag ].map.panTo(getPosition);
        
}

//example : 
//var position = { lat: 12.97, lng: 77.59 };
//googlemap_panTo( position );




// info windows
// https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple?hl=zh-tw
function googleapis_infowindow(getPosition) {

        infowindow.open(map, marker);

}

//example : 
var contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
        '<div id="bodyContent">' +
        '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
        'sandstone rock formation in the southern part of the ' +
        'Northern Territory, central Australia. It lies 335?km (208?mi) ' +
        'south west of the nearest large town, Alice Springs; 450?km ' +
        '(280?mi) by road. Kata Tjuta and Uluru are the two major ' +
        'features of the Uluru - Kata Tjuta National Park. Uluru is ' +
        'sacred to the Pitjantjatjara and Yankunytjatjara, the ' +
        'Aboriginal people of the area. It has many springs, waterholes, ' +
        'rock caves and ancient paintings. Uluru is listed as a World ' +
        'Heritage Site.</p>' +
        '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
        'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
        '(last visited June 22, 2009).</p>' +
        '</div>' +
        '</div>';

//var infowindow = new google.maps.InfoWindow({
//    content: contentString
//});
//
//var marker = new google.maps.Marker({
//    position: uluru,
//    map: map,
//    title: 'Uluru (Ayers Rock)'
//});

//example : 
//googlemap_infowindow( position );
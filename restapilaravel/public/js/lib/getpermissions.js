/*
 * 判別使用者 sidebar 權限
 *  auth : jack <founder@oort.com.tw>
 */
function GetLoginPermissions( fn_callback ) {

    var html = location.href.split("/")[location.href.split("/").length - 1];
    if (html != "") {
            html = html.split("?")[0].split("#")[0].split(".php")[0];

            $("#sidebar a[h_target=" + html + "]").parents(".collapse").addClass("in");
            $("#sidebar a[h_target=" + html + "]").parents(".set_open").addClass("active");
    }

    //$( "#sidebar a[h_target=" + html + "]" ).parents( ".set_open" ).addClass( "open" );
    //$( "#sidebar a[h_target=" + html + "]" ).parents( ".nav-hide" ).addClass( "nav-show" ).removeClass( "nav-hide" ).css( "display" , "block" );

    return fn_GetLoginPermissions( fn_callback );
}

function fn_GetLoginPermissions( fn_callback ) {
        //登入憑證true有/false無
        var Permissions = false;
        var tmp_index = location.pathname.split( "/" );
        tmp_index = tmp_index[ tmp_index.length - 1 ].split( "_" )[0] ;

        $.ajax({
            type: "POST",
            dataType: "text",
            async: false,
            url: "service/users/getPermissions",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                //                    'Authorization': "Token 34e8bd4b51f6cf0b5068c0e4c24ac637"//Administrator
                //                    'Authorization': "Token c7b6db9df4c16480a11ab9447e7a31d8"//Dispatcher
                //                    'Authorization': "Token c85df655cc8ea4ad8756c944bac571f8"//Operator
                //                    'Authorization': "Token e3c9a1cddef7a723730e33d2d419af7e"//Accounting
                'Authorization': "Token "+getCookie( "lnbCookie" )
            },
            data: {},
            success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    if( data.success ) 
                    {
                            $.each( data.data , function(index, value) {
                                    if ( value[2] == 1 )
                                    {
                                            //AL 20170317
                                            //$("[h_target=" + index + "_d]").parent().show();
                                            $("[h_target]").parent().show();
                                    }else{
                                            console.log( index + " display none" );
                                    }

                                    if ( index == tmp_index )
                                    {
                                            $.LoginPermissions = value ;
                                            
                                            if( $.LoginPermissions[0] == "1" )
                                            {
                                                    $("#createbtn").parent().parent().show();
                                                    $("#createbtn").show();
                                            }

                                            fn_callback();
                                    }


                                    //$( "[h_target]" ).parent().hide();
                            });

                            Permissions = true;
                            fn_callback();
                    }
                    else{
                            show_remind( "你被踢出了" , "error" );
                            setTimeout( function(){ location.href = "register" }, 3000);
                    }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });

        return Permissions;
};
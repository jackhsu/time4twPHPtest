/*
 * 使用者註冊 function
 *  auth : jack <founder@oort.com.tw>
 */
function processLogin(){

        var bool = true;
        var msg = "";
        $("#loginPlace .form-group").addClass("has-success").removeClass("has-error").removeClass("has-warning");
        if($("#inputLoginUserName").val()===""){
            $("#inputLoginUserName").closest(".form-group").addClass("has-error").removeClass("has-success");
            bool = false;
            msg += msg===""? "Please input email" : "、input email";
        }
        if($("#inputLoginUserPassword").val()===""){
            $("#inputLoginUserPassword").closest(".form-group").addClass("has-error").removeClass("has-success");
            bool = false;
            msg += msg===""? "Please input password" : "、input password";
        }

        if(!bool){
            swal("ERROR", msg, "error");
            return false;
        }

        $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/login",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                'Authorization': "Basic "+btoa(md5($("#inputLoginUserPassword").val()))
            },
            data: {
                // email => username (~)
                "userinfo_UserName": $("#inputLoginUserName").val()
            },
            success: function(data){
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                        // show_remind( "登入成功" , "error" );
                        setCookie("lnbCookie",data.data);
                        // log in front-end member profile
                        location.href = "mgd_index";//AL 20170512 
                }
                else {
                        swal("錯誤", data.msg, "error");
                        // show_remind( data.msg , "error" );
                }
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });

}

function processRegister(){

        var bool = true;
        var msg = "";
        var emailReg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        $("#registerPlace .col-lg-12").removeClass("has-error").removeClass("has-warning");
        if($("#inputRegUserName").val()===""){
            $("#inputRegUserName").closest(".col-lg-12").addClass("has-error");
            bool = false;
            msg += msg===""? "請輸入 ID (信箱)" : "、ID (信箱)";
        } else {
           // ID (email) format check
           if(!emailReg.test($("#inputRegUserName").val())) {
                $("#inputRegUserName").closest(".col-lg-12").addClass("has-error");
                bool = false;
                msg += msg===""? "請輸入正確格式的 ID (信箱)" : "、正確格式的 ID (信箱)";
            }
        }
        if($("#inputRegUserPassword").val()===""){
            $("#inputRegUserPassword").closest(".col-lg-12").addClass("has-error");
            bool = false;
            msg += msg===""? "請輸入密碼" : "、密碼";
        }
        if($("#inputRegComfirmPassword").val()===""){
            $("#inputRegComfirmPassword").closest(".col-lg-12").addClass("has-error");
            bool = false;
            msg += msg===""? "請輸入確認密碼" : "、確認密碼";
        }
        if($("#inputRegInExUser").val()===""){
            $("#inputRegInExUser").closest(".col-lg-12").addClass("has-error");
            bool = false;
            msg += msg===""? "請選擇申請類型" : "、申請類型";
        }
        if($("#inputRegUserPassword").val()!=$("#inputRegComfirmPassword").val()){
            $("#inputRegComfirmPassword").closest(".col-lg-12").addClass("has-error");
            bool = false;
            msg += msg===""? "密碼與確認密碼不相符" : "、密碼與確認密碼不相符";
        }

        if(!bool){
            swal("錯誤", msg, "error");
            return false;
        }
        
        console.log('註冊資料正確');
        
        $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/register",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                "userinfo_InExUser": $("#inputRegInExUser").val(),
                "userinfo_Password": btoa(md5($("#inputRegComfirmPassword").val())),
                "userinfo_Email": $("#inputRegUserName").val()
            },
            success: function(data){
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                       swal("感謝您註冊IUNING!", "系統給您發送了一封認證信件，快去登入Email認證啟動帳號吧! (請注意垃圾信件or廣告信件夾)。", "success");
                       $(".confirm").bind('click', function(){
                           location.href = "login";
                       });
                }
                else 
                {
                        swal("錯誤", data.msg, "error");
                        $("#inputRegUserName").closest(".col-lg-12").addClass("has-error");
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });
}

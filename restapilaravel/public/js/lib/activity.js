/*
 * activity function
 * auth : phoebe
 */

                                            
if( $.login == undefined )
    $.login = {} ;

$.login.gpsData = [];

    // ajax gps daily
    $.login.gpsData =
        [
            {
            "starttimeyear":"2016",
            "totalduringtime":"20:30:35",
            "totaldistance":"10.55km",
            "totalcount": "#00002" ,
            "totalrawdata":
                [
                    {
                        "starttimeyear" : "2016" ,
                        "starttimemonth" : "04" ,
                        "starttimeday" : "01" ,
                        "starttime" : "2016/04/01 AM11:28:33" ,
                        "duringtime" : "12:45:55",
                        "distance" : [1, 3, 4, 5, 2, '', '', '', '', '', '', ''],
                        "gpsvalue" : 1
                    },
                    {
                        "starttimeyear" : "2016" ,
                        "starttimemonth" : "04" ,
                        "starttimeday" : "01" ,
                        "starttime" : "2016/04/01 PM12:22:33" ,
                        "duringtime" : "11:11:11",
                        "distance" : [1, 3, 4, 5, 2, '', '', '', '', '', '', ''],
                        "gpsvalue" : 2
                    }
                ],
                "totalrawdatatable": [
                                                [ "Gavin Joyce", "2016/3/27 PM 10:28:22", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
                                                [ "Jennifer Chang", "2016/3/27 PM 11:28:22", "Singapore", "9239", "2010/11/14", "$357,650" ],
                                                [ "Brenden Wagner", "2016/3/27 PM 12:28:22", "San Francisco", "1314", "2011/06/07", "$206,850" ],
                                                [ "Fiona Green", "2016/3/27 PM 11:28:22", "San Francisco", "2947", "2010/03/11", "$850,000" ],
                                                [ "Shou Itou", "2016/3/27 PM 11:28:22", "Tokyo", "8899", "2011/08/14", "$163,000" ]
                                      ]
            },
            {
            "starttimeyear":"2017",
            "totalduringtime":"30:30:30",
            "totaldistance":"30.55km",
            "totalcount": "#00003" ,
            "totalrawdata":
                [
                    {
                        "starttimeyear" : "2017" ,
                        "starttimemonth" : "05" ,
                        "starttimeday" : "01" ,
                        "starttime" : "2017/05/01 AM11:28:33" ,
                        "duringtime" : "11:11:11",
                        "distance" : ['', '', '', '', '', 1, 3, 4, 5, 2, 3, 3.5],
                        "gpsvalue" : 3
                    },
                    {
                        "starttimeyear" : "2017" ,
                        "starttimemonth" : "06" ,
                        "starttimeday" : "02" ,
                        "starttime" : "2017/06/02 PM12:12:12" ,
                        "duringtime" : "12:12:12",
                        "distance" : ['', '', '', '', '', 1, 3, 4, 5, 2, 3, 3.5],
                        "gpsvalue" : 4
                    },
                    {
                        "starttimeyear" : "2017" ,
                        "starttimemonth" : "07" ,
                        "starttimeday" : "02" ,
                        "starttime" : "2017/07/02 PM08:01:12" ,
                        "duringtime" : "01:02:02",
                        "distance" : ['', '', '', '', '', 1, 3, 4, 5, 2, 3, 3.5],
                        "gpsvalue" : 5
                    }
                ],
                "totalrawdatatable": [
                                                [ "Gavin Joyce", "2017/3/27 PM 10:28:22", "Edinburgh", "8822", "2010/12/22", "$92,575" ],
                                                [ "Jennifer Chang", "2017/3/27 PM 11:28:22", "Singapore", "9239", "2010/11/14", "$357,650" ],
                                                [ "Brenden Wagner", "2017/3/27 PM 12:28:22", "San Francisco", "1314", "2011/06/07", "$206,850" ]
                                      ]
            }
        ];
function activityInit() {

    // addSelectYear
    var selectYear = '';

    for (i = 0; i < $.login.gpsData.length; i++) {
        selectYear = $.login.gpsData[i].starttimeyear;
        addSelectYear(selectYear);
    }

    $('#selectYear option[value="'+ $.login.gpsData[0].starttimeyear+'"]').attr("selected",true);
}

function addSelectYear(selectYear) {
    $('#selectYear').prepend($("<option></option>").attr("value", selectYear).text(selectYear));
}

function showGpsYear(year) {
    var $startTimeYear = $('#startTimeYear');
    var $totalCount = $('#totalCount');
    var $totalDuringTime = $('#totalDuringTime');
    var $totalDistance = $('#totalDistance');
    var gpsStartyYear, gpsTotalDuring, gpsTotalDistance, gpsTotalCount, gpsTotalRawData;

    for (i = 0; i < $.login.gpsData.length; i++) {

        gpsStartyYear =  $.login.gpsData[i].starttimeyear;
        gpsTotalDuring =  $.login.gpsData[i].totalduringtime;
        gpsTotalDistance =  $.login.gpsData[i].totaldistance;
        gpsTotalCount =  $.login.gpsData[i].totalcount;
        gpsTotalRawData =  $.login.gpsData[i].totalrawdata;

        if(year !== undefined && year === gpsStartyYear) {
            $startTimeYear.text(gpsStartyYear);
            $totalDuringTime.text(gpsTotalDuring);
            $totalDistance.text(gpsTotalDistance);
            $totalCount.text(gpsTotalCount);

            // showRawData
            showRawData(gpsTotalRawData);
        }
    }
}

function showRawData(rawData) {
    // console.log(rawData);
}

function highcharts_init(year) {
    var ascent_data = [];
    var gpsStartyYear = '';

    for (i = 0; i < $.login.gpsData.length; i++) {
        gpsStartyYear =  $.login.gpsData[i].starttimeyear;

        if(year !== undefined && year === gpsStartyYear) {
            ascent_data = $.login.gpsData[i].totalrawdata[0].distance;
        }  
    }
    create_highcharts(ascent_data);
}

function create_highcharts(highcart_data) {

     // highcharts sample
    Highcharts.chart('container', {

        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: ''
            }
        },

        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },

        series: [{
            name: '1',
            data: highcart_data
        }
        // , {
        //     name: '2',
        //     data: ['', '', '', '', '', 1, 3, 4, 5, 2, 3, 3.5]
        // }
        ]
    });
}

// demo: https://jsfiddle.net/m1oLnft0/6/
function initMap( focus_map )
{
    var map = new google.maps.Map( focus_map , {
        zoom: 15,
        center: {lat: 25.083, lng: 121.303},
        mapTypeId: 'roadmap'
    });

    var gpsValue = [
            {lat: 25.081, lng: 121.301},
            {lat: 25.082, lng: 121.302},
            {lat: 25.083, lng: 121.303},
            {lat: 25.084, lng: 121.304},
            {lat: 25.085, lng: 121.305},
            {lat: 25.084, lng: 121.306},
            {lat: 25.083, lng: 121.307},
            {lat: 25.084, lng: 121.308}
    ];

    var gpsPath = new google.maps.Polyline({
            path: gpsValue,
            geodesic: true,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2
    });

    gpsPath.setMap(map);
}




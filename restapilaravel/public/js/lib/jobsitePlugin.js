
/*
 * datatable 使用的  dataPicker
 *  auth : jack
 */
if( $.plugin == undefined )
$.plugin = {} ;
$.plugin.jobsite = {} ;


// ajax 取得 jobsite 清單
$.plugin.jobsite.getList = function ( jobsiteId , callback ) {

    $.ajax({
        type:"GET",
        dataType: "json",
        async: false,
        url: "../APIServices/slim3/public/jobsites/toSelect2" + ( jobsiteId ? "/" + jobsiteId : "" ),
        headers: {
            'Authorization': "Token " + getCookie( "lnbCookie" )
        },
        success: callback ,
        error:function(xhr, ajaxOptions, thrownError){
            console.log(xhr.status); 
            console.log(thrownError); 
        }
    });
    
};


$.plugin.jobsite.getById = function ( jobsiteId , callback ) {
        var jobsite = {};
        $.ajax({
                type: "GET",
                dataType: "json",
                async: false,
                url: "../APIServices/slim3/public/jobsites/" + jobsiteId ,
                headers: {
                        'Authorization': "Token " + getCookie("lnbCookie")
                },
                data: {},
                success: callback ,
                error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                }
        });
};



$.plugin.jobsite.infoByjobsite_detail = function(data) {

        $.each(data, function(key, value) {
                //瀏覽顯示
                var tag = $("#" + key);
                if (tag[0] != undefined) {
                        if (key == "jobsite_ID") {
                                tag.html($.addZero(value, 5));
                        } else {
                                tag.html(value);
                        }
                        //編輯區的顯示
                        if (["address", "emergency", "foreman", "supervisor", "superintend"].toString().includes(key)) {
                                var id = key.split(".")[0];
                                id = "create" + id.charAt(0).toUpperCase() + id.slice(1);
                                $("#" + id).find("span").html(value);
                        }
                }

                //編輯的值
                //key如果有以下字串
                if (key.indexOf("Emergency_") !== -1 || key.indexOf("Foreman_") !== -1 ||
                        key.indexOf("Supervisor_") !== -1 || key.indexOf("Superintend_") !== -1) {
                        key = "jobsite_" + key.replace("_", ".");
                }
                if (key.indexOf("jobsite_Address_") !== -1) {
                        key = key.replace("jobsite_Address_", "jobsite_Address.");
                }
                if (key == "jobsite_TypemployeeOfWorks") {
                        var works = value.split(",");
                        for (var i = 0; i < works.length; i++) {
                                var checkbox = $("input[type=checkbox][name='jobsite_TypemployeeOfWorks[]'][value='" + works[i] + "']");
                                //checkbox點選其他選向的處理
                                if (checkbox[0] == undefined) {
                                        $("#ofWorksOtherCheckbox").prop("checked", true);
                                        var s = "";
                                        for (var j = i; j < works.length; j++) {
                                                s += works[j] + ",";
                                        }
                                        s = s.substr(0, s.length - 1);
                                        $("#ofWorksOtherCheckbox").val(s);
                                        $("#ofWorksOtherInput").val(s);
                                        break;
                                } else {
                                        checkbox.prop("checked", true);
                                }
                        }
                }
                var input = $("[name='" + key + "']");
                if (input[0] != undefined) {
                        input.val(value);
                        if (key == "jobsite_CustomerID") {
                                $("#createCustomer").change();
                        }
                }
        });

};



$.plugin.jobsite.infoByworkorder_create = function(data) {
        $.each(data, function(key, value) {
                //瀏覽顯示
                var tag = $("#"+key);
                if(tag[0] != undefined) {
                    tag.html(value);
                }
        });
};


$.plugin.jobsite.infoByworkorder_detail = function(data) {
        $.each(data, function(key, value) {
                //瀏覽顯示
                var tag = $("form#workorderEdit #"+key);
                if(tag[0] != undefined) {
                    if(key == 'jobsite_ID') {
                        tag.html($.addZero(value, 5));
                    } else {
                        tag.html(value);
                    }

                    //編輯
                    var input = $("[name='"+key+"']");
                    input.val(value);
                }
        });
};
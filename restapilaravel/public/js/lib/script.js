$(function() {
	//niceScroll
	$('body').niceScroll();
	
	//mobile
	var $mobile = $('#navigation').html();
	$('#header .wrap').append('<a id="mobi-btn"><hr></a>');
	$('#copyright').after('<div id="mask">遮罩</div><div id="mobile"><a class="close"></a><ul>' + $mobile + '</ul></div>')
	
	$('#mobi-btn').click(function(e) {
        $('#mask').fadeIn();
		$('#mobile').show().animate({right:0},300);
    });
	
	$('#mobile .close').click(function(e) {
        $('#mask').hide();
		$('#mobile').css('right','-280px').hide();
    });
	
	$(window).on('load resize',function(){
		if($(this).width() > 768){
			$('#mask').hide();
			$('#mobile').css('right','-280px').hide();
		}
	});
	
	//lang 
	$('#top-link .lang > a').click(function(e) {
        $(this).siblings('.sub-menu').slideToggle();
    });
	
	//left-menu
	$('#container > .left .control').click(function(e) {
        $(this).toggleClass('active').siblings('.left-menu').slideToggle();
    });
	
	$('#container > .left .left-menu > li h2').click(function(e) {
        $(this).siblings('.sub-menu').slideToggle().parent().siblings().find('.sub-menu').slideUp();
    });
	
	$(window).on('resize',function(){
		if($(this).width() > 768){
			$('#container > .left .control').removeClass('active');
			$('#container > .left .left-menu , #container > .left .sub-menu').css('display','');
		}
	});
	
	//side-direct
	$('#side-direct .wechat a').click(function(e) {
        $(this).siblings('.qrcode').toggle();
    });
	
	//gotop
	$('#side-direct .gotop a').click(function(e) {
        $('html , body').animate({scrollTop:$('body').offset().top},900);
    });
});
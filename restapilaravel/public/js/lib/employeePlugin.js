
/*
 * datatable 使用的  dataPicker
 *  auth : jack
 */
if( $.plugin == undefined )
$.plugin = {} ;
$.plugin.employee = {} ;


// ajax 取得 jobsite 清單
$.plugin.employee.getList = function ( callback ) {

    $.ajax({
            type:"GET",
            dataType: "json",
            async: false,
            url: "../APIServices/slim3/public/users/getEmployeesSimple",
            headers: {
                    'Authorization': "Token " + getCookie( "lnbCookie" )
            },
            data: {
            },
            success: callback ,
            error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
            }
    });
};
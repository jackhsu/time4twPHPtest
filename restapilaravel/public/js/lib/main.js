$(function() {
	//mobile
	var $mobile = $('#navigation').html();
                $('#header .wrap').append('<a id="mobi-btn"><hr></a>');
                $('#copyright').after('<div id="mask">遮罩</div><div id="mobile"><a class="close"></a><ul>' + $mobile + '</ul></div>');

                $('#mobi-btn').click(function(e) {
                $('#mask').fadeIn();
                $('#mobile').show().animate({right:0},300);
        });

        $('#mobile .close').click(function(e) {
                $('#mask').hide();
                $('#mobile').css('right','-280px').hide();
        });

        $(window).on('load resize',function(){
                if($(this).width() > 768){
                        $('#mask').hide();
                        $('#mobile').css('right','-280px').hide();
                }
        });

        //lang 
        $('#top-link .lang > a').click(function(e) {
                $(this).siblings('.sub-menu').slideToggle();
        });
	
	//side-direct
	$('#side-direct .wechat a').click(function(e) {
                $(this).siblings('.qrcode').toggle();
        });
	
	//gotop
	$('#side-direct .gotop a').click(function(e) {
                $('html , body').animate({
                    scrollTop:$('body').offset().top
                },900);
        });
}); 
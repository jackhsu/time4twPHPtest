/*
 * 取得個別單一使用者的資料 function
 *  auth : vincent <>
 */

function getUserinfoValue(userinfo_id, userinfo_oTableColumn){

        var bool = true;
        var msg = "";
        
        if(userinfo_id===""){
            bool = false;
            msg += msg===""? "Please input Userinfo Id" : "、input Userinfo ID";
        }
        if(userinfo_oTableColumn===""){
            bool = false;
            msg += msg===""? "Please input userinfo_oTableColumn" : "、input userinfo_oTableColumn";
        }
      
        if(!bool){
            return msg;
        }

        $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/getUserinfoValue",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Authorization': "Token "+getCookie( "lnbCookie" )
            },
            data: {
                "userinfo_id": userinfo_id,
                "userinfo_oTableColumn": userinfo_oTableColumn,
            },
            success: function(data){
                console.log(data);
                data = JSON.parse(data);
                if( data.success ) {
                       return data;
                }
                else {
//                       swal("ERROR", data.msg, "error");
                       return data.msg;
                }
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });
}

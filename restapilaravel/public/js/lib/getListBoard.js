/*
 * 個人公告, 最新消息 function
 *  auth : vincent <hyde31456@gmail.com>
 */


/* @param String boardName */
function getListBoard( boardName ){

     $.ajax({
            type:"POST",
            dataType: "text",
            url: "service/users/listGlobalValue",
            headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                       'global_oTable': boardName
            },
            success: function(data){

                data = JSON.parse(data);
                if( data.success ) {

                        var unsorted_times = [];
                        var compareMilli = function (a,b) {
                                // Compare dates to sort
                                if(a.milli > b.milli) return -1;
                                if(a.milli < b.milli) return 1;
                                return 0;
                        };

                        $.each( data.data, function(index, value){
                            var unsorted_time = new Object();
                            unsorted_time.date = moment(value['b_date']).format('YYYY/MM/DD');
                            unsorted_time.milli = moment(value['b_date']).valueOf();
                            unsorted_time.title = value['b_title'];
                            unsorted_times.push(unsorted_time);
                        });

                        unsorted_times.sort(compareMilli);

                        var tmp = '', tmp2 = '';
                        for(var i=0; i<unsorted_times.length; i++) {
                            if(i<5)
                            {
                                tmp += '<li><span>' +unsorted_times[i]['date']+ '</span><a href="#">' +unsorted_times[i]['title']+ '</a></li>';
                            } else 
                            {
                                tmp2 += '<li><span>' +unsorted_times[i]['date']+ '</span><a href="#">' +unsorted_times[i]['title']+ '</a></li>';
                            }
                        }

                        console.log(tmp);
                        console.log(tmp2);

                        if (boardName==='board') {

                            var more_div = '<div id="more" style="display:none"></div>';

                            $(".latest-new ul").html( tmp+more_div );
                            $(".more").one('click', function(){ 
                               $("#more").append( tmp2 );
                               $("#more").slideToggle("slow");
                               $(".more").unbind('click').bind('click',function(){  
                                   $("#more").slideToggle("slow");
                                    return false;
                                });
                                return false;
                            });

                        } else {
                            
                            $(".personal-news ul").html(tmp);
                            tmp2 = null;
                        }
                }
                else 
                {
                        console.log( data.msg );
                        show_remind( data.msg , "error" );
                } 
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError);

            }
    });

}
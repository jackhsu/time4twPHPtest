/*
 * datatable 使用的  dataPicker
 *  auth : jack
 */
if ($.plugin == undefined)
        $.plugin = {};
$.plugin.customer = {};


//ajax取得customer清單
$.plugin.customer.getList = function(callback) {
        $.ajax({
                type: "GET",
                dataType: "json",
                async: false,
                url: "../APIServices/slim3/public/customers/toSelect2",
                headers: {
                        'Authorization': "Token " + getCookie("lnbCookie")
                },
                data: {},
                success: callback,
                error: function(xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                }
        });
};
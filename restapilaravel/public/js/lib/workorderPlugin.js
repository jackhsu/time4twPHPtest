
/*
 * datatable 使用的  dataPicker
 *  auth : jack
 */
if( $.plugin == undefined )
$.plugin = {} ;
$.plugin.workorderPlugin = {} ;


$.plugin.workorderPlugin.getById = function ( workorderId , callback ) {
            var workorder = {};
            $.ajax({
                type:"GET",
                dataType: "json",
                async: false,
                url: "../APIServices/slim3/public/workorders/" + workorderId ,
                headers: {
                    'Authorization': "Token " + getCookie( "lnbCookie" )
                },
                data: {},
                success: callback ,
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(xhr.status); 
                    console.log(thrownError); 
                }
            });
};

$.plugin.workorderPlugin.initByjobsite_detail = function() {
            // Collapse ibox function (for workOrder)
            $('.collapse-link').on('click', function() {
                    var ibox = $(this).closest('div.ibox');
                    var button = $(this).find('i');
                    var content = ibox.find('div.ibox-content');
                    content.slideToggle(200);
                    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                    ibox.toggleClass('').toggleClass('border-bottom');
                    setTimeout(function() {
                            ibox.resize();
                            ibox.find('[id^=map-]').resize();
                    }, 50);
            });

            //Data picker
            $("#condition_Datatable_starttime").datepicker();
            $("#condition_Datatable_starttime").datepicker("setDate", "01/01/" + (new Date().getFullYear()));

            $("#condition_Datatable_endtime").datepicker();
            $("#condition_Datatable_endtime").datepicker("setDate", (new Date().getMonth() + 1) + "/" + (new Date().getDate()) + "/" + (new Date().getFullYear()));
};

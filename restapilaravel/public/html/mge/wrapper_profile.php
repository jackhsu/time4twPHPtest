<style>
    form a,.modal a {
        color: #2E75CC;
        text-decoration: underline;
    }
    form a:hover,.modal a:hover {
        opacity: .5;
    }
    .form input.input-lg{
        width: 40%;
    }
    .form input.input-sm{
        width: 43px;
    }
    /*checkbox radio*/
    .radio-inline {
        display: inline-block;
    }
    .radio input[type="radio"] {
        position: absolute;
        width: 10px;
    }
    .radio label::after {
        width: 13px;
        height: 13px;
    }
    .radio label.m-radio::before {
        top:8px;
    }
    .radio label.m-radio::after {
        top:11px;
    }
    /*select*/
    .chosen-container-single .chosen-single {
        color: #333;
        border: 0;
        border-radius: 4px;
        height: 33px;
        line-height: 33px;
    }
    .chosen-container-multi {
        margin: 10px 0 0;
    }
    /*upload_progress*/
    .img-lg {
        height: 120px
    }
    .upload_success::before {
        content: "";
        display: inline-block;
        width: 26px;
        height: 26px;
        margin-right: 5px;
        background: url(./images/check.png);
        vertical-align: middle;
    }
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員中心</a>　/　<span>個人資料</span></div>

<?php include("../sidebar.php"); ?>

<div class="right"> 
    <div id="main-title">個人資料</div>
    <div class="row animated fadeInRight">
        <div class="tab-block">
            <div class="tabs"><a class="active">個人資料</a><a>個人設定</a></div>
            <ul class="tab-content">
                <!--個人資料-->
                <li class="form">
                    <form id="profileEdit">

                        <div class="m-b-md" target-otablecolumn="userinfo.userinfo_ID">
                            <span>編碼：</span>
                            <p id="userinfo_ID" target-edit>VA124154</p>
                        </div>

                        <div class="m-b-md" target-otablecolumn="userinfo_job.userinfo_job">
                            <span>*公司行業別：</span>
                            <select id="userinfo_job" data-placeholder="請選擇..." class="chosen-select" tabindex="2">
                                <option value="">請選擇</option>
                                <optgroup label="資訊科技">
                                    <option value="軟體網路">軟體網路</option>
                                    <option value="電信通訊">電信通訊</option>
                                    <option value="電腦週邊">電腦週邊</option>
                                    <option value="光電光學">光電光學</option>
                                    <option value="電子相關">電子相關</option>
                                    <option value="半導體業">半導體業</option>
                                </optgroup>
                                <optgroup label="傳產製造">
                                    <option value="食品飲料">食品飲料</option>
                                    <option value="紡織＆鞋類">紡織＆鞋類</option>
                                    <option value="家具家飾">家具家飾</option>
                                    <option value="化學製造">化學製造</option>
                                    <option value="石油製造">石油製造</option>
                                    <option value="橡膠塑膠">橡膠塑膠</option>
                                    <option value="金屬製造">金屬製造</option>
                                    <option value="機械設備">機械設備</option>
                                    <option value="運輸工具">運輸工具</option>
                                    <option value="醫材相關">醫材相關</option>
                                    <option value="營建土木">營建土木</option>
                                    <option value="其他製造">其他製造</option>
                                </optgroup>
                                <optgroup label="農林漁牧礦">
                                    <option value="農業">農業</option>
                                    <option value="林業">林業</option>
                                    <option value="漁業">漁業</option>
                                    <option value="牧業">牧業</option>
                                    <option value="礦石/土石採取業">礦石/土石採取業</option>
                                    <option value="石油/天然氣礦業">石油/天然氣礦業</option>
                                </optgroup>
                                <optgroup label="工商服務">
                                    <option value="法律服務">法律服務</option>
                                    <option value="會計服務">會計服務</option>
                                    <option value="顧問研發">顧問研發</option>
                                    <option value="人力仲介">人力仲介</option>
                                    <option value="租賃業">租賃業</option>
                                    <option value="汽車維修">汽車維修</option>
                                    <option value="徵信保全">徵信保全</option>
                                    <option value="物流倉儲">物流倉儲</option>
                                    <option value="貿易業">貿易業</option>
                                </optgroup>
                                <optgroup label="民生服務">
                                    <option value="批發零售">批發零售</option>
                                    <option value="金融保險">金融保險</option>
                                    <option value="投資理財">投資理財</option>
                                    <option value="政治社福">政治社福</option>
                                </optgroup>
                                <optgroup label="軍公教">
                                    <option value="軍人">軍人</option>
                                    <option value="警察">警察</option>
                                    <option value="消防員">消防員</option>
                                    <option value="公務人員">公務人員</option>
                                    <option value="老師">老師</option>
                                    <option value="研究員">研究員</option>
                                    <option value="公營企業">公營企業</option>
                                </optgroup>
                                <optgroup label="自由業">
                                    <option value="自由接案工作者">自由接案工作者</option>
                                    <option value="計時工作者">計時工作者</option>
                                </optgroup>
                                <optgroup label="學生">
                                    <option value="大學生">大學生</option>
                                    <option value="碩士生">碩士生</option>
                                    <option value="博士生">博士生</option>
                                </optgroup>
                                <optgroup label="自營商">
                                    <option value="自營商">自營商</option>
                                </optgroup>
                                <optgroup label="其他">
                                    <option value="家管">家管</option>
                                    <option value="退休人員">退休人員</option>
                                    <option value="待業">待業</option>
                                </optgroup>
                                <optgroup label="其他">
                                    <option value="其他">其他</option>
                                </optgroup>
                            </select>
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_company_ID_card.userinfo_company_ID_card">
                            <span>*公司名統編：</span>
                            <input placeholder="請輸入" type="text" target-edit maxlength="8">
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_companyowner.userinfo_companyowner">
                            <span>*公司負責人姓名：</span>
                            <input placeholder="請輸入" type="text" target-edit>
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo.userinfo_UserName">
                            <span>*使用者姓名：</span>
                            <input placeholder="請輸入" type="text" target-edit>
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_companyphone.userinfo_companyphone">
                            <span>*公司電話：</span>
                            <input placeholder="02-0000-0000" type="text" class="required">
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_gender.userinfo_gender">
                            <span>*性別：</span>
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadio1" value="男" name="userinfo_gender">
                                <label for="inlineRadio1"> 男 </label>
                            </div>
                            <div class="radio radio-success radio-inline m-l-sm">
                                <input type="radio" id="inlineRadio2" value="女" name="userinfo_gender">
                                <label for="inlineRadio2"> 女 </label>
                            </div>
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_usertype.userinfo_usertype">
                            <span>*身 份 別：</span>
                            <div class="radio radio-success m-b-xs" style="display: inline-block;">
                                <input type="radio" id="inlineRadio3" value="地政士" name="userinfo_usertype">
                                <label for="inlineRadio3"> 地政士 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio4" value="代辦" name="userinfo_usertype">
                                <label for="inlineRadio4"> 代辦 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio5" value="介紹人" name="userinfo_usertype">
                                <label for="inlineRadio5"> 介紹人 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio6" value="金主" name="userinfo_usertype">
                                <label for="inlineRadio6"> 金主 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio7" value="銀行" name="userinfo_usertype">
                                <label for="inlineRadio7"> 銀行 </label>
                            </div>
                        </div>

                        <div class="m-b-md" target-otablecolumn="userinfo_expectrate.userinfo_expectrate">
                            <span>最低利率：</span>
                            <input type="number" placeholder="例如：2.66" step="0.01">％
                            <!--<p id="userinfo_expectrate" target-view>0.3</p>-->
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_creditlimits.userinfo_creditlimits">
                            <span>*資金額度上限：</span>
                            <input class="userinfo_creditlimits" type="text" placeholder="最低金額"><span class="m-r-sm">-</span><input class="userinfo_creditlimits" type="text" placeholder="最高金額">
                            <div class="radio radio-success radio-inline m-l-sm">
                                <input type="radio" id="Radio4" value="-1" name="userinfo_creditlimits">
                                <label for="Radio40"> 無上限 </label>
                            </div>
                        </div>

                        <div class="m-b-md" target-otablecolumn="userinfo_orgcost.userinfo_orgcost">
                            <span>開辦費：</span>
                            <span class="m-r-sm">最低</span>
                            <input type="text" placeholder="例如：1000">元
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_servicearea.userinfo_servicearea">
                            <span>*可承作地區：</span>
                            <select class="chosen-select m-b-md" data-placeholder="請選擇縣市，可複選" multiple >
                                <option value="">請選擇縣市</option>
                            </select>
                            <select class="chosen-select" data-placeholder="請選擇鄉鎮，可複選" multiple>
                                <option value="">請選擇鄉鎮</option>
                            </select>
                        </div>
                        
                        <div class="m-b-md" target-otablecolumn="userinfo_serviceitems.userinfo_serviceitems">
                            <span>*承辦項目：</span>
                            <div class="radio radio-success m-b-xs" style="display:inline-block">
                                <input type="radio" id="Radio20" value="銀行貸款" name="userinfo_serviceitems">
                                <label for="Radio20"> 銀行貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio21" value="房屋貸款" name="userinfo_serviceitems">
                                <label for="Radio21"> 房屋貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio22" value="土地貸款" name="userinfo_serviceitems">
                                <label for="Radio22"> 土地貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio23" value="汽車貸款" name="userinfo_serviceitems">
                                <label for="Radio23"> 汽車貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio24" value="機車貸款" name="userinfo_serviceitems">
                                <label for="Radio24"> 機車貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio25" value="小額貸款" name="userinfo_serviceitems">
                                <label for="Radio25"> 小額貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio26" value="婦女貸款" name="userinfo_serviceitems">
                                <label for="Radio26"> 婦女貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio27" value="勞保貸款" name="userinfo_serviceitems">
                                <label for="Radio27"> 勞保貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio28" value="軍公教貸款" name="userinfo_serviceitems">
                                <label for="Radio28"> 軍公教貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio29" value="企業貸款" name="userinfo_serviceitems">
                                <label for="Radio29"> 企業貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio30" value="自營商貸款" name="userinfo_serviceitems">
                                <label for="Radio30"> 自營商貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio31" value="負債整合" name="userinfo_serviceitems">
                                <label for="Radio31"> 負債整合 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio32" value="融資貸款" name="userinfo_serviceitems">
                                <label for="Radio32"> 融資貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio33" value="其他" name="userinfo_serviceitems">
                                <label for="Radio33" style="display: inline;"> 其他 </label>
                                <input id="radio-other" type="text" placeholder="(自填)" target-edit>
                            </div>
                            <div class="chosen-container chosen-container-multi m-b-xl m-t-sm" style="width: calc(100% - 100px);margin-left: 100px;" title="">
                                <ul id="userinfo_serviceitems" class="chosen-choices" style="padding: 10px;">
                                    <p style="display: block;padding: 10px;">強力主打：(最多三項) </p>
<!--                                    <li class="search-choice"><span>銀行貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>
                                    <li class="search-choice"><span>土地貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>
                                    <li class="search-choice"><span>汽車貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </form>

                    <button id="btnEditCancel" type="submit">取消</button>
                    <button id="btnEditComplete" type="submit">完成</button>
                    <!--<button id="btnEdit" type="submit" target-view>修改</button>-->


                </li>
                
                <!--個人設定-->
                <li class="form" style="display: none;">
                    <form>
                        <div class="m-b-md">
                            <span>Email：</span>
                            <p class="LabelProfileNowEmail">email@example.com</p>
                        </div>

                        <div class="m-b-md">
                            <span>狀態：</span>
                            <p id="LabelProfileState" style="color: #6b9e13;">已認證</p>
                            <button id="btnEmail" type="submit" style="width: 130px; display: inline-block;">寄送認證信</button>
                        </div>

                        <div class="m-b-md">
                            <span>註冊時間：</span>
                            <p id="LabelProfileRegistrationTime">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入時間：</span>
                            <p id="LabelProfileLastLogin">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入IP：</span>
                            <p id="LabelProfileLastIP">115.43.46.4</p>
                        </div>
                    </form>
                    
                    <!--更換Email-->
                    <form>
                        <div class="m-b-md"><h2>更換Email<small>您如果想更改電子郵件地址，请填寫下列欄位。出於安全原因請同時填寫密碼。</small></h2></div>
                        <div class="m-b-md">
                            <span>密碼：</span>
                            <input id="inputProfileCheckPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新的Email：</span>
                            <input id="inputProfileNewEmail" placeholder="Email" class="form-control" type="New Email">
                            <span class="help-block m-b-none">你目前的電子信箱地址是：<c class="LabelProfileNowEmail">al@gmail.com</c></span>
                        </div>

                        <div class="m-b-md">
                            <span>再次確認Email：</span>
                            <input id="inputProfileCheckEmail" placeholder="Email" class="form-control" type="New Email">
                        </div>
                        
                        <button id="btnProfileChangeEmail" type="submit">儲存變更</button>
                    </form>
                    
                    <!--更換密碼-->
                    <form>
                        <div class="m-b-md"><h2>更換密碼</h2></div>

                        <div class="m-b-md">
                            <span>舊的密碼：</span>
                            <input id="inputProfileOldPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新密碼：</span>
                            <input id="inputProfileNewPassword" placeholder="New Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>確認新密碼：</span>
                            <input id="inputProfileCheckNewPassword" placeholder="New Password" class="form-control" type="password">
                        </div>
                        
                        <button id="btnProfileChangePassword" type="submit">儲存變更</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
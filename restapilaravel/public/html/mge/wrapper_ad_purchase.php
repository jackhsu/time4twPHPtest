<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>廣告加購</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">廣告加購</div>
    <div class="form">
        <form id="profileEdit">
            <div class="m-b-md">
                <h2 class="m-b-md">上傳廣告圖片：</h2>
                <div target-view>
                    <img src="images/upload/ad_02.jpg">
                </div>
                <div target-edit>
                    <div class="dropzone" id="selfie">
                        <div class="am-text-success dz-message">
                            將圖片拖移至此<br>或點此上傳圖片
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="m-b-md">廣告刊登內容：</h2>
            <div class="m-b-md">
                <span>公司名稱：</span>
                <input placeholder="公司名稱" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>XXX股份有限公司</p>
            </div>

            <div class="m-b-md">
                <span>公司統編：</span>
                <input placeholder="公司統編" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>12345678</p>
            </div>

            <div class="m-b-md">
                <span>公司TEL：</span>
                <input placeholder="公司TEL" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>02-12345678</p>
            </div>

            <div class="m-b-md">
                <span>負責人姓名：</span>
                <input placeholder="負責人姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>許先生</p>
            </div>

            <div class="m-b-md">
                <span>負責人身份證ID：</span>
                <input placeholder="負責人身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                <p id="customerInfo_TaxNumber" target-view>A123456789</p>
            </div>

            <div class="m-b-md">
                <span>負責人手機號碼：</span>
                <input placeholder="負責人手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                <p id="customerInfo_TaxNumber" target-view>0988123888</p>
            </div>

            <div class="m-b-md">
                <span>公司地址：</span>
                <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                    <option>請選擇地區</option>
                </select>
                <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                <p id="customerInfo_TaxNumber"target-view> </p>
            </div>

            <div class="m-b-md">
                <span>服務地區：</span>
                <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                    <option>請選擇地區</option>
                    <option>北區</option>
                    <option>中區</option>
                    <option>南區</option>
                    <option>全省</option>
                </select>
                <p id="customerInfo_TaxNumber" target-view>全省</p>
            </div>

            <div class="m-b-md">
                <span>申請身份：</span>
                <input placeholder="ex:代書、代辦、介紹人" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>代書</p>
            </div>

            <div class="m-b-md" target-edit>
                <span>手機驗證：</span>
                <input placeholder="" type="text" target-otablecolumn="userinfo.userinfo_UserName">
                <button type="submit">發送驗證碼</button>
            </div>
        </form>

        <button id="btnEditCancel" type="submit">取消</button>
        <button id="btnEditComplete" type="submit">完成</button>
        <button id="btnEdit" type="submit" target-view>修改</button>
        <button type="submit" target-view>加購</button>


    </div>
</div>
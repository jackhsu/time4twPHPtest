<style>
    input[type="radio"] {
        margin-right: 10px;
        width: 15px;
        vertical-align: middle;
    }
    #main-title input {
        height: 35px;
        padding: 0 10px;
        border: 1px solid #ddd;
        border-radius: 5px;
        font-size: 18px;
        color: #333;
        line-height: 35px;
        box-sizing: border-box;
    }
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>加購分身</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">加購分身 
        <div class="pull-right">
            <input class="m-r-xs" placeholder=" (台幣1元等於3pt)" type="text"><button class="btn-success" type="submit">購買點數</button>
        </div>
    </div>
    <div class="form">
            <form>
                    <div class="m-b-md">
                        <span>公司名稱：</span>
                        <input placeholder="公司名稱" type="text">
                    </div>

                    <div class="m-b-md">
                        <span>公司統編：</span>
                        <input placeholder="確認後無法更改">
                    </div>

                    <div class="m-b-md">
                        <span>手機驗證：</span>
                        <input type="text"><button type="button">發送驗證碼</button>
                    </div>

                    <button id="btnAdd" class="btn btn-outline btn-primary" type="submit">產生代號</button>

                    <div class="m-b-md m-t-lg" target-edit>
                        <span>代號選擇：<small class="">一次只能選擇一組</small></span><br>
                        <label> <input value="option1" id="optionsRadios1" type="radio">Test55688</label>
                        <label> <input value="option1" id="optionsRadios2" type="radio">Test55678</label>
                        <label> <input value="option1" id="optionsRadios3" type="radio">Test55888</label>
                    </div>

                    <button id="btnCancel" type="submit" class="m-r-sm" style="background:#e2e2e2;">取消</button>
                    <button id="btnComplete" type="submit" data-toggle="modal" data-target="#myModal">加購分身</button>
            </form>
    </div>

    <h2 class="m-t-md m-b-md">分身管理</h2>
    <table id="example" class="display" width="100%">
        <thead>
            <tr><th>分身帳號</th><th>點數</th><th>操作</th></tr>
        </thead>
        <tbody>
<!--            <tr class="odd">
                <td>Test55688</td>
                <td>213456 pt</td>
                <td>
                    <a href="#" class="m-r-xs">轉入點數</a>
                    <a href="#" class="m-r-xs">停用帳號</a>
                </td>
            </tr>
            <tr class="even">
                <td>Test55688</td>
                <td>56 pt</td>
                <td>
                    <a href="#" class="m-r-xs">啓用帳號</a>
                </td>
            </tr>
            <tr class="odd">
                <td>Test55688</td>
                <td>213456 pt</td>
                <td>
                    <a href="#" class="m-r-xs">轉入點數</a>
                    <a href="#" class="m-r-xs">停用帳號</a>
                </td>
            </tr>-->
        </tbody>
    </table>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">訊息通知</h4>
            </div>
            <div class="modal-body text-center">
                <p>您確定要購買<strong>乙個企業用分身</strong>嗎？</p>
                <p>（需支付60pt）</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-disable" data-dismiss="modal">取消交易</button>
                <button type="button" class="btn-success" data-dismiss="modal">確定購買</button>
            </div>
        </div>
    </div>
</div>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>分身管理</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">分身管理</div>
    <div class="row animated fadeInRight">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="table-responsive"> 
                    <!--sample code-->
                    <table id="example" class="display select dataTable" width="100%">
                        <thead>
                            <tr><th>分身帳號</th><th>點數</th><th>操作</th></tr>
                        </thead>
                        <tbody>
                            <tr class="odd">
                                <td>Test55688</td>
                                <td>213456 pt</td>
                                <td>
                                    <a href="#" class="m-r-xs">加值點數</a>
                                    <a href="#" class="m-r-xs">停用帳號</a>
                                </td>
                            </tr>
                            <tr class="even">
                                <td>Test55688</td>
                                <td>56 pt</td>
                                <td>
                                    <a href="#" class="m-r-xs">啓用帳號</a>
                                </td>
                            </tr>
                            <tr class="odd">
                                <td>Test55688</td>
                                <td>213456 pt</td>
                                <td>
                                    <a href="#" class="m-r-xs">加值點數</a>
                                    <a href="#" class="m-r-xs">停用帳號</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
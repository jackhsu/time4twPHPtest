<style>
    #upload_progress p {
        padding: 10px 5px;
        font-size: 22px;
        color: #000;
        width: calc(100%/2 - 30px);
    }
    #upload_progress span {
        color: gray;
        padding: 5px 15px 0 0;
    }
    #upload_progress p::before {
        content: "";
        display: inline-block;
        width: 26px;
        height: 26px;
        margin-right: 5px;
        background: url(./images/check.png);
        vertical-align: middle;
        opacity: .3;
    }
    #upload_progress p.upload_success::before {
        opacity: 1;
    }
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>刊登資料</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">刊登資料</div>
    <div class="form">
        <form id="profileEdit">
            <div class="m-b-md">
                <span>編碼：</span>
                <p id="customerInfo_TaxNumber" class="form-control-static">FV2277567</p> 
            </div>

            <div class="m-b-md">
                <span>姓名：</span>
                <input placeholder="姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>王先生</p>
            </div>

            <div class="m-b-md">
                <span>地址：</span>
                <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                    <option>請選擇地區</option>
                </select>
                <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                <p id="customerInfo_TaxNumber"target-view> </p>
            </div>

            <div class="m-b-md">
                <span>職業：</span>
                <select target-edit>
                    <option>服務業</option>
                </select>
                <p id="customerInfo_TaxNumber" target-otablecolumn="userinfo_gender.userinfo_gender" target-view>服務業</p>
            </div>

            <div class="m-b-md">
                <span>希望利率：</span>
                <input placeholder="希望利率" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                <p id="customerInfo_TaxNumber" target-view>1.2%</p>
            </div>

            <div class="m-b-md">
                <span>借款理由：</span>
                <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                   Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                </textarea>
                <p id="customerInfo_TaxNumber" target-view>Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,...</p>
            </div>
        </form>

        <button id="btnEditCancel" type="submit">取消</button>
        <button id="btnEditComplete" type="submit">完成</button>
        <button id="btnEdit" type="submit" target-view>修改</button>

        <hr class="hr-line-dashed m-t-lg m-b-lg">

        <div id="upload_progress">
            <p class="upload_success">薪轉資料 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">身份證 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">勞保資料 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">戶籍謄本 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p>房屋權狀 <span></span></p>
            <p class="upload_success">財產清冊 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">土地權狀 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">所得清單 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p class="upload_success">存摺資料 <span class="pull-right m-r-md">2017/07/01 上傳</span></p>
            <p>自然人憑證 <span></span></p>
        </div>

    </div>
</div>
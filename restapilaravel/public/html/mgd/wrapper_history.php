<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>瀏覽紀錄</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">瀏覽紀錄<a class="btn-success pull-right m-t-sm">清除瀏覽紀錄</a></div>
    <table id="example" class="display nowrap" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs">資訊</td>
                <th class="p-h-xs nowrap">借款額度</th>
                <th class="p-h-xs">借款理由</th>
                <th class="p-h-xs">評價</th>
                <th class="p-h-xs">銀行貸款</th>
                <th class="p-h-xs">民間貸款</th>
                <th class="p-h-xs">操作</th>
            </tr>
        </thead>
        <tbody>
<!--            <tr class="odd">
                <td>
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
            </tr>
            <tr class="even">
                <td>
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td> 
            </tr>-->
        </tbody>
    </table>
</div>


<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs">
                        <a>借款資訊</a>
                    </div>
                    <ul class="tab-content">
                        <!--借款資訊-->
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>編號：</span>
                                    <p id="customerInfo_TaxNumber" class="form-control-static h-no"></p> 
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <p id="customerInfo_TaxNumber" class="h-gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>地區：</span>
                                    <p id="customerInfo_TaxNumber" class="h-area" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_TaxNumber" class="h-job" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款額度：</span>
                                    <p id="customerInfo_TaxNumber" class="h-amt" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>評價：</span>
                                    <p id="customerInfo_TaxNumber" class="h-eval" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款理由：</span>
                                    <p id="customerInfo_TaxNumber" class="h-reason" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>銀行有無欠款：</span>
                                    <p id="customerInfo_TaxNumber" class="h-bank" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>民間有無欠款：</span>
                                    <p id="customerInfo_TaxNumber" class="h-priv" target-view></p>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">確定購買</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>
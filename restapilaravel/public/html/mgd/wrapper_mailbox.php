<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
                <h2> 訊息通知 </h2>
                <ol class="breadcrumb">
                        <li class="active"><strong>Overview</strong></li>
                </ol>
        </div>
        <div class="col-lg-2"></div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-content mailbox-content">
                    <div class="file-manager">
                        <div class="space-25"></div>
                        <h5>分類</h5>
                        <ul class="folder-list m-b-md" style="padding: 0">
                            <li><a id="unread"> <i class="fa fa-inbox "></i> 通知 <span class="label label-warning pull-right">16</span> </a></li>
                            <li><a id="read"> <i class="fa fa-envelope-o"></i> 已處理通知</a></li>
                            <li><a href="#"> <i class="fa fa-trash-o"></i> 垃圾桶</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <form method="get" action="#" class="pull-right mail-search">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" placeholder="搜尋通知">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                搜尋
                            </button>
                        </div>
                    </div>
                </form>
                <h2>通知 (16)</h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i></button>
                        <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i></button>

                    </div>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refresh inbox"><i class="fa fa-refresh"></i> 重新整理</button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="標示已處理通知"><i class="fa fa-eye"></i> </button>
                    <button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="移至垃圾桶"><i class="fa fa-trash-o"></i> </button>

                </div>
            </div>
            <div class="mail-box">
                
                <table class="table table-hover table-mail">
                    <tbody>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a>編號:FV2277567</a></td>
                            <td class="mail-subject"><a>民間代書 - 王代書 向您提出購買需求</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>
                            <td class="text-right mail-date">6.10 AM</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a href="mail_detail.html">編號:FV9527123</a> <span class="label label-warning pull-right">雙重認證</span> </td>
                            <td class="mail-subject"><a href="mail_detail.html">XXX股份有限公司 向您提出購買需求</a></td>
                            <td class=""></td>
                            <td class="text-right mail-date">2017/04/13</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a>編號:FV2277567</a></td>
                            <td class="mail-subject"><a>民間代書 - 王代書 向您提出購買需求</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>
                            <td class="text-right mail-date">6.10 AM</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a href="mail_detail.html">編號:FV9527123</a> <span class="label label-warning pull-right">雙重認證</span> </td>
                            <td class="mail-subject"><a href="mail_detail.html">XXX股份有限公司 向您提出購買需求</a></td>
                            <td class=""></td>
                            <td class="text-right mail-date">2017/04/13</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a>編號:FV2277567</a></td>
                            <td class="mail-subject"><a>民間代書 - 王代書 向您提出購買需求</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>
                            <td class="text-right mail-date">6.10 AM</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a href="mail_detail.html">編號:FV9527123</a> <span class="label label-warning pull-right">雙重認證</span> </td>
                            <td class="mail-subject"><a href="mail_detail.html">XXX股份有限公司 向您提出購買需求</a></td>
                            <td class=""></td>
                            <td class="text-right mail-date">2017/04/13</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a>編號:FV2277567</a></td>
                            <td class="mail-subject"><a>民間代書 - 王代書 向您提出購買需求</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>
                            <td class="text-right mail-date">6.10 AM</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a href="mail_detail.html">編號:FV9527123</a> <span class="label label-warning pull-right">雙重認證</span> </td>
                            <td class="mail-subject"><a href="mail_detail.html">XXX股份有限公司 向您提出購買需求</a></td>
                            <td class=""></td>
                            <td class="text-right mail-date">2017/04/13</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a>編號:FV2277567</a></td>
                            <td class="mail-subject"><a>民間代書 - 王代書 向您提出購買需求</a></td>
                            <td class=""><i class="fa fa-paperclip"></i></td>
                            <td class="text-right mail-date">6.10 AM</td>
                        </tr>
                        <tr class="unread" data-toggle="modal" data-target="#myModal2">
                            <td class="check-mail">
                                <input type="checkbox" class="i-checks">
                            </td>
                            <td class="mail-ontact"><a href="mail_detail.html">編號:FV9527123</a> <span class="label label-warning pull-right">雙重認證</span> </td>
                            <td class="mail-subject"><a href="mail_detail.html">XXX股份有限公司 向您提出購買需求</a></td>
                            <td class=""></td>
                            <td class="text-right mail-date">2017/04/13</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!--message modal-->
<div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">訊息通知</h4>
            </div>
            <div class="modal-body">
                <p>您好，<strong>XXX股份有限公司</strong>向您提出購買通知</p>
                <p>您是否願意進行此筆交易？</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">取消</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">立即與投資人聯絡</button>
            </div>
        </div>
    </div>
</div>
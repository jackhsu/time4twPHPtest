<style>
    form a {
        color: #2E75CC;
        text-decoration: underline;
    }
    form a:hover {
        opacity: .5;
    }
    .form input.input-lg{
        width: 40%;
    }
    .form input.input-sm{
        width: 43px;
    }
    /*checkbox radio*/
    .radio-inline {
        display: inline-block;
    }
    .radio input[type="radio"] {
        position: absolute;
        width: 10px;
    }
    .radio label::after {
        width: 13px;
        height: 13px;
    }
    .radio label.m-radio::before {
        top:8px;
    }
    .radio label.m-radio::after {
        top:11px;
    }
    /*select*/
    .chosen-container-single .chosen-single {
        /*color: #333;*/
        border: 0;
        border-radius: 4px;
        height: 33px;
        line-height: 33px;
    }
    /*touchspin*/
    .bootstrap-touchspin,input.touchspin{
        display: inline !important;
    }
    .form button.btn {
        width: 30px;
        background: #878787;
    }
    input.touchspin {
        width: 43px;
        border-radius: 0;
        margin-right: 0
    }
    /*upload_progress*/
    .img-lg {
        height: 120px
    }
    .upload_success::before {
        content: "";
        display: inline-block;
        width: 26px;
        height: 26px;
        margin-right: 5px;
        background: url(./images/check.png);
        vertical-align: middle;
    }
    /* TOOLTIPS */
    .tooltip {
      position: absolute;
      z-index: 1030;
      display: block;
      visibility: visible;
      font-size: 12px;
      line-height: 1.4;
      opacity: 0;
      filter: alpha(opacity=0);
    }
    .tooltip.in {
      opacity: 0.9;
      filter: alpha(opacity=90);
    }
    .tooltip-inner {
      max-width: 300px;
      font-size: 14px;
      line-height: 30px;
      padding: 3px 8px;
      color: #fff;
      text-decoration: none;
      background-color: #EFAE22;
      border-radius: 4px;
    }
    .tooltip-arrow {
      position: absolute;
      width: 0;
      height: 0;
      border-color: transparent;
      border-style: solid;
    }
    .tooltip.right {
      margin-left: 3px;
      padding: 0 5px;
    }
    .tooltip.right .tooltip-arrow {
      top: 50%;
      left: 0;
      margin-top: -5px;
      border-width: 5px 5px 5px 0;
      border-right-color: #EFAE22;
    }
</style>    
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員中心</a>　/　<span>個人資料</span></div>

<?php include("../sidebar.php"); ?>

<div class="right"> 
    <div id="main-title">個人資料</div>
    <div class="row animated fadeInRight">
        <div class="tab-block">
            <div class="tabs"><a class="active">個人資料</a><a>個人設定</a></div>
            <ul class="tab-content">
                <!--個人資料-->
                <li class="form">
                    <div class="step-progress">
                        <div class="step-slider">
                            <div data-id="step1" class="steps-one step-slider-item">
                                <div class="circle"></div>
                                <h3 class="steps-name">1.個人資料</h3>
                            </div>
                            <div data-id="step2" class="steps-two step-slider-item">
                                <div class="circle"></div>
                                <h3 class="steps-name">2.職業資料</h3>
                            </div>
                            <div data-id="step3" class="steps-three step-slider-item">
                                <div class="circle"></div>
                                <h3 class="steps-name">3.信用&財產狀況</h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="step-content">
                            <div id="step1" class="step-content-body">
                                <form>
                                    <div class="row">
                                        <div class="m-b-md">
                                            <span>資料完整度：</span>
                                            <span class="starTag active"></span>
                                            <span class="starTag active"></span>
                                            <span class="starTag active"></span>
                                            <span class="starTag"></span>
                                            <span class="starTag"></span>
                                        </div>
                                        <div class="m-b-md">
                                            <span>會員編碼：</span>
                                            <p id="userinfo_ID">B01451017</p>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*姓名：</span>
                                            <input placeholder="請輸入姓名" type="text" class="required">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*性別：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" value="option1" id="inlineRadio1" checked="">
                                                <label for="inlineRadio1"> 男 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" value="option2" id="inlineRadio2">
                                                <label for="inlineRadio2"> 女 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*手機號碼：</span>
                                            <input placeholder="請輸入手機號碼如：0912345678" type="text" class="input-lg">
                                            <button id="btnUpload" type="submit">傳送驗證碼</button>
                                            <span class="upload_success"></span><br><br>
                                            <input placeholder="請輸入驗證碼" type="text" class="input-lg" style="margin-left:100px;">
                                            <button id="btnUpload" type="submit">驗證碼確認</button>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*身分證字號：</span>
                                            <input placeholder="請輸入身分證字號" type="text" class="required">
                                            <button type="submit" data-toggle="modal" data-target="#myModal">上傳</button>
                                            <span class="upload_success"></span>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*出生年月日：</span>
                                            <input type="text" value="2012-05-15" id="datetimepicker">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*地址：</span>
                                            <select class="chosen-select">
                                                <option>請選擇縣市</option>
                                            </select>
                                            <select class="chosen-select">
                                                <option>請選擇區域</option>
                                            </select>
                                            <br>
                                            <input placeholder="詳細地址" type="text" style="margin-left:65px;width: 60%;" class="m-t-sm">
                                        </div>
                                        <div class="m-b-md">
                                            <span class="m-b-sm">*學歷：</span>
                                            <div class="radio radio-success" style="display: inline-block;">
                                                <input type="radio" id="inlineRadio3" value="option1" name="radioInline" checked="">
                                                <label for="inlineRadio3"> 博士/碩士 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left: 65px;">
                                                <input type="radio" id="inlineRadio4" value="option2" name="radioInline">
                                                <label for="inlineRadio4"> 大學 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left: 65px;">
                                                <input type="radio" id="inlineRadio5" value="option1" name="radioInline" checked="">
                                                <label for="inlineRadio5"> 專科 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left: 65px;">
                                                <input type="radio" id="inlineRadio6" value="option2" name="radioInline">
                                                <label for="inlineRadio6"> 高中/職 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left: 65px;">
                                                <input type="radio" id="inlineRadio7" value="option1" name="radioInline" checked="">
                                                <label for="inlineRadio7"> 國中 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left: 65px;">
                                                <input type="radio" id="inlineRadio8" value="option2" name="radioInline">
                                                <label for="inlineRadio8"> 其他 </label>
                                                <input placeholder="其他" type="text" target-other>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*婚姻：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="inlineRadio9" value="option1" name="radioInline" checked="">
                                                <label for="inlineRadio9"> 未婚 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="inlineRadio10" value="option2" name="radioInline">
                                                <label for="inlineRadio10"> 已婚 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="inlineRadio10" value="option2" name="radioInline">
                                                <label for="inlineRadio10"> 分居 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="inlineRadio10" value="option2" name="radioInline">
                                                <label for="inlineRadio10"> 離婚 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="inlineRadio10" value="option2" name="radioInline">
                                                <label for="inlineRadio10"> 喪偶 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>子女：</span>
                                            <input placeholder="" type="text" class="required m-r-sm input-sm">
                                            <span class="m-r-sm">人</span>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*貸款金額：</span>
                                            <select id="userinfo_loanamounts" class="chosen-select">
                                                <option>請選擇</option>
                                                <option>1萬以下</option>
                                                <option>1萬-5萬</option>
                                                <option>20萬-50萬</option>
                                                <option>50萬-100萬</option>
                                                <option>其他金額(自填寫)</option>
                                            </select>
                                            <input placeholder="其他金額(自填寫)" type="text" target-other>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*希望月付：</span>
                                            <select class="chosen-select">
                                                <option>請選擇</option>
                                                <option>1千-5千</option>
                                                <option>5千-1萬</option>
                                                <option>1萬-2萬</option>
                                                <option>2萬-3萬</option>
                                                <option>3萬-4萬</option>
                                                <option>4萬-5萬</option>
                                                <option>其他金額(自填寫)</option>
                                            </select>
                                            <input id="userinfo_expectrate" placeholder="其他金額(自填寫)" type="text" target-other>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*希望申辦方案：</span>
                                            <select class="chosen-select" data-placeholder="可複選不限次數..." multiple >
                                                <option value="">可複選不限次數</option>
                                                <option value="銀行貸款">銀行貸款</option>
                                                <option value="民間貸款">民間貸款</option>
                                                <option value="房屋貸款">房屋貸款</option>
                                                <option value="土地貸款">土地貸款</option>
                                                <option value="汽車貸款">汽車貸款</option>
                                                <option value="機車貸款">機車貸款</option>
                                                <option value="小額貸款">小額貸款</option>
                                                <option value="婦女貸款">婦女貸款</option>
                                                <option value="勞保貸款">勞保貸款</option>
                                                <option value="軍公教貸款">軍公教貸款</option>
                                                <option value="企業貸款">企業貸款</option>
                                                <option value="自營商貸款">自營商貸款</option>
                                                <option value="負債整合">負債整合</option>
                                                <option value="融資貸款">融資貸款</option>
                                                <option value="不限方案(可以申貸到即可)">不限方案(可以申貸到即可)</option>
                                                <option value="其他(自填)">其他(自填寫)</option>
                                            </select>
                                            <br><br>
                                            <input placeholder="其他(自填)" type="text" target-other style="margin-left:100px;">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*資金用途：</span>
                                            <select class="chosen-select" target-otablecolumn="userinfo.userinfo_expectrate">
                                                <option>請選擇</option>
                                                <option>裝修房屋</option>
                                                <option>購買土地</option>
                                                <option>預計興建建築</option>
                                                <option>置產投資</option>
                                                <option>公司商業週轉</option>
                                                <option>個人週轉</option>
                                                <option>現增認股</option>
                                                <option>消費支出</option>
                                                <option>購車基金</option>
                                                <option>結婚基金</option>
                                                <option>子女教育</option>
                                                <option>投資理財</option>
                                                <option>償還貸款</option>
                                                <option>其他(點選其他下方會跳出填寫空格)</option>
                                            </select>
                                            <input placeholder="其他(自填寫)" type="text" target-other>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            <div id="step2" class="step-content-body out">
                                <form>
                                    <div class="row">
                                        <div class="m-b-md">
                                            <span>有無工作：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio1" value="option01" name="radioInline" checked="">
                                                <label for="Radio1"> 有 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio2" value="option02" name="radioInline">
                                                <label for="Radio2"> 待業 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>是否為公司負責人：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="companyY" value="companyY" name="company" onclick="document.getElementById('companyDiv').style.display=''">
                                                <label for="companyY"> 是 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="companyN" value="companyN" name="company" onclick="document.getElementById('companyDiv').style.display='none'">
                                                <label for="companyN"> 否 </label>
                                            </div>
                                        </div>
                                        <!--點選為公司負責人，則跳出以下部分-->
                                        <div class="m-b-md" id="companyDiv" style="display:none">
                                            <span>是否可提供營登證明：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio5" value="option05" name="radioInline" checked="">
                                                <label for="Radio5"> 可 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio6" value="option06" name="radioInline">
                                                <label for="Radio6"> 否 </label>
                                            </div>
                                            <div class="m-b-md m-t-md">
                                                <span>是否可提供401報表：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="Radio7" value="option07" name="radioInline" checked="">
                                                    <label for="Radio7"> 可 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="Radio8" value="option08" name="radioInline">
                                                    <label for="Radio8"> 否 </label>
                                                </div>
                                            </div>
                                            <div class="m-b-md">
                                                <span>是否可提供變更事項登記表：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="Radio9" value="option09" name="radioInline" checked="">
                                                    <label for="Radio9"> 可 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="Radio10" value="option10" name="radioInline">
                                                    <label for="Radio10"> 否 </label>
                                                </div>
                                            </div>
                                            <div class="m-b-md">
                                                <span>是否可提供結算申報書：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="Radio11" value="option11" name="radioInline" checked="">
                                                    <label for="Radio11"> 可 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="Radio12" value="option12" name="radioInline">
                                                    <label for="Radio12"> 否 </label>
                                                </div>
                                            </div>
                                            <div class="m-b-md">
                                                <span>是否可提供公司帳戶主要往來存摺(備註:提供近一年最佳)：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="Radio13" value="option13" name="radioInline" checked="">
                                                    <label for="Radio13"> 可 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="Radio14" value="option14" name="radioInline">
                                                    <label for="Radio14"> 否 </label>
                                                </div>
                                            </div>
                                            <div class="m-b-md">
                                                <span>必要時是否可提供保人：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="Radio15" value="option15" name="radioInline" checked="">
                                                    <label for="Radio15"> 可 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="Radio16" value="option16" name="radioInline">
                                                    <label for="Radio16"> 否 </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--點選為公司負責人部分結束-->

                                        <div class="m-b-md">
                                            <span>*行 業 別：</span>
                                            <select id="userinfo_worktype" data-placeholder="請選擇..." class="chosen-select"  tabindex="2">
                                                <option value="">請選擇</option>
                                                <optgroup label="資訊科技">
                                                    <option value="軟體網路">軟體網路</option>
                                                    <option value="電信通訊">電信通訊</option>
                                                    <option value="電腦週邊">電腦週邊</option>
                                                    <option value="光電光學">光電光學</option>
                                                    <option value="電子相關">電子相關</option>
                                                    <option value="半導體業">半導體業</option>
                                                </optgroup>
                                                <optgroup label="傳產製造">
                                                    <option value="食品飲料">食品飲料</option>
                                                    <option value="紡織＆鞋類">紡織＆鞋類</option>
                                                    <option value="家具家飾">家具家飾</option>
                                                    <option value="化學製造">化學製造</option>
                                                    <option value="石油製造">石油製造</option>
                                                    <option value="橡膠塑膠">橡膠塑膠</option>
                                                    <option value="金屬製造">金屬製造</option>
                                                    <option value="機械設備">機械設備</option>
                                                    <option value="運輸工具">運輸工具</option>
                                                    <option value="醫材相關">醫材相關</option>
                                                    <option value="營建土木">營建土木</option>
                                                    <option value="其他製造">其他製造</option>
                                                </optgroup>
                                                <optgroup label="農林漁牧礦">
                                                    <option value="農業">農業</option>
                                                    <option value="林業">林業</option>
                                                    <option value="漁業">漁業</option>
                                                    <option value="牧業">牧業</option>
                                                    <option value="礦石/土石採取業">礦石/土石採取業</option>
                                                    <option value="石油/天然氣礦業">石油/天然氣礦業</option>
                                                </optgroup>
                                                <optgroup label="工商服務">
                                                    <option value="法律服務">法律服務</option>
                                                    <option value="會計服務">會計服務</option>
                                                    <option value="顧問研發">顧問研發</option>
                                                    <option value="人力仲介">人力仲介</option>
                                                    <option value="租賃業">租賃業</option>
                                                    <option value="汽車維修">汽車維修</option>
                                                    <option value="徵信保全">徵信保全</option>
                                                    <option value="物流倉儲">物流倉儲</option>
                                                    <option value="貿易業">貿易業</option>
                                                </optgroup>
                                                <optgroup label="民生服務">
                                                    <option value="批發零售">批發零售</option>
                                                    <option value="金融保險">金融保險</option>
                                                    <option value="投資理財">投資理財</option>
                                                    <option value="政治社福">政治社福</option>
                                                </optgroup>
                                                <optgroup label="軍公教">
                                                    <option value="軍人">軍人</option>
                                                    <option value="警察">警察</option>
                                                    <option value="消防員">消防員</option>
                                                    <option value="公務人員">公務人員</option>
                                                    <option value="老師">老師</option>
                                                    <option value="研究員">研究員</option>
                                                    <option value="公營企業">公營企業</option>
                                                </optgroup>
                                                <optgroup label="自由業">
                                                    <option value="自由接案工作者">自由接案工作者</option>
                                                    <option value="計時工作者">計時工作者</option>
                                                </optgroup>
                                                <optgroup label="學生">
                                                    <option value="大學生">大學生</option>
                                                    <option value="碩士生">碩士生</option>
                                                    <option value="博士生">博士生</option>
                                                </optgroup>
                                                <optgroup label="自營商">
                                                    <option value="自營商">自營商</option>
                                                </optgroup>
                                                <optgroup label="其他">
                                                    <option value="家管">家管</option>
                                                    <option value="退休人員">退休人員</option>
                                                    <option value="待業">待業</option>
                                                </optgroup>
                                                <optgroup label="其他">
                                                    <option value="其他">其他</option>
                                                </optgroup>
                                            </select>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*職　　稱：</span>
                                            <select class="chosen-select">
                                                <option>請選擇</option>
                                                <option>企業負責人/董監事</option>
                                                <option>高階主管</option>
                                                <option>一般主管</option>
                                                <option>一般職員</option>
                                                <option>待業</option>
                                            </select>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*公司名稱：</span>
                                            <input placeholder="請輸入公司名稱" type="text" class="required">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*公司電話：</span>
                                            <input placeholder="02-0000-0000" type="text" class="required">
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio17" value="option17" name="radioInline">
                                                <label for="Radio17"> 無公司電話 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>公司統編：</span>
                                            <input placeholder="請輸入公司統編" type="text" class="required">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*平均月薪：</span>
                                            <input placeholder="35,000" type="text" class="required">
                                        </div>
                                        <div class="m-b-md">
                                            <span>*勞保投保：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio18" value="option18" name="radioInline" checked="">
                                                <label for="Radio18"> 有 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio19" value="option19" name="radioInline">
                                                <label for="Radio19"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span class="m-b-sm">*支薪方式：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio20" value="option20" name="radioInline" checked="">
                                                <label for="Radio20"> 薪資轉帳 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio21" value="option21" name="radioInline">
                                                <label for="Radio21"> 薪資單+現金 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio22" value="option22" name="radioInline">
                                                <label for="Radio22"> 薪資單+現金 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-xl m-t-lg">
                                            <span>年　　資：</span>
                                            <select class="chosen-select">
                                                <option>請選擇</option>
                                                <option>半年以下</option>
                                                <option>半年(含)~1年</option>
                                                <option>2年(含)~3年</option>
                                                <option>3年(含)~5年</option>
                                                <option>5年(含)~7年</option>
                                                <option>7年(含)~10年</option>
                                                <option>10年(含)以上</option>
                                            </select>
                                            <span style="color:red;"> (若現職未滿六個月，請務必填寫前職資料)</span>
                                        </div>
                                        <div class="m-b-md">
                                            <span>前職工作：</span>
                                            <input placeholder="請輸入前職工作" type="text" class="required">
                                        </div>
                                        <div class="m-b-md">
                                            <span>前職年資：</span>
                                            <select class="chosen-select">
                                                <option>請選擇</option>
                                                <option>半年以下</option>
                                                <option>半年(含)~1年</option>
                                                <option>2年(含)~3年</option>
                                                <option>3年(含)~5年</option>
                                                <option>5年(含)~7年</option>
                                                <option>7年(含)~10年</option>
                                                <option>10年(含)以上</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                            
                            <div id="step3" class="step-content-body out">
                                <form>
                                    <div class="row">
                                        <div class="m-b-md">
                                            <span class="m-b-sm">*銀行信用狀況：</span>
                                            <div class="radio radio-success" style="display:inline-block">
                                                <input type="radio" id="Radio20" value="option20" name="radioInline" checked="">
                                                <label for="Radio20"> 小白(與銀行無往來) </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left:137px;">
                                                <input type="radio" id="Radio21" value="option21" name="radioInline">
                                                <label for="Radio21"> 遲繳 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left:137px;">
                                                <input type="radio" id="Radio22" value="option22" name="radioInline">
                                                <label for="Radio22"> 協商 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left:137px;">
                                                <input type="radio" id="Radio23" value="option23" name="radioInline">
                                                <label for="Radio23"> 親屬代償註記 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left:137px;">
                                                <input type="radio" id="Radio24" value="option24" name="radioInline">
                                                <label for="Radio24"> 更生 </label>
                                            </div>
                                            <div class="radio radio-success m-b-sm" style="margin-left:137px;">
                                                <input type="radio" id="Radio25" value="option25" name="radioInline">
                                                <label for="Radio25"> 警示帳戶 </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="m-b-md">
                                            <div class="m-b-md m-r-sm">
                                                <span class="m-b-sm">*持有信用卡：</span>
                                                <div class="radio radio-success radio-inline">
                                                    <input type="radio" id="creditCardY" value="creditCardY" name="creditCard" onclick="document.getElementById('creditCardDiv').style.display=''">
                                                    <label for="creditCardY"> 有 </label>
                                                </div>
                                                <div class="radio radio-success radio-inline m-l-sm">
                                                    <input type="radio" id="creditCardN" value="creditCardN" name="creditCard" onclick="document.getElementById('creditCardDiv').style.display='none'">
                                                    <label for="creditCardN"> 無 </label>
                                                </div>
                                            </div>
                                            <div id="creditCardDiv" class="m-b-md" style="margin-left:120px;display: none;">
                                                <span class="m-b-md">銀行名稱：</span>
                                                <select class="chosen-select" data-placeholder="請選擇..." tabindex="4">
                                                    <option value="">請選擇...</option>
                                                    <option value="臺灣銀行">臺灣銀行</option>
                                                    <option value="土地銀行">土地銀行</option>
                                                    <option value="合作金庫">合作金庫</option>
                                                    <option value="第一銀行">第一銀行</option>
                                                    <option value="華南銀行">華南銀行</option>
                                                    <option value="彰化銀行">彰化銀行</option>
                                                    <option value="上海儲蓄銀行">上海儲蓄銀行</option>
                                                    <option value="富邦銀行">富邦銀行</option>
                                                    <option value="國泰世華銀行">國泰世華銀行</option>
                                                    <option value="高雄銀行">高雄銀行</option>
                                                    <option value="兆豐銀行">兆豐銀行</option>
                                                    <option value="花旗銀行">花旗銀行</option>
                                                    <option value="澳盛銀行">澳盛銀行</option>
                                                    <option value="王道銀行">王道銀行</option>
                                                    <option value="臺灣企銀">臺灣企銀</option>
                                                    <option value="渣打銀行">渣打銀行</option>
                                                    <option value="台中銀行">台中銀行</option>
                                                    <option value="京城銀行">京城銀行</option>
                                                    <option value="滙豐銀行">滙豐銀行</option>
                                                    <option value="瑞興銀行">瑞興銀行</option>
                                                    <option value="華泰銀行">華泰銀行</option>
                                                    <option value="新光銀行">新光銀行</option>
                                                    <option value="陽信銀行">陽信銀行</option>
                                                    <option value="板信銀行">板信銀行</option>
                                                    <option value="三信銀行">三信銀行</option>
                                                    <option value="聯邦銀行">聯邦銀行</option>
                                                    <option value="遠東銀行">遠東銀行</option>
                                                    <option value="元大銀行">元大銀行</option>
                                                    <option value="永豐銀行">永豐銀行</option>
                                                    <option value="玉山銀行">玉山銀行</option>
                                                    <option value="凱基銀行">凱基銀行</option>
                                                    <option value="星展銀行">星展銀行</option>
                                                    <option value="台新銀行">台新銀行</option>
                                                    <option value="大眾銀行">大眾銀行</option>
                                                    <option value="日盛銀行">日盛銀行</option>
                                                    <option value="安泰銀行">安泰銀行</option>
                                                    <option value="中國信託">中國信託</option>
                                                </select>
                                                <div class="m-b-md" style="margin-left:97px;">
                                                    <span class="m-b-sm m-r-sm">額度</span><input type="text"><span class="m-l-sm">萬</span>
                                                    <a class="m-l-sm">取消</a><a class="m-l-sm">完成</a>
                                                </div>
                                                <button type="submit" style="width:180px;" class="m-t-sm m-b-lg">+新增信用卡</button>
                                            </div>
                                            <div class="m-b-md" style="margin-left:120px;" target-other>
                                                <span>信用卡名稱：</span>
                                                <select class="chosen-select" data-placeholder="請選擇..." tabindex="4">
                                                    <option value="">可複選不限次數</option>
                                                    <option value="臺灣銀行">臺灣銀行</option>
                                                    <option value="土地銀行">土地銀行</option>
                                                    <option value="合作金庫">合作金庫</option>
                                                    <option value="第一銀行">第一銀行</option>
                                                    <option value="華南銀行">華南銀行</option>
                                                    <option value="彰化銀行">彰化銀行</option>
                                                    <option value="上海儲蓄銀行">上海儲蓄銀行</option>
                                                    <option value="富邦銀行">富邦銀行</option>
                                                    <option value="國泰世華銀行">國泰世華銀行</option>
                                                    <option value="高雄銀行">高雄銀行</option>
                                                    <option value="兆豐銀行">兆豐銀行</option>
                                                    <option value="花旗銀行">花旗銀行</option>
                                                    <option value="澳盛銀行">澳盛銀行</option>
                                                    <option value="王道銀行">王道銀行</option>
                                                    <option value="臺灣企銀">臺灣企銀</option>
                                                    <option value="渣打銀行">渣打銀行</option>
                                                    <option value="台中銀行">台中銀行</option>
                                                    <option value="京城銀行">京城銀行</option>
                                                    <option value="滙豐銀行">滙豐銀行</option>
                                                    <option value="瑞興銀行">瑞興銀行</option>
                                                    <option value="華泰銀行">華泰銀行</option>
                                                    <option value="新光銀行">新光銀行</option>
                                                    <option value="陽信銀行">陽信銀行</option>
                                                    <option value="板信銀行">板信銀行</option>
                                                    <option value="三信銀行">三信銀行</option>
                                                    <option value="聯邦銀行">聯邦銀行</option>
                                                    <option value="遠東銀行">遠東銀行</option>
                                                    <option value="元大銀行">元大銀行</option>
                                                    <option value="永豐銀行">永豐銀行</option>
                                                    <option value="玉山銀行">玉山銀行</option>
                                                    <option value="凱基銀行">凱基銀行</option>
                                                    <option value="星展銀行">星展銀行</option>
                                                    <option value="台新銀行">台新銀行</option>
                                                    <option value="大眾銀行">大眾銀行</option>
                                                    <option value="日盛銀行">日盛銀行</option>
                                                    <option value="安泰銀行">安泰銀行</option>
                                                    <option value="中國信託">中國信託</option>
                                                </select>
                                                <span class="m-l-sm">額度：</span><input type="text" style="width:80px;"><span class="m-l-sm">萬</span>
                                                <br><button type="submit" class="m-t-sm m-b-lg">+新增信用卡</button>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*名下房屋：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio28" value="option28" name="radioInline" checked="">
                                                <label class="m-radio" for="Radio28">
                                                    <span class="m-r-sm">有</span><input class="touchspin" type="text" value="" name="demo1"><span class="m-l-sm">間</span>
                                                </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio29" value="option29" name="radioInline">
                                                <label for="Radio29"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*名下土地：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio30" value="option30" name="radioInline" checked="">
                                                <label class="m-radio" for="Radio30">
                                                    <span class="m-r-sm">有</span><input class="touchspin" type="text" value="" name="demo1"><span class="m-l-sm">筆</span>
                                                </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio31" value="option31" name="radioInline">
                                                <label for="Radio31"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*名下汽車：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio32" value="option32" name="radioInline" checked="">
                                                <label class="m-radio" for="Radio32">
                                                    <span class="m-r-sm">有</span><input class="touchspin" type="text" value="" name="demo1"><span class="m-l-sm">輛</span>
                                                </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio33" value="option33" name="radioInline">
                                                <label for="Radio33"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>*名下機車：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio34" value="option34" name="radioInline" checked="">
                                                <label class="m-radio" for="Radio34">
                                                    <span class="m-r-sm">有</span><input class="touchspin" type="text" value="" name="demo1"><span class="m-l-sm">輛</span>
                                                </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio35" value="option35" name="radioInline">
                                                <label for="Radio35"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>其他：(填寫越詳細越加分喔!)：</span>
                                            <input placeholder="請輸入" type="text">
                                        </div>
                                    </div>

                                    <h2 class="m-b-md text-center m-t-xl">負債</h2>
                                    <div class="row">
                                        <div class="m-b-md">
                                            <span class="m-b-sm m-r-sm">*是否使用現金卡</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Radio36" value="option36" name="radioInline" checked="">
                                                <label class="m-radio" for="Radio36">
                                                    <span class="m-r-sm">有</span><input class="touchspin" type="text" value="" name="demo1"><span class="m-l-sm">張</span></label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="Radio37" value="option37" name="radioInline">
                                                <label for="Radio37"> 無 </label>
                                            </div>
                                        </div>
                                        <div class="m-b-md m-r-sm">
                                            <span class="m-b-sm">*銀行負債：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="bankY" value="bankY" name="bank" onclick="document.getElementById('bankDiv').style.display=''">
                                                <label for="bankY"> 有 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="bankN" value="bankN" name="bank" onclick="document.getElementById('bankDiv').style.display='none'">
                                                <label for="bankN"> 無 </label>
                                            </div>
                                        </div>
                                        <div id="bankDiv" class="m-b-md" style="margin-left:100px;display: none;">
                                            <span class="m-b-md">銀行名稱：</span>
                                            <select class="chosen-select" data-placeholder="請選擇..." tabindex="4">
                                                <option value="">請選擇...</option>
                                                <option value="臺灣銀行">臺灣銀行</option>
                                                <option value="土地銀行">土地銀行</option>
                                                <option value="合作金庫">合作金庫</option>
                                                <option value="第一銀行">第一銀行</option>
                                                <option value="華南銀行">華南銀行</option>
                                                <option value="彰化銀行">彰化銀行</option>
                                                <option value="上海儲蓄銀行">上海儲蓄銀行</option>
                                                <option value="富邦銀行">富邦銀行</option>
                                                <option value="國泰世華銀行">國泰世華銀行</option>
                                                <option value="高雄銀行">高雄銀行</option>
                                                <option value="兆豐銀行">兆豐銀行</option>
                                                <option value="花旗銀行">花旗銀行</option>
                                                <option value="澳盛銀行">澳盛銀行</option>
                                                <option value="王道銀行">王道銀行</option>
                                                <option value="臺灣企銀">臺灣企銀</option>
                                                <option value="渣打銀行">渣打銀行</option>
                                                <option value="台中銀行">台中銀行</option>
                                                <option value="京城銀行">京城銀行</option>
                                                <option value="滙豐銀行">滙豐銀行</option>
                                                <option value="瑞興銀行">瑞興銀行</option>
                                                <option value="華泰銀行">華泰銀行</option>
                                                <option value="新光銀行">新光銀行</option>
                                                <option value="陽信銀行">陽信銀行</option>
                                                <option value="板信銀行">板信銀行</option>
                                                <option value="三信銀行">三信銀行</option>
                                                <option value="聯邦銀行">聯邦銀行</option>
                                                <option value="遠東銀行">遠東銀行</option>
                                                <option value="元大銀行">元大銀行</option>
                                                <option value="永豐銀行">永豐銀行</option>
                                                <option value="玉山銀行">玉山銀行</option>
                                                <option value="凱基銀行">凱基銀行</option>
                                                <option value="星展銀行">星展銀行</option>
                                                <option value="台新銀行">台新銀行</option>
                                                <option value="大眾銀行">大眾銀行</option>
                                                <option value="日盛銀行">日盛銀行</option>
                                                <option value="安泰銀行">安泰銀行</option>
                                                <option value="中國信託">中國信託</option>
                                            </select>
                                            <div class="m-b-md" style="margin-left:97px;">
                                                <span class="m-b-sm m-r-sm">總負債</span><input type="text"><span class="m-l-sm">元</span><br>
                                                <span class="m-b-sm m-r-sm">月  付</span><input type="text"><span class="m-l-sm">元</span><br>
                                                <span class="m-b-sm m-r-sm">剩  餘</span><input type="text"><span class="m-l-sm">元</span>
                                                <a class="m-l-sm">取消</a><a class="m-l-sm">完成</a>
                                            </div>
                                            <button type="submit" style="width:180px;" class="m-t-sm m-b-lg">+新增銀行名稱</button>
                                        </div>
                                        <div class="m-b-md">
                                            <span class="m-b-sm m-r-sm">*民間負債：</span>
                                            <div class="radio radio-success radio-inline">
                                                <input type="radio" id="Y" value="Y" name="Y" onclick="document.getElementById('Div').style.display=''">
                                                <label for="Y"> 有 </label>
                                            </div>
                                            <div class="radio radio-success radio-inline m-l-sm">
                                                <input type="radio" id="N" value="N" name="Y" onclick="document.getElementById('Div').style.display='none'">
                                                <label for="N"> 無 </label>
                                            </div>
                                            <div id="Div" class="m-b-md" style="margin-left:113px;display: none;">
                                                <span class="m-b-sm m-r-sm">總負債</span><input type="text"><span class="m-l-sm">元</span><br>
                                                <span class="m-b-sm m-r-sm">月  付</span><input type="text"><span class="m-l-sm">元</span><br>
                                                <span class="m-b-sm m-r-sm">剩  餘</span><input type="text"><span class="m-l-sm">元</span>
                                            </div>
                                        </div>
                                        <div class="m-b-md">
                                            <span>其他負債：</span>
                                            <input placeholder="請輸入" type="text">
                                        </div>
                                    </div>

                                    <h2 class="m-b-md text-center m-t-xl">上傳資料補充 >></h2>
                                    <div class="row">
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                1.上傳戶籍謄本
                                                <a href="img/sample/01.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="可臨櫃至最近戶政事務所申辦，或是使用自然人憑證上網申請電子資料。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadLand">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                2.上傳薪資證明
                                                <a href="img/sample/02.jpg" title="Image from Unsplash" data-gallery="">範例1</a>
                                                <a href="img/sample/03.png" title="Image from Unsplash" data-gallery="">範例2</a>
                                                <a href="img/sample/04.jpg" title="Image from Unsplash" data-gallery="">範例3</a>
                                                <a href="img/sample/05.jpg" title="Image from Unsplash" data-gallery="">範例4</a>
                                                <a data-toggle="tooltip" data-placement="right" title="每月公司薪資轉帳的存摺內頁+封面，如提供三個月以上有加分。如無薪資轉帳可拍薪資單上傳，證明收入。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                3.上傳房屋權狀或謄本
                                                <a href="img/sample/06.jpg" title="Image from Unsplash" data-gallery="">範例1</a>
                                                <a href="img/sample/07.jpg" title="Image from Unsplash" data-gallery="">範例2</a>
                                                <a data-toggle="tooltip" data-placement="right" title="房屋權狀應都在自家保管，若無可提供謄本亦可。謄本如何申請?可至最近地政，臨櫃辦理，或是使用自然人憑證或是登入地政網頁申請電子資料。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadHouse">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                4.上傳土地權狀或謄本
                                                <a href="img/sample/06.jpg" title="Image from Unsplash" data-gallery="">範例1</a>
                                                <a href="img/sample/07.jpg" title="Image from Unsplash" data-gallery="">範例2</a>
                                                <a data-toggle="tooltip" data-placement="right" title="土地權狀應都在自家保管，若無可提供謄本亦可。謄本如何申請?可至最近地政，臨櫃辦理，或是使用自然人憑證或是登入地政網頁申請電子資料。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadLand">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                5.上傳行照
                                                <a href="img/sample/08.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的機車與汽車行照，可加速申待審核，若無打算使用動產擔保借款，可不必上傳，但上傳越詳細越加分。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                6.上傳401報表
                                                <a href="img/sample/09.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的401報表，可加速申待審核，近一年為最佳。">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                7.上傳公司營業登記證
                                                <a href="img/sample/10.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的公司營業登記證，可加速申待審核">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                8.上傳變更事項登記表
                                                <a href="img/sample/11.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的變更事項登記表，可加速申待審核">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                9.上傳結算申報書
                                                <a href="img/sample/12.jpg" title="Image from Unsplash" data-gallery="">範例</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的結算申報表，可加速申待審核，近三年最佳">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                        <div class="m-b-md tooltip-demo">
                                            <span class="m-b-sm">
                                                10.上傳公司帳戶主要往來存摺(備註:提供近一年最佳)
                                                <a href="img/sample/02.jpg" title="Image from Unsplash" data-gallery="">範例1</a>
                                                <a href="img/sample/03.png" title="Image from Unsplash" data-gallery="">範例2</a>
                                                <a data-toggle="tooltip" data-placement="right" title="上傳您的公司帳本(封面+存摺內頁)，可加速申待審核，近一年最佳">(?)</a>
                                            </span>
                                            <div class="dropzone" id="userinfo_uploadPayroll">
                                                <div class="am-text-success dz-message">
                                                    將圖片拖移至此<br>或點此上傳圖片
                                                </div>
                                            </div><br>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                            
                            <div class="step-content-foot">
                                <button type="button" class="active" name="prev">上一步</button>
                                <button type="button" class="active" name="next">下一步</button>
                                <button type="button" class="active out" name="finish">完成</button>
                            </div>
                        </div>
                    </div>
                    
                </li>
                
                <!--個人設定-->
                <li class="form" style="display: none;">
                    <form>
                        <div class="m-b-md">
                            <span>Email：</span>
                            <p class="LabelProfileNowEmail">email@example.com</p>
                        </div>

                        <div class="m-b-md">
                            <span>狀態：</span>
                            <p id="LabelProfileState" style="color: #6b9e13;">已認證</p>
                            <button id="btnEmail" type="submit" style="width: 130px; display: inline-block;">寄送認證信</button>
                        </div>

                        <div class="m-b-md">
                            <span>註冊時間：</span>
                            <p id="LabelProfileRegistrationTime">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入時間：</span>
                            <p id="LabelProfileLastLogin">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入IP：</span>
                            <p id="LabelProfileLastIP">115.43.46.4</p>
                        </div>
                    </form>
                    
                    <!--更換Email-->
                    <form>
                        <div class="m-b-md"><h2>更換Email<small>您如果想更改電子郵件地址，请填寫下列欄位。出於安全原因請同時填寫密碼。</small></h2></div>
                        <div class="m-b-md">
                            <span>密碼：</span>
                            <input id="inputProfileCheckPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新的Email：</span>
                            <input id="inputProfileNewEmail" placeholder="Email" class="form-control" type="New Email">
                            <span class="help-block m-b-none">你目前的電子信箱地址是：<c class="LabelProfileNowEmail">al@gmail.com</c></span>
                        </div>

                        <div class="m-b-md">
                            <span>再次確認Email：</span>
                            <input id="inputProfileCheckEmail" placeholder="Email" class="form-control" type="New Email">
                        </div>
                        
                        <button id="btnProfileChangeEmail" type="submit">儲存變更</button>
                    </form>
                    
                    <!--更換密碼-->
                    <form>
                        <div class="m-b-md"><h2>更換密碼</h2></div>

                        <div class="m-b-md">
                            <span>舊的密碼：</span>
                            <input id="inputProfileOldPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新密碼：</span>
                            <input id="inputProfileNewPassword" placeholder="New Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>確認新密碼：</span>
                            <input id="inputProfileCheckNewPassword" placeholder="New Password" class="form-control" type="Password">
                        </div>
                        
                        <button id="btnProfileChangePassword" type="submit">儲存變更</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">上傳身分證</h4>
            </div>
            <div class="modal-body" style="padding: 20px 50px 30px;">
                <div class="m-b-md">
                    <img alt="image" class="img-responsive" src="img/sample/14.jpg" target-img="identityCardFrontImg" style="width: 49%;display: inline-block;margin-right: 1%;">
                    <img alt="image" class="img-responsive" src="img/sample/13.jpg" target-img="identityCardBackImg" style="width: 49%;display: inline-block;">
                </div>
                
                <div class="dropzone text-center m-b-xl" id="userinfo_uploadIdCard" style="width: 49%;display: inline-block;margin-right: 1%;">
                    <div class="am-text-success dz-message" style="font-size: 2rem;">上傳<br>身分證正面</div><div class="am-text-success dz-message">
                        將圖片拖移至此<br>或點此上傳圖片
                    </div>
                </div>
                
                <div class="dropzone text-center m-b-xl" id="userinfo_uploadIdCard" style="width: 49%;display: inline-block;">
                    <div class="am-text-success dz-message" style="font-size: 2rem;">上傳<br>身分證反面</div><div class="am-text-success dz-message">
                        將圖片拖移至此<br>或點此上傳圖片
                    </div>
                </div>
                            
                <button type="submit" class="btn-success" data-dismiss="modal">完成</button>
                <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
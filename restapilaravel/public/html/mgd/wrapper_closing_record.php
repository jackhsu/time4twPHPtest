<style>
    .step ul{overflow:hidden;}
    .step li{position:relative; float:left; background:#dadada; box-shadow:3px 0 0 #FFF; font-size:15px; color:#6c6c6c; line-height:26px; text-align:center;}
    .step li:nth-child(1),.step li:nth-child(2),
    .step li:nth-child(3),.step li:nth-child(4),
    .step li:nth-child(5){width:calc(100% / 5);}
    .step li:before,
    .step li:after{content:""; position:absolute; top:-2px; display:block; width:0; height:0; border-style:solid; border-width:15px 0 15px 10px;}
    .step li:before{left:100%; border-color:transparent transparent transparent #FFF; z-index:1;}
    .step li:after{left:99%; border-color:transparent transparent transparent #dadada; z-index:2;}
    .step li.active{background:#2e75cc; color:#FFF;}
    .step li.active:after{border-left-color:#2e75cc;}
    
    .ready{color:#2e75cc;}
    .success{color:#6b9e13;}
    .failed{color:#ff0000;}
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員專區</a>　/　<span>結案紀錄</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">結案紀錄
        <button id="btnNewBid" class="btn-success pull-right" type="submit">新增競標</button>
        <input id="inputBidMoney" placeholder="競標金額" value="" class="form-control pull-right" type="number" target-edit>
    </div>
    <table id="example" class="display nowrap" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs" data-class="expand">資訊</td>
                <th class="p-h-xs" data-hide="phone">信用額度</th>
                <th class="p-h-xs" data-hide="phone">備註</th>
                <th class="p-h-xs" data-hide="phone">申請人數</th>
                <th class="p-h-xs" data-hide="phone">利率</th>
                <th class="p-h-xs" data-hide="phone">撥款時間</th>
                <th class="p-h-xs" data-hide="phone">狀態</th>
                <th class="p-h-xs">操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <ul class="info">
                        <li><span>編號</span><span>FV888994471</span></li>
                        <li><span>公司名稱</span><span>XX企業股份有限公司</span></li>
                        <li><span>地區</span><span>台北市</span></li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="ready">資料準備</b></td>
                <td><a class="agree">送件</a></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li class="active">1.資料準備</li>
                        <li>2.送件</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                        <li>5.完成</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="info">
                        <li><span>編號</span><span>FV888994471</span></li>
                        <li><span>公司名稱</span><span>XX企業股份有限公司</span></li>
                        <li><span>地區</span><span>台北市</span></li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="ready">送件</b></td>
                <td><a class="agree">送件</a></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.資料準備</li>
                        <li class="active">2.送件</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                        <li>5.完成</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="info">
                        <li><span>編號</span><span>FV888994471</span></li>
                        <li><span>公司名稱</span><span>XX企業股份有限公司</span></li>
                        <li><span>地區</span><span>台北市</span></li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="success">完成</b></td>
                <td><a class="agree"></a></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.資料準備</li>
                        <li>2.送件</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                        <li class="active">5.完成</li>
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    
</div>
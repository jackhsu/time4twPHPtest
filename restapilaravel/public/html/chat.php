<div id="talk">
    <a class="toggle">聊聊</a>
    <div class="window">
        <!--側欄-->
        <div class="left">
            <div class="user p-xs" style="background: #5d8911;position: fixed;width: 250px;color:#fff;"></div>
            <div class="search" style="position: fixed;width: 250px;margin-top: 41px;"><input id="inputChatObjectSearch" type="text" placeholder="搜尋使用者"></div>
            <div id="chatBorrowList" style="display: none;margin-top: 83px;">
                <div class="top_btn talk_side_title">置頂</div>
                <ul class="list top"></ul>
                <div class="bot_btn talk_side_title">IUNING BOT</div>
                <ul class="list bot">
                    <li class="C">
                        <div>
                            <p>IUNING BOT</p><p>客服</p>
                        </div>
                    </li>
                </ul>
                <div class="bid_btn talk_side_title">競標中</div>
                <ul class="list bid">
    <!--                <li class="active">
                        <div>
                            <p>許先生</p><p>台北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>張小姐</p><p>新北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>陳先生</p><p>台中市</p>
                        </div>
                    </li>-->
                </ul>
                <div class="agree_btn talk_side_title">已同意借款人列表</div>
                <ul class="list agree">
                </ul>
                <div class="ask_btn talk_side_title">請求同意借款人列表</div>
                <ul class="list ask">
                </ul>
                <div class="all_btn talk_side_title">所有借款人列表</div>
                <ul class="list all">
                </ul>
            </div>
            <div id="chatinvestorList" style="display: none;margin-top: 83px;">
                <div class="top_btn talk_side_title">置頂</div>
                <ul class="list top"></ul>
                <div class="bot_btn talk_side_title">IUNING BOT</div>
                <ul class="list bot">
                    <li class="C">
                        <div>
                            <p>IUNING BOT</p><p>客服</p>
                        </div>
                    </li>
                </ul>
                <div class="agree_btn talk_side_title">已同意投資人列表</div>
                <ul class="list agree">
                </ul>
                <div class="all_btn talk_side_title">所有投資人列表</div>
                <ul class="list all">
                </ul>
            </div>
            <div id="visitorList" style="display: none;margin-top: 83px;">
                <div class="bot_btn talk_side_title">IUNING BOT</div>
                <ul class="list bot">
                    <li class="C">
                        <div>
                            <p>IUNING BOT</p><p>客服</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <!--聊天視窗-->
        <div class="right">
            <div class="id">
                <c>正在跟</c> <c></c> <c>聊聊中</c>
                <a class="close">關閉視窗</a>
<!--                <button id="btnBuildTransaction" class="m-r-md" style="display:none;">建立交易</button>-->
                <a id="btnGoToBid" class="m-r-sm bid_btn" style="display:none;">前往借款人競標</a>
                <a id="btnBorrowInfo" class="btnBorrowInfo m-r-sm bid_btn" borrowID style="display:none;">顯示借款人資料</a>
            </div>

            <div class="msg">
<!--                <dl class="recieve">
                    <dt>聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息</dt>
                    <dd>2016-21-12 15:16</dd>
                </dl>
                <dl class="sent">
                    <dt>聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息</dt>
                    <dd>2016-21-12 15:16</dd>
                </dl>
                <dl class="sent">
                    <div class="illustration m-b-sm"><img src="images/illustration/1.png"></div>
                    <dd>2016-21-12 15:16</dd>
                </dl>
                <dl class="recieve">
                    <div class="illustration m-b-sm"><img src="images/illustration/1.png"></div>
                    <dd>2016-21-12 15:16</dd>
                </dl>-->
            </div>

            <div class="reply">
                <div class="reply-group">
                    <span id="btnUploadFile" class="input-group-addon fileUpload upload m-xs"><i class="fa fa-paperclip"></i><input target="ChatFile" class="upload" id="transient_file" multiple="" type="file"></span>
                    <span id="illustrationListBtn" class="input-group-addon m-r-xs cursor"><i class="fa fa-smile-o"></i></span>
                    <span id="textListBtn" class="input-group-addon cursor"><i class="fa fa-font"></i></span>
                </div>
                <!--輸入問候語-->
                <div id="textList" class="textList p-m">
                    <h3 class="m-b-sm">問候語列表</h3>
                    <ul>
                        <li id="ModalBtn" class="cursor m-b-sm"><i class="fa fa-pencil"></i> 編輯</li>
                        <li class="cursor m-b-sm"><a href=""> 安安你好</a></li>
                        <li class="cursor m-b-sm"><a href=""> 不好意思，已在匯款</a></li>
                        <li class="cursor"><a href=""> 嗯嗯</a></li>
                    </ul>
                </div>
                <!--貼圖-->
                <div id="illustrationList" class="illustrationList p-xxs">
                    <ul>
                        <li><img src="images/illustration/1.png"></li>
                        <li><img src="images/illustration/2.png"></li>
                        <li><img src="images/illustration/3.png"></li>
                        <li><img src="images/illustration/4.png"></li>
                        <li><img src="images/illustration/5.png"></li>
                        <li><img src="images/illustration/6.png"></li>
                        <li><img src="images/illustration/7.png"></li>
                        <li><img src="images/illustration/8.png"></li>
                        <li><img src="images/illustration/9.png"></li>
                        <li><img src="images/illustration/10.png"></li>
                        <li><img src="images/illustration/11.png"></li>
                        <li><img src="images/illustration/12.png"></li>
                        <li><img src="images/illustration/13.png"></li>
                        <li><img src="images/illustration/14.png"></li>
                        <li><img src="images/illustration/15.png"></li>
                        <li><img src="images/illustration/16.png"></li>
                        <li><img src="images/illustration/17.png"></li>
                        <li><img src="images/illustration/18.png"></li>
                        <li><img src="images/illustration/19.png"></li>
                        <li><img src="images/illustration/20.png"></li>
                        <li><img src="images/illustration/21.png"></li>
                        <li><img src="images/illustration/22.png"></li>
                        <li><img src="images/illustration/23.png"></li>
                        <li><img src="images/illustration/24.png"></li>
                    </ul>
                </div>

                <input id="inputMsg" placeholder="輸入訊息......"></input>
                <button id="btnSendMsg">送出</button>
            </div>



        </div>
    </div>
</div>

<!--modal 編輯問候語-->
<div class="modal inmodal fade" id="Modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header p-h-sm">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">編輯問候語</h4>
            </div>
            <div class="modal-body" style="min-height: 200px;">
                <div class="form">
                    <form id="ChatEdit">
                        <div class="m-b-md">
                            <span>1.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                        <div class="m-b-md">
                            <span>2.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                        <div class="m-b-md">
                            <span>3.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                    </form>

                    <button id="btnChatEditCancel" type="submit" data-dismiss="modal">取消</button>
                    <button id="btnChatEditAdd" type="submit">新增</button>
                    <button id="btnChatEditComplete" type="submit" data-dismiss="modal">完成</button>
                    <button id="btnChatEdit" type="submit" target-view>修改</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal-->
<div class="modal inmodal" id="myModalBorrowInfo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs"><a class="active">借款資訊</a><a>借款人資料</a></div>
                    <ul class="tab-content">
                        <!--借款資訊-->
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>編碼：</span>
                                    <p id="customerInfo_ID" class="form-control-static">FV2277567</p> 
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <p id="customerInfo_username" target-view>王先生</p>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <p id="customerInfo_address"target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_job" target-otablecolumn="userinfo_gender.userinfo_gender" target-view>服務業</p>
                                </div>

                                <div class="m-b-md">
                                    <span>希望利率：</span>
                                    <p id="customerInfo_hoptax" target-view>1.2%</p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款理由：</span>
                                    <p id="customerInfo_reason" target-view>Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,...</p>
                                </div>
                            </form>
                        </li>
                        
                        <!--借方資料-->
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <p id="customerInfo_username" target-view>王先生</p>
                                </div>

                                <div class="m-b-md">
                                    <span>手機號碼：</span>
                                    <p id="customerInfo_phone" target-view>0988123888</p>
                                </div>

                                <div class="m-b-md">
                                    <span>出生年月日：</span>
                                    <p id="customerInfo_birthday" target-view>1980/08/08</p>
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <p id="customerInfo_gender" target-otablecolumn="userinfo_gender.userinfo_gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_job" target-otablecolumn="userinfo_gender.userinfo_gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <p id="customerInfo_address"target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>服務地區：</span>
                                    <p id="customerInfo_place" target-view>全省</p>
                                </div>

                                <div class="m-b-md">
                                    <span>申請身份：</span>
                                    <p id="customerInfo_identity" target-view>代書</p>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">確定</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    table.dataTable tr {
        cursor: pointer;
    }
    .wrapper > .right {
        float: right;
        width: 100%;
    }
    /*chat icon*/
    .control a.chatIcon::before {
        width: 30px;
        height: 23px;
        margin-right: 5px;
        background: url(images/talk_icon.png);
    }
    .control a.chatIcon {
        background: #7aa82a;
        color: #fff;
    }
    /*progress*/
    .progress-mini, .progress-mini .progress-bar {
        height: 5px;
        margin-bottom: 0;
    }
    .progress {
        width: calc(100% - 50px);
        display: inline-block;
        overflow: hidden;
        background-color: #f5f5f5;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    }
    .progress-bar {
        float: left;
        width: 0;
        height: 100%;
        line-height: 20px;
        color: #fff;
        background-color: #337ab7;
        -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
        -webkit-transition: width .6s ease;
        -o-transition: width .6s ease;
        transition: width .6s ease;
    }
    .progress-title {
        width: 20px;
    }
    .bg-success {background-color: #1ab394;}
    .bg-warning {background-color: #f8ac59;}
    .bg-danger {background-color: #ed5565;}
</style> 
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<span>企業投資人/投資人列表</span></div>

<div class="right"> 
    <div id="main-title">企業投資人/投資人列表</div>
    <table id="example" class="display dataTable" width="100%"> 
        <thead>
            <tr>
                <th class="p-h-xs">資訊</td>
                <th class="p-h-xs">服務地區</th>
                <th class="p-h-xs">貸款上限</th>
                <th class="p-h-xs">最低利率</th>
                <th class="p-h-xs">開辦費</th>
                <th class="p-h-xs" style="width: 200px;">評價</th>
                <th class="p-h-xs">強力<br>主打專案</th>
                <!--<th class="p-h-xs">可承作<br>專案</th>-->
                <th class="p-h-xs">申貸人數</th>
                <th class="p-h-xs">功能</th>
            </tr>
        </thead>
        
        <!--sample code-->
        <tbody>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>LA124154</span></li>
                        <li><span>公司名稱</span><span>新新貸款中心</span></li>
                        <li><span>稱謂</span><span>地政士</span></li>
                    </ul>
                </td>
                <td>新北市<br>台北市<br>基隆市<br>桃園市<br>新竹市<br>苗栗市</td>
                <td>100萬內<br>無上限</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <div>
                        <span class="progress-title">服務</span>
                        <div class="progress progress-mini">
                            <div style="width: 78%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">快速</span>
                        <div class="progress progress-mini">
                            <div style="width: 38%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <!--<td> </td>-->
                <td>2144455</td>
                <td>
                    <div class="control nowrap">
                        <a class="collect chatIcon">開始聊聊</a>
                        <a class="collect">加入最愛</a>
                    </div>
                </td>
            </tr>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>LA124154</span></li>
                        <li><span>公司名稱</span><span>新新貸款中心</span></li>
                        <li><span>稱謂</span><span>地政士</span></li>
                    </ul>
                </td>
                <td>台北市</td>
                <td>100萬內</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <div>
                        <span class="progress-title">服務</span>
                        <div class="progress progress-mini">
                            <div style="width: 40%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">快速</span>
                        <div class="progress progress-mini">
                            <div style="width: 80%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業</span>
                        <div class="progress progress-mini">
                            <div style="width: 60%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <!--<td> </td>-->
                <td>2144455</td>
                <td>
                    <div class="control nowrap">
                        <a class="collect chatIcon">開始聊聊</a>
                        <a class="collect">加入最愛</a>
                    </div>
                </td>
            </tr>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>LA124154</span></li>
                        <li><span>公司名稱</span><span>新新貸款中心</span></li>
                        <li><span>稱謂</span><span>地政士</span></li>
                    </ul>
                </td>
                <td>桃園市<br>新竹市<br>苗栗市</td>
                <td>100萬內</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <div>
                        <span class="progress-title">服務</span>
                        <div class="progress progress-mini">
                            <div style="width: 78%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">快速</span>
                        <div class="progress progress-mini">
                            <div style="width: 38%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <!--<td> </td>-->
                <td>2144455</td>
                <td>
                    <div class="control nowrap">
                        <a class="collect chatIcon">開始聊聊</a>
                        <a class="collect">加入最愛</a>
                    </div>
                </td>
            </tr>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>LA124154</span></li>
                        <li><span>公司名稱</span><span>新新貸款中心</span></li>
                        <li><span>稱謂</span><span>地政士</span></li>
                    </ul>
                </td>
                <td>苗栗市</td>
                <td>100萬內</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <div>
                        <span class="progress-title">服務</span>
                        <div class="progress progress-mini">
                            <div style="width: 78%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">快速</span>
                        <div class="progress progress-mini">
                            <div style="width: 38%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <!--<td> </td>-->
                <td>2144455</td>
                <td>
                    <div class="control nowrap">
                        <a class="collect chatIcon">開始聊聊</a>
                        <a class="collect">加入最愛</a>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header" style="padding:10px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body">
                <div class="form row">
                    <div class="m-b-md">
                        <span>編   碼：</span>
                        <p id="userinfo_ID" target-view>LA124154</p>
                    </div>
                    <div class="m-b-md">
                        <span>姓   名：</span>
                        <p id="userinfo_ID" target-view>王小姐</p>
                    </div>
                    <div class="m-b-md">
                        <span>性   別：</span>
                        <p id="userinfo_ID" target-view>女</p>
                    </div>
                    <div class="m-b-md">
                        <span>稱   謂：</span>
                        <p id="userinfo_ID" target-view>地政士</p>
                    </div>
                    <div class="m-b-md">
                        <span>最低利率：</span>
                        <p id="userinfo_ID" target-view>0.68 ％</p>
                    </div>
                    <div class="m-b-md">
                        <span>資金額度上限：</span>
                        <p id="userinfo_ID" target-view>最低 10000 元，最高 1000000 元。</p>
                    </div>
                    <div class="m-b-md">
                        <span>開辦費：</span>
                        <p id="userinfo_ID" target-view>最低 1980 元</p>
                    </div>
                    <div class="m-b-md">
                        <span>可承作地區：</span>
                        <p id="userinfo_ID" target-view>台北市、新北市、台中市、桃園市</p>
                    </div>
                   <div class="m-b-md">
                        <span>從事的工作單位：</span>
                        <p id="userinfo_ID" target-view>大大小小國際股份有限公司</p>
                    </div>
                   <div class="m-b-md">
                        <span>強力主打：</span>
                        <p id="userinfo_ID" target-view>銀行貸款、房屋貸款、軍公教貸款</p>
                    </div>
                   <div class="m-b-xxs">
                        <span>承辦項目：</span>
                        <span>銀行貸款</span>
                    </div>
                    <div class="m-b-md" style="margin-left: 94px;">
                        <p>民間貸款</p><br>
                        <p>房屋貸款</p><br>
                        <p>土地貸款</p><br>
                        <p>汽車貸款</p><br>
                        <p>機車貸款</p><br>
                        <p>小額貸款</p><br>
                        <p>婦女貸款</p><br>
                        <p>勞保貸款</p><br>
                        <p>軍公教貸款</p><br>
                        <p>企業貸款</p><br>
                        <p>自營商貸款</p><br>
                        <p>負債整合</p><br>
                        <p>融資貸款</p><br>
                        <p>其他：機械融資</p>
                    </div>
                </div>
                        
                
                <div class="m-t-lg" style="text-align: center;">
                    <button type="submit" class="btn-success" data-dismiss="modal">確定</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
                
            </div>
        </div>
    </div>
</div>
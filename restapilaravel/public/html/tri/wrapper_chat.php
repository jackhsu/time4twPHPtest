<style>
    #talk {position: relative;right:0;}
    #talk .window{width:100%;position: relative;}
    #talk .window .left{height: 600px;}
    #talk .window .right{width:calc( 100% - 170px);}
    #talk .window .right .msg{min-height: 444px;}
    #talk .window .right .reply{position:relative; padding:15px 85px 15px 100px;}
</style>

<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<span> 聊聊</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">聊聊</div>
    
    <!-- talk -->
    <div id="talk">
        <div class="window">
            <!--側欄-->
            <div class="left">
                <div class="search"><input type="text" placeholder="搜尋使用者"></div>
                <div class="bot_btn talk_side_title">IUNING BOT</div>
                <ul class="list bot">
                    <li class="active">
                        <div>
                            <p>IUNING BOT</p><p>客服</p>
                        </div>
                    </li>
                </ul>
                <div class="bid_btn talk_side_title">競標中</div>
                <ul class="list bid">
                    <li class="active">
                        <div>
                            <p>許先生</p><p>台北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>張小姐</p><p>新北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>陳先生</p><p>台中市</p>
                        </div>
                    </li>
                </ul>
                <div class="all_btn talk_side_title">所有借款人列表</div>
                <ul class="list all">
                    <li>
                        <div>
                            <p>張小姐</p><p>新北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>陳先生</p><p>台中市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>張小姐</p><p>新北市</p>
                        </div>
                    </li>
                    <li>
                        <div>
                            <p>陳先生</p><p>台中市</p>
                        </div>
                    </li>
                </ul>
            </div>
            
            <!--聊天視窗-->
            <div class="right">
                <div class="id">許先生
                    <button>建立交易</button>
                </div>
                
                <div class="msg">
                    <dl class="recieve">
                        <dt>聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息</dt>
                        <dd>2016-21-12 15:16</dd>
                    </dl>
                    <dl class="sent">
                        <dt>聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息聊天訊息</dt>
                        <dd>2016-21-12 15:16</dd>
                    </dl>
                    <dl class="sent">
                        <div class="illustration m-b-sm"><img src="images/illustration/1.png"></div>
                        <dd>2016-21-12 15:16</dd>
                    </dl>
                    <dl class="recieve">
                        <div class="illustration m-b-sm"><img src="images/illustration/1.png"></div>
                        <dd>2016-21-12 15:16</dd>
                    </dl>
                </div>
                
                <div class="reply">
                    <div class="reply-group">
                        <span id="btnUploadFile" class="input-group-addon fileUpload upload m-sm"><i class="fa fa-paperclip"></i><input target="ChatFile" class="upload" id="transient_file" multiple="" type="file"></span>
                        <span id="illustrationListBtn" class="input-group-addon m-r-sm cursor"><i class="fa fa-smile-o"></i></span>
                        <span id="textListBtn" class="input-group-addon cursor"><i class="fa fa-font"></i></span>
                    </div>
                    <!--輸入問候語-->
                    <div id="textList" class="textList p-m">
                        <h3 class="m-b-sm">問候語列表</h3>
                        <ul>
                            <li id="ModalBtn" class="cursor m-b-sm"><i class="fa fa-pencil"></i> 編輯</li>
                            <li class="cursor m-b-sm"><a href=""> 安安你好</a></li>
                            <li class="cursor m-b-sm"><a href=""> 不好意思，已在匯款</a></li>
                            <li class="cursor"><a href=""> 嗯嗯</a></li>
                        </ul>
                    </div>
                    <!--貼圖-->
                    <div id="illustrationList" class="illustrationList p-xxs">
                        <ul>
                            <li class="active"><img src="images/illustration/1.png"></li>
                            <li><img src="images/illustration/2.png"></li>
                            <li><img src="images/illustration/3.png"></li>
                            <li><img src="images/illustration/4.png"></li>
                            <li><img src="images/illustration/5.png"></li>
                            <li><img src="images/illustration/6.png"></li>
                            <li><img src="images/illustration/7.png"></li>
                            <li><img src="images/illustration/8.png"></li>
                            <li><img src="images/illustration/9.png"></li>
                            <li><img src="images/illustration/10.png"></li>
                            <li><img src="images/illustration/11.png"></li>
                            <li><img src="images/illustration/12.png"></li>
                            <li><img src="images/illustration/13.png"></li>
                            <li><img src="images/illustration/14.png"></li>
                            <li><img src="images/illustration/15.png"></li>
                            <li><img src="images/illustration/16.png"></li>
                            <li><img src="images/illustration/17.png"></li>
                            <li><img src="images/illustration/18.png"></li>
                            <li><img src="images/illustration/19.png"></li>
                            <li><img src="images/illustration/20.png"></li>
                            <li><img src="images/illustration/21.png"></li>
                            <li><img src="images/illustration/22.png"></li>
                            <li><img src="images/illustration/23.png"></li>
                            <li><img src="images/illustration/24.png"></li>
                        </ul>
                    </div>
                    
                    <textarea id="inputMsg" placeholder="輸入訊息......"></textarea>
                    <button id="btnSendMsg">送出</button>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!--modal 編輯問候語-->
<div class="modal inmodal fade" id="Modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header p-h-sm">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">編輯問候語</h4>
            </div>
            <div class="modal-body" style="min-height: 200px;">
                <div class="form">
                    <form id="ChatEdit">
                        <div class="m-b-md">
                            <span>1.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                        <div class="m-b-md">
                            <span>2.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                        <div class="m-b-md">
                            <span>3.</span>
                            <input placeholder="請輸入問候語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                            <p id="customerInfo_TaxNumber" target-view>安安，您好！</p>
                        </div>
                    </form>

                    <button id="btnChatEditCancel" type="submit" data-dismiss="modal">取消</button>
                    <button id="btnChatEditAdd" type="submit">新增</button>
                    <button id="btnChatEditComplete" type="submit" data-dismiss="modal">完成</button>
                    <button id="btnChatEdit" type="submit" target-view>修改</button>
                </div>
            </div>
        </div>
    </div>
</div>
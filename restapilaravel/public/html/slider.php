<!-- slider -->
<div id="slider" class="owl-carousel">
    <div class="item" style="background-image:url(images/upload/index_slider.png);">
        <div class="outer">
            <div class="inner">
                <h2>借款人</h2>
                <p>線上申請好簡單，個資安全有保障</p>
                <p>一對一諮詢，電話不占線好順暢</p>
                <div class="btn"><a href="#" onclick="show_remind( '需要再提供設計稿製作並會連至快速刊登' )">借款申請</a>or<a href="mgd_investor_list">瀏覽投資人</a></div>
            </div>
        </div>
    </div>
    <div class="item" style="background-image:url(images/upload/index_slider.png);">
        <div class="outer">
            <div class="inner">
                <h2>借款人</h2>
                <p>線上申請好簡單，個資安全有保障</p>
                <p>一對一諮詢，電話不占線好順暢</p>
                <div class="btn"><a href="#" onclick="show_remind( '需要再提供設計稿製作並會連至快速刊登' )">借款申請</a>or<a href="mgd_investor_list">瀏覽投資人</a></div>
            </div>
        </div>
    </div>
    <div class="item" style="background-image:url(images/upload/index_slider.png);">
        <div class="outer">
            <div class="inner">
                <h2>借款人</h2>
                <p>線上申請好簡單，個資安全有保障</p>
                <p>一對一諮詢，電話不占線好順暢</p>
                <div class="btn"><a href="#" onclick="show_remind( '需要再提供設計稿製作並會連至快速刊登' )">借款申請</a>or<a href="mgd_investor_list">瀏覽投資人</a></div>
            </div>
        </div>
    </div>
</div>

<!-- marquee -->
<div id="marquee">
    <div class="wrap">
        <marquee direction="left">
            <a href="#">恭喜何先生透過iuning順利借到理想金額</a><a href="#">恭喜陳老闆被評分為五星級</a><a href="#">感謝！林老闆購買一萬元點數</a><a href="#">公告：Ａ級</a>
        </marquee>
    </div>
</div>


<style>
    /* 星星 */
    .star_bg {
        width: 120px; height: 20px;
        background: url('images/star.png') repeat-x;
        position: relative;
        overflow: hidden;
    }
    .star {
        height: 100%; width: 24px;
        line-height: 6em;
        position: absolute;
        z-index: 3;
    }
    .star:hover,.star.active {
        background: url('images/star.png') repeat-x 0 -20px!important;
        left: 0; z-index: 2;
    }
    .star_1 { left: 0; }
    .star_2 { left: 24px; }
    .star_3 { left: 48px; }
    .star_4 { left: 72px; }
    .star_5 { left: 96px; }
    .star_1:hover,.star_1.active { width: 24px; }
    .star_2:hover,.star_2.active { width: 48px; }
    .star_3:hover,.star_3.active { width: 72px; }
    .star_4:hover,.star_4.active { width: 96px; }
    .star_5:hover,.star_5.active { width: 120px; }

    label { 
        display: block; _display:inline;
        height: 100%; width: 100%;
        cursor: pointer;
    }

    .score { position: absolute; clip: rect(0 0 0 0); }
    .score:checked + .star {    
        background: url('images/star.png') repeat-x 0 -20px;
        left: 0; z-index: 1;
    }
    .score_1:checked ~ .star_1 { width: 24px; }
    .score_2:checked ~ .star_2 { width: 48px; }
    .score_3:checked ~ .star_3 { width: 72px; }
    .score_4:checked ~ .star_4 { width: 96px; }
    .score_5:checked ~ .star_5 { width: 120px; }

    .star_bg:hover .star {  background-image: none; }

    .star_checked {
        background: url('images/star.png') repeat-x 0 -20px;
        left: 0; z-index: 1;
    }
</style>

<div id="sidebar" class="left">
    <!--評價星星-->
    <div id="sidebarStar" class="m-b-md">
        <span>資料完整度：</span>
        <span class="starTag"></span>
        <span class="starTag"></span>
        <span class="starTag"></span>
        <span class="starTag"></span>
        <span class="starTag"></span>
    </div>
<!--    <div id="starBg" class="star_bg pull-right m-b-md">  -->
<!--        <input type="radio" id="starScore1" class="score score_1" value="1" name="score">
        <a href="#starScore1" class="star star_1"></a>
        <input type="radio" id="starScore2" class="score score_2" value="2" name="score">
        <a href="#starScore2" class="star star_2"></a>
        <input type="radio" id="starScore3" class="score score_3" value="3" name="score">
        <a href="#starScore3" class="star star_3"></a>
        <input type="radio" id="starScore4" class="score score_4" value="4" name="score">
        <a href="#starScore4" class="star star_4"></a>
        <input type="radio" id="starScore5" class="score score_5" value="5" name="score">
        <a href="#5" class="star star_5"></a>-->
<!--    </div>-->

    <div class="clearfix"></div>
    
    <!--編號-->
    <div id="sidebarUserinfoID" style="color:#858585;font-size: 16px;" class="p-w-md m-b-xs"></div>
    <!--點數-->
    <div id="sidebarUserinfoPoint" style="color:#858585;font-size: 16px;" class="p-w-md m-b-sm"></div>
    
    <!--使用者名稱-->
    <div id="sidebar_userinfo_UserName" class="user">jason2001820</div>
    <!--<a class="control">身份切換</a>-->
    
    <!--借款人側欄-->
    <ul class="left-menu" userinfo_InExUser="Borrower" style="display: none;" >
        <li>
            <h2>借款人側欄</h2>
            <ul class="sub-menu">
                <!--<li><a href="mgd_points_to_buy" h_target="mgd_points_to_buy">點數購買</a></li> AL 20170817 hide-->
                <li><a href="mgd_collection" h_target="mgd_collection">收藏名單</a></li>
                <li><a href="mgd_history" h_target="mgd_history">瀏覽紀錄</a></li>
                <li><a href="mgd_post_ad" h_target="mgd_post_ad">刊登資料</a></li>
                <li><a href="mgd_closing_record" h_target="mgd_closing_record">結案紀錄</a></li>
                <!--<li><a href="tri_service" h_target="tri_service">客服人員</a></li> AL 20170707 hide-->
            </ul>
        </li>
        <li>
            <h2>會員中心</h2>
            <ul class="sub-menu">
                <li><a href="tri_guide" h_target="tri_guide">使用指南</a></li>
                <li><a href="mgd_profile">個人資料</a></li>
                <li><a id="logoutBtn" href="#">登出</a></li>
            </ul>
        </li>
    </ul>
    
    <!--投資人側欄-->
    <ul class="left-menu" userinfo_InExUser="Investor" style="display: none;" >
        <li>
            <h2>投資人側欄</h2>
            <ul class="sub-menu">
                <li><a href="mgi_post_ad" h_target="mgi_post_ad">張貼廣告</a></li>
                <li><a href="mgi_ad_management" h_target="mgi_ad_management">廣告管理</a></li>
                <li><a href="mgi_points_to_buy" h_target="mgi_points_to_buy">點數購買</a></li>
                <li><a href="mgi_points_history" h_target="mgi_points_history" onclick="show_remind( '需要再提供設計稿製作' )">點數歷史紀錄</a></li>
                <li><a href="mgi_collection" h_target="mgi_collection">收藏名單</a></li>
<!--                <li><a href="mgi_purchased_list" h_target="mgi_purchased_list">已購買名單</a></li>-->
                <li><a href="mgi_bid_record" h_target="mgi_bid_record">競標紀錄</a></li>
                <li><a href="mgi_closing_record" h_target="mgi_closing_record">結案紀錄</a></li>
                <li><a href="mgi_history" h_target="mgi_history">瀏覽紀錄</a></li>
<!--                <li><a href="#">廣告加購(僅限企業戶)</a></li> AL 20170707 hide
                <li><a href="#">分身加購(僅限企業戶)</a></li>
                <li><a href="tri_service" h_target="tri_service">客服人員</a></li>-->
            </ul>
        </li>
        <li>
            <h2>會員中心</h2>
            <ul class="sub-menu">
                <li><a href="tri_guide" h_target="tri_guide">使用指南</a></li>
                <li><a href="mgi_profile">個人資料</a></li>
                <li><a id="logoutBtn" href="#">登出</a></li>
            </ul>
        </li>
    </ul>
    
    <!--企業投資人側欄-->
    <ul class="left-menu" userinfo_InExUser="Company" style="display: none;" >
        <li>
            <h2>企業投資人側欄</h2>
            <ul class="sub-menu">
                <li><a href="mge_post_ad" h_target="mge_post_ad">張貼廣告</a></li>
                <li><a href="mge_ad_management" h_target="mge_ad_management">廣告管理</a></li>
                <li><a href="mge_points_to_buy" h_target="mge_points_to_buy">點數購買</a></li>
                <li><a href="mge_points_history" h_target="mge_points_history" onclick="show_remind( '需要再提供設計稿製作' )">點數歷史紀錄</a></li>
                <li><a href="mge_collection" h_target="mge_collection">收藏名單</a></li>
<!--                <li><a href="mge_purchased_list" h_target="mge_purchased_list">已購買名單</a></li>-->
                <li><a href="mge_bid_record" h_target="mge_bid_record">競標紀錄</a></li>
                <li><a href="mge_closing_record" h_target="mge_closing_record">結案紀錄</a></li>
                <li><a href="mge_history" h_target="mge_history">瀏覽紀錄</a></li>
                <li><a href="mge_ad_purchase" h_target="mge_ad_purchase">廣告加購(僅限企業戶)</a></li>
                <li><a href="mge_account_purchase" h_target="mge_account_purchase">分身加購(僅限企業戶)</a></li>
                <!--<li><a href="mge_account_management" h_target="mge_account_management">分身管理(企金)</a></li>-->
                <!--<li><a href="tri_service" h_target="tri_service">客服人員</a></li> AL 20170707 hide -->
            </ul>
        </li>
        <li>
            <h2>會員中心</h2>
            <ul class="sub-menu">
                <li><a href="tri_guide" h_target="tri_guide">使用指南</a></li>
                <li><a href="mge_profile">個人資料</a></li>
                <li><a id="logoutBtn" href="#">登出</a></li>
            </ul>
        </li>
    </ul>
    
    <!--管理員側欄-->
    <ul class="left-menu" userinfo_InExUser="Administrator" style="display: none;" >
        <li>
            <h2>管理員側欄</h2>
            <ul class="sub-menu">
                <li><a href="mgm_member_management" h_target="mgm_member_management">會員管理</a></li>
                <li><a href="mgm_financial_management" h_target="mgm_financial_management">金流管理</a></li>
            </ul>
        </li>
    </ul>
</div>

<script type="text/javascript" src="js/lib/getpermissions.js"></script>
<script type="text/javascript">
    
        //left-menu
        $('.wrapper > .left .control').click(function(e) {
                $(this).toggleClass('active').siblings('.left-menu').slideToggle();
        });	
        $('.wrapper > .left .left-menu > li h2').click(function(e) {
                $(this).siblings('.sub-menu').slideToggle().parent().siblings().find('.sub-menu').slideUp();
        });
        $(window).on('resize',function(){
                if($(this).width() > 768){
                        $('.wrapper > .left .control').removeClass('active');
                        $('.wrapper > .left .left-menu , .wrapper > .left .sub-menu').css('display','');
                }
        });
    
        $.ajax({
            type:"GET",
            url: "service/users/checkToken",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                'Authorization': "Token "+$.lnbCookie
            },
            data: {},
            success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    
                    $( "[userinfo_inexuser]" ).hide();
                    $( "[userinfo_inexuser=" + data.data.userinfo_InExUser + "]" ).show();

                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });

        
    
        // GetLoginPermissions();
        logoutBtnEvent();
        
</script>
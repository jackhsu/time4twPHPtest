<style>
    .al-menu {
        position: absolute;
        width: 100px;
        top: 86px;
        display: none;
        padding: 10px;
        background: #DDD;
        z-index: 1;
    }
    .al-menu a{
        color: #444 !important;
        font-weight: 400 !important;
    }
    .al-menu a:hover{
        color: #2e75cc !important;
    }
    #navigation li a::before {
        background: url(images/debit_icon.svg) no-repeat;
    }
    #navigation li.btn-3 a::before {
        background: url(images/investor_icon.svg) no-repeat;
    }
    #navigation li.btn-2 a::before,#navigation li.btn-3 a::before {
        background-position: center;
    }
</style>


<div id="header">
    <div class="wrap"> 
        
        <!-- top-link -->     
        <div id="top-link" style="opacity:0;">
            <a class="link" href="#" target="_blank">唉呦尼頻道</a>
            <div class="visit">當前線上人數：<span>12,521,631</span>人</div>
            <div class="lang">
                <a>Language</a>
                <div class="sub-menu"><a href="#">繁體中文</a><a href="#">English</a></div>
            </div>
        </div>

        <!-- logo -->
        <a href="index"><h1 id="logo" style="cursor: pointer;">IUNING 薪實貸貸款媒合平台</h1></a>

        <!-- navigation -->
        <ul id="navigation">
            <li class="btn-2"><a href="tri_borrowers_list">借款人</a></li>
            <li class="btn-3"><a href="tri_investor_list">企業投資人/投資人</a></li>
        </ul>

        <!-- member -->
        <ul class="member">
            <li class="hot"><a href="#" onclick="show_remind( '需要再提供設計稿製作' )" >快速刊登</a></li>
            <li class="link"><a href="register">IUN 免費註冊</a></li>
            <li class="link">
                <div class="al"><a>IUN 會員專區</a></div>
                <div class="al-menu">
                    <span class="login-block"><a href="login">會員登入</a><br></span>
                    <span class="logout-block"><a href="mgd_index">會員專區</a><br><a id="logoutBtn" class="logout-block">登出</a></span>
                </div>
            </li>
            <li class="msg"><a onclick="show_remind( '需要再提供設計稿製作' )"  >訊息<span>1</span></a></li>
        </ul>
    </div>
</div>

<script>
        $('.al').click(function() {
                    $(this).toggleClass('active').siblings('.al-menu').slideToggle();
        });
    
        $.ajax({
            type:"GET",
            url: "service/users/checkToken",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') ,
                'Authorization': "Token "+$.lnbCookie
            },
            data: {},
            success: function(data){
                    console.log(data);
                    data = JSON.parse(data);
                    
                    $( "[userinfo_inexuser]" ).hide();
                    $( "[userinfo_inexuser=" + data.data.userinfo_InExUser + "]" ).show();

                
            },
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(xhr.status); 
                console.log(thrownError); 
            }
        });

    
    check_login();
</script>
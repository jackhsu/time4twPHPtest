<div class="left">
    <div class="expand">
        <h2 style="background-color: #EFEFEF;text-align: left;" class="p-sm m-b-sm">篩選工員</h2>
        
        <button id="sendFilter">送出篩選</button>
        <input type="checkbox" id="option1" class="expand-input">
        <label class="expand-label" for="option1">職稱</label>
        <div id="userinfoTitlePlace" class="expand-wrapper">
            <div class="expand-content">
<!--                <input type="checkbox" id="job1" class="tag-input">-->
                <label class="tag-label block" for="job1">專業人士</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job1" value="受僱">受僱<br>
                    <input type="radio" class="m-r-xs" name="job1" value="開業">開業
                </p>
<!--                <input type="checkbox" id="job2" class="tag-input">-->
                <label class="tag-label block" for="job2">醫療人士</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job2" value="管理階層">管理階層<br>
                    <input type="radio" class="m-r-xs" name="job2" value="正式人員">正式人員<br>
                    <input type="radio" class="m-r-xs" name="job2" value="兼任或約聘/外勤人員/警衛/保全/工員">兼任或約聘/外勤人員/警衛/保全/工員
                </p>
<!--                <input type="checkbox" id="job3" class="tag-input">-->
                <label class="tag-label block" for="job3">公教人員</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job3" value="正式人員">正式人員<br>
                    <input type="radio" class="m-r-xs" name="job3" value="兼任或約聘">兼任或約聘<br>
                    <input type="radio" class="m-r-xs" name="job3" value="外勤人員/警衛/保全/工員">外勤人員/警衛/保全/工員
                </p>
<!--                <input type="checkbox" id="job4" class="tag-input">-->
                <label class="tag-label block" for="job4">績優公司</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job4" value="管理階層">管理階層<br>
                    <input type="radio" class="m-r-xs" name="job4" value="正式人員">正式人員<br>
                    <input type="radio" class="m-r-xs" name="job4" value="外勤或線上作業人員">外勤或線上作業人員<br>
                    <input type="radio" class="m-r-xs" name="job4" value="警衛/保全/工員">警衛/保全/工員<br>
                    <input type="radio" class="m-r-xs" name="job4" value="獎金制人員">獎金制人員
                </p>
<!--                <input type="checkbox" id="job5" class="tag-input">-->
                <label class="tag-label block" for="job5">一般公司行號</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job5" value="管理階層">管理階層<br>
                    <input type="radio" class="m-r-xs" name="job5" value="非管理階層">非管理階層
                </p>
<!--                <input type="checkbox" id="job6" class="tag-input">-->
                <label class="tag-label block" for="job6">軍警人員</label>
                <p class="m-b-sm m-l-sm">
                    <input type="radio" class="m-r-xs" name="job6" value="校級以上軍官">校級以上軍官<br>
                    <input type="radio" class="m-r-xs" name="job6" value="警正以上警官">警正以上警官<br>
                    <input type="radio" class="m-r-xs" name="job6" value="志願役尉級/志願役士官長/警佐">志願役尉級/志願役士官長/警佐<br>
                    <input type="radio" class="m-r-xs" name="job6" value="約聘/工員">約聘/工員<br>
                    <input type="radio" class="m-r-xs" name="job6" value="其他">其他
                </p>
                <input type="checkbox" id="job7" class="tag-input" value="退休人員">
                <label class="tag-label block" for="job7">退休人員</label>
                <input type="checkbox" id="job8" class="tag-input" value="自營商">
                <label class="tag-label block" for="job8">自營商</label>
                <input type="checkbox" id="job9" class="tag-input" value="企業主">
                <label class="tag-label block" for="job9">企業主</label>
                <input type="checkbox" id="job10" class="tag-input" value="家管">
                <label class="tag-label block" for="job10">家管</label>
                <input type="checkbox" id="job11" class="tag-input" value="自由業">
                <label class="tag-label block" for="job11">自由業</label>
                <input type="checkbox" id="job12" class="tag-input" value="其他">
                <label class="tag-label block" for="job12">其他</label>
                <input type="checkbox" id="job13" class="tag-input" value="網路賣家">
                <label class="tag-label block" for="job13">網路賣家</label>
            </div>
        </div>
    </div>
    <div id="userinfoPayPlace" class="expand">
        <input type="checkbox" id="option3" class="expand-input">
        <label class="expand-label" for="option3">月付金額</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <div class="filter-group">
                    <span>元</span>
                    <input id="output1" class="filter-input m-b-md" type="text">
                </div>
                <input id="B1" type="text"
                    data-slider-id="rangeLight"
                    data-slider-min="1000" data-slider-max="50000"
                    data-slider-step="1000" data-slider-value="2000" />
                
                <p class="filter-text">
                    <output for="range" class="output1">NT.1000</output>
                    <span class="pull-right">NT.50000</span>
                </p>
            </div>
        </div>
    </div>
    <div id="userinfoApplicationPlace" class="expand">
        <input type="checkbox" id="option7" class="expand-input">
        <label class="expand-label" for="option7">希望申貸方案</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <input type="checkbox" id="project1" class="tag-input" value="銀行貸款">
                <label class="tag-label col3" for="project1">銀行貸款</label>
                <input type="checkbox" id="project2" class="tag-input" value="房屋貸款">
                <label class="tag-label col3" for="project2">房屋貸款</label>
                <input type="checkbox" id="project3" class="tag-input" value="土地貸款">
                <label class="tag-label col3" for="project3">土地貸款</label>
                <input type="checkbox" id="project4" class="tag-input" value="汽車貸款">
                <label class="tag-label col3" for="project4">汽車貸款</label>
                <input type="checkbox" id="project5" class="tag-input" value="機車貸款">
                <label class="tag-label col3" for="project5">機車貸款</label>
                <input type="checkbox" id="project6" class="tag-input" value="小額貸款">
                <label class="tag-label col3" for="project6">小額貸款</label>
                <input type="checkbox" id="project7" class="tag-input" value="婦女貸款">
                <label class="tag-label col3" for="project7">婦女貸款</label>
                <input type="checkbox" id="project8" class="tag-input" value="勞保貸款">
                <label class="tag-label col3" for="project8">勞保貸款</label>
                <input type="checkbox" id="project9" class="tag-input" value="企業貸款">
                <label class="tag-label col3" for="project9">企業貸款</label>
                <input type="checkbox" id="project10" class="tag-input" value="負債貸款">
                <label class="tag-label col3" for="project10">負債貸款</label>
                <input type="checkbox" id="project11" class="tag-input" value="融資貸款">
                <label class="tag-label col3" for="project11">融資貸款</label>
                <input type="checkbox" id="project12" class="tag-input" value="自營商貸款">
                <label class="tag-label col3" for="project12">自營商貸款</label>
                <input type="checkbox" id="project13" class="tag-input" value="軍公教貸款">
                <label class="tag-label col3" for="project13">軍公教貸款</label>
                <input type="checkbox" id="project14" class="tag-input" value="其他貸款">
                <label class="tag-label col3" for="project14">其他貸款</label>
            </div>
        </div>
    </div>
    <div id="userinfoYearPlace" class="expand">
        <input type="checkbox" id="option4" class="expand-input">
        <label class="expand-label" for="option4">年資</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <input type="radio" class="m-r-xs" name="year" value="半年以下">半年以下<br>
                <input type="radio" class="m-r-xs" name="year" value="半年(含)~1年">半年(含)~1年<br>
                <input type="radio" class="m-r-xs" name="year" value="2年(含)~3年">2年(含)~3年<br>
                <input type="radio" class="m-r-xs" name="year" value="3年(含)~5年">3年(含)~5年<br>
                <input type="radio" class="m-r-xs" name="year" value="7年(含)~10年">7(含)~10年<br>
                <input type="radio" class="m-r-xs" name="year" value="10年(含)以上">10年(含)以上
<!--                <input type="checkbox" id="year1" class="tag-input" value="半年以下"> 
                <label class="tag-label block" for="year1">半年以下</label>
                <input type="checkbox" id="year2" class="tag-input" value="半年(含)~1年">
                <label class="tag-label block" for="year2">半年(含)~1年</label>
                <input type="checkbox" id="year3" class="tag-input" value="2年(含)~3年">
                <label class="tag-label block" for="year3">2年(含)~3年</label>
                <input type="checkbox" id="year4" class="tag-input" value="3年(含)~5年">
                <label class="tag-label block" for="year4">3年(含)~5年</label>
                <input type="checkbox" id="year5" class="tag-input" value="5年(含)~7年">
                <label class="tag-label block" for="year5">5年(含)~7年</label>
                <input type="checkbox" id="year6" class="tag-input" value="7(含)~10年">
                <label class="tag-label block" for="year6">7(含)~10年</label>
                <input type="checkbox" id="year7" class="tag-input" value="10年(含)以上">
                <label class="tag-label block" for="year7">10年(含)以上</label>-->
            </div>
        </div>
    </div>
    <div id="userinfoSalaryPlace" class="expand">
        <input type="checkbox" id="option4" class="expand-input">
        <label class="expand-label" for="option4">月收入</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <div class="filter-group">
                    <span>元</span>
                    <input id="output2" class="filter-input m-b-md" type="text">
                </div>
            </div>
        </div>
    </div>
    <div id="userinfoPropertyPlace" class="expand">
        <input type="checkbox" id="option5" class="expand-input">
        <label class="expand-label" for="option5">財產</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <input type="checkbox" id="job1" class="tag-input" value="信用卡">
                <label class="tag-label block" for="job1">信用卡</label>
                <input type="checkbox" id="job2" class="tag-input" value="房屋">
                <label class="tag-label block" for="job2">房屋</label>
                <input type="checkbox" id="job3" class="tag-input" value="土地">
                <label class="tag-label block" for="job3">土地</label>
                <input type="checkbox" id="job4" class="tag-input" value="汽車">
                <label class="tag-label block" for="job4">汽車</label>
                <input type="checkbox" id="job5" class="tag-input" value="機車">
                <label class="tag-label block" for="job5">機車</label>
            </div>
        </div>
    </div>
    <div id="userinfoAreaPlace" class="expand">
        <input type="checkbox" id="option6" class="expand-input">
        <label class="expand-label" for="option6">地區</label>
        <div class="expand-wrapper" style="padding-bottom: 30px;">
            <div class="expand-content">
                <p>北部地區:</p>
                <input type="checkbox" id="area1" class="tag-input" value="台北">
                <label class="tag-label" for="area1">台北</label>
                <input type="checkbox" id="area2" class="tag-input" value="基隆">
                <label class="tag-label" for="area2">基隆</label>
                <input type="checkbox" id="area3" class="tag-input" value="桃園">
                <label class="tag-label" for="area3">桃園</label>
                <input type="checkbox" id="area4" class="tag-input" value="新竹">
                <label class="tag-label" for="area4">新竹</label>
                <input type="checkbox" id="area5" class="tag-input" value="宜蘭">
                <label class="tag-label" for="area5">宜蘭</label>
                <p>中部地區:</p>
                <input type="checkbox" id="area6" class="tag-input" value="苗栗">
                <label class="tag-label" for="area6">苗栗</label>
                <input type="checkbox" id="area7" class="tag-input" value="台中">
                <label class="tag-label" for="area7">台中</label>
                <input type="checkbox" id="area8" class="tag-input" value="彰化">
                <label class="tag-label" for="area8">彰化</label>
                <input type="checkbox" id="area9" class="tag-input" value="南投">
                <label class="tag-label" for="area9">南投</label>
                <p>南部地區:</p>
                <input type="checkbox" id="area10" class="tag-input" value="雲林">
                <label class="tag-label" for="area10">雲林</label>
                <input type="checkbox" id="area11" class="tag-input" value="嘉義">
                <label class="tag-label" for="area11">嘉義</label>
                <input type="checkbox" id="area12" class="tag-input" value="台南">
                <label class="tag-label" for="area12">台南</label>
                <input type="checkbox" id="area13" class="tag-input" value="高雄">
                <label class="tag-label" for="area13">高雄</label>
                <input type="checkbox" id="area14" class="tag-input" value="屏東">
                <label class="tag-label" for="area14">屏東</label>
                <p>東部地區:</p>
                <input type="checkbox" id="area15" class="tag-input" value="花蓮">
                <label class="tag-label" for="area15">花蓮</label>
                <input type="checkbox" id="area16" class="tag-input" value="台東">
                <label class="tag-label" for="area16">台東</label>
                <p>外島地區:</p>
                <input type="checkbox" id="area17" class="tag-input" value="澎湖">
                <label class="tag-label" for="area17">澎湖</label>
                <input type="checkbox" id="area18" class="tag-input" value="金門">
                <label class="tag-label" for="area18">金門</label>
                <input type="checkbox" id="area19" class="tag-input" value="馬祖">
                <label class="tag-label" for="area19">馬祖</label>
            </div>
        </div>
    </div>
</div>
<script>
    $("#userinfoPayPlace input[id=output1], #userinfoSalaryPlace input[id=output2]").bind("keydown", function(e){
            //0-9 / BackSpace / Delete / Shift / 上下左右
            if( ( (e.keyCode < 48) || (e.keyCode > 105) || (e.keyCode > 57 && e.keyCode < 96) ) 
                    && (e.keyCode !== 8)  && (e.keyCode !== 16) && (e.keyCode !== 46) 
                    && (e.keyCode !== 37) && (e.keyCode !== 38) && (e.keyCode !== 39) && (e.keyCode !== 40) ) {
                    e.preventDefault();
            }
    });
    
    $("#searchReasonBtn").bind("change", function(){
            $("#sendFilter").click();
    });
    
    $("#sendFilter").bind("click",function(){
        var filter = {};

        //職稱
        $.each($("#userinfoTitlePlace input[type=radio]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_title = "";
                filter["userinfo_title"] = filter["userinfo_title"] + $(v).val();
        });
        $.each($("#userinfoTitlePlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_title = "";
                filter["userinfo_title"] = filter["userinfo_title"] + $(v).val();
        });
        //月付金額
        if( $("#userinfoPayPlace input[id=output1]").val() !== "" ) {
                filter["userinfo_pay"] = parseFloat($("#userinfoPayPlace input[id=output1]").val());
        }
        //希望申貸方案
        $.each($("#userinfoApplicationPlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_application = "";
                filter["userinfo_application"] = filter["userinfo_application"] + $(v).val();
        });
        //年資
        if( $("#userinfoYearPlace input[type=radio]:checked").val() !== "undefined" ) {
                filter["userinfo_year"] = $("#userinfoYearPlace input[type=radio]").val();
        }
        //月收入
        if( $("#userinfoSalaryPlace input[id=output2]").val() !== "" ) {
                filter["userinfo_salary"] = parseFloat($("#userinfoSalaryPlace input[id=output2]").val());
        }
        //財產
        $.each($("#userinfoPropertyPlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_property = "";
                filter["userinfo_property"] = filter["userinfo_property"] + $(v).val();
        });
        //地區
        $.each($("#userinfoAreaPlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_area = [];
                filter["userinfo_area"][filter["userinfo_area"].length] = $(v).val();
        });
        //console.log(filter);
        getBorrowersList(filter); 
    });
</script>
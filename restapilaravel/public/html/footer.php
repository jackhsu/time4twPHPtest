<div id="footer">
    <div class="wrap">
        <div class="left">
            <div class="link">
                <h2>相關連結</h2>
                <a class="fb" href="#">facebook</a> <a class="youtube" href="#">youtube</a> 
            </div>
            <div class="contact">
                <h2>客服免費通話</h2>
                <a class="wechat" href="#">wechat</a> <a class="skype" href="#">skype</a> <a class="line" href="#">line</a>
            </div>
        </div>
        <ul class="right">
            <li>
                <h2>服務項目</h2>
                <p><a href="#">民間信貸</a></p>
                <p><a href="#">房屋貸款</a></p>
                <p><a href="#">土地借貸</a></p>
                <p><a href="#">企業貸款</a></p>
                <p><a href="#">辦手機換現金</a></p>
                <p><a href="#">汽機車換現金</a></p>
                <p><a href="#">刷卡購物換現金</a></p>
                </li>
            <li>
                <h2>關於薪貸</h2>
                <p><a href="#">新手上路</a></p>
                <p><a href="#">借款功略</a></p>
                <p><a href="#">信用評估</a></p>
                <p><a href="#">優惠試算</a></p>
                </li>
            <li>
                <h2>相關連結</h2>
                <p><a href="#">辦手機換現金</a></p>
                <p><a href="#">粉絲團專頁</a></p>
                <p><a href="#">續約換現金</a></p>
                <p><a href="#">辦機車換現金</a></p>
                <p><a href="#">手機買賣換現金</a></p>
                <p><a href="#">資金週轉</a></p>
            </li>
            <li>
                <h2>其他服務</h2>
                <p><a href="#">客戶滿意度調查</a></p>
                <p><a href="#">檔案上傳</a></p>
            </li>
        </ul>
  </div>
</div>

<!-- copyright -->
<div id="copyright">
    <p>服務專線：(02)8952-0522 / 服務時間:10:00AM-10:00PM 線上客服 新北市板橋區中山路二段412巷8-2號5樓</p>
    <p>版權所有 IUN薪實貸貸款媒合平台 隱私權保護政策 / IUN法律顧問：惠博法律事務所 白宗弘律師</p>
</div>

<!-- side-direct -->
<ul id="side-direct">
    <li class="home"><a>回首頁</a></li>
    <li class="call"><a>電話</a></li>
    <li class="wechat"><a>WeChat</a><span class="qrcode"><img src="images/qrcode.png"></span></li>
    <li class="line"><a>Line</a></li>
    <li class="skype"><a>Skype</a></li>
    <li class="gotop"><a>回頂端</a></li>
</ul>
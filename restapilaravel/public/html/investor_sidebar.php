<div class="left">
    <div class="expand">
        <h2 style="background-color: #EFEFEF;text-align: left;" class="p-sm m-b-sm">篩選工具</h2>
        
        <button id="sendFilter">送出篩選</button>
        <input type="checkbox" id="option1" class="expand-input">
        <label class="expand-label" for="option1">評價</label>
        <div id="userinfoEvaPlace" class="expand-wrapper">
            <div class="expand-content">
                <p class="m-b-sm">服務態度</p>
                <input id="R" type="text"
                    data-slider-id="slider1"
                    data-slider-min="0" data-slider-max="6"
                    data-slider-step="1" data-slider-value="1"
                    data-slider-id="RC" data-slider-tooltip="hide" data-slider-handle="round"
                    data-slider-rangeHighlights='[{ "start": 9, "end": 10, "class": "category" },
                                                  { "start": 19, "end": 20, "class": "category" },
                                                  { "start": 29, "end": 30, "class": "category" },
                                                  { "start": 39, "end": 40, "class": "category" }]' />
                <p class="m-b-sm">撥款速度</p>
                <input id="Y" type="text"
                    data-slider-id="slider1"
                    data-slider-min="0" data-slider-max="6"
                    data-slider-step="1" data-slider-value="1"
                    data-slider-id="YC" data-slider-tooltip="hide" data-slider-handle="round"
                    data-slider-rangeHighlights='[{ "start": 9, "end": 10, "class": "category" },
                                                  { "start": 19, "end": 20, "class": "category" },
                                                  { "start": 29, "end": 30, "class": "category" },
                                                  { "start": 39, "end": 40, "class": "category" }]' />
                <p class="m-b-sm">專業能力</p>
                <input id="B" type="text"
                    data-slider-id="slider1"
                    data-slider-min="0" data-slider-max="6"
                    data-slider-step="1" data-slider-value="1"
                    data-slider-id="BC" data-slider-tooltip="hide" data-slider-handle="round"
                    data-slider-rangeHighlights='[{ "start": 9, "end": 10, "class": "category" },
                                                  { "start": 19, "end": 20, "class": "category" },
                                                  { "start": 29, "end": 30, "class": "category" },
                                                  { "start": 39, "end": 40, "class": "category" }]' />
                <p class="m-b-sm">整體感受</p>
                <input id="G" type="text"
                    data-slider-id="slider1"
                    data-slider-min="0" data-slider-max="6"
                    data-slider-step="1" data-slider-value="1"
                    data-slider-id="GC" data-slider-tooltip="hide" data-slider-handle="round"
                    data-slider-rangeHighlights='[{ "start": 9, "end": 10, "class": "category" },
                                                  { "start": 19, "end": 20, "class": "category" },
                                                  { "start": 29, "end": 30, "class": "category" },
                                                  { "start": 39, "end": 40, "class": "category" }]' />
            </div>
        </div>
    </div>
    <div id="userinfoLimitPlace" class="expand">
        <input type="checkbox" id="option2" class="expand-input">
        <label class="expand-label" for="option2">申貸金額</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <div class="filter-group">
                    <span>元</span>
                    <input id="output1" class="filter-input m-b-md" type="text">
                </div>
            </div>
        </div>
    </div>
<!--希望月付屬於借款人列表??-->
<!--    <div class="expand">
        <input type="checkbox" id="option3" class="expand-input">
        <label class="expand-label" for="option3">希望月付</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <div class="filter-group">
                    <span>元</span>
                    <input id="output1" class="filter-input m-b-md" type="text">
                </div>
                <input id="B1" type="text"
                    data-slider-id="rangeLight"
                    data-slider-min="1000" data-slider-max="50000"
                    data-slider-step="1000" data-slider-value="2000" />
                
                <p class="filter-text">
                    <output for="range" class="output1">NT.1000</output>
                    <span class="pull-right">NT.50000</span>
                </p>
            </div>
        </div>
    </div>-->
    <div id="userinfoStagingPlace" class="expand">
        <input type="checkbox" id="option4" class="expand-input">
        <label class="expand-label" for="option4">希望期數</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <div class="filter-group">
                    <span>期</span>
                    <input id="output2" class="filter-input m-b-md" type="text">
                </div>
                <input id="B2" type="text"
                    data-slider-id="rangeLight"
                    data-slider-min="1" data-slider-max="48"
                    data-slider-step="1" data-slider-value="1" />
                
                <p class="filter-text">
                    <output for="range" class="output2">1期</output>
                    <span class="pull-right">48期</span>
                </p>
            </div>
        </div>
    </div>
    <div id="userinfoServiceAreaPlace" class="expand">
        <input type="checkbox" id="option5" class="expand-input">
        <label class="expand-label" for="option5">服務地區</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <p>北部地區:</p>
                <input type="checkbox" id="area1" class="tag-input" value="台北">
                <label class="tag-label" for="area1">台北</label>
                <input type="checkbox" id="area2" class="tag-input" value="基隆">
                <label class="tag-label" for="area2">基隆</label>
                <input type="checkbox" id="area3" class="tag-input" value="桃園">
                <label class="tag-label" for="area3">桃園</label>
                <input type="checkbox" id="area4" class="tag-input" value="新竹">
                <label class="tag-label" for="area4">新竹</label>
                <input type="checkbox" id="area5" class="tag-input" value="宜蘭">
                <label class="tag-label" for="area5">宜蘭</label>
                <p>中部地區:</p>
                <input type="checkbox" id="area6" class="tag-input" value="苗栗">
                <label class="tag-label" for="area6">苗栗</label>
                <input type="checkbox" id="area7" class="tag-input" value="台中">
                <label class="tag-label" for="area7">台中</label>
                <input type="checkbox" id="area8" class="tag-input" value="彰化">
                <label class="tag-label" for="area8">彰化</label>
                <input type="checkbox" id="area9" class="tag-input" value="南投">
                <label class="tag-label" for="area9">南投</label>
                <p>南部地區:</p>
                <input type="checkbox" id="area10" class="tag-input" value="雲林">
                <label class="tag-label" for="area10">雲林</label>
                <input type="checkbox" id="area11" class="tag-input" value="嘉義">
                <label class="tag-label" for="area11">嘉義</label>
                <input type="checkbox" id="area12" class="tag-input" value="台南">
                <label class="tag-label" for="area12">台南</label>
                <input type="checkbox" id="area13" class="tag-input" value="高雄">
                <label class="tag-label" for="area13">高雄</label>
                <input type="checkbox" id="area14" class="tag-input" value="屏東">
                <label class="tag-label" for="area14">屏東</label>
                <p>東部地區:</p>
                <input type="checkbox" id="area15" class="tag-input" value="花蓮">
                <label class="tag-label" for="area15">花蓮</label>
                <input type="checkbox" id="area16" class="tag-input" value="台東">
                <label class="tag-label" for="area16">台東</label>
                <p>外島地區:</p>
                <input type="checkbox" id="area17" class="tag-input" value="澎湖">
                <label class="tag-label" for="area17">澎湖</label>
                <input type="checkbox" id="area18" class="tag-input" value="金門">
                <label class="tag-label" for="area18">金門</label>
                <input type="checkbox" id="area19" class="tag-input" value="馬祖">
                <label class="tag-label" for="area19">馬祖</label>
            </div>
        </div>
    </div>
    <div id="userinfoTitlePlace" class="expand">
        <input type="checkbox" id="option6" class="expand-input">
        <label class="expand-label" for="option6">職稱</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <input type="checkbox" id="job1" class="tag-input" value="地政士代書">
                <label class="tag-label block" for="job1">地政士代書</label>
                <input type="checkbox" id="job2" class="tag-input" value="代辦人員">
                <label class="tag-label block" for="job2">代辦人員</label>
                <input type="checkbox" id="job3" class="tag-input" value="財務規劃機構">
                <label class="tag-label block" for="job3">財務規劃機構</label>
                <input type="checkbox" id="job4" class="tag-input" value="專業投資人">
                <label class="tag-label block" for="job4">專業投資人</label>
                <input type="checkbox" id="job5" class="tag-input" value="銀行專員">
                <label class="tag-label block" for="job5">銀行專員</label>
            </div>
        </div>
    </div>
    <div id="userinfoProPlace" class="expand">
        <input type="checkbox" id="option7" class="expand-input">
        <label class="expand-label" for="option7">強力主打</label>
        <div class="expand-wrapper">
            <div class="expand-content">
                <input type="checkbox" id="project1" class="tag-input" value="銀行貸款">
                <label class="tag-label col3" for="project1">銀行貸款</label>
                <input type="checkbox" id="project2" class="tag-input" value="房屋貸款">
                <label class="tag-label col3" for="project2">房屋貸款</label>
                <input type="checkbox" id="project3" class="tag-input" value="土地貸款">
                <label class="tag-label col3" for="project3">土地貸款</label>
                <input type="checkbox" id="project4" class="tag-input" value="汽車貸款">
                <label class="tag-label col3" for="project4">汽車貸款</label>
                <input type="checkbox" id="project5" class="tag-input" value="機車貸款">
                <label class="tag-label col3" for="project5">機車貸款</label>
                <input type="checkbox" id="project6" class="tag-input" value="小額貸款">
                <label class="tag-label col3" for="project6">小額貸款</label>
                <input type="checkbox" id="project7" class="tag-input" value="婦女貸款">
                <label class="tag-label col3" for="project7">婦女貸款</label>
                <input type="checkbox" id="project8" class="tag-input" value="勞保貸款">
                <label class="tag-label col3" for="project8">勞保貸款</label>
                <input type="checkbox" id="project9" class="tag-input" value="企業貸款">
                <label class="tag-label col3" for="project9">企業貸款</label>
                <input type="checkbox" id="project10" class="tag-input" value="負債貸款">
                <label class="tag-label col3" for="project10">負債貸款</label>
                <input type="checkbox" id="project11" class="tag-input" value="融資貸款">
                <label class="tag-label col3" for="project11">融資貸款</label>
                <input type="checkbox" id="project12" class="tag-input" value="自營商貸款">
                <label class="tag-label col3" for="project12">自營商貸款</label>
                <input type="checkbox" id="project13" class="tag-input" value="軍公教貸款">
                <label class="tag-label col3" for="project13">軍公教貸款</label>
                <input type="checkbox" id="project14" class="tag-input" value="其他貸款">
                <label class="tag-label col3" for="project14">其他貸款</label>
            </div>
        </div>
    </div>
    <div id="userinfoFeePlace" class="expand">
        <input type="checkbox" id="option8" class="expand-input">
        <label class="expand-label" for="option8">開辦費</label>
        <div class="expand-wrapper">
            <div class="expand-content" style="padding: 10px 30px 30px 38px;">
                <input id="B3" type="text"
                    data-slider-id="rangeDark"
                    data-slider-min="1000" data-slider-max="10000"
                    data-slider-step="1000" data-slider-value="2000" />
                
                <p class="filter-text">
                    <output for="range" class="output3">NT.1000</output>
                    <span class="pull-right">NT.10000</span>
                </p>
            </div>
        </div>
    </div>
</div>
<script>
    $("#userinfoLimitPlace input[id=output1], #userinfoStagingPlace input[id=output2]").bind("keydown", function(e){
            console.log(e.keyCode);
            if( ( (e.keyCode < 48) || (e.keyCode > 105) || (e.keyCode > 57 && e.keyCode < 96) ) 
                    && (e.keyCode !== 8)  && (e.keyCode !== 16) && (e.keyCode !== 46) 
                    && (e.keyCode !== 37) && (e.keyCode !== 38) && (e.keyCode !== 39) && (e.keyCode !== 40) ) {
                    e.preventDefault();
            }
    });

    $("#searchReasonBtn").bind("change", function(){
            $("#sendFilter").click();
    });

    $("#sendFilter").bind("click",function(){
        var filter = {
            'userinfo_serviceeva': 0,
            'userinfo_speedeva'  : 0,
            'userinfo_proeva'    : 0,
            'userinfo_toleva'    : 0,
            'userinfo_fee'       : 0
        };

        //服務態度
        filter["userinfo_serviceeva"] = parseFloat($('#userinfoEvaPlace #R').val());
        //撥款速度
        filter["userinfo_speedeva"] = parseFloat($('#userinfoEvaPlace #Y').val());
        //專業能力
        filter["userinfo_proeva"] = parseFloat($('#userinfoEvaPlace #B').val());
        //整體感受
        filter["userinfo_toleva"] = parseFloat($('#userinfoEvaPlace #G').val());
        //申貸金額
        if( $("#userinfoLimitPlace input[id=output1]").val() !== "" ) {
                filter["userinfo_limit"] = parseFloat($("#userinfoLimitPlace input[id=output1]").val());
        }
        //希望期數
        if( $("#userinfoStagingPlace input[id=output1]").val() !== "" ) {
                filter["userinfo_staging"] = parseFloat($("#userinfoStagingPlace input[id=output2]").val());
        }
        //服務地區
        $.each($("#userinfoServiceAreaPlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_servicearea = [];
                filter["userinfo_servicearea"][filter["userinfo_servicearea"].length] = $(v).val();
        });
        //職稱
        $.each($("#userinfoTitlePlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_title = "";
                filter["userinfo_title"] = filter["userinfo_title"] + $(v).val();
        });
        //希望申貸方案
        $.each($("#userinfoProPlace input[type=checkbox]:checked").get(),function(k,v){
                if(k == 0) filter.userinfo_pro = "";
                filter["userinfo_pro"] = filter["userinfo_pro"] + $(v).val();
        });
        //開辦費
        filter["userinfo_fee"] = parseFloat($("#userinfoFeePlace #B3").val());
        //console.log(filter);
        getInvestorList(filter); 
    });
</script>
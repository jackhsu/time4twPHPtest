<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<span>會員管理</span></div>

<?php include("../sidebar.php"); ?>


<div class="right">
    <div id="main-title">會員管理
        <a id="btnClearProjectChatroom" class="btn-success pull-right m-t-sm">重製聊天權限與競標</a>
        <a class="btn-success pull-right m-t-sm" data-toggle="modal" data-target="#myModalAdd">新增投資人</a>
    </div>
    <table id="example" class="display" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs">資訊</th>
                <th class="p-h-xs">權限</th>
                <th class="p-h-xs">帳號</th>
                <th class="p-h-xs">評價</th>
                <th class="p-h-xs">最後上線IP</th>
                <th class="p-h-xs">最後上線時間</th>
                <th class="p-h-xs">操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <ul class="info nowrap">
                        <li> 
                            <span>編號</span> 
                            <span>FV123456789</span>
                        </li>
                        <li> 
                            <span>使用者</span> 
                            <span>李先生</span>
                        </li>
                        <li> 
                            <span>地區</span> 
                            <span>新北市</span>
                        </li>
                    </ul>
                </td>
                <td>借款人</td>
                <td>test@gmail.com</td>
                <td style="text-align: center;">
                    <div id="starBg" class="star_bg">                    	
                        <input type="radio" id="starScore1" class="score score_1" value="1" name="score">
                        <a href="#starScore1" class="star star_1"></a>
                        <input type="radio" id="starScore2" class="score score_2" value="2" name="score">
                        <a href="#starScore2" class="star star_2"></a>
                        <input type="radio" id="starScore3" class="score score_3" value="3" name="score">
                        <a href="#starScore3" class="star star_3"></a>
                        <input type="radio" id="starScore4" class="score score_4" value="4" name="score">
                        <a href="#starScore4" class="star star_4"></a>
                        <input type="radio" id="starScore5" class="score score_5" value="5" name="score">
                        <a href="#5" class="star star_5"></a>
                    </div>
                </td>
                <td>108.0.0.1</td>
                <td>2017/08/01</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal">編輯</a><br><a>停權</a>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="info nowrap">
                        <li> 
                            <span>編號</span> 
                            <span>FV123456789</span>
                        </li>
                        <li> 
                            <span>使用者</span> 
                            <span>全國股份有限公司</span>
                        </li>
                        <li> 
                            <span>地區</span> 
                            <span>台中市</span>
                        </li>
                    </ul>
                </td>
                <td>企業</td>
                <td>test3@gmail.com</td>
                <td style="text-align: center;">
                    <div id="starBg" class="star_bg">                    	
                        <input type="radio" id="starScore1" class="score score_1" value="1" name="score">
                        <a href="#starScore1" class="star star_1"></a>
                        <input type="radio" id="starScore2" class="score score_2" value="2" name="score">
                        <a href="#starScore2" class="star star_2"></a>
                        <input type="radio" id="starScore3" class="score score_3" value="3" name="score">
                        <a href="#starScore3" class="star star_3"></a>
                        <input type="radio" id="starScore4" class="score score_4" value="4" name="score">
                        <a href="#starScore4" class="star star_4"></a>
                        <input type="radio" id="starScore5" class="score score_5" value="5" name="score">
                        <a href="#5" class="star star_5"></a>
                    </div>
                </td>
                <td>111.0.0.1</td>
                <td>2017/07/15</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal">編輯</a><br><a>停權</a>
                </td>
            </tr>
            <tr>
                <td>
                    <ul class="info nowrap">
                        <li> 
                            <span>編號</span> 
                            <span>FV123456789</span>
                        </li>
                        <li> 
                            <span>使用者</span> 
                            <span>范先生</span>
                        </li>
                        <li> 
                            <span>地區</span> 
                            <span>高雄市</span>
                        </li>
                    </ul>
                </td>
                <td>個金</td>
                <td>test5@gmail.com</td>
                <td style="text-align: center;">
                    <div id="starBg" class="star_bg">                    	
                        <input type="radio" id="starScore1" class="score score_1" value="1" name="score">
                        <a href="#starScore1" class="star star_1"></a>
                        <input type="radio" id="starScore2" class="score score_2" value="2" name="score">
                        <a href="#starScore2" class="star star_2"></a>
                        <input type="radio" id="starScore3" class="score score_3" value="3" name="score">
                        <a href="#starScore3" class="star star_3"></a>
                        <input type="radio" id="starScore4" class="score score_4" value="4" name="score">
                        <a href="#starScore4" class="star star_4"></a>
                        <input type="radio" id="starScore5" class="score score_5" value="5" name="score">
                        <a href="#5" class="star star_5"></a>
                    </div>
                </td>
                <td>119.0.8.1</td>
                <td>2017/07/23</td>
                <td>
                    <a data-toggle="modal" data-target="#myModal">編輯</a><br><a>停權</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>


<!--modal 編輯-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs"><a class="active">貸款資訊(個、金)</a><a>貸款資訊(企業)</a><a>詳細資料(個、金)</a><a>詳細資料(企業)</a></div>
                    <ul class="tab-content">
                        <!--貸款資訊 個人、投資人-->
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>編碼：</span>
                                    <input placeholder="FV2277567" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>希望利率：</span>
                                    <input placeholder="希望利率" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>借款理由：</span>
                                    <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                                       Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                                    </textarea>
                                </div>
                            </form>
                        </li>
                        
                        <!--貸款資訊 企業-->
                        <li class="form" style="display:none;">
                            <form>
                                <div class="m-b-md">
                                    <span>標語：</span>
                                    <input placeholder="標語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>放款速度：</span>
                                    <input placeholder="一天" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>還款金額：</span>
                                    <input placeholder="1000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>


                                <div class="m-b-md">
                                    <span>貸款期限：</span>
                                    <input placeholder="60期" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>利率：</span>
                                    <input placeholder="利率" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>可撥款金額：</span>
                                    <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>期數：</span>
                                    <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>申請條件：</span>
                                    <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                                       Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                                    </textarea>
                                </div>

                                <div class="m-b-md">
                                    <span>貸款優勢：</span>
                                    <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                                       Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                                    </textarea>
                                </div>
                            </form>
                        </li>
                        
                        <!--詳細資料 個人、投資人-->
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>手機號碼：</span>
                                    <input placeholder="手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>身份證ID：</span>
                                    <input placeholder="身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <input placeholder="姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>出生年月日：</span>
                                    <div id="data_1" class="input-group date" style="display: inline;" target-otablecolumn="userinfo.userinfo_UserAge" target-edit>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" value="03/04/2014">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <select target-edit>
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <select target-otablecolumn="userinfo_Job.userinfo_Job" target-edit>
                                        <option selected disabled>請選擇</option>
                                        <option value="農林漁牧業">農林漁牧業</option>
                                        <option value="製造業">製造業</option>
                                        <option value="營造業">營造業</option>
                                        <option value="批發及零售業">批發及零售業</option>
                                        <option value="運輸及倉儲業">運輸及倉儲業</option>
                                        <option value="住宿及餐飲業">住宿及餐飲業</option>
                                        <option value="資訊及通訊傳播業">資訊及通訊傳播業</option>
                                        <option value="金融及保險業">金融及保險業</option>
                                        <option value="不動產業">不動產業</option>
                                        <option value="科學及技術服務業">科學及技術服務業</option>
                                        <option value="公共行政及國防">公共行政及國防</option>
                                        <option value="教育服務業">教育服務業</option>
                                        <option value="醫療保健及社會工作服務業">醫療保健及社會工作服務業</option>
                                        <option value="藝術、娛樂及休閒服務業">藝術、娛樂及休閒服務業</option>
                                        <option value="其他">其他</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                    </select>
                                    <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>
                            </form>
                        </li>
                        
                        <!--詳細資料 企業-->
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>公司名稱：</span>
                                    <input placeholder="公司名稱" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司統編：</span>
                                    <input placeholder="公司統編" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司TEL：</span>
                                    <input placeholder="公司TEL" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人姓名：</span>
                                    <input placeholder="負責人姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人身份證ID：</span>
                                    <input placeholder="負責人身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人手機號碼：</span>
                                    <input placeholder="負責人手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司地址：</span>
                                    <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                    </select>
                                    <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>服務地區：</span>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                        <option>北區</option>
                                        <option>中區</option>
                                        <option>南區</option>
                                        <option>全省</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>申請身份：</span>
                                    <input placeholder="ex:代書、代辦、介紹人" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">儲存</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal 新增-->
<div class="modal inmodal" id="myModalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">新增投資人</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs"><a class="active">貸款資訊</a><a>詳細資料</a></div>
                    <ul class="tab-content">
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>標語：</span>
                                    <input placeholder="標語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>放款速度：</span>
                                    <input placeholder="一天" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>還款金額：</span>
                                    <input placeholder="1000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>


                                <div class="m-b-md">
                                    <span>貸款期限：</span>
                                    <input placeholder="60期" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>利率：</span>
                                    <input placeholder="利率" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>可撥款金額：</span>
                                    <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>期數：</span>
                                    <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>申請條件：</span>
                                    <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                                       Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                                    </textarea>
                                </div>

                                <div class="m-b-md">
                                    <span>貸款優勢：</span>
                                    <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                                       Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                                    </textarea>
                                </div>
                            </form>
                        </li>
                        
                        <!--詳細資料 個人、投資人-->
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>手機號碼：</span>
                                    <input placeholder="手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>身份證ID：</span>
                                    <input placeholder="身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <input placeholder="姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>出生年月日：</span>
                                    <div id="data_1" class="input-group date" style="display: inline;" target-otablecolumn="userinfo.userinfo_UserAge" target-edit>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" value="03/04/2014">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <select target-edit>
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <select target-otablecolumn="userinfo_Job.userinfo_Job" target-edit>
                                        <option selected disabled>請選擇</option>
                                        <option value="農林漁牧業">農林漁牧業</option>
                                        <option value="製造業">製造業</option>
                                        <option value="營造業">營造業</option>
                                        <option value="批發及零售業">批發及零售業</option>
                                        <option value="運輸及倉儲業">運輸及倉儲業</option>
                                        <option value="住宿及餐飲業">住宿及餐飲業</option>
                                        <option value="資訊及通訊傳播業">資訊及通訊傳播業</option>
                                        <option value="金融及保險業">金融及保險業</option>
                                        <option value="不動產業">不動產業</option>
                                        <option value="科學及技術服務業">科學及技術服務業</option>
                                        <option value="公共行政及國防">公共行政及國防</option>
                                        <option value="教育服務業">教育服務業</option>
                                        <option value="醫療保健及社會工作服務業">醫療保健及社會工作服務業</option>
                                        <option value="藝術、娛樂及休閒服務業">藝術、娛樂及休閒服務業</option>
                                        <option value="其他">其他</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                    </select>
                                    <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>
                            </form>
                        </li>
                        
                        <!--詳細資料 企業-->
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>公司名稱：</span>
                                    <input placeholder="公司名稱" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司統編：</span>
                                    <input placeholder="公司統編" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司TEL：</span>
                                    <input placeholder="公司TEL" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人姓名：</span>
                                    <input placeholder="負責人姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人身份證ID：</span>
                                    <input placeholder="負責人身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>負責人手機號碼：</span>
                                    <input placeholder="負責人手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>公司地址：</span>
                                    <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                    </select>
                                    <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                                </div>

                                <div class="m-b-md">
                                    <span>服務地區：</span>
                                    <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                        <option>請選擇地區</option>
                                        <option>北區</option>
                                        <option>中區</option>
                                        <option>南區</option>
                                        <option>全省</option>
                                    </select>
                                </div>

                                <div class="m-b-md">
                                    <span>申請身份：</span>
                                    <input placeholder="ex:代書、代辦、介紹人" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">儲存</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .verify input , .verify img , .verify a {
        display: inline;
        height: 34px;
        vertical-align: top;
    }
    .verify input {
        width: 100px;
    }
    .verify .refresh {
        width: 34px;
        background: #87cdcb url("./img/refresh.png") no-repeat scroll center center;
        border-radius: 100%;
        cursor: pointer;
        text-indent: -9999px;
        transition: all 0.3s ease 0s;
        position: absolute;
    }
</style>
<div class="row"> 
    <div class="passwordBox animated fadeInDown" style="max-width: 600px;min-height:  600px;">
        <div id="forgotPlace" class="col-md-12" >
            <div class="ibox-content">

                <h2 class="font-bold">登入</h2>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal">
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">ID</label>
                                <div class="col-lg-10">
                                    <input id="inputLoginUserName" type="text" class="form-control" placeholder="Your Email" required="">
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">建立密碼</label>
                                <div class="col-lg-10">
                                    <input id="inputLoginUserPassword" type="password" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">安全性驗證碼</label>
                                <div class="col-lg-10 verify captcha">
                                    <input id="input_captcha2" type="password" class="form-control m-r-xs" placeholder="輸入驗證碼" onblur="input_event($(this))" onfocus="clear_event($(this))" disabled="">
                                    <img class=" m-r-xs" src="php/verification.php"><a onclick="re_captcha()" class="refresh">重新整理</a>
                                </div>
                            </div>
                            
                            <div class="col-lg-2"></div>
                            <div class="col-lg-10">
                                <a href="forgot_password"><span class="text-muted m-b-md">忘記密碼?</span></a><br><br>
                                <button id="btnLogin" type="submit" class="btn btn-success float-right m-r-sm">登入</button>
                                <br><br>
                            </div>
                            <div class="col-lg-12">
                                
                                <button id="btnLoginTestAdministrator" type="submit" class="btn btn-success float-right m-r-xxs">借款人測試</button>
                                <button id="btnLoginTestInvestor" type="submit" class="btn btn-success float-right m-r-xxs">投資人測試</button>
                                <button id="btnLoginTestCompany" type="submit" class="btn btn-success float-right m-r-xxs">企業投資人測試</button>
                                <button id="btnLoginTestAdmin" type="submit" class="btn btn-success float-right m-r-xxs">管理員測試</button>
                                
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
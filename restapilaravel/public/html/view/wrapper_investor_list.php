<style>
    .wrapper {
        width: 100%;
        max-width: 95%;
    }
    @media screen and (min-width: 1200px) {
        .wrapper > .left {
            width: 25%;
            border: 0;
        }
        .wrapper > .right {
            width: 75%;
        }
    }
    @media screen and (min-width: 768px) and (max-width: 1200px) {
        .wrapper > .left {
            width: 31%;
        }
        .wrapper > .right {
            width: 69%;
        }
    }
    /*progress*/
    .progress-mini, .progress-mini .progress-bar {
        height: 9px;
        margin-bottom: 0;
    }
    .progress {
        width: calc(100% - 50px);
        display: inline-block;
        overflow: hidden;
        background-color: #DADFE1;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
        box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    }
    .progress-bar {
        float: left;
        width: 0;
        height: 100%;
        line-height: 20px;
        border-radius: 4px;
        -webkit-transition: width .6s ease;
        -o-transition: width .6s ease;
        transition: width .6s ease;
    }
    .progress-title {
        width: 20px;
    }
    .bg-success {background-color: #88BF2F;}
    .bg-warning {background-color: #FDCC49;}
    .bg-danger {background-color: #FA361C;}
    .bg-primary {background-color: #6DC9EB;}
    
    /*搜尋結果*/
    .filter-msg {
        margin: 19px 0 30px;
    }
    .filter-msg input,.filter-msg select{    
        height: 35px;
        padding: 0 10px;
        border: 1px solid #ddd;
        font-size: 18px;
        color: #333;
        line-height: 35px;
        box-sizing: border-box;
    }
    
</style> 

<?php include("../investor_sidebar.php"); ?>

<div class="right"> 
    <!--<div id="main-title">企業投資人/投資人列表</div>-->
    
    <div class="filter-msg"> 
        <span>全部共有471筆投資人符合項目</span>
        <select id="searchReasonBtn" class="pull-right">
            <option value="0">最新上線時間↓ </option>
            <option value="1">最低開辦費用↓ </option>
            <option value="2">最高貸款金額↓ </option>
            <option value="3">最低利率費用↓ </option>
            <option value="4">最高評分標準↓ </option>
        </select>
        <input placeholder="關鍵字" type="text" class="pull-right m-r-sm">
    </div>
    
    <table id="example" class="display nowrap dataTable" width="100%" style="width:100%;"> 
        <thead>
            <tr>
                <th class="p-h-xs">資訊</td>
                <th class="p-h-xs" style="width: 200px;">評價</th>
                <th class="p-h-xs">貸款上限</th>
                <th class="p-h-xs">利率</th>
                <th class="p-h-xs">開辦費</th>
                <th class="p-h-xs">服務地區</th>
                <th class="p-h-xs">強力主打</th>
                <th class="p-h-xs">申請人數</th>
                <th class="p-h-xs"></th>
            </tr>
        </thead>
        
        <!--sample code-->
        <tbody>
<!--            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>V123456789</span></li>
                        <li><span>名稱</span><span>吳先生</span></li>
                        <li><span>職稱</span><span>地政士代書</span></li>
                    </ul>
                </td>
                <td>
                    <div>
                        <span class="progress-title">服務態度</span>
                        <div class="progress progress-mini">
                            <div style="width: 78%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">撥款速度</span>
                        <div class="progress progress-mini">
                            <div style="width: 38%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業能力</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-primary"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">整體感受</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>最高50萬</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <span>北部地區：共<c>5</c>區</span><br>
                    <span>中部地區：共<c>0</c>區</span><br>
                    <span>南部地區：共<c>0</c>區</span><br>
                    <span>東部地區：共<c>0</c>區</span><br>
                    <span>外島地區：共<c>0</c>區</span>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <td>2144455人</td>
                <td>
                    <div class="control nowrap">
                        <a class="chat">開始聊聊</a>
                        <a class="favorite">加入收藏</a>
                    </div>
                </td>
            </tr>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap">
                        <li><span>編碼</span><span>V123456789</span></li>
                        <li><span>名稱</span><span>吳先生</span></li>
                        <li><span>職稱</span><span>地政士代書</span></li>
                    </ul>
                </td>
                <td>
                    <div>
                        <span class="progress-title">服務態度</span>
                        <div class="progress progress-mini">
                            <div style="width: 78%;" class="progress-bar bg-danger"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">撥款速度</span>
                        <div class="progress progress-mini">
                            <div style="width: 38%;" class="progress-bar bg-warning"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">專業能力</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-primary"></div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-title">整體感受</span>
                        <div class="progress progress-mini">
                            <div style="width: 18%;" class="progress-bar bg-success"></div>
                        </div>
                    </div>
                </td>
                <td>最高50萬</td>
                <td>1.88%</td>
                <td>最低2000元</td>
                <td>
                    <span>北部地區：共<c>5</c>區</span><br>
                    <span>中部地區：共<c>0</c>區</span><br>
                    <span>南部地區：共<c>0</c>區</span><br>
                    <span>東部地區：共<c>0</c>區</span><br>
                    <span>外島地區：共<c>0</c>區</span>
                </td>
                <td>房屋貸款<br>銀行貸款<br>民間貸款</td>
                <td>2144455人</td>
                <td>
                    <div class="control nowrap">
                        <a class="chat">開始聊聊</a>
                        <a class="unfavorite">取消收藏</a>
                    </div>
                </td>
            </tr>-->
        </tbody>
    </table>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header" style="padding:10px;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body">
                <div class="form row">
                    <div class="m-b-md">
                        <span>編　　碼：</span>
                        <p id="userinfo_ID" target-view>LA124154</p>
                    </div>
                    <div class="m-b-md">
                        <span>名　　稱：</span>
                        <p id="userinfo_name" target-view>王小姐</p>
                    </div>
                    <div class="m-b-md">
                        <span>性　　別：</span>
                        <p id="userinfo_gender" target-view>女</p>
                    </div>
                    <div class="m-b-md">
                        <span>職　　稱：</span>
                        <p id="userinfo_title" target-view>地政士</p>
                    </div>
                    <div class="m-b-md">
                        <span>利　　率：</span>
                        <p id="userinfo_rate" target-view>0.68 ％</p>
                    </div>
                    <div class="m-b-md">
                        <span>貸款上限：</span>
                        <p id="userinfo_limit" target-view>最低 10000 元，最高 1000000 元。</p>
                    </div>
                    <div class="m-b-md">
                        <span>開&nbsp&nbsp;辦&nbsp;&nbsp;費：</span>
                        <p id="userinfo_fee" target-view>最低 1980 元</p>
                    </div>
                    <div class="m-b-md">
                        <span>服務地區：</span>
                        <p id="userinfo_servicearea" target-view>台北市、新北市、台中市、桃園市</p>
                    </div>
                   <div class="m-b-md">
                        <span>工作單位：</span>
                        <p id="userinfo_company" target-view>大大小小國際股份有限公司</p>
                    </div>
                   <div class="m-b-md">
                        <span>強力主打：</span>
                        <p id="userinfo_pro" target-view>銀行貸款、房屋貸款、軍公教貸款</p>
                    </div>
                   <div class="m-b-xxs">
                        <span>承辦項目：</span>
                        <span id="userinfo_contractor_1">銀行貸款</span>
                    </div>
                    <div class="m-b-md" id="userinfo_contractor_2" style="margin-left: 94px;">
<!--                        <p>民間貸款</p><br>
                        <p>房屋貸款</p><br>
                        <p>土地貸款</p><br>
                        <p>汽車貸款</p><br>
                        <p>機車貸款</p><br>
                        <p>小額貸款</p><br>
                        <p>婦女貸款</p><br>
                        <p>勞保貸款</p><br>
                        <p>軍公教貸款</p><br>
                        <p>企業貸款</p><br>
                        <p>自營商貸款</p><br>
                        <p>負債整合</p><br>
                        <p>融資貸款</p><br>
                        <p>其他：機械融資</p>-->
                    </div>
                </div>
                        
                
                <div class="m-t-lg" style="text-align: center;">
<!--                    <button type="submit" class="btn-success" data-dismiss="modal">確定</button>-->
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
                
            </div>
        </div>
    </div>
</div>

<style>
    .wrapper {
        width: 100%;
        max-width: 95%;
    }
    @media screen and (min-width: 1200px) {
        .wrapper > .left {
            width: 25%;
            border: 0;
        }
        .wrapper > .right {
            width: 75%;
        }
    }
    @media screen and (min-width: 768px) and (max-width: 1200px) {
        .wrapper > .left {
            width: 31%;
        }
        .wrapper > .right {
            width: 69%;
        }
    }
    
    /*搜尋結果*/
    .filter-msg {
        margin: 19px 0 30px;
    }
    .filter-msg input,.filter-msg select{    
        height: 35px;
        padding: 0 10px;
        border: 1px solid #ddd;
        font-size: 18px;
        color: #333;
        line-height: 35px;
        box-sizing: border-box;
    }
    
</style>

<?php include("../borrowers_sidebar.php"); ?>

<div class="right"> 
    <!--<div id="main-title">借款人列表</div>-->
    
    <div class="filter-msg">
        <span>全部共有471筆投資人符合項目</span>
        <select id="searchReasonBtn" class="pull-right">
            <option value="0">最新上線時間↓ </option>
            <option value="1">最低開辦費用↓ </option> 
            <option value="2">最高貸款金額↓ </option>
            <option value="3">最低利率費用↓ </option>
            <option value="4">最高評分標準↓ </option>
        </select>
        <input placeholder="關鍵字" type="text" class="pull-right m-r-sm" id="datatable_searchbox">
    </div>
    
    <table id="example" class="display nowrap dataTable" width="100%" style="width:100%;"> 
        <thead>
            <tr>
                <th class="p-h-xs">資訊</td>
                <th class="p-h-xs">貸款金額</th>
                <th class="p-h-xs">希望月付</th>
                <th class="p-h-xs">工作年資</th>
                <th class="p-h-xs">資料完整度</th>
                <th class="p-h-xs">希望申貸方案</th>
                <th class="p-h-xs"></th>
            </tr>
        </thead>
        
        <!--sample code-->
        <tbody>
<!--            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap" >
                        <li> 
                            <span>編號</span> 
                            <span>BA125761</span>
                        </li>
                        <li> 
                            <span>名稱</span> 
                            <span>王小姐</span>
                        </li>
                        <li> 
                            <span>職稱</span>
                            <span>金融保險</span>
                        </li>
                        <li> 
                            <span>地區</span> 
                            <span>新北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬以下</td>
                <td>1千-5千</td>
                <td>半年(含)~一年</td>
                <td>
                    <span class="starTag active"></span>
                    <span class="starTag active"></span>
                    <span class="starTag active"></span>
                    <span class="starTag"></span>
                    <span class="starTag"></span>
                </td>
                <td>銀行貸款<br>信用貸款<br>汽車貸款</td>
                <td>
                    <div class="control nowrap">
                        <a class="chat">開始聊聊</a>
                        <a class="favorite">加入收藏</a>
                    </div>
                </td>
            </tr>
            <tr style="text-align: right;background-color: #ccc;">
                <td colspan="7" style="padding:2px 10px;">
                    <span class="m-r-md">刊登時間:2017/08/3</span>
                    <span>最後登入時間:2017/08/3</span>
                </td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
            </tr>
            <tr data-toggle="modal" data-target="#myModal">
                <td>
                    <ul class="info nowrap" >
                        <li> 
                            <span>編號</span> 
                            <span>BA125761</span>
                        </li>
                        <li> 
                            <span>名稱</span> 
                            <span>王小姐</span>
                        </li>
                        <li> 
                            <span>職稱</span>
                            <span>金融保險</span>
                        </li>
                        <li> 
                            <span>地區</span> 
                            <span>新北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬以下</td>
                <td>1千-5千</td>
                <td>半年(含)~一年</td>
                <td>
                    <span class="starTag active"></span>
                    <span class="starTag active"></span>
                    <span class="starTag active"></span>
                    <span class="starTag"></span>
                    <span class="starTag"></span>
                </td>
                <td>銀行貸款<br>信用貸款<br>汽車貸款</td>
                <td>
                    <div class="control nowrap">
                        <a class="chat">開始聊聊</a>
                        <a class="unfavorite">加入收藏</a>
                    </div>
                </td>
            </tr>
            <tr style="text-align: right;background-color: #ccc;">
                <td colspan="7" style="padding:2px 10px;">
                    <span class="m-r-md">刊登時間:2017/08/3</span>
                    <span>最後登入時間:2017/08/3</span>
                </td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
                 <td style="display: none;"></td>
            </tr>-->
        </tbody>
    </table>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body">
                <div class="form row">
                    <div class="m-b-md" id="userinfo_complete">
                        <span>資料完整度：</span>
                        <span class="starTag active"></span>
                        <span class="starTag active"></span>
                        <span class="starTag active"></span>
                        <span class="starTag"></span>
                        <span class="starTag"></span>
                    </div>
                    <div class="m-b-md">
                        <span>會員編碼：</span>
                        <p id="userinfo_ID" target-view>B01451017</p>
                    </div>
                    <div class="m-b-md">
                        <span>名　　稱：</span>
                        <p id="userinfo_name" target-view>王小姐</p>
                    </div>
                    <div class="m-b-md">
                        <span>地　　區：</span>
                        <p id="userinfo_area" target-view>新北市</p>
                    </div>
                    <div class="m-b-md">
                        <span>貸款金額：</span>
                        <p id="userinfo_loanamounts" target-view>1萬以下</p>
                    </div>
                    <div class="m-b-md">
                        <span>希望月付：</span>
                        <p id="userinfo_pay" target-view>1千-5千</p>
                    </div>
                    <div class="m-b-md">
                        <span>行　　業：</span>
                        <p id="userinfo_job" target-view>金融保險</p>
                    </div>
                    <div class="m-b-md">
                        <span>職　　稱：</span>
                        <p id="userinfo_title" target-view>一般職員</p>
                    </div>
                    <div class="m-b-md">
                        <span>年　　資：</span>
                        <p id="userinfo_year" target-view>半年(含)~1年</p>
                    </div>
                    <div class="m-b-md">
                        <span>平均月薪：</span>
                        <p id="userinfo_salary" target-view>35,000</p>
                    </div>
                    <div class="m-b-md">
                        <span>勞保投保：</span>
                        <p id="userinfo_labor" target-view>無</p>
                    </div>
                    <div class="m-b-md">
                        <span>收入證明：</span>
                        <p id="userinfo_prove" target-view>薪資轉帳</p>
                    </div>
                    <div class="m-b-md">
                        <span>資金用途：</span>
                        <p id="userinfo_usage" target-view>消費支出</p>
                    </div>
                    <div class="m-b-md">
                        <span>希望申貸方案：</span>
                        <p id="userinfo_application" target-view>10000</p>
                    </div>
                    <div class="m-b-md">
                        <span>銀行負債：</span>
                        <p id="userinfo_bankstatus" target-view>有　負債金額：(購買名單顯示)</p>
                    </div>
                    <div class="m-b-md">
                        <span>民間負債：</span>
                        <p id="userinfo_privatestatus" target-view>有　負債金額：(購買名單顯示)</p>
                    </div>
                    <div class="m-b-md">
                        <span>其他負債：</span>
                        <p id="userinfo_otherstatus" target-view>無</p>
                    </div>
                    <div class="m-b-md">
                        <span>借款理由：</span>
                        <p id="userinfo_loanreason" target-view>急需用錢！！！</p>
                    </div>
                    <div style="text-align: right;">
                        <span id="userinfo_published" class="m-r-md">刊登時間:2017/08/3</span>
                        <span id="userinfo_login">最後登入時間:2017/08/3</span>
                    </div>
                </div>
                        
                
                <div class="m-t-lg" style="text-align: center;">
<!--                    <button type="submit" class="btn-success" data-dismiss="modal">確定購買</button>-->
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
                
            </div>
        </div>
    </div>
</div>

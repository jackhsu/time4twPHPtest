<div class="row"> 
     <div class="passwordBox animated fadeInDown" style="max-width: 600px;min-height:  600px;">

        <!--Forgot password-->
        <div id="forgotPlace" class="col-md-12" >
            <div class="ibox-content">

                <h2 class="font-bold">忘記密碼</h2>
                <p>請輸入您的帳戶所用的完整電子郵件信箱，我們將向您傳送重置密碼連結</p>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="m-t" role="form" action="index" class="form-horizontal">
                            <div class="form-group col-lg-12 no-padding">
                                <input id="inputEmail" type="email" class="form-control" placeholder="Email address" required="">
                            </div>

                            <button id="btnSendNewPassword" type="submit" class="btn btn-success block full-width m-b demo2">重置密碼</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--Change password-->
        <div id="changePlace" class="col-md-12" >
            <div class="ibox-content">

                <h2 class="font-bold">重設新密碼</h2>
                <p>請輸入您的新密碼</p>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="m-t" role="form" action="index" class="form-horizontal">
                            <div class="form-group col-lg-12 no-padding">
                                <input id="inputEmail" type="email" class="form-control" placeholder="Email address" required="">
                            </div>
                            <div class="form-group col-lg-12 no-padding">
                                <input id="inputNewPassword" type="txt" class="form-control" placeholder="New Password" required="">
                            </div>
                            <div class="form-group col-lg-12 no-padding">
                                <input id="inputConfirmPassword" type="txt" class="form-control" placeholder="Confirm Password" required="">
                            </div>

                            <button id="btnSave" type="submit" class="btn btn-success block full-width m-b">送出</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    #progress dt img {
        width: 206px;
    }
    .display-block {
        display: inline-block;
    }
    /*checkbox*/
    .checkbox label::before {
        top: 3px;
    }
    .checkbox label::after {
        padding-top: 6px;
    }
    .checkbox {
        float: left;
    }
    /*search*/
    .fixed {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 999998;
    }
    .search-box {    
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        width: 50vw;
        z-index: 999999;
        border-radius: 5px;
        background-color: #fff;
    }
    .search-box-header {
        height: 46px;
        line-height: 46px;
        background: #f7f7f7;
        padding: 0 20px;
        border-radius: 5px;
    }
    .search-box-title {    
        font-size: 20px;
        color: #333;
        display: inline-block;
        padding-top: 3px;
    }
    .search-box-close {
        float: right;
        background: url( 'images/close.png' ) no-repeat;
        display: inline-block;
        width: 24px;
        height: 24px;
        margin-top: 10px;
    }
    .search-box-body {
        /*padding-left: 20px;*/
    }
    .search-box-body dl {
        font-size: 16px;
    }
    .search-box-body dt {
        font-weight: bold;
        width: 10%;
    }
    .search-box-body dd {
        width: 90%;
    }
    .search-box-body dd.search-box-full {
        width: 100%;
    }
    .search-box-col4 > .checkbox {
        width: 25%;
        padding: 0;
    }
    .search-box-col5 > .checkbox {
        width: 20%;
        padding: 0;
    }
    
    @media screen and (max-width:1000px){
        .search-box {    
            width: 90vw;
        }
        .search-box-col4 > .checkbox,
        .search-box-col5 > .checkbox{
            width: 50%;
            padding: 0;
        }
    }
</style>

<!-- search -->
<div id="search">
    <div class="tab-block">
        <div class="tabs">
            
            <a>找借款人</a>
<!--            <a>找貸款需求</a>-->
            <a>找投資人</a>
<!--            <a>找借貸業者</a>-->
        </div>
        <ul class="tab-content">
            <li>
                <form>
                    <select id="searchAreaBtn"><option>區域</option></select>
                    <select id="searchMoneyBtn"><option>需要金額</option></select>
                    <select id="searchReasonBtn"><option>借貸需求</option></select>
                    <input type="text" placeholder="請輸入關鍵字">
                    <button type="submit">搜尋</button>
                </form>
                <div class="keyword">熱門搜尋：<a >辦手機換現金</a><a >續約換現金</a><a >辦機車換現金</a><a >手機買賣換現金</a><a >資金週轉</a></div>
            </li>
            <li>
                <form>
                    <select id="searchAreaBtn"><option>區域</option></select>
                    <select id="searchContractorsBtn"><option>承辦項目</option></select>
                    <select id="searchEvaluationBtn"><option>評價</option></select>
                    <input type="text" placeholder="請輸入關鍵字">
                    <button type="submit">搜尋</button>
                </form>
                <div class="keyword">熱門搜尋：<a >辦手機換現金</a><a >續約換現金</a><a >辦機車換現金</a><a >手機買賣換現金</a><a >資金週轉</a></div>
            </li>
        </ul>
    </div>
</div>

<!-- hot-loan -->
<div id="hot-loan">
    <h2>熱門借貸</h2>
    <div class="tab-block">
        <div class="tabs"><a>目前共申貸金額</a><a>目前共借出金額</a></div>
        <ul class="tab-content">
            <li>
                <ul class="list">
                    <li>
                        <div class="type">
                        <h3>房屋貸款</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">30,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                    </li>
                    <li>
                        <div class="type">
                        <h3>手機買賣換現金</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">10,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                    </li>
                    <li>
                        <div class="type">
                        <h3>房屋貸款</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">30,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                    </li>
                    <li>
                        <div class="type">
                        <h3>手機買賣換現金</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">10,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                        </li>
                    <li>
                        <div class="type">
                        <h3>房屋貸款</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">30,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                    </li>
                    <li>
                        <div class="type">
                        <h3>手機買賣換現金</h3>
                        <span class="location">高雄市</span><span class="require">借錢需求</span> </div>
                        <div class="time">刊登時間: 2017-05-22</div>
                        <div class="money">10,000</div>
                        <div class="control"><a class="collect" >加入收藏</a><a class="more" >查看內容</a></div>
                    </li>
                </ul> 
                <div class="ad"><a  target="_blank"><img src="images/upload/ad_01.jpg"></a><a  target="_blank"><img src="images/upload/ad_02.jpg"></a><a  target="_blank"><img src="images/upload/ad_03.jpg"></a></div>
            </li>
            <li>目前共借出金額</li>
        </ul>
    </div>
</div>

<!-- progress -->
<div id="progress">
<h2>如何使用</h2>
    <div class="tab-block">
        <div class="tabs"><a>借款流程</a><a>放款流程</a></div>
        <ul class="tab-content">
            <li>
                <dl>
                    <dt><img src="images/upload/index_progress_01.svg"></dt>
                    <dd><span>第一步</span>
                    <h3>加入會員</h3>
                    <p>說明文字說明文字說明文字</p>
                    </dd>
                </dl>
                <dl>
                    <dt><img src="images/upload/index_progress_02.svg"></dt>
                    <dd><span>第二步</span>
                    <h3>填寫借款需求</h3>
                    <p>說明文字說明文字說明文字</p>
                    </dd>
                </dl>
                <dl>
                    <dt><img src="images/upload/index_progress_03.svg"></dt>
                    <dd><span>第三步</span>
                    <h3>用站內信與業者溝通</h3>
                    <p>說明文字說明文字說明文字</p>
                    </dd>
                </dl>
                <dl>
                    <dt><img src="images/upload/index_progress_04.svg"></dt>
                    <dd><span>第四步</span>
                    <h3>交易成功</h3>
                    <p>說明文字說明文字說明文字</p>
                    </dd>
                </dl>
            </li>
            <li>放款流程</li>
        </ul>
    </div>
</div>

<!-- calculation -->
<div id="calculation">
    <h2>月付優惠試算</h2>
    <h3>立刻評估我的貸款分數，IUN為您選出適合的貸款業者</h3>
    <p>為了量身規劃您的貸款額度及利率</p>
    <p>透過簡單的問卷填答，3分鐘就能精準試算您貸額度和利率</p>
    <p>精準額度利率分析讓您一次掌握</p>
    <p>節省申請流程，24小時線上為您服務，縮短您寶貴的時間</p>
    <a onclick="show_remind( '需要再提供設計稿製作' )" >馬上試算</a> 
</div>

<!-- report-story -->
<!--<div id="report-story">
    <h2>媒體報導<a class="more" >更多報導</a></h2>
    <div class="wrap owl-carousel">
        <div class="item"><a ><img src="images/upload/index_report_01.jpg">
        <p>申請房貸流程原來是這樣!?</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_02.jpg">
        <p>現金或紅利點數可抽，繳費不妨到指定超商</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_03.jpg">
        <p>尋找替代方案，提高你的儲蓄率</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_04.jpg">
        <p>申請房貸流程原來是這樣!?</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_01.jpg">
        <p>申請房貸流程原來是這樣!?</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_02.jpg">
        <p>現金或紅利點數可抽，繳費不妨到指定超商</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_03.jpg">
        <p>尋找替代方案，提高你的儲蓄率</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_report_04.jpg">
        <p>申請房貸流程原來是這樣!?</p>
        </a></div>
    </div>
    <h2>申貸故事<a class="more" >更多故事</a></h2>
    <div class="wrap owl-carousel">
        <div class="item"><a ><img src="images/upload/index_story_01.jpg">
        <p>創業血本無歸 "無息貸款"成就夢想</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_02.jpg">
        <p>「對不起」是最具療癒力量的三個字</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_03.jpg">
        <p>未來，保住工作不如保住收入</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_04.jpg">
        <p>多方探索、嘗試、堅定 改變永遠不嫌晚</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_01.jpg">
        <p>創業血本無歸 "無息貸款"成就夢想</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_02.jpg">
        <p>「對不起」是最具療癒力量的三個字</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_03.jpg">
        <p>未來，保住工作不如保住收入</p>
        </a></div>
        <div class="item"><a ><img src="images/upload/index_story_04.jpg">
        <p>多方探索、嘗試、堅定 改變永遠不嫌晚</p>
        </a></div>
    </div>
</div>-->

<!-- news -->
<div id="news">
    <h2>最新消息</h2>
    <ul class="wrap">
        <li><span>2017/04/25</span><a >【法扣】不論您信用瑕疵/法扣/小白都可為您解決 小額週轉/資金困難/大額借貸/企業週轉 </a></li>
        <li><span>2017/03/20</span><a >柴米油鹽醬醋茶－什麼都漲，就荷包不漲！年輕人呀~不要亂花錢啊~ 分享【感同身受】天無絕人之路...</a></li>
        <li><span>2017/02/21</span><a >關於【手機換現金】相關問答！攜碼換現金</a></li>
        <li><span>2017/02/18</span><a >【票貼是甚麼意思呢】合法安全的民間信貸　小額週轉/企業週轉/資金困難/現金需求</a></li>
        <li><span>2017/02/12</span><a >行動『自然人憑證』智慧生活安全又便捷！您申請了嗎？</a></li>
    </ul>
    <a class="more" >更多消息</a> 
</div>

<!-- search area-->
<div id="searchArea" class="fixed" style="display:none;">
    <div class="search-box">
        <div class="search-box-header">
            <div class="search-box-title">區域類別選擇</div>
            <a href="#" class="search-box-close" data-dismiss="modal"></a>
        </div>
        <div class="search-box-body p-md">
            <dl class="m-b-md">
                <dt class="pull-left">北部</dt>
                <dd class="display-block">
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            台北市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox2" type="checkbox">
                        <label for="checkbox2">
                            新北市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            桃園市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            新竹市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox5" type="checkbox">
                        <label for="checkbox5">
                            新竹縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox6" type="checkbox">
                        <label for="checkbox6">
                            宜蘭縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox7" type="checkbox">
                        <label for="checkbox7">
                            基隆市
                        </label>
                    </div>
                </dd>
            </dl>
            
            <dl class="m-t-md m-b-md">
                <dt class="pull-left">中部</dt>
                <dd class="display-block">
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox8" type="checkbox">
                        <label for="checkbox8">
                            台中市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox9" type="checkbox">
                        <label for="checkbox9">
                            彰化縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox9" type="checkbox">
                        <label for="checkbox9">
                            雲林縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox10" type="checkbox">
                        <label for="checkbox10">
                            苗栗縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox11" type="checkbox">
                        <label for="checkbox11">
                            南投縣
                        </label>
                    </div>
                </dd>
            </dl>
            
            <dl class="m-t-md m-b-md">
                <dt class="pull-left">南部</dt>
                <dd class="display-block">
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox12" type="checkbox">
                        <label for="checkbox12">
                            高雄市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox13" type="checkbox">
                        <label for="checkbox13">
                            台南市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox14" type="checkbox">
                        <label for="checkbox14">
                            嘉義市
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox15" type="checkbox">
                        <label for="checkbox15">
                            嘉義縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox16" type="checkbox">
                        <label for="checkbox16">
                            屏東縣
                        </label>
                    </div>
                </dd>
            </dl>
            
            <dl class="m-t-md">
                <dt class="pull-left">東部</dt>
                <dd class="display-block">
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox17" type="checkbox">
                        <label for="checkbox17">
                            台東縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox18" type="checkbox">
                        <label for="checkbox18">
                            花蓮縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox19" type="checkbox">
                        <label for="checkbox19">
                            澎湖縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox20" type="checkbox">
                        <label for="checkbox20">
                            金門縣
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor">
                        <input id="checkbox21" type="checkbox">
                        <label for="checkbox21">
                            連江縣
                        </label>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</div>

<!-- search Money-->
<div id="searchMoney" class="fixed" style="display:none;">
    <div class="search-box">
        <div class="search-box-header">
            <div class="search-box-title">申貸金額</div>
            <a href="#" class="search-box-close" data-dismiss="modal"></a>
        </div>
        <div class="search-box-body p-md">
            <dl>
                <dt class="pull-left"></dt>
                <dd class="display-block search-box-col4">
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            不設限制
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox2" type="checkbox">
                        <label for="checkbox2">
                            1萬以下
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            1萬 至 5萬
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            5萬 至 10萬
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox5" type="checkbox">
                        <label for="checkbox5">
                            10萬 至 20萬
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox6" type="checkbox">
                        <label for="checkbox6">
                            20萬 至 50萬
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox7" type="checkbox">
                        <label for="checkbox7">
                            50萬以上
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox7" type="checkbox">
                        <label for="checkbox7">
                            100萬以上
                        </label>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</div>

<!-- search Reason-->
<div id="searchReason" class="fixed" style="display:none;">
    <div class="search-box">
        <div class="search-box-header">
            <div class="search-box-title">借款理由</div>
            <a href="#" class="search-box-close" data-dismiss="modal"></a>
        </div>
        <div class="search-box-body p-md">
            <dl>
                <dt class="pull-left"></dt>
                <dd class="display-block search-box-col5">
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            裝修房屋
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox2" type="checkbox">
                        <label for="checkbox2">
                            購買土地
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            投資置產
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            個人週轉
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox5" type="checkbox">
                        <label for="checkbox5">
                            現增認股
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox6" type="checkbox">
                        <label for="checkbox6">
                            消費支出
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox7" type="checkbox">
                        <label for="checkbox7">
                            購車資金
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox8" type="checkbox">
                        <label for="checkbox8">
                            結婚資金
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox9" type="checkbox">
                        <label for="checkbox9">
                            子女教育
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox10" type="checkbox">
                        <label for="checkbox10">
                            投資理財
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox11" type="checkbox">
                        <label for="checkbox11">
                            償還貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox12" type="checkbox">
                        <label for="checkbox12">
                            預計興建建築
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox13" type="checkbox">
                        <label for="checkbox13">
                            公司商業週轉
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox14" type="checkbox">
                        <label for="checkbox14">
                            其他
                        </label>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</div>

<!-- search Contractors-->
<div id="searchContractors" class="fixed" style="display:none;">
    <div class="search-box">
        <div class="search-box-header">
            <div class="search-box-title">承辦項目</div>
            <a href="#" class="search-box-close" data-dismiss="modal"></a>
        </div>
        <div class="search-box-body p-md">
            <dl>
                <dt class="pull-left"></dt>
                <dd class="display-block search-box-col4">
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            銀行貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox2" type="checkbox">
                        <label for="checkbox2">
                            房屋貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            土地貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            汽車貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox5" type="checkbox">
                        <label for="checkbox5">
                            機車貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox6" type="checkbox">
                        <label for="checkbox6">
                            小額貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox7" type="checkbox">
                        <label for="checkbox7">
                            婦女貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox8" type="checkbox">
                        <label for="checkbox8">
                            勞保貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox9" type="checkbox">
                        <label for="checkbox9">
                            軍公教貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox10" type="checkbox">
                        <label for="checkbox10">
                            企業貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox11" type="checkbox">
                        <label for="checkbox11">
                            自營商貸款
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox12" type="checkbox">
                        <label for="checkbox12">
                            負債整合
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox13" type="checkbox">
                        <label for="checkbox13">
                            融資貸款
                        </label>
                    </div>
                </dd>
            </dl>
        </div>
    </div>
</div>

<!-- search Reason-->
<div id="searchEvaluation" class="fixed" style="display:none;">
    <div class="search-box">
        <div class="search-box-header">
            <div class="search-box-title">評價</div>
            <a href="#" class="search-box-close" data-dismiss="modal"></a>
        </div>
        <div class="search-box-body p-md">
            <dl>
                <dt class="pull-left"></dt>
                <dd class="display-block search-box-col4">
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox1" type="checkbox">
                        <label for="checkbox1">
                            整體感受
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox2" type="checkbox">
                        <label for="checkbox2">
                            撥款速度
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox3" type="checkbox">
                        <label for="checkbox3">
                            服務態度
                        </label>
                    </div>
                    <div class="checkbox checkbox-success cursor m-b-md">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">
                            專業能力
                        </label>
                    </div>
                    <p>*可複選</p>
                </dd>
            </dl>
        </div>
    </div>
</div>
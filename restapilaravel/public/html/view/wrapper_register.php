<style>
    .verify input , .verify img , .verify a {
        display: inline;
        height: 34px;
        vertical-align: top;
    }
    .verify input {
        width: 100px;
    }
    .verify .refresh {
        width: 34px;
        background: #87cdcb url("./img/refresh.png") no-repeat scroll center center;
        border-radius: 100%;
        cursor: pointer;
        text-indent: -9999px;
        transition: all 0.3s ease 0s;
        position: absolute;
    }
</style>
<div class="row"> 
    <div class="passwordBox animated fadeInDown" style="max-width: 600px;min-height:  600px;">
        <div id="forgotPlace" class="col-md-12" >
            <div class="ibox-content">

                <h2 class="font-bold">註冊</h2>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-horizontal">
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">ID</label>
                                <div class="col-lg-10">
                                    <input id="inputRegUserName" type="text" class="form-control" placeholder="Your Email" required="">
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">建立密碼</label>
                                <div class="col-lg-10">
                                    <input id="inputRegUserPassword" type="password" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">重新輸入密碼</label>
                                <div class="col-lg-10">
                                    <input id="inputRegComfirmPassword" type="password" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm">安全性驗證碼</label>
                                <div class="col-lg-10 verify captcha">
                                    <input id="input_captcha2" type="password" class="form-control m-r-xs" placeholder="輸入驗證碼" onblur="input_event($(this))" onfocus="clear_event($(this))" disabled="">
                                    <img class=" m-r-xs" src="php/verification.php"><a onclick="re_captcha()" class="refresh">重新整理</a>
                                </div>
                            </div>
                            
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm"></label>
                                <div class="col-lg-10">
                                    <input type="checkbox">
                                    <span class="m-l-xs">我同意
                                        <a style="color: #676a6c;" class="m-l-xs m-r-xxs">[ 一般會員服務條款 ]</a>、
                                        <a style="color: #676a6c;" class="m-l-xxs">[ 隱私權政策 ]</a>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="col-lg-12 no-padding m-b-sm">
                                <label class="col-lg-2 control-label no-padding m-t-sm"></label>
                                <div class="col-lg-10">
                                    <select id="inputRegInExUser" name="InExUser" class="form-control">
                                            <option value="">請選擇</option>
                                            <option value="Borrower">借款人</option>
                                            <option value="Investor">投資人</option>
                                            <option value="Company">企業投資人</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="col-lg-12 text-center">
                                <button id="btnRegister" type="submit" class="btn btn-danger m-r-sm">註冊新帳戶</button>
                                <!--<button id="btnLoginTestAdministrator" type="submit" class="btn btn-success">Facebook登入</button> AL 20170707 hide-->
<!--                                <button id="btnTest" class="btn btn-sm m-r-sm">測試</button>-->
<!--                                <button id="btnTest2" class="btn btn-sm m-r-sm">測試註冊</button>-->
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
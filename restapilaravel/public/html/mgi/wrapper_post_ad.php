<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>張貼廣告</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">張貼廣告</div>
    <div class="form">
        <form id="profileEdit">

            <div class="m-b-md">
                <span>公司名稱：</span>
                <input placeholder="固定公司名稱無法修改" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>ＸＸＸ股份有限公司</p>
            </div>

            <div class="m-b-md">
                <span>標語：</span>
                <input placeholder="標語" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>東借西借不如跟我們借</p>
            </div>

            <div class="m-b-md">
                <span>放款速度：</span>
                <input placeholder="一天" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>一天</p>
            </div>

            <div class="m-b-md">
                <span>還款金額：</span>
                <input placeholder="1000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>1000</p>
            </div>


            <div class="m-b-md">
                <span>貸款期限：</span>
                <input placeholder="60期" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>60期</p>
            </div>
            
            <div class="m-b-md">
                <span>利率：</span>
                <input placeholder="利率" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                <p id="customerInfo_TaxNumber" target-view>1.2%</p>
            </div>

            <div class="m-b-md">
                <span>可撥款金額：</span>
                <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>100,000</p>
            </div>

            <div class="m-b-md">
                <span>期數：</span>
                <input placeholder="100,000" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                <p id="customerInfo_TaxNumber" target-view>100,000</p>
            </div>

            <div class="m-b-md">
                <span>申請條件：</span>
                <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                   Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                </textarea>
                <p id="customerInfo_TaxNumber" target-view>Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,...</p>
            </div>

            <div class="m-b-md">
                <span>貸款優勢：</span>
                <textarea placeholder="" class="p-xs" style="display: inline-block; width: calc(100% - 200px);vertical-align: top;border: 0;border-radius: 7px;height: 100px;" target-edit>
                   Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,... 
                </textarea>
                <p id="customerInfo_TaxNumber" target-view>Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,...</p>
            </div>
        </form>

        <button id="btnEditCancel" type="submit">取消</button>
        <button id="btnEditComplete" type="submit">完成</button>
        <button id="btnEdit" type="submit" target-view>修改</button>
        <div class="m-b-md">
            <span>目前刊登到期時間：</span>
            <small id="publishEndTime">無</small><br>
        </div>
        <div class="m-b-md">
            <span>購買天數：</span>
<!--                <input placeholder="點數金額" type="text">-->
            <select id="selectAdDays" style="border:1px solid #000;">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <button id="btnBuyDays">購買刊登時間</button><br><br>
            <small>每天花費五十點</small><br>
        </div>

    </div>
</div>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員中心</a>　/　<span>廣告管理</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">廣告管理</div>
    <div class="m-b-md" style="background-color:#ddd;width:100%;height:300px;">HIGHCHAT</div>
    
    <table id="example" class="display" width="100%">
        <thead>
            <tr><th>圖片</th><th>上架日期</th><th>操作</th></tr>
        </thead>
        <tbody>
            <tr>
                <td><img src="images/upload/ad_01.jpg"></td>
                <td>2017/08/01</td>
                <td>
                    <a href="#" class="m-r-xs">下架</a>
                    <a href="#" class="m-r-xs" data-toggle="modal" data-target="#myModal">修改</a>
                </td>
            </tr>
            <tr>
                <td><img src="images/upload/ad_02.jpg"></td>
                <td>2017/08/01</td>
                <td>
                    <a href="#" class="m-r-xs">下架</a>
                    <a href="#" class="m-r-xs" data-toggle="modal" data-target="#myModal">修改</a>
                </td>
            </tr>
            <tr>
                <td><img src="images/upload/ad_03.jpg"></td>
                <td>2017/08/01</td>
                <td>
                    <a href="#" class="m-r-xs">下架</a>
                    <a href="#" class="m-r-xs" data-toggle="modal" data-target="#myModal">修改</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">修改刊登廣告</h4>
            </div>
            <div class="modal-body text-center">
                
                <div class="form">
                    <form id="profileEdit">
                        <div class="m-b-md">
                            <h2 class="m-b-md">上傳廣告圖片：</h2>
                            <div target-view>
                                <img src="images/upload/ad_02.jpg">
                            </div>
                            <div target-edit>
                                <div class="dropzone" id="selfie">
                                    <div class="am-text-success dz-message">
                                        將圖片拖移至此<br>或點此上傳圖片
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h2 class="m-b-md">廣告刊登內容：</h2>
                        <div class="m-b-md">
                            <span>公司名稱：</span>
                            <input placeholder="公司名稱" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>公司統編：</span>
                            <input placeholder="公司統編" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>公司TEL：</span>
                            <input placeholder="公司TEL" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>負責人姓名：</span>
                            <input placeholder="負責人姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>負責人身份證ID：</span>
                            <input placeholder="負責人身份證ID" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>負責人手機號碼：</span>
                            <input placeholder="負責人手機號碼" type="text" target-otablecolumn="userinfo.userinfo_UserPhone" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>公司地址：</span>
                            <select target-otablecolumn="userinfo.userinfo_Area" target-edit></select>
                            <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                <option>請選擇地區</option>
                            </select>
                            <input placeholder="地址" type="text" target-otablecolumn="userinfo.userinfo_Address" target-edit>
                        </div>

                        <div class="m-b-md">
                            <span>服務地區：</span>
                            <select target-otablecolumn="userinfo.userinfo_District" target-edit>
                                <option>請選擇地區</option>
                                <option>北區</option>
                                <option>中區</option>
                                <option>南區</option>
                                <option>全省</option>
                            </select>
                        </div>

                        <div class="m-b-md">
                            <span>申請身份：</span>
                            <input placeholder="ex:代書、代辦、介紹人" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-disable" data-dismiss="modal">取消</button>
                <button type="button" class="btn-success" data-dismiss="modal">重新刊登</button>
            </div>
        </div>
    </div>
</div>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<span>會員專區</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">會員專區</div>
    <div id="member-center"> 

        <!-- slide -->
        <div class="slide owl-carousel">
            <div class="item"><a href="#"><img src="images/upload/ad_05.png"></a></div>
            <div class="item"><a href="#"><img src="images/upload/ad_05.png"></a></div>
            <div class="item"><a href="#"><img src="images/upload/ad_05.png"></a></div>
        </div>
        
        <div class="tab-block">
            <div class="tabs"><a class="active">個人公告</a><a>最新消息</a></div>
            <ul class="tab-content">
                <li class="form">
                    <div class="news">
                        <h2>個人公告</h2>
                        <ul>
                            <li><span>2017/04/25</span><a href="#">柴米油鹽醬醋茶－什麼都漲，就荷包不漲！年輕人呀~不要亂花...</a></li>
                            <li><span>2017/04/25</span><a href="#">動『自然人憑證』智慧生活安全又便捷！您申請了嗎？</a></li>
                            <li><span>2017/04/25</span><a href="#">關於【手機換現金】相關問答！攜碼換現金</a></li>
                            <li><span>2017/04/25</span><a href="#">分享【感同身受】天無絕人之路，如果您也正要幫助，就再手機...</a></li>
                            <li><span>2017/04/25</span><a href="#">經太標斷東單得：有必教改製能提總出的手也球從細；相懷不化自下就為體是房...</a></li>
                        </ul>
                    </div>
                    
                </li>
                
                <li class="form" style="display: none;">
                    <div class="news">
                        <h2>最新消息</h2>
                        <ul class="wrap">
                            <li><span>2017/04/25</span><a href="#">【法扣】不論您信用瑕疵/法扣/小白都可為您解決 小額週轉/資金困難/大額借貸/企業週轉 </a></li>
                            <li><span>2017/03/20</span><a href="#">柴米油鹽醬醋茶－什麼都漲，就荷包不漲！年輕人呀~不要亂花錢啊~ 分享【感同身受】天無絕人之路...</a></li>
                            <li><span>2017/02/21</span><a href="#">關於【手機換現金】相關問答！攜碼換現金</a></li>
                            <li><span>2017/02/18</span><a href="#">【票貼是甚麼意思呢】合法安全的民間信貸　小額週轉/企業週轉/資金困難/現金需求</a></li>
                            <li><span>2017/02/12</span><a href="#">行動『自然人憑證』智慧生活安全又便捷！您申請了嗎？</a></li>
                        </ul>
                        <a class="more" href="#">更多消息</a> 
                    </div>
                </li>
            </ul>
        </div>

        <!-- link -->
        <ul class="link">
            <li class="btn-1"><a href="#">如何操作</a></li>
            <li class="btn-2"><a href="#">優惠項目</a></li>
            <li class="btn-3"><a href="#">購買點數</a></li>
            <li class="btn-4"><a href="#">關於我們</a></li>
        </ul>
    </div>
</div>
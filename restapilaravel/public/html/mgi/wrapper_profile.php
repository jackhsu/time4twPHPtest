<style>
    form a,.modal a {
        color: #2E75CC;
        text-decoration: underline;
    }
    form a:hover,.modal a:hover {
        opacity: .5;
    }
    .form input.input-lg{
        width: 40%;
    }
    .form input.input-sm{
        width: 43px;
    }
    /*checkbox radio*/
    .radio-inline {
        display: inline-block;
    }
    .radio input[type="radio"] {
        position: absolute;
        width: 10px;
    }
    .radio label::after {
        width: 13px;
        height: 13px;
    }
    .radio label.m-radio::before {
        top:8px;
    }
    .radio label.m-radio::after {
        top:11px;
    }
    /*select*/
    .chosen-container-single .chosen-single {
        color: #333;
        border: 1px solid #ccc;
        border-radius: 4px;
        height: 33px;
        line-height: 33px;
    }
    .chosen-container-multi {
        margin: 10px 0 0;
    }
    /*upload_progress*/
    .img-lg {
        height: 120px
    }
    .upload_success::before {
        content: "";
        display: inline-block;
        width: 26px;
        height: 26px;
        margin-right: 5px;
        background: url(./images/check.png);
        vertical-align: middle;
    }
    
    
    
     .pricing-plan {
        /*margin: 20px 30px 0 30px;*/
        width: calc(100% / 2 - 10px);
        display: inline-block;
        border-radius: 4px;
        background-color: #fff;
    }
    .list-unstyled {
        padding-left: 0;
        list-style: none;
    }

    .pricing-plan li {
        padding: 10px 16px;
        border-top: 1px solid #e7eaec;
        text-align: left;
        color: #aeaeae;
    }
     .pricing-plan li:after {
        content: "";
        display: inline-block;
        width: 26px;
        height: 26px;
        margin-right: 5px;
        background: url(./images/check.png);
        vertical-align: middle;
        float: right;
    }
    .pricing-plan li.pricing-title {
        background: #1ab394;
        color: #fff;
        padding: 10px;
        border-radius: 4px 4px 0 0;
        font-size: 22px;
        font-weight: 600;
        text-align: center;
    }
    .pricing-plan li.pricing-title:after {
        content: "";
        display: none;
    }
     .pricing-plan li.no:after {
        background: url(./css/plugins/chosen/chosen-sprite.png) right top no-repeat;
        background-size: 134px;
    }
</style> 
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員中心</a>　/　<span>個人資料</span></div>

<?php include("../sidebar.php"); ?>

<div class="right"> 
    <div id="main-title">個人資料</div>
    <div class="row animated fadeInRight">
        <div class="tab-block">
            <div class="tabs"><a class="active">個人資料</a><a>個人設定</a></div>
            <ul class="tab-content">
                <!--個人資料-->
                <li class="form">
                    <form id="profileEdit">
                        <!--<img src="images/L.svg" class="img-lg m-b-md">-->

                        <div class="m-b-md">
                            <span>編碼：</span>
                            <p id="userinfo_ID" target-edit>L01451017</p>
                        </div>
                        
                        <div class="m-b-md">
                            <span>是否為公司負責人：</span>
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="Radio" value="option03" name="radioInline">
                                <label for="Radio"> 是 </label>
                            </div>
                            <div class="radio radio-success radio-inline m-l-sm">
                                <input type="radio" id="Radio4" value="option04" name="radioInline" checked="">
                                <label for="Radio4"> 否 </label>
                            </div>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*姓名：</span>
                            <input placeholder="請輸入姓名" type="text" target-otablecolumn="userinfo.userinfo_UserName" target-edit>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*性別：</span>
                            <div class="radio radio-success radio-inline">
                                <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked="">
                                <label for="inlineRadio1"> 男 </label>
                            </div>
                            <div class="radio radio-success radio-inline m-l-sm">
                                <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
                                <label for="inlineRadio2"> 女 </label>
                            </div>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*手機號碼：</span>
                            <input placeholder="請輸入手機號碼如：0912345678" type="text" class="input-lg">
                            <button id="btnUpload" type="submit">傳送驗證碼</button>
                            <span class="upload_success"></span><br><br>
                            <input placeholder="請輸入驗證碼" type="text" class="input-lg" style="margin-left:100px;">
                            <button id="btnUpload" type="submit">驗證碼確認</button>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*身分證字號：</span>
                            <input id="userinfo_ID_card" placeholder="請輸入身分證字號" type="text" target-otablecolumn="userinfo.userinfo_ID_card" target-edit class="required">
                            <button type="submit"  data-toggle="modal" data-target="#myModal">上傳</button>
                            <span class="upload_success"></span>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*出生年月日：</span>
                            <input type="text" value="2012-05-15" id="datetimepicker">
                        </div>
                        
                        <div class="m-b-md">
                            <span>*身 份 別：</span>
                            <div class="radio radio-success m-b-xs" style="display: inline-block;">
                                <input type="radio" id="inlineRadio3" value="option1" name="radioInline" checked="">
                                <label for="inlineRadio3"> 地政士 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio4" value="option2" name="radioInline">
                                <label for="inlineRadio4"> 代辦 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio5" value="option1" name="radioInline" checked="">
                                <label for="inlineRadio5"> 介紹人 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio6" value="option2" name="radioInline">
                                <label for="inlineRadio6"> 金主 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 93px;">
                                <input type="radio" id="inlineRadio7" value="option1" name="radioInline" checked="">
                                <label for="inlineRadio7"> 銀行 </label>
                            </div>
                        </div>

                        <div class="m-b-md">
                            <span>最低利率：</span>
                            <input type="number" placeholder="例如：2.66">％
                            <!--<p id="userinfo_expectrate" target-view>0.3</p>-->
                        </div>
                        
                        <div class="m-b-md">
                            <span>*是否使用信用卡：</span>
                            <input type="text" placeholder="最低金額"><span class="m-r-sm">-</span><input type="text" placeholder="最高金額">
                            <div class="radio radio-success radio-inline m-l-sm">
                                <input type="radio" id="Radio4" value="option40" name="radioInline">
                                <label for="Radio40"> 無上限 </label>
                            </div>
                        </div>

                        <div class="m-b-md">
                            <span>開辦費：</span>
                            <span class="m-r-sm">最低</span>
                            <input type="text" placeholder="例如：1000">元
                        </div>
                        
                        <div class="m-b-md">
                            <span>*可承作地區：</span><br>
                            <select class="chosen-select m-b-md" data-placeholder="請選擇縣市，可複選" multiple >
                                <option value="">請選擇縣市</option>
                            </select><br>
                            <select class="chosen-select" data-placeholder="請選擇鄉鎮，可複選" multiple>
                                <option value="">請選擇鄉鎮</option>
                            </select>
                        </div>

                        <div class="m-b-md">
                            <span>您目前從事的工作單位：</span>
                            <input type="text" placeholder="填寫公司名稱" target-otablecolumn="" target-edit>
                        </div>

                        <div class="m-b-md" style="margin-left: 108px;">
                            <span>公司電話：</span>
                            <input type="text" placeholder="填寫公司電話" target-otablecolumn="" target-edit>
                            <br><br>
                            <span>公司統編：</span>
                            <input type="text" placeholder="填寫公司統編" target-otablecolumn="" target-edit>
                        </div>
                        
                        <div class="m-b-md">
                            <span>*承辦項目：</span>
                            <div class="radio radio-success m-b-xs" style="display:inline-block">
                                <input type="radio" id="Radio20" value="option20" name="radioInline" checked="">
                                <label for="Radio20"> 銀行貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio21" value="option21" name="radioInline">
                                <label for="Radio21"> 房屋貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio22" value="option22" name="radioInline">
                                <label for="Radio22"> 土地貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio23" value="option23" name="radioInline">
                                <label for="Radio23"> 汽車貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio24" value="option24" name="radioInline">
                                <label for="Radio24"> 機車貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio25" value="option25" name="radioInline">
                                <label for="Radio25"> 小額貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio26" value="option26" name="radioInline">
                                <label for="Radio26"> 婦女貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio27" value="option27" name="radioInline">
                                <label for="Radio27"> 勞保貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio28" value="option28" name="radioInline">
                                <label for="Radio28"> 軍公教貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio29" value="option29" name="radioInline">
                                <label for="Radio29"> 企業貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio30" value="option30" name="radioInline">
                                <label for="Radio30"> 自營商貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio31" value="option31" name="radioInline">
                                <label for="Radio31"> 負債整合 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio32" value="option32" name="radioInline">
                                <label for="Radio32"> 融資貸款 </label>
                            </div>
                            <div class="radio radio-success m-b-xs" style="margin-left: 101px;">
                                <input type="radio" id="Radio33" value="option33" name="radioInline">
                                <label for="Radio33" style="display: inline;"> 其他 </label>
                                <input type="text" placeholder="(自填)" target-otablecolumn="" target-edit>
                            </div>
                            <div class="chosen-container chosen-container-multi m-b-xl m-t-sm" style="width: calc(100% - 100px);margin-left: 100px;" title="">
                                <ul class="chosen-choices" style="padding: 10px;">
                                    <p style="display: block;padding: 10px;">強力主打：(最多三項) </p>
                                    <li class="search-choice"><span>銀行貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>
                                    <li class="search-choice"><span>土地貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>
                                    <li class="search-choice"><span>汽車貸款</span>
                                        <a class="search-choice-close"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="m-b-md text-right">
                            <button id="btnEditCancel" type="submit">取消</button>
                            <button id="btnEditComplete" type="submit">完成</button>
                            <!--<button id="btnEdit" type="submit" target-view>修改</button>-->
                        </div>
                    </form>


                </li>
                
                <!--個人設定-->
                <li class="form" style="display: none;">
                    <form>
                        <div class="m-b-md">
                            <span>Email：</span>
                            <p class="LabelProfileNowEmail">email@example.com</p>
                        </div>

                        <div class="m-b-md">
                            <span>狀態：</span>
                            <p id="LabelProfileState" style="color: #6b9e13;">已認證</p>
                            <button id="btnEmail" type="submit" style="width: 130px; display: inline-block;">寄送認證信</button>
                        </div>

                        <div class="m-b-md">
                            <span>註冊時間：</span>
                            <p id="LabelProfileRegistrationTime">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入時間：</span>
                            <p id="LabelProfileLastLogin">2017-01-01 00:00:12</p>
                        </div>

                        <div class="m-b-md">
                            <span>最後登入IP：</span>
                            <p id="LabelProfileLastIP">115.43.46.4</p>
                        </div>
                    </form>
                    
                    <!--更換Email-->
                    <form>
                        <div class="m-b-md"><h2>更換Email<small>您如果想更改電子郵件地址，请填寫下列欄位。出於安全原因請同時填寫密碼。</small></h2></div>
                        <div class="m-b-md">
                            <span>密碼：</span>
                            <input id="inputProfileCheckPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新的Email：</span>
                            <input id="inputProfileNewEmail" placeholder="Email" class="form-control" type="New Email">
                            <span class="help-block m-b-none">你目前的電子信箱地址是：<c class="LabelProfileNowEmail">al@gmail.com</c></span>
                        </div>

                        <div class="m-b-md">
                            <span>再次確認Email：</span>
                            <input id="inputProfileCheckEmail" placeholder="Email" class="form-control" type="New Email">
                        </div>
                        
                        <button id="btnProfileChangeEmail" type="submit">儲存變更</button>
                    </form>
                    
                    <!--更換密碼-->
                    <form>
                        <div class="m-b-md"><h2>更換密碼</h2></div>

                        <div class="m-b-md">
                            <span>舊的密碼：</span>
                            <input id="inputProfileOldPassword" placeholder="Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>新密碼：</span>
                            <input id="inputProfileNewPassword" placeholder="New Password" class="form-control" type="password">
                        </div>

                        <div class="m-b-md">
                            <span>確認新密碼：</span>
                            <input id="inputProfileCheckNewPassword" placeholder="New Password" class="form-control" type="password">
                        </div>
                        
                        <button id="btnProfileChangePassword" type="submit">儲存變更</button>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>



<!--Ask modal-->
<div class="modal inmodal" id="myModalAsk" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">建議您可以申請企業投資人<br>有更多優惠與功能喔！</h4>
            </div>
            <div class="modal-body text-center">
                <a  data-toggle="modal" data-target="#myModal1">點我了解企業投資人與投資人的差別</a><br><br>
                <a href="register">按此連結立即註冊企業投資人</a><br><br>
                            
                <button type="submit" class="btn-success" data-dismiss="modal">確定</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">瞭解企業投資人與投資人差別</h4>
            </div>
            <div class="modal-body text-center">
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">投資人</li>
                    <li>一對一購買名單</li>
                    <li>確實掌握風險投資</li>
                    <li>篩選出良好的優質客戶</li>
                    <li>防詐騙假帳滲入管理</li>
                    <li>廣告文宣標籤使用</li>
                    <li class="no">購買banner廣告使用權</li>
                    <li class="no">購買分身使用者</li>
                </ul>
                <ul class="pricing-plan list-unstyled">
                    <li class="pricing-title">企業投資人</li>
                    <li>一對一購買名單</li>
                    <li>確實掌握風險投資</li>
                    <li>篩選出良好的優質客戶</li>
                    <li>防詐騙假帳滲入管理</li>
                    <li>廣告文宣標籤使用</li>
                    <li>購買banner廣告使用權</li>
                    <li>購買分身使用者</li>
                </ul>
                <br>
                <br>
                <button type="submit" class="btn-success" data-dismiss="modal" onclick="location.href='register'">還不趕快註冊成企業投資人！</button>
            </div>
        </div>
    </div>
</div>

<!--upload modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">上傳身份證</h4>
            </div>
            <div class="modal-body" style="padding: 20px 50px 30px;">
                <div class="m-b-md">
                    <img alt="image" class="img-responsive" src="img/sample/14.jpg" target-img="identityCardFrontImg" style="width: 49%;display: inline-block;margin-right: 1%;">
                    <img alt="image" class="img-responsive" src="img/sample/13.jpg" target-img="identityCardBackImg" style="width: 49%;display: inline-block;">
                </div>
                
                <div class="dropzone text-center m-b-xl" id="userinfo_uploadIdCardFront" style="width: 49%;display: inline-block;margin-right: 1%;">
                    <div class="am-text-success dz-message" style="font-size: 2rem;">上傳<br>身分證正面</div><div class="am-text-success dz-message">
                        將圖片拖移至此<br>或點此上傳圖片
                    </div>
                </div>
                
                <div class="dropzone text-center m-b-xl" id="userinfo_uploadIdCardBack" style="width: 49%;display: inline-block;">
                    <div class="am-text-success dz-message" style="font-size: 2rem;">上傳<br>身分證反面</div><div class="am-text-success dz-message">
                        將圖片拖移至此<br>或點此上傳圖片
                    </div>
                </div>
                            
                <button type="submit" class="btn-success" data-dismiss="modal">完成</button>
                <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<style>
    #closed-record .step li:nth-child(1),
    #closed-record .step li:nth-child(2),
    #closed-record .step li:nth-child(3),
    #closed-record .step li:nth-child(4) {
        width: 25%;
    }
    
</style>
<!-- path -->
<div id="path"><b class="home" href="#">首頁</b>　/　<b href="#">會員專區</b>　/　<span>已購買名單</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">已購買名單</div>
    <table id="closed-record"> 
        <thead>
            <tr>
                <th class="col-2" data-hide="phone">日期</th>
                <th class="col-1" data-class="expand">資訊</td>
                <th class="col-3" data-hide="phone">金額</th>
                <th class="col-4" data-hide="phone">利息</th>
                <th class="col-5" data-hide="phone">狀態</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>姓名</span>王建民</li>
                        <li><span>地區</span>台北市</li>
                        <li><span>職業</span>服務業</li>
                    </ul>
                </td>
                <td class="col-4">3萬</td>
                <td class="col-4">1.2%</td>
                <td class="col-3"><b class="ready">資料準備</b></td>
            </tr>
            <tr>
                <td colspan="5" class="step">
                    <ul>
                        <li class="active">1.資料準備</li>
                        <li>2.資料審核</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <b class="success">有薪轉</b>
                    <b class="success">有勞保</b>
                    <b class="success">有房屋權狀</b>
                    <b class="success">有土地權狀</b>
                    <b class="success">有存摺資料</b>
                    <b class="success">身份證</b>
                    <b class="success">戶籍謄本</b>
                    <b class="success">財產清冊</b>
                    <b class="success">所得清單</b>
                    <b class="success">自然人憑證</b>
                </td>
            </tr>
        </tbody>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>姓名</span>王建民</li>
                        <li><span>地區</span>台北市</li>
                        <li><span>職業</span>服務業</li>
                    </ul>
                </td>
                <td class="col-4">3萬</td>
                <td class="col-4">1.2%</td>
                <td class="col-3"><b class="ready">資料準備</b></td>
            </tr>
            <tr>
                <td colspan="5" class="step">
                    <ul>
                        <li class="active">1.資料準備</li>
                        <li>2.資料審核</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <b class="success">有薪轉</b>
                    <b class="success">有勞保</b>
                    <b class="success">有房屋權狀</b>
                    <b class="success">有土地權狀</b>
                    <b class="success">有存摺資料</b>
                    <b class="success">身份證</b>
                    <b class="success">戶籍謄本</b>
                    <b class="success">財產清冊</b>
                    <b class="success">所得清單</b>
                    <b class="success">自然人憑證</b>
                </td>
            </tr>
        </tbody>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>姓名</span>王建民</li>
                        <li><span>地區</span>台北市</li>
                        <li><span>職業</span>服務業</li>
                    </ul>
                </td>
                <td class="col-4">3萬</td>
                <td class="col-4">1.2%</td>
                <td class="col-3"><b class="ready">資料準備</b></td>
            </tr>
            <tr>
                <td colspan="5" class="step">
                    <ul>
                        <li class="active">1.資料準備</li>
                        <li>2.資料審核</li>
                        <li>3.資料審核</li>
                        <li>4.撥款</li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <b class="success">有薪轉</b>
                    <b class="success">有勞保</b>
                    <b class="success">有房屋權狀</b>
                    <b class="success">有土地權狀</b>
                    <b class="success">有存摺資料</b>
                    <b class="success">身份證</b>
                    <b class="success">戶籍謄本</b>
                    <b class="success">財產清冊</b>
                    <b class="success">所得清單</b>
                    <b class="success">自然人憑證</b>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<style>
    .list li {
        display: inline-block;
        width: calc(100%/2 - 5px);
        padding: 25px;
        border-bottom: 1px solid #e8e8e8;
        background: #f6f6f6;
        box-sizing: border-box;
    }
    .list li .type h3 {
        margin-bottom: 10px;
        font-size: 30px;
        color: #222;
    }
    .list li .money {
        font-size: 30px;
        color: #1a4da8;
        font-weight: bold;
        word-break: break-all;
    }
    .list li .money::before {
        content: "NT $";
    }
    .form small {
        color: #2E75CC;
        padding-left: 0;
    }
    
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>點數購買</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">點數購買</div>
    <div class="form">
        <form>
            <h2 class="m-b-md">民間代書 - 李先生 <span class="pull-right" style="color:#6DA018;">目前點數：<b id="nowPoint">0</b>pt</span></h2>
            <div class="m-b-xs">
                <span>公司負責人：</span>
                <p>潘小姐</p>
            </div>
            <div class="m-b-xs">
                <span>公司統編：</span>
                <p>25881234</p>
            </div>
          
            <ul class="list m-t-md m-b-md">
              <li>
                <div class="type">
                  <h3>100pt</h3>
                <div class="money">100</div>
              </li>
              <li>
                <div class="type">
                  <h3>1000pt</h3>
                <div class="money">950</div>
              </li>
              <li>
                <div class="type">
                  <h3>1500pt</h3>
                <div class="money">1400</div>
              </li>
              <li>
                <div class="type">
                  <h3>10000pt</h3>
                <div class="money">9800</div>
              </li>
            </ul>

            <h2 class="m-b-md">輸入點數金額</h2>
            <div class="m-b-md">
                <span>點數金額：</span>
<!--                <input placeholder="點數金額" type="text">-->
                <select id="selectPoint">
                    <option value="100">$100</option>
                    <option value="1000">$950</option>
                    <option value="1500">$1400</option>
                    <option value="10000">$9800</option>
                </select>
                <button id="btnBuy">購買</button><br><br>
                <small>指定金額100pt至1000000pt</small><br>
                <small>台幣1元等於3pt</small>
            </div>

            <h2 class="m-t-lg m-b-md" target-edit>選擇付款方式</h2>
            <ul class="list m-t-md m-b-md" target-edit>
              <li>
                <div class="type">
                  <h3>信用卡付費</h3>
              </li>
              <li>
                <div class="type">
                  <h3>超商付款</h3>
              </li>
            </ul>
            <button id="cancle" type="submit">取消</button>
        </form>
    </div>
</div>
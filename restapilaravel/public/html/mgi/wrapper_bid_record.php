<style>
    .step ul{overflow:hidden;}
    .step li{position:relative; float:left; background:#dadada; box-shadow:3px 0 0 #FFF; font-size:15px; color:#6c6c6c; line-height:26px; text-align:center;}
    .step li:nth-child(1),.step li:nth-child(2),
    .step li:nth-child(3),.step li:nth-child(4),
    .step li:nth-child(5),.step li:nth-child(6),
    .step li:nth-child(7){width:calc(100% / 7);}
    .step li:before,
    .step li:after{content:""; position:absolute; top:-2px; display:block; width:0; height:0; border-style:solid; border-width:15px 0 15px 10px;}
    .step li:before{left:100%; border-color:transparent transparent transparent #FFF; z-index:1;}
    .step li:after{left:99%; border-color:transparent transparent transparent #dadada; z-index:2;}
    .step li.active{background:#2e75cc; color:#FFF;}
    .step li.active:after{border-left-color:#2e75cc;}
    
    .ready{color:#2e75cc;}
    .success{color:#6b9e13;}
    .failed{color:#ff0000;}
    
    .dataTables_wrapper input[type="number"]{
        padding: 10px;
        border: 1px solid #ddd;
        border-radius: 3px;
    }
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員專區</a>　/　<span>競標紀錄</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">競標紀錄
    </div>
    <!--sample code-->
    <table id="example" class="display" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs" style="width:200px;">資訊</td>
                <th class="p-h-xs">信用<br>額度</th>
                <th class="p-h-xs">備註</th>
                <th class="p-h-xs">申請<br>人數</th>
                <th class="p-h-xs">利率</th>
                <th class="p-h-xs">撥款<br>時間</th>
                <th class="p-h-xs">借款<br>金額</th>
                <th class="p-h-xs">競標<br>金額</th>
                <th class="p-h-xs">倒數<br>計時</th>
                <th class="p-h-xs">狀態</th>
                <th class="p-h-xs">操作</th>
            </tr>
        </thead>
        <tbody>
<!--            <tr class="odd">
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="ready">資料準備</b></td>
                <td>
                    <a class="btn-success nowrap">參與競標</a>
                </td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.閒置</li>
                        <li class="active">2.競標中</li>
                        <li>3.資料準備中</li>
                        <li>4.送件中</li>
                        <li>5.資料審核</li>
                        <li>6.撥款</li>
                        <li>7.完成</li>
                    </ul>
                </td>
            </tr>
            
            <tr class="even">
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="ready">送件中</b></td>
                <td><a class="btn-success nowrap">同意</a></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.閒置</li>
                        <li>2.競標中</li>
                        <li>3.資料準備中</li>
                        <li class="active">4.送件中</li>
                        <li>5.資料審核</li>
                        <li>6.撥款</li>
                        <li>7.完成</li>
                    </ul>
                </td>
            </tr>
            
            <tr class="odd">
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="success">成功</b></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.閒置</li>
                        <li>2.競標中</li>
                        <li>3.資料準備中</li>
                        <li>4.送件中</li>
                        <li>5.資料審核</li>
                        <li>6.撥款</li>
                        <li class="active">7.完成</li>
                    </ul>
                </td>
            </tr>
            
            <tr class="even">
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
                <td>1萬～10萬</td>
                <td>讓您安心貸安心還，無壓力借款！</td>
                <td>1865741人</td>
                <td>0.6%</td>
                <td>1天</td>
                <td><b class="failed">失敗</b></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="8" class="step">
                    <ul>
                        <li>1.閒置</li>
                        <li>2.競標中</li>
                        <li>3.資料準備中</li>
                        <li>4.送件中</li>
                        <li>5.資料審核</li>
                        <li>6.撥款</li>
                        <li>7.完成</li>
                    </ul>
                </td>
            </tr>-->
        </tbody>
    </table>
</div>


<!--modal-->
<!--<div class="modal inmodal" id="myModalBorrowInfo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs"><a class="active">借款資訊</a><a>借款人資料</a></div>
                    <ul class="tab-content">
                        借款資訊
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>編碼：</span>
                                    <p id="customerInfo_ID" class="form-control-static">FV2277567</p> 
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <p id="customerInfo_username" target-view>王先生</p>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <p id="customerInfo_address"target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_job" target-otablecolumn="userinfo_gender.userinfo_gender" target-view>服務業</p>
                                </div>

                                <div class="m-b-md">
                                    <span>希望利率：</span>
                                    <p id="customerInfo_hoptax" target-view>1.2%</p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款理由：</span>
                                    <p id="customerInfo_reason" target-view>Google was founded in 1996 by Larry Page and Sergey Brin while they were Ph.D. students at Stanford University,...</p>
                                </div>
                            </form>
                        </li>
                        
                        借方資料
                        <li class="form" style="display: none;">
                            <form>
                                <div class="m-b-md">
                                    <div class="col-sm-10" target-view>
                                        <img src="img/a4.jpg" class="img-thumbnail img-lg" target-img="selfie">
                                    </div>
                                </div>

                                <div class="m-b-md">
                                    <span>姓名：</span>
                                    <p id="customerInfo_username" target-view>王先生</p>
                                </div>

                                <div class="m-b-md">
                                    <span>手機號碼：</span>
                                    <p id="customerInfo_phone" target-view>0988123888</p>
                                </div>

                                <div class="m-b-md">
                                    <span>出生年月日：</span>
                                    <p id="customerInfo_birthday" target-view>1980/08/08</p>
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <p id="customerInfo_gender" target-otablecolumn="userinfo_gender.userinfo_gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_job" target-otablecolumn="userinfo_gender.userinfo_gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>地址：</span>
                                    <p id="customerInfo_address"target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>服務地區：</span>
                                    <p id="customerInfo_place" target-view>全省</p>
                                </div>

                                <div class="m-b-md">
                                    <span>申請身份：</span>
                                    <p id="customerInfo_identity" target-view>代書</p>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">確定</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>-->
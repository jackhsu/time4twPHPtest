<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">功能業務</a>　/　<span>收藏名單</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">收藏名單</div>
    <table id="example" class="display nowrap" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs">資訊</td>
                <th class="p-h-xs nowrap">借款額度</th>
                <th class="p-h-xs">借款理由</th>
                <th class="p-h-xs">評價</th>
                <th class="p-h-xs">銀行貸款</th>
                <th class="p-h-xs">民間貸款</th> 
                <th class="p-h-xs">操作</th>
            </tr>
        </thead>
        <tbody>
<!--            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>公司名稱</span>XX企業股份有限公司</li>
                        <li><span>地區</span>台北市</li>
                    </ul>
                </td>
                <td class="col-2">1萬～10萬</td>
                <td class="col-3">讓您安心貸安心還，無壓力借款！</td>
                <td class="col-4">1865741人</td>
                <td class="col-5">0.6%</td>
                <td class="col-5">1天</td>
                <td class="col-8">
                    <a class="agree">詳細</a><br>
                    <a class="agree">刪除</a>
                </td>
            </tr>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>公司名稱</span>XX企業股份有限公司</li>
                        <li><span>地區</span>台北市</li>
                    </ul>
                </td>
                <td class="col-2">1萬～10萬</td>
                <td class="col-3">讓您安心貸安心還，無壓力借款！</td>
                <td class="col-4">1865741人</td>
                <td class="col-5">0.6%</td>
                <td class="col-5">1天</td>
                <td class="col-8">
                    <a class="agree">詳細</a><br>
                    <a class="agree">刪除</a>
                </td>
            </tr>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>公司名稱</span>XX企業股份有限公司</li>
                        <li><span>地區</span>台北市</li>
                    </ul>
                </td>
                <td class="col-2">1萬～10萬</td>
                <td class="col-3">讓您安心貸安心還，無壓力借款！</td>
                <td class="col-4">1865741人</td>
                <td class="col-5">0.6%</td>
                <td class="col-5">1天</td>
                <td class="col-8">
                    <a class="agree">詳細</a><br>
                    <a class="agree">刪除</a>
                </td>
            </tr>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>公司名稱</span>XX企業股份有限公司</li>
                        <li><span>地區</span>台北市</li>
                    </ul>
                </td>
                <td class="col-2">1萬～10萬</td>
                <td class="col-3">讓您安心貸安心還，無壓力借款！</td>
                <td class="col-4">1865741人</td>
                <td class="col-5">0.6%</td>
                <td class="col-5">1天</td>
                <td class="col-8">
                    <a class="agree">詳細</a><br>
                    <a class="agree">刪除</a>
                </td>
            </tr>
            <tr>
                <td class="col-2">2017/05/30</td>
                <td class="col-1">
                    <ul class="info">
                        <li><span>編號</span>FV888994471</li>
                        <li><span>公司名稱</span>XX企業股份有限公司</li>
                        <li><span>地區</span>台北市</li>
                    </ul>
                </td>
                <td class="col-2">1萬～10萬</td>
                <td class="col-3">讓您安心貸安心還，無壓力借款！</td>
                <td class="col-4">1865741人</td>
                <td class="col-5">0.6%</td>
                <td class="col-5">1天</td>
                <td class="col-8">
                    <a class="agree">詳細</a><br>
                    <a class="agree">刪除</a>
                </td>
            </tr>-->
        </tbody>
    </table>
</div>


<!--modal-->
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated flipInY">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">詳細內容</h4>
            </div>
            <div class="modal-body text-center">
                <div class="tab-block">
                    <div class="tabs">
                        <a>借款資訊</a>
                    </div>
                    <ul class="tab-content">
                        <!--借款資訊-->
                        <li class="form">
                            <form>
                                <div class="m-b-md">
                                    <span>編號：</span>
                                    <p id="customerInfo_TaxNumber" class="form-control-static b-no"></p> 
                                </div>

                                <div class="m-b-md">
                                    <span>性別：</span>
                                    <p id="customerInfo_TaxNumber" class="b-gender" target-view> </p>
                                </div>

                                <div class="m-b-md">
                                    <span>地區：</span>
                                    <p id="customerInfo_TaxNumber" class="b-area" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>職業：</span>
                                    <p id="customerInfo_TaxNumber" class="b-job" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款額度：</span>
                                    <p id="customerInfo_TaxNumber" class="b-amt" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>評價：</span>
                                    <p id="customerInfo_TaxNumber" class="b-eval" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>借款理由：</span>
                                    <p id="customerInfo_TaxNumber" class="b-reason" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>銀行有無欠款：</span>
                                    <p id="customerInfo_TaxNumber" class="b-bank" target-view></p>
                                </div>

                                <div class="m-b-md">
                                    <span>民間有無欠款：</span>
                                    <p id="customerInfo_TaxNumber" class="b-priv" target-view></p>
                                </div>
                            </form>
                        </li>
                    </ul>
                            
                    <button type="submit" class="btn-success" data-dismiss="modal">確定購買</button>
                    <button type="submit" class="btn-disable" data-dismiss="modal">取消</button>
                </div>
            </div>
        </div>
    </div>
</div>
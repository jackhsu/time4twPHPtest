<style>
    .step ul{overflow:hidden;}
    .step li{position:relative; float:left; background:#dadada; box-shadow:3px 0 0 #FFF; font-size:15px; color:#6c6c6c; line-height:26px; text-align:center;}
    .step li:nth-child(1),.step li:nth-child(2),
    .step li:nth-child(3),.step li:nth-child(4),
    .step li:nth-child(5),.step li:nth-child(6),
    .step li:nth-child(7){width:calc(100% / 7);}
    .step li:before,
    .step li:after{content:""; position:absolute; top:-2px; display:block; width:0; height:0; border-style:solid; border-width:15px 0 15px 10px;}
    .step li:before{left:100%; border-color:transparent transparent transparent #FFF; z-index:1;}
    .step li:after{left:99%; border-color:transparent transparent transparent #dadada; z-index:2;}
    .step li.active{background:#2e75cc; color:#FFF;}
    .step li.active:after{border-left-color:#2e75cc;}
    
    .ready{color:#2e75cc;}
    .success{color:#6b9e13;}
    .failed{color:#ff0000;}
    
</style>
<!-- path -->
<div id="path"><a class="home" href="/">首頁</a>　/　<a href="#">會員專區</a>　/　<span>結案紀錄</span></div>

<?php include("../sidebar.php"); ?>


<div class="right"> 
    <div id="main-title">結案紀錄</div>
    <!--sample code-->
    <table id="example" class="display" width="100%">
        <thead>
            <tr>
                <th class="p-h-xs">結案編號</td>
                <th class="p-h-xs">結案日期</td>
                <th class="p-h-xs">貸款金額</td>
                <th class="p-h-xs">利率</th>
                <th class="p-h-xs">資訊</td>
            </tr>
        </thead>
        <tbody>
<!--            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>-->
<!--            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <td>00000001</td>
                <td>2017/8/9</td>
                <td>3000000</td>
                <td>1.6%</td>
                <td class="nowrap">
                    <ul class="info">
                        <li>
                            <span>編號</span>
                            <span>FV888994471</span>
                        </li>
                        <li>
                            <span>公司名稱</span>
                            <span>XX企業股份有限公司</span>
                        </li>
                        <li>
                            <span>地區</span>
                            <span>台北市</span>
                        </li>
                    </ul>
                </td>
            </tr>-->
        </tbody>
    </table>
</div>

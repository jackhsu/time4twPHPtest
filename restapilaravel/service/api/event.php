<?php	
        /*
        * @file json_mgm_account.php
        * @brief TABLE:account

        * detail 

        * @author arod ( howareu520@gmail.com )
        * @date 2016-08-30 */
        
        include 'php/config.php';
        include 'php/global.php';
        
        $func = $_REQUEST["func"];

        switch ($func) {
            case "changeTableType":
                $echo = changeTableType();
                break;
            case "changeUserID":
                $echo = changeUserID();
                break;
        }
        echo json_encode($echo);
        
    
    function changeTableType(){
        $callback = array();
        try{   
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $result = mysqli_query($con, "SHOW TABLES LIKE 'userinfo_%'");
                
                if (!$result) {
                    echo "DB Error, could not list tables\n";
                    echo 'MySQL Error: ' . mysql_error();
                    exit;
                }
                
                while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                    echo "{$row['Tables_in_iuning (userinfo_%)']}<br>\n";
                    //ALTER TABLE `chat_room` CHANGE `cr_borrower_user_id` `cr_borrower_user_id` VARCHAR(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
//                    mysqli_query($con, "alter table ".$row['Tables_in_iuning (userinfo_%)']." modify userinfo_ID varchar(30)");
                    mysqli_query($con, "alter table ".$row['Tables_in_iuning (userinfo_%)']." CHANGE `userinfo_ID` `userinfo_ID` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");
                    
//            print_r($row);
                    echo '<br>';
                }
                
                $callback['data'] = $data ? $data : array();
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function changeUserID(){
        $callback = array();
        $callback['success']=0;
        $callback['fail']=0;
        try{    
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                
                $userinfoDB = get_sql($con, "userinfo" );
                if(!$userinfoDB){
                        $callback['msg'] = "sql fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                foreach ($userinfoDB as $key => $value) {
                        
                    $userinfo_old_ID = $value["userinfo_ID"];
                    
//                    $userinfo_old_ID=101;
                    
                    $searchIndex = array("userinfo_ID"=>$userinfo_old_ID);
                    
                    if( $value["userinfo_InExUser"]==="Borrower" ){
                        $keyWord = "B";
                    }
                    else if( $value["userinfo_InExUser"]==="Investor" ){
                        $keyWord = "L";
                    }
                    else if( $value["userinfo_InExUser"]==="Company" ){
                        $keyWord = "V";
                    }
                    else{
                        continue;
                    }
                    
                    while (1){
                        $userinfo_ID = $keyWord.getEnglishRandom(1).getNumberRandom(8);
                        $check = get_sql($con, "userinfo"  , "WHERE userinfo_ID='$userinfo_ID'");
                        if(!$check){
                                break;
                        }
                    }


                    if( update_sql( $con , "userinfo" , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "borrowers_list" , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "chat_file" , array("cf_a_id"=>$userinfo_ID) , array("cf_a_id"=>$userinfo_old_ID) ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "chat_room" , array("cr_investor_user_id"=>$userinfo_ID) , array("cr_investor_user_id"=>$userinfo_old_ID) ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "chat_room" , array("cr_borrower_user_id"=>$userinfo_ID) , array("cr_borrower_user_id"=>$userinfo_old_ID) ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "collection" , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "console" , array("userinfo_id"=>$userinfo_ID) , array("userinfo_id"=>$userinfo_old_ID) ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "history" , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
                    if( update_sql( $con , "investor_list" , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                        $callback['success']++;
                    }
                    else{
                        $callback['fail']++;
                    }
    //                print_r($callback);
                    $result = mysqli_query($con, "SHOW TABLES LIKE 'userinfo_%'");
                    if (!$result) {
                    }
                    else{
                        while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
                            if( $row['Tables_in_iuning (userinfo_%)']==="userinfo_project" ){

                                if( update_sql( $con , "userinfo_project" , array("up_investor_uid"=>$userinfo_ID) , array("up_investor_uid"=>$userinfo_old_ID) ) ){
                                    $callback['success']++;
                                }
                                else{
                                    $callback['fail']++;
                                }  
                                if( update_sql( $con , "userinfo_project" , array("up_borrower_uid"=>$userinfo_ID) , array("up_borrower_uid"=>$userinfo_old_ID) ) ){
                                    $callback['success']++;
                                }
                                else{
                                    $callback['fail']++;
                                }


                            }
                            else if($row['Tables_in_iuning (userinfo_%)']!=="userinfo_column_order"&&$row['Tables_in_iuning (userinfo_%)']!=="userinfo_save" ){
                                if( update_sql( $con , $row['Tables_in_iuning (userinfo_%)'] , array("userinfo_ID"=>$userinfo_ID) , $searchIndex ) ){
                                    $callback['success']++;
                                }
                                else{
                                    $callback['fail']++;
                                }
    //                            echo $row['Tables_in_iuning (userinfo_%)'];
    //                            print_r($callback);
                            }
                        }
                    }
                    
//                    break;
                    
                }
                
                
                
                
                
                
                $callback['data'] = $userinfo_ID;
                $callback['old_data'] = $userinfo_old_ID;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function customerList(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                //搜尋
                $condition = "";
                if( !empty($_REQUEST[ "CustomerName" ]) ){//信箱
                        $condition .= ($condition == "")? "WHERE c.Cust_Name like '%".$_REQUEST[ "CustomerName" ]."%' " : " AND c.Cust_Name like '%".$_REQUEST[ "CustomerName" ]."%' ";
                }
                if( !empty($_REQUEST[ "Address" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE c.Cust_Add like '%".$_REQUEST[ "Address" ]."%' " : " AND c.Cust_Add like '%".$_REQUEST[ "Address" ]."%' ";
                }
                if( !empty($_REQUEST[ "Telephone" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE c.Cust_Phone like '%".$_REQUEST[ "Telephone" ]."%' " : " AND c.Cust_Phone like '%".$_REQUEST[ "Telephone" ]."%' ";
                }
//                $condition = "";
//                $this_month = date('Y-m', strtotime('this month') );
//               order by name "Customer Name", "Address", "Telephone", "Dollar Amount of Last Year's Purchases", "Dollar Amount of This Year's Purchase"
                $data = get_sql_MYSQLI_NUM($con, "customer as c"
                                    . " LEFT JOIN work as w on c.Customer_id=w.Customer AND w.Status=2"
                        , "$condition GROUP BY c.Customer_id" 
                        , "c.Customer_id, c.Cust_Name, c.Cust_Add, c.Cust_Phone, SUM(CASE WHEN YEAR(w.Date_Sold) = YEAR(DATE_SUB(NOW(), INTERVAL 1 YEAR)) THEN w.Price ELSE 0 END) 'Sales_Year_To_Date', SUM(CASE WHEN YEAR(w.Date_Sold) = YEAR(NOW()) THEN w.Price ELSE 0 END) 'Sales_Year_To_Date', '' as 'Operation'");
                
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
//                $callback['condi'] = "$condition GROUP BY a.Artist_id" ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function paymentList(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                //搜尋
                $condition = "";
                if( !empty($_REQUEST[ "CustomerName" ]) ){//信箱
                        $condition .= ($condition == "")? "WHERE c.Cust_Name like '%".$_REQUEST[ "CustomerName" ]."%' " : " AND c.Cust_Name like '%".$_REQUEST[ "CustomerName" ]."%' ";
                }
                if( !empty($_REQUEST[ "Address" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE c.Cust_Add like '%".$_REQUEST[ "Address" ]."%' " : " AND c.Cust_Add like '%".$_REQUEST[ "Address" ]."%' ";
                }
                if( !empty($_REQUEST[ "Telephone" ]) ){//暱稱
                        $condition .= ($condition == "")? "WHERE c.Cust_Phone like '%".$_REQUEST[ "Telephone" ]."%' " : " AND c.Cust_Phone like '%".$_REQUEST[ "Telephone" ]."%' ";
                }
                $condition = "";
                
//               "Owner Name", "Owner Address", "Artist Name", "Owner Social Security Number", "Title", "Type", "Medium", "Style", "Size", "Salesperson", "Selling Price", "Amount Remitted"
                $data = get_sql_MYSQLI_NUM($con, "work as w"
                                    . " LEFT JOIN customer as c on w.Customer=c.Customer_id AND w.Status IN (1,2)"
                                    . " LEFT JOIN artist as a on w.Artist=a.Artist_id"
                                    . " LEFT JOIN artist as owner on w.Artist=owner.Artist_id"
                        , "$condition GROUP BY c.Customer_id"
                        , "w.work_id, owner.Name as 'owner_Name', owner.Address as 'owner_Address', a.Name, '' as 'SecurityNumber', w.paint_title, w.paint_Type, w.paint_Medium, w.paint_Style, w.Size, a.Name, w.Price, w.AmountRemitted, '' as 'Operation'");
                
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
//                $callback['condi'] = "$condition GROUP BY a.Artist_id" ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function artistReport(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
//                $this_month = date('Y-m', strtotime('this month') );
//               "Artist Name", "Artist Address", For each work that artist "Title", "Type", "Medium", "Style", "Asking Price", "Selling Price", "Date Sold"
                $data = get_sql_MYSQLI_NUM($con, "artist as a"
                                    . " JOIN work as w on a.Artist_id=w.Artist"
                        , ""
                        , "a.Artist_id, a.Name, a.Address, w.paint_title, w.paint_Type, w.paint_Medium, w.paint_Style, w.asking_Price"
                        . ", (CASE WHEN w.Price=0 THEN '' ELSE w.Price END ) as 'Price'"
                        . ", (CASE WHEN w.Date_sold='0000-00-00' THEN '' ELSE w.Date_sold END ) as 'Date_sold'"
                        . ", '' as 'Operation'");
                
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
//                $callback['condi'] = "$condition GROUP BY a.Artist_id" ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function salespersonPerformanceReport(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                $startTime = $_REQUEST[ "startTime" ];
                $endTime = $_REQUEST[ "endTime" ];
                $salesPerson = $_REQUEST[ "salesPerson" ];
                
                $condi = empty($salesPerson) ? "" :" AND salesperson = '$salesPerson'";
//                $this_month = date('Y-m', strtotime('this month') );
//               "Title", "Artist", "Asking Price", "Selling Price"
                $data = get_sql_MYSQLI_NUM($con, "work as w"
                                    . " LEFT JOIN artist as a on w.Artist=a.Artist_id"
                        , "WHERE w.Status=2 AND w.Date_of_Show BETWEEN '$startTime' AND '$endTime'".$condi
                        , "w.work_id, w.paint_title, a.Name, w.Price, w.asking_Price, '' as 'Operation', w.salesperson, w.Date_of_Show");
                
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
                $callback['condi'] = "WHERE Date_of_Show BETWEEN $startTime AND $endTime".$condi ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function galleryList(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                
                $opera_condi = 'CASE WHEN w.Status=0 THEN \'<div class="center"><a id="sell" class="mark msg_box">賣</a><br><a class="mark msg_box page_delete" id="delete">刪除</a></div>\''
                                . " ELSE '' END";
                
//               "Title", "Artist", "Asking Price", "Selling Price"
                $data = get_sql_MYSQLI_NUM($con, "work as w"
                                    . " LEFT JOIN artist as a on w.Artist=a.Artist_id"
                                    . " LEFT JOIN artist as owner on w.Owner=owner.Artist_id"
                        , ""
                            , "w.work_id, w.paint_title, a.Name, owner.name as 'owner_name', concat('<image width=\"30px\" src=\'../data/gallery/',w.paint_Image,'\'>') as paint_Image, w.paint_Type, w.paint_Medium, w.paint_Style, w.asking_price, Date_of_Show, w.Price, w.AmountRemitted, w.Date_Sold, w.salesperson"
                            . ",($opera_condi) as 'opera_condi'");
                
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
//                $callback['condi'] = "$condition GROUP BY a.Artist_id" ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
    function salesList(){
        $callback = array();
        try{    
                $cart = array();
                
                if( !check_empty( array("token" ) ) ) {
                        $callback['msg'] = "輸入資料不完整";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $con=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
                $con->query("SET NAMES utf8");
                // Check connection
                if (mysqli_connect_errno()) {
                        $callback['msg'] = "SQL connect fail";
                        $callback['success'] = false;
                        return $callback;
                }
                
                $token = md5( $_REQUEST[ "token" ] );
                
                $Check_Admin = Check_Admin( $con , $token );
                if( ! $Check_Admin['success'] ){
                        $callback = $Check_Admin;
//                        $callback['msg'] = "you dont have admin";
//                        $callback['success'] = false;
                        mysqli_close($con);
                        return $callback;
                }
                
                $data = get_sql_MYSQLI_NUM($con, "work"
                        , "WHERE salesperson != '' GROUP BY salesperson"
                        , "salesperson");
                
                $callback['success'] = true;
                $callback['data'] = $data ? $data : array();
//                $callback['data2'] = $data2 ? $data2 : array();
//                $callback['condi'] = "$condition GROUP BY a.Artist_id" ;
                mysqli_close($con);
        }
        catch (Exception $e)
        {
                $callback['msg'] = $e;
                $callback['success'] = false;
        }
        
        return $callback;
    }
    
?>

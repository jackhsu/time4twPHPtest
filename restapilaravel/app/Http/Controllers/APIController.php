<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;

class APIController extends Controller
{
    
    public function clearProjectChatroom(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            
            $truncate = DB::table('userinfo_project')->truncate();
            
            $updateStatus = array("cr_status"=>0);
            $DBCMD = DB::table("chat_room")
                ->where( 'cr_status', ">",0 )
                ->update( $updateStatus );

            if($DBCMD==0){
                    $callback["success"]=false;
                    $callback["msg"]="寄送失敗";
            }
            else{
                    $callback["success"]=true;
            }
            
            
//            $callback['data'] = $data;
//            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    private function insertAPI( $cca_user_id, $cca_call_user_id, $cca_command_id, $cca_command_user ){
            
            $callback = array();
            $DBCMD = DB::table('chat_command_api')->insert(
                [   'cca_user_id' => $cca_user_id, 
                    'cca_call_user_id' => $cca_call_user_id, 
                    'cca_command_id' => $cca_command_id, 
                    'cca_command_user' => $cca_command_user,
                    'cca_call_time' => date('Y-m-d H:i:s'), 
                    'cca_state' => 0 ]
            );
            if (!$DBCMD) {
                $callback["success"]=false;
                $callback["msg"]="create api fail";
            }
            
            $callback["success"]=true;
            return $callback;
    }
    
    public function udate($format = 'u', $utimestamp = null) {
            if (is_null($utimestamp))
                $utimestamp = microtime(true);

            $timestamp = floor($utimestamp);
            $milliseconds = round(($utimestamp - $timestamp) * 1000000);

            return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }

    private function callCurl($Url,$Type,$Json=""){
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $Url);//"https://leancloud.cn/1.1/classes/className"
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            if($Type==="POST"){
                curl_setopt($ch, CURLOPT_POST, TRUE);
            }
            else if($Type==="PUT"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            else if($Type==="GET"){
                
            }
            else if($Type==="DELETE"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }
            
            if(!empty($Json))
                curl_setopt($ch, CURLOPT_POSTFIELDS, $Json);/*"{
                \"title\": \"工程师周会\",
                \"content\": \"每周工程师会议，周一下午2点\"
              }"*/

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Content-Type: application/json",
              "X-LC-Id: MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz",
              "X-LC-Key: VaIUCWxS03vG3wX707yfbYxg,master"
            ));

            $response = curl_exec($ch);
            curl_close($ch);

            //var_dump($response);
            return $response;
    }
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
            echo json_encode($data);
            exit;
    }
    
    private function updateMysql($tableName,$condiColumnArr,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    private function updateMysqlIncludeIn($tableName,$condiColumnArr,$condiInColumn,$condiInValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->whereIn($condiInColumn,$condiInValue)//('chat_room.cr_status', [1,2])
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->whereIn($condiInColumn,$condiInValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;

class ChatController extends Controller
{
    
    public function getAllBorrowerByInvestor(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            //未修改
            if($usersInfo[0]['userinfo_InExUser']!=="Investor"){
                $callback["success"]=false;
                $callback["msg"]="您不是金主";
                return $this->jsonResponse( $callback);
            }
            
            $check = DB::table('userinfo')
//                    ->leftjoin('chat_room', 'userinfo.userinfo_ID', '=', 'chat_room.cr_borrower_user_id')
                    ->select('userinfo_ID','userinfo_UserName','userinfo_UserIdentity','userinfo_Area')
                    ->where('userinfo_InExUser', "Borrower")
                    ->get();
            
            $data = array();
            $data["allBorrowerList"] = $check;
            
            $check = DB::table('chat_room')
                    ->join('userinfo', 'userinfo.userinfo_ID', '=', 'chat_room.cr_borrower_user_id')
                    ->select('userinfo.userinfo_ID','userinfo.userinfo_UserName','userinfo.userinfo_UserIdentity','userinfo.userinfo_Area')
                    ->where('chat_room.cr_investor_user_id', $usersInfo[0]['userinfo_ID'])
                    ->where('chat_room.cr_status', "3")
                    ->get();
            $data["AgreeBorrowerList"] = $check;

            $check = DB::table('chat_room')
                    ->join('userinfo', 'userinfo.userinfo_ID', '=', 'chat_room.cr_borrower_user_id')
                    ->select('userinfo.userinfo_ID','userinfo.userinfo_UserName','userinfo.userinfo_UserIdentity','userinfo.userinfo_Area')
                    ->where('chat_room.cr_investor_user_id', $usersInfo[0]['userinfo_ID'])
                    ->whereIn('chat_room.cr_status', ["1","2"])
                    ->get();
            $data["askBorrowerList"] = $check;
            
            $check = DB::table('userinfo_project')
                    ->join('userinfo', 'userinfo.userinfo_ID', '=', 'userinfo_project.up_borrower_uid')
                    ->select('userinfo.userinfo_ID','userinfo.userinfo_UserName','userinfo.userinfo_UserIdentity','userinfo.userinfo_Area')
                    ->whereIn('userinfo_project.up_status', [0,1])
//                    ->groupBy('userinfo_project.up_borrower_uid')
                    ->get();
            $data["biding"] = $check;
            
            $callback['data'] = $data;
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function getInvestorByBorrower(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            //未修改
            if($usersInfo[0]['userinfo_InExUser']!=="Borrower"){
                $callback["success"]=false;
                $callback["msg"]="您不是金主";
                return $this->jsonResponse( $callback);
            }
            
            $data = array();
            
//            $check = DB::table('chat_room')
//                    ->join('userinfo', 'userinfo.userinfo_ID', '=', 'chat_room.cr_investor_user_id')
//                    ->select('userinfo.userinfo_ID','userinfo.userinfo_UserName','userinfo.userinfo_UserIdentity','userinfo.userinfo_Area')
//                    ->where('chat_room.cr_borrower_user_id', $usersInfo[0]['userinfo_ID'])
//                    ->whereIn('chat_room.cr_status', [1,2])
//                    ->get();
//            $data["unAgreeBorrowerList"] = $check;
            
            $check = DB::table('userinfo')
                    ->select('userinfo_ID','userinfo_UserName','userinfo_UserIdentity','userinfo_Area')
                    ->where('userinfo_InExUser', "Investor")
                    ->orderby('userinfo_evaluation','desc')
                    ->get();
            $data["allBorrowerList"] = $check;
            
            $check = DB::table('chat_room')
                    ->join('userinfo', 'userinfo.userinfo_ID', '=', 'chat_room.cr_investor_user_id')
                    ->select('userinfo.userinfo_ID','userinfo.userinfo_UserName','userinfo.userinfo_UserIdentity','userinfo.userinfo_Area')
                    ->where('chat_room.cr_borrower_user_id', $usersInfo[0]['userinfo_ID'])
                    ->where('chat_room.cr_status', 3)
                    ->get();
            $data["AgreeBorrowerList"] = $check;

            $callback['data'] = $data;
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function getChatroomID(Request $request) {
            
            $callback = array();
            // check empty
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在1";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($request["Investor"],$usersInfo[0]['userinfo_ID'],$check[0]['userinfo_UserName'],$usersInfo[0]['userinfo_UserName']);
                $request->session()->put('lastChatUser', $request["Investor"]);
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在2".$request["Borrower"];
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($usersInfo[0]['userinfo_ID'],$request["Borrower"],$usersInfo[0]['userinfo_UserName'],$check[0]['userinfo_UserName']);
                $request->session()->put('lastChatUser', $request["Investor"]);
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $callback['data'] = array( "cr_ask_money"=>$conv['cr_ask_money'], "cr_status"=>$conv['cr_status'], "cr_convid" =>$conv['cr_convid'] );
            $callback['success'] = true;
            
            $request->session()->put('lastChatConvid', $conv['cr_convid']);
            $request->session()->put('lastLoginUser', $usersInfo[0]['userinfo_ID']);
            
            return $this->jsonResponse($callback);
            
    }
    
    public function getTalkContent(Request $request) {
            
            $callback = array();
            // check empty
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在1";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($request["Investor"],$usersInfo[0]['userinfo_ID'],$check[0]['userinfo_UserName'],$usersInfo[0]['userinfo_UserName']);
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在2".$request["Borrower"];
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($usersInfo[0]['userinfo_ID'],$request["Borrower"],$usersInfo[0]['userinfo_UserName'],$check[0]['userinfo_UserName']);
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            //
            if( $usersInfo[0]['userinfo_InExUser']==="Investor" && in_array( $conv['cr_status'] , array(1,2) ) ){
                $data = array();
            }
            else{
                $data = $this->callCurl('https://api.leancloud.cn/1.1/rtm/messages/history?convid='.$conv['cr_convid'],'GET');
                $data = json_decode($data,true);
                foreach ($data as $key => $value) {
                    if(strpos($value["data"] ,"cf_id=") !== false){
                        $cf_id = substr($value["data"], 6,strlen($value["data"])-6);
                        $check = DB::table('chat_file')
                                ->where('cf_id', $cf_id)
                                ->get();
                        if ($check->count() == 0) {
                            $data[$key]["data"] = "<a href=''>附件錯誤</a>";
                        }
                        else{
                            $chatFile = json_decode(json_encode($check),true);
                             //href="v_download.php?mod=attachment&page=' + value.page_id + '&file=' + value.pf_id + '" target="_blank"';
                            //$data[$key]["data"] = "<a href='../service/download/v_download.php?cf_id=".$chatFile[0]["cf_id"]."'>".$chatFile[0]["cf_original_name"]." 下載</a>";
                            $data[$key]["data"] = "<a href='../service/download/v_download.php?cf_conv_id=".$chatFile[0]["cf_conv_id"]."&cf_content=".$chatFile[0]["cf_content"]."&cf_original_name=".$chatFile[0]["cf_original_name"]."'>".$chatFile[0]["cf_original_name"]." 下載</a>";
                        }
                    }
                }
            }
            
            $callback['data'] = array( "cr_ask_money"=>$conv['cr_ask_money'], "cr_status"=>$conv['cr_status'], "message" =>$data , "userinfo_Status"=>$usersInfo[0]["userinfo_Status"] );
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function getTalkContentWithCustomerService(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
           
            $data = array();
            
            $conv = $this->searchConvid($usersInfo[0]['userinfo_ID'],"客服",$usersInfo[0]['userinfo_UserName'],"客服");
        
            
            $data = $this->callCurl('https://api.leancloud.cn/1.1/rtm/messages/history?convid='.$conv['cr_convid'],'GET');
            $data = json_decode($data,true);
            foreach ($data as $key => $value) {
                if(strpos($value["data"] ,"cf_id=") !== false){
                    $cf_id = substr($value["data"], 6,strlen($value["data"])-6);
                    $check = DB::table('chat_file')
                            ->where('cf_id', $cf_id)
                            ->get();
                    if ($check->count() == 0) {
                        $data[$key]["data"] = "<a href=''>附件錯誤</a>";
                    }
                    else{
                        $chatFile = json_decode(json_encode($check),true);
                         //href="v_download.php?mod=attachment&page=' + value.page_id + '&file=' + value.pf_id + '" target="_blank"';
                        //$data[$key]["data"] = "<a href='../service/download/v_download.php?cf_id=".$chatFile[0]["cf_id"]."'>".$chatFile[0]["cf_original_name"]." 下載</a>";
                        $data[$key]["data"] = "<a href='../service/download/v_download.php?cf_conv_id=".$chatFile[0]["cf_conv_id"]."&cf_content=".$chatFile[0]["cf_content"]."&cf_original_name=".$chatFile[0]["cf_original_name"]."'>".$chatFile[0]["cf_original_name"]." 下載</a>";
                    }
                }
            }
            
            $callback['data'] = array( "message" =>$data );
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    private function searchConvid($Investor,$Borrower,$InvestorName,$BorrowerName) {
            
            $check = DB::table('chat_room')
                    ->select('cr_convid','cr_status','cr_ask_money')
                    ->where('cr_investor_user_id', $Investor)
                    ->where('cr_borrower_user_id', $Borrower)
                    ->get();
            if ($check->count() == 0) {
                $Json=array("name"=>"$InvestorName&$BorrowerName Room",
                        "m"=> [$InvestorName, $BorrowerName],
                        "tr"=> true,
                        "sys"=> true);
                $Json = json_encode($Json);
                $data = $this->callCurl('https://api.leancloud.cn/1.1/classes/_Conversation','POST',$Json);
                $data = json_decode($data,true);
                $insert_id = $data["objectId"];
                
                $DBCMD = DB::table('chat_room')->insert(
                    [   'cr_investor_user_id' => $Investor, 
                        'cr_borrower_user_id' => $Borrower, 
                        'cr_convid' => $insert_id, 
                        'cr_create_datetime' => date('Y-m-d H:i:s'), 
                        'cr_status' => 0  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="create chat room db fail";
                    return $this->jsonResponse($callback);
                }
                $chat_room = array('cr_convid'=>$insert_id,'cr_status'=>0,'cr_ask_money'=>0);
            }
            else{
                $chat_room = json_decode(json_encode($check),true)[0];
            }
            return $chat_room;
    }
    
    private function searchConvidWithService($User,$UserName) {
            
            $check = DB::table('chat_room_service')
                    ->select('cr_convid')
                    ->where('cr_user_id', $User)
                    ->get();
            if ($check->count() == 0) {
                $Json=array("name"=>"$UserName&客服 Room",
                        "m"=> [$InvestorName, $BorrowerName],
                        "tr"=> true,
                        "sys"=> true);
                $Json = json_encode($Json);
                $data = $this->callCurl('https://api.leancloud.cn/1.1/classes/_Conversation','POST',$Json);
                $data = json_decode($data,true);
                $insert_id = $data["objectId"];
                
                $DBCMD = DB::table('chat_room_service')->insert(
                    [   'cr_user_id' => $Investor, 
                        'cr_borrower_user_id' => $Borrower, 
                        'cr_convid' => $insert_id, 
                        'cr_create_datetime' => date('Y-m-d H:i:s'), 
                        'cr_status' => 0  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="create chat room db fail";
                    return $this->jsonResponse($callback);
                }
                $chat_room = array('cr_convid'=>$insert_id,'cr_status'=>0,'cr_ask_money'=>0);
            }
            else{
                $chat_room = json_decode(json_encode($check),true)[0];
            }
            return $chat_room;
    }
    
    public function sendMessagetToRoom(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            if ( !checkEmpty($request,"content") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($request["Investor"],$usersInfo[0]['userinfo_ID'],$check[0]['userinfo_UserName'],$usersInfo[0]['userinfo_UserName']);
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $conv = $this->searchConvid($usersInfo[0]['userinfo_ID'],$request["Borrower"],$usersInfo[0]['userinfo_UserName'],$check[0]['userinfo_UserName']);
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $Json = ["from_peer"=> (string)$usersInfo[0]['userinfo_UserName'], 
                                "message"=> $request["content"], 
                                "conv_id"=> $conv["cr_convid"], 
                                "transient"=> false
                            ];
            $Json = json_encode($Json);
            $result = $this->callCurl('https://api.leancloud.cn/1.1/rtm/messages','POST',$Json);
            $result = json_decode($result,true);
//            print_r($result);
//            echo($result["result"]);
//            print_r($result["result"]);
            $result["status"] = $conv["cr_status"];//
            $result["success"] = true;
            
            if($conv["cr_status"]===0){
                $this->updateMysql("chat_room",array("cr_convid"=>$conv["cr_convid"],"cr_status"=>0),array("cr_status"=>1));
                $this->insertAPI( $request["Investor"]?$request["Investor"]:$request["Borrower"], $usersInfo[0]['userinfo_ID'], 5, $usersInfo[0]['userinfo_ID'] );
//                $this->insertAPI( $usersInfo[0]['userinfo_ID'], $usersInfo[0]['userinfo_ID'], 3, $usersInfo[0]['userinfo_ID'] );
            }
            else{
                $callback = $this->insertAPI( $request["Investor"]?$request["Investor"]:$request["Borrower"], $usersInfo[0]['userinfo_ID'], 2, $usersInfo[0]['userinfo_ID'] );
            }
            
            return $result;
            
    }
    
    public function sendFile(Request $request) {
            
            $callback = array();
                // check empty
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            
            // User need permission: Administrator 
            $usersInfo = $request["token_info"];
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $check = json_decode(json_encode($check),true);
                $conv = $this->searchConvid($request["Investor"],$usersInfo[0]['userinfo_ID'],$check[0]['userinfo_UserName'],$usersInfo[0]['userinfo_UserName']);
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $check = json_decode(json_encode($check),true);
                $conv = $this->searchConvid($usersInfo[0]['userinfo_ID'],$request["Borrower"],$usersInfo[0]['userinfo_UserName'],$check[0]['userinfo_UserName']);
            }
            else{
                return $this->parameterErrorResponse();
            }

            $FileName = $_FILES["file"]['name'];
            $FileSub = explode( "." , $FileName );
            $FileSub = $FileSub[count($FileSub)-1];

            define('upload_chatroom', "C:\\xampp\\htdocs\\iuning\\resources\\data\\chatroom\\" );
            define('http_upload_chatroom', "http://www.fooddy.co/bohan/iuning/resources/data/chatroom/" );

            if( !file_exists(upload_chatroom.$conv["cr_convid"]."\\") ){
                mkdir(upload_chatroom.$conv["cr_convid"]."\\", 0777, true);
            }

            $i = 0;
            $time = $this -> udate('YmdHisu');
            $file_final_name = $time.".".$FileSub;
            $filepath = upload_chatroom.$conv["cr_convid"]."\\" . $file_final_name;
            //$httppath = "http://".$_SERVER["SERVER_NAME"]."/ttshow/transient_file/".$time.".".$_REQUEST['subname'];
            while(1){
                if( file_exists($filepath) ){
                    $i++;
                    $file_final_name = $time."_".(string)$i.".".$FileSub;
                    $filepath = upload_chatroom.$conv["cr_convid"]."\\".$file_final_name;
                }
                else{  break;}
            }

            move_uploaded_file( $_FILES["file"]["tmp_name"] , $filepath );
            if( !file_exists($filepath) ) {
                $callback['msg'] = "Upload fail";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }

            $callback['data'] = array( "file" => $file_final_name ,
                                       "ori_file" => $_FILES["file"]['name'] ,
                                       "path" => http_upload_chatroom.$conv["cr_convid"]."/" );
            $callback['success'] = true;

            $DBCMD = DB::table('chat_file')->insertGetId(
                [   'cf_a_id' => $usersInfo[0]['userinfo_ID'], 
                    'cf_conv_id' => $conv["cr_convid"], 
                    'cf_content' => $file_final_name, 
                    'cf_original_name' => $_FILES["file"]['name'], 
                    'cf_display' => 'block', 
                    'cf_size' => $_FILES["file"]['size'],
                    'cf_date' => date('Y-m-d H:i:s') ]
            );

            if (!$DBCMD) {
                $callback["success"]=false;
                $callback["msg"]="Insert data fail";
                return $this->jsonResponse($callback);
            }
            
            $Json = ["from_peer"=> (string)$usersInfo[0]['userinfo_UserName'], 
                                "message"=> 'cf_id='.$DBCMD, 
                                "conv_id"=> $conv["cr_convid"], 
                                "transient"=> false
                            ];
            $Json = json_encode($Json);
            $this->callCurl('https://api.leancloud.cn/1.1/rtm/messages','POST',$Json);
            //不知道失敗的回傳直
            
            $callback["success"]=true;

            return $this->jsonResponse($callback);
            
    }
    
    public function borrowerResChat(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,array("Investor","response")) )
            {
                return $this->parameterErrorResponse();
            }
            
            if( !in_array($request["response"], array("true","false")) ){
                    $callback['msg'] = "輸入資料錯誤";
                    $callback['success'] = false;
                    return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                
                if($request["response"]==="true"){
                    $updateStatus = array("cr_status"=>3,"cr_response_datetime"=>date('Y-m-d H:i:s'));
                }
                else{
                    $updateStatus = array("cr_status"=>4,"cr_response_datetime"=>date('Y-m-d H:i:s'));
                }
                
                $updateResult = $this->updateMysqlIncludeIn("chat_room",array("cr_investor_user_id"=>$request["Investor"],"cr_borrower_user_id"=>$usersInfo[0]['userinfo_ID']),"cr_status",array(1,2),$updateStatus);
                if ($updateResult) {
                        $callback["response"]=$request["response"];
                        $callback["success"]=true;
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="操作失敗";
                }
                
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $callback["success"] = true;
            return $callback;
            
    }
    
    public function investorResChat(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,array("Borrower","response")) )
            {
                return $this->parameterErrorResponse();
            }
            
            if( !in_array($request["response"], array("true","false")) ){
                    $callback['msg'] = "輸入資料錯誤";
                    $callback['success'] = false;
                    return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                
                if($request["response"]==="true"){
                    $updateStatus = array("cr_status"=>3,"cr_response_datetime"=>date('Y-m-d H:i:s'));
                }
                else{
                    $updateStatus = array("cr_status"=>4,"cr_response_datetime"=>date('Y-m-d H:i:s'));
                }
                
                $updateResult = $this->updateMysqlIncludeIn("chat_room",array("cr_investor_user_id"=>$usersInfo[0]['userinfo_ID'],"cr_borrower_user_id"=>$request["Borrower"]),"cr_status",array(1,2),$updateStatus);
                if ($updateResult) {
                        $callback["response"]=$request["response"];
                        $callback["success"]=true;
                        
                        $this->insertAPI( $usersInfo[0]['userinfo_ID'], 0, 6, $request["Borrower"] );
                        $this->insertAPI( $request["Borrower"], 0, 3, 0 );
                        
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="操作失敗";
                }
                
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $callback["success"] = true;
            return $callback;
            
    }
    
    public function getMyChatCommand(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            
            $check = DB::table('chat_command_api')
                    ->select('cca_id','cca_command_id','cca_command_user')
                    ->where('cca_state', 0)
                    ->where('cca_user_id', $usersInfo[0]['userinfo_ID'])
                    ->get();
            
            if ($check->count() == 0) {
                $data = array();
            }
            else{
                $data = $check;
            }
            
            $callback['data'] = $data;
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function returnAPIState(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,array("cca_id")) )
            {
                return $this->parameterErrorResponse();
            }
            
            $this->updateMysql("chat_command_api",array("cca_id"=>$request["cca_id"],"cca_user_id"=>$usersInfo[0]['userinfo_ID'],"cca_state"=>0),array("cca_state"=>1));
            
//            $callback['data'] = $data;
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function pushAPI(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,array("user","command_id","command_user")) )
            {
                return $this->parameterErrorResponse();
            }
            
            $check = DB::table('userinfo')
                    ->where('userinfo_ID', $request['user'])
                    ->get();
            if ($check->count() == 0) {
                $callback['success'] = false;
                $callback['msg'] = "command user not exist";
                return $this->jsonResponse($callback);
            }
            
            $callback = $this->insertAPI( $request['user'], $usersInfo[0]['userinfo_ID'], $request['command_id'], ($request['command_user']?$request['command_user']:0) );
            
//            $callback['data'] = $data;
//            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    public function getBorrowInfoByBorrowIdFromInvestor(Request $request) {
            
            $callback = array();
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,array("borrowID")) )
            {
                return $this->parameterErrorResponse();
            }
            
            if( $usersInfo[0]["userinfo_InExUser"] !== "Investor" ){
                $callback['success'] = false;
                $callback['msg'] = "您不是金主";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('chat_room')
                    ->where('cr_investor_user_id', $usersInfo[0]['userinfo_ID'])
                    ->where('cr_borrower_user_id', $request['borrowID'])
                    ->where('cr_status',"=", 3)
                    ->get();
            if ($check->count() == 0) {
                $callback['success'] = false;
                $callback['msg'] = "you do not allow";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('userinfo')
                    ->where('userinfo_ID', $request['borrowID'])
                    ->get();
            if ($check->count() == 0) {
                $callback['success'] = false;
                $callback['msg'] = "borrowID not exist";
                return $this->jsonResponse($callback);
            }
            
            $callback['data'] = $check;
            $callback['success'] = true;
            return $this->jsonResponse($callback);
            
    }
    
    private function insertAPI( $cca_user_id, $cca_call_user_id, $cca_command_id, $cca_command_user ){
            
            $callback = array();
            $DBCMD = DB::table('chat_command_api')->insert(
                [   'cca_user_id' => $cca_user_id, 
                    'cca_call_user_id' => $cca_call_user_id, 
                    'cca_command_id' => $cca_command_id, 
                    'cca_command_user' => $cca_command_user,
                    'cca_call_time' => date('Y-m-d H:i:s'), 
                    'cca_state' => 0 ]
            );
            if (!$DBCMD) {
                $callback["success"]=false;
                $callback["msg"]="create api fail";
            }
            
            $callback["success"]=true;
            return $callback;
    }
    
    public function udate($format = 'u', $utimestamp = null) {
            if (is_null($utimestamp))
                $utimestamp = microtime(true);

            $timestamp = floor($utimestamp);
            $milliseconds = round(($utimestamp - $timestamp) * 1000000);

            return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }

    private function callCurl($Url,$Type,$Json=""){
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $Url);//"https://leancloud.cn/1.1/classes/className"
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            if($Type==="POST"){
                curl_setopt($ch, CURLOPT_POST, TRUE);
            }
            else if($Type==="PUT"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            else if($Type==="GET"){
                
            }
            else if($Type==="DELETE"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }
            
            if(!empty($Json))
                curl_setopt($ch, CURLOPT_POSTFIELDS, $Json);/*"{
                \"title\": \"工程师周会\",
                \"content\": \"每周工程师会议，周一下午2点\"
              }"*/

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Content-Type: application/json",
              "X-LC-Id: MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz",
              "X-LC-Key: VaIUCWxS03vG3wX707yfbYxg,master"
            ));

            $response = curl_exec($ch);
            curl_close($ch);

            //var_dump($response);
            return $response;
    }
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
            echo json_encode($data);
            exit;
    }
    
    private function updateMysql($tableName,$condiColumnArr,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    private function updateMysqlIncludeIn($tableName,$condiColumnArr,$condiInColumn,$condiInValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->whereIn($condiInColumn,$condiInValue)//('chat_room.cr_status', [1,2])
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->whereIn($condiInColumn,$condiInValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
}

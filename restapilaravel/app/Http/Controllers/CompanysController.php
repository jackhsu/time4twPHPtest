<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;
define('upload_profile', "C:\\xampp\\htdocs\\arod\\iuning\\resources\\data\\profile\\" );
define('http_upload_profile', "http://www.fooddy.co/arod/iuning/resources/data/profile/" );

class CompanysController extends Controller
{
    
    public function getAllMyAccountListByCompany(Request $request) {
        
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if($usersInfo[0]['userinfo_InExUser']!=="Company"){
                $callback["success"]=false;
                $callback["msg"]="您不是企業用戶";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('userinfo')
                            ->select('userinfo.*','userinfo_point.userinfo_point')
                            ->leftjoin('userinfo_point', 'userinfo_point.userinfo_ID', '=', 'userinfo.userinfo_ID')
                            ->where('userinfo.userinfo_belong_company_id', $usersInfo[0]["userinfo_ID"])
//                            ->where('up_id', $request["up_id"])
                            ->get();
            if ($check->count() == 0) {
//                $callback['msg'] = "錯誤";
//                $callback['success'] = false;
//                return $this->jsonResponse($callback);
                $callback["data"]=array();
            }
            else{
                $callback["data"]=$check;
            }
            $callback["success"]=true;
            
            return $callback;
            
    }
    
    public function turnOutPoint(Request $request) {
        
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,array("point","u_id")) )
            {
                return $this->parameterErrorResponse();
            }
            
            if($usersInfo[0]['userinfo_InExUser']!=="Company"){
                $callback["success"]=false;
                $callback["msg"]="您不是企業用戶";
                return $this->jsonResponse($callback);
            }
            
            
            $giveMember = DB::table('userinfo')
                        ->where('userinfo_ID', $request["u_id"])
                        ->where('userinfo_belong_company_ID', $usersInfo[0]["userinfo_ID"])
                        ->get();
            if ($giveMember->count() == 0) {
                $callback["success"]=false;
                $callback["msg"]="分身會員出現錯誤";
                return $this->jsonResponse($callback);
            }
            $giveMember = json_decode(json_encode($giveMember),true)[0];
            
            $check = DB::table('userinfo_point')
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->get();
            if ($check->count() == 0) {
                $DBCMD = DB::table('userinfo_point')->insert(
                    [   'userinfo_ID' => $usersInfo[0]["userinfo_ID"], 
                        'userinfo_point' => $request["point"]  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="餘額不足";
                    return $this->jsonResponse($callback);
                }
                $callback["success"]=false;
                $callback["msg"]="餘額不足";
                return $this->jsonResponse($callback);
            }
            else{
                $check = json_decode(json_encode($check),true)[0];
                $userinfo_point = $check["userinfo_point"];
                if( $userinfo_point < $request["point"] ){
                    $callback["success"]=false;
                    $callback["msg"]="餘額不足";
                    return $this->jsonResponse($callback);
                }
                ////////////////////
                $DBCMD = DB::table("userinfo_point")
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->decrement('userinfo_point',$request["point"]);
                if($DBCMD==0){
                    $callback["success"]=false;
                    $callback["msg"]="購買失敗";
                    return $this->jsonResponse($callback);
                }
                
                $check = DB::table('userinfo_point')
                        ->where('userinfo_ID', $giveMember["userinfo_ID"])
                        ->get();
                if ($check->count() == 0) {
                    $DBCMD = DB::table('userinfo_point')->insert(
                        [   'userinfo_ID' => $giveMember["userinfo_ID"], 
                            'userinfo_point' => $request["point"]  ]
                    );
                }
                else{
                    $DBCMD = DB::table("userinfo_point")
                            ->where('userinfo_ID', $giveMember["userinfo_ID"])
                            ->increment('userinfo_point',$request["point"]);
                    if($DBCMD==0){
                        $callback["success"]=false;
                        $callback["msg"]="購買失敗";
                        return $this->jsonResponse($callback);
                    }
                }
                ////////////////////
                
            }
            
            
            $callback["data"]=$userinfo_point - $request["point"];
            $callback["success"]=true;
            
            return $callback;
            
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
        echo json_encode($data);
        exit;
    }
    
    private function updateMysql($tableName,$condiColumn,$condiValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumn , $condiValue ];
        
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
        
        $DBCMD = DB::table($tableName)
                ->where($condiColumn,$condiValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    
    // table('zipcode') 全台縣市及其鄉鎮區
    /* @param String $userinfo_InExUser
     * @param String $userinfo_Password
     * @param String $userinfo_Email
     */
    public function gerCityDistrict(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
//        var_dump($usersInfo);exit;
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
                
        $area = DB::table('zipcode')
            ->select('zipcode_City')
            ->orderBy('zipcode_ID', 'asc')
            ->groupBy('zipcode_City')
            ->get();
        if ($area->count()===0) {
            $callback["area"] = [];
        } else {
            $callback["area"] = json_decode(json_encode($area), true);
        }
        
        $district = DB::table('zipcode')
            ->select('zipcode_Country')
            ->where('zipcode_City', $usersInfo[0]["userinfo_Area"])
            ->orderBy('zipcode_ID', 'asc')
            ->get();
        if ($district->count()===0) {
            $callback["district"] = [];
        } else {
            $callback["district"] = json_decode(json_encode($district), true);
        }
        
        $districtAll = DB::table('zipcode')
            ->select('zipcode_City', 'zipcode_Country')
            ->orderBy('zipcode_ID', 'asc')
            ->get();
        if ($district->count()===0) {
            $callback["district"] = [];
        } else {
            $callback["districtAll"] = json_decode(json_encode($districtAll), true);
        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    
    public function uploadImg(Request $request) {
            
            $callback = array();
                // check empty
    //        if ( !(checkEmpty($request,array("userinfo_id","userinfo_oTableColumn"))) )
    //        {
    //            return $this->parameterErrorResponse();
    //        }

            // User need permission: Administrator 
            $usersInfo = $request["token_info"];
    //        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
    //                $callback["success"]=false;
    //                $callback["msg"]="you are not Administrator.";
    //                return $this->jsonResponse($callback);
    //        }

            $FileName = $_FILES["file"]['name'];
            $FileSub = explode( "." , $FileName );
            $FileSub = $FileSub[count($FileSub)-1];

//            define('upload_profile', "C:\\xampp\\htdocs\\arod\\iuning\\resources\\data\\profile\\" );
//            define('http_upload_profile', "http://www.fooddy.co/arod/iuning/resources/data/profile/" );

            if( !file_exists(upload_profile . $usersInfo[0]['userinfo_ID'] . "\\") ){
                mkdir(upload_profile . $usersInfo[0]['userinfo_ID'] . "\\", 0777, true);
            }

            $filepath = upload_profile . $usersInfo[0]['userinfo_ID'] . '\\' . $request["imgName"] . '_tmp.' . $FileSub;

            move_uploaded_file( $_FILES["file"]["tmp_name"] , $filepath );
            if( !file_exists($filepath) ) {
                $callback['msg'] = "Upload fail filepath: $filepath";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            $callback['data'] = array( "file" => $request["imgName"] . '_tmp.' . $FileSub ,
                                       "ori_file" => $_FILES["file"]['name'] ,
                                       "path" => http_upload_profile . $usersInfo[0]['userinfo_ID'] . '/' . $request["imgName"] . '_tmp.' . $FileSub );
            $callback['success'] = true;

            return $this->jsonResponse($callback);
            
    }
    
    
    public function releaseImg(Request $request) {
            $callback = array();
            
            $usersInfo = $request["token_info"];
            $dir = upload_profile . $usersInfo[0]['userinfo_ID'] . "\\";
            
            if ( ! is_dir($dir) ){
                    $callback['msg'] = "is_dir fail: $dir";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
            }
            
            if ( ! $dh = opendir($dir)){
                $callback['msg'] = "opendir fail";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            while( $file = readdir($dh) ){
                if( strpos($file, "_tmp") !== false ){
                    $file_new = str_replace('_tmp', '', $file);//移除檔名之_tmp成為正式檔名
                    rename( $dir . $file , $dir . $file_new );
                    
                    
                    ////DB存取更新時間////
                    //table名稱為檔案名稱拿掉附檔名
                    $table = 'userinfo_' . explode('.', $file_new)[0] . 'Img';
                    
                    $check = DB::table($table)
                        ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
                        ->get();
                    if ($check->count()>0) {
                            //UPDATE
                            $DBCMD = DB::table($table)->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])->update( array($table => date('Y-m-d')) );
                    } else {
                            //INSERT
                            $DBCMD = DB::table($table)->insert(
                                [
                                    'userinfo_ID' => $usersInfo[0]['userinfo_ID'], 
                                    $table => date('Y-m-d')
                                ]
                            );
                    }
                }
            }

            closedir($dh);
            
            $callback['success'] = true;
            return $this->jsonResponse($callback);
    }
    
    
    public function removeTmpImg(Request $request) {
            $usersInfo = $request["token_info"];
            $dir = upload_profile . $usersInfo[0]['userinfo_ID'] . "\\";
            
            if ( ! is_dir($dir) ){
                    $callback['msg'] = "is_dir fail: $dir";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
            }
            
            if ( ! $dh = opendir($dir)){
                $callback['msg'] = "opendir fail";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            while( $file = readdir($dh) ){
                if( strpos($file, "_tmp") !== false ){
                    unlink( $dir . $file );
                }
            }

            closedir($dh);
            
            $callback['success'] = true;

            return $this->jsonResponse($callback);
            
    }
    
    public function getUserinfoImage(Request $request) {
        
        // check empty
        if ( !(checkEmpty($request,array("userinfo_oTableColumn"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        
        // User need permission: Administrator 
        $usersInfo = $request["token_info"];
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse($callback);
//        }
        
        $TableColumnName = $request['userinfo_oTableColumn'];
        
        $check = DB::table($TableColumnName)
                            ->select('userinfo_ID', $TableColumnName)
                            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
//                ->toSql();
//                echo $check;exit;
                            ->get();
        if ($check->count() <> 1) {
            $callback["success"]=false;
        }
        else{
            $img_name = explode('_', $TableColumnName)[1];
            $img_name = str_replace('Img', '', $img_name);
            // add another element(userinfo_oTableColumn) in an array
            $obj_to_array = array();
//            var_dump($check[0]->$TableColumnName);exit;
//            var_dump($check->item[0]->$TableColumnName);exit;
            $check = (array)$check[0];
            $obj_to_array['date'] = $check[$TableColumnName];
            $obj_to_array['imgUrl'] = 'http://www.fooddy.co/arod/iuning/resources/data/profile/' . $usersInfo[0]['userinfo_ID'] . '/' . $img_name . '.jpg';
            // wrap result
            $callback["data"] = json_decode(json_encode($obj_to_array), true);
            $callback["success"]=true;
        }
       
        return $this->jsonResponse($callback);
    }

    public function setCollection(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable", "global_value"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
  
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
     
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName = $checkKey[0]->Column_name;
        $global_oTableKeyName2 = $checkKey[1]->Column_name;
        
        $checkData = DB::select("SELECT * FROM $global_oTable "
                             . "WHERE $global_oTableKeyName = ".$request['global_value']["$global_oTableKeyName"]
                              . " AND $global_oTableKeyName2 = ".$request['global_value']["$global_oTableKeyName2"]);
        
        if (count($checkData)===0) {
                $DBCMD = DB::table($global_oTable)->insert([$request['global_value']]);
                        $callback["data"] = $global_oTable;

                if ($DBCMD) {
                        $callback["success"] = true;
                } else {
                        $callback["success"] = false;
                        $callback["msg"] = "Insert collection fail";
                }
        } else {
                $callback["success"] = true;
        }
        
        return $this->jsonResponse($callback);
    }    
                
    public function delCollection(Request $request) {
        
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTableKeyValue2", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // avoid from userinfo table 
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        // get primary key
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName  = $checkKey[0]->Column_name;
        $global_oTableKeyName2 = $checkKey[1]->Column_name;
        
        $DBCMD = DB::table($global_oTable)
            ->where([[$global_oTableKeyName, $request['global_oTableKeyValue']],
                    [$global_oTableKeyName2, $request['global_oTableKeyValue2']]])
            ->delete();
        if ($DBCMD) {
                $callback["success"] = true;
        } else {
                $callback["success"] = false;
                $callback["msg"] = "Delete collection fail";
        }

        return $this->jsonResponse($callback);   
    }    

    public function delHistory(Request $request) {
        
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // avoid from userinfo table 
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        // get primary key
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName  = $checkKey[0]->Column_name;
        
        $DBCMD = DB::table($global_oTable)
            ->where($global_oTableKeyName, $request['global_oTableKeyValue'])
            ->delete();
        if ($DBCMD) {
                $callback["success"] = true;
        } else {
                $callback["success"] = false;
                $callback["msg"] = "Delete history fail";
        }

        return $this->jsonResponse($callback);   
    }     
}

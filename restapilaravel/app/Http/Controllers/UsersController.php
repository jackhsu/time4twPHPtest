<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;
define('upload_profile', "C:\\xampp\\htdocs\\arod\\iuning\\resources\\data\\profile\\" );
define('http_upload_profile', "http://www.fooddy.co/arod/iuning/resources/data/profile/" );

class UsersController extends Controller
{
    //
    public function nothing() {
        //echo "aaa";
//        Schema;
    }
    
    // User Register
    /* @param String $userinfo_InExUser
     * @param String $userinfo_Password
     * @param String $userinfo_Email
     */
    public function register(Request $request) {
        
        // check empty
        if ( !(checkEmpty($request,array("userinfo_InExUser","userinfo_Password","userinfo_Email"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        
        if( !in_array($request["userinfo_InExUser"], array("Borrower","Investor","Company")) ){
                $callback['msg'] = "輸入資料錯誤";
                $callback['success'] = false;
                return $callback;
        }
        
        if( $request["userinfo_InExUser"]==="Borrower" ){
            $keyWord = "B";
        }
        else if( $request["userinfo_InExUser"]==="Investor" ){
            $keyWord = "L";
        }
        else if( $request["userinfo_InExUser"]==="Company" ){
            $keyWord = "V";
        }
        
        /* $check = DB::table('userinfo')
            ->where('userinfo_UserName', $request['userinfo_UserName'])
            ->get();
        if ($check->count()>0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"帳號重複註冊"));
        }*/
        
        // checkd duplicate Email 
        $check = DB::table('userinfo')
            ->where('userinfo_Email', $request['userinfo_Email'])
            ->get();
        if ($check->count()>0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"Email 重複註冊"));
        }
        
        while (1){
            $userinfo_ID = $keyWord.getEnglishRandom(1).getNumberRandom(8);
            $check = DB::table('userinfo')
                ->where('userinfo_ID', $userinfo_ID)
                ->get();
            if ($check->count()==0) {
                break;
            }
        }
        
        // get token for email authentication
        $token = getRandom(20);
        $filterPostData["userinfo_email_auth"]=$token;
        
        // create a new user
        $DBCMD = DB::table('userinfo')->insert(
            [   'userinfo_ID' => $userinfo_ID, 
                'userinfo_InExUser' => $request['userinfo_InExUser'], 
                'userinfo_UserName' => $request['userinfo_Email'], 
                'userinfo_Password' => $request['userinfo_Password'], 
                'userinfo_Email' => $request['userinfo_Email'], 
                'userinfo_Status' => 'active', 
                'userinfo_email_auth' => $token,
                'userinfo_CreateDateTime' => date('Y-m-d H:i:s'),
                'userinfo_UserPhoto' => '',
                'userinfo_UserIdentity' => 'F55688',
                'userinfo_District' => '',
                'userinfo_Address' => ''    ]
        );
        
        if ($DBCMD) {
            // success
            // send authentication to check email address
            $callback = $this -> send("authenticate", $token, "http://www.fooddy.co/iuning/public", '[IUNING] Email 地址驗證', $request["userinfo_Email"]);
        }
        else{
            // fail
            $callback["success"]=false;
            $callback["msg"]="Insert data fail";
        }
        
        return $this->jsonResponse($callback);
    }
    
    /* @param String $userinfo_id
     * @param String $userinfo_iduserinfo_oTableColumn
     * ex: ('3', 'userinfo.userinfo_work')
     */
    public function getUserinfoValue(Request $request) {
        
        // check empty
        if ( !(checkEmpty($request,array("userinfo_oTableColumn"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $usersInfo = $request["token_info"];
    
        // User need permission: Administrator 
        if(strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")===false)
        {
                $userinfo_id = $usersInfo[0]['userinfo_ID'];
        } 
        else 
        {       // if there's parameter userinfo_id, then user it
                if ( !(checkEmpty($request, "userinfo_id")) ) {
                     $userinfo_id = $usersInfo[0]['userinfo_ID'];
                } else {
                     $userinfo_id = $request['userinfo_id'];
                }
        }
        
        $userinfo_oTableColumn = explode(".", $request['userinfo_oTableColumn']);
        $columnName = $userinfo_oTableColumn[0] === "userinfo" ? 'userinfo_ID' : 'userinfo_ID';
        
        $check = DB::table($userinfo_oTableColumn[0])
                ->select('userinfo_ID', $userinfo_oTableColumn[1].' as userinfo_value')
                ->where($columnName, $userinfo_id)
                ->get();
        
        // when there is no data for $userinfo_ID in this table $userinfo_oTableColumn
        if ($check->count() <> 1) {
            $callback["msg"]="no result";
            $callback["success"]=false;
        }
        else{
            // add another element(userinfo_oTableColumn) in an array
            $obj_to_array = (array)$check[0];
            $obj_to_array['userinfo_oTableColumn'] = $request['userinfo_oTableColumn'];
            // wrap result
            $callback["data"] = json_decode(json_encode($obj_to_array), true);
            $callback["success"]=true;
        }
       
        return $this->jsonResponse($callback);
    }
    
    public function getUserinfoByID(Request $request) {
        
        // check empty
        if ( !(checkEmpty($request,array("userinfo_id"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // User need permission: Administrator 
        $usersInfo = $request["token_info"];
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse($callback);
//        }
        
        $check = DB::table("userinfo")
                ->where("userinfo_ID", $request['userinfo_id'])
                ->get();
        
        // when there is no data for $userinfo_ID in this table $userinfo_oTableColumn
        if ($check->count() <> 1) {
            $callback["msg"]="no result";
            $callback["success"]=false;
        }
        else{
            $callback["data"] = $check;
            $callback["success"]=true;
        }
       
        return $this->jsonResponse($callback);
    }
    
    /*
     * @param String $global_oTableKeyValue
     * @param String $global_oTable
     */
    public function getGlobalValue(Request $request) {
        
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // avoid from userinfo table 
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        // get primary key
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName = $checkKey[0]->Column_name;
        $data = DB::table($global_oTable)
            ->where($global_oTableKeyName, $request['global_oTableKeyValue'])
            ->get();
        
        if ($data->count() < 1) {
            $callback["msg"]="no result";
            $callback["success"]=false;
        }
        else{
            $callback["data"] = json_decode(json_encode($data), true);
            $callback["success"]=true;
        }
        
        return $this->jsonResponse($callback);
    }
    
    /*
     * @param String $global_oTableKeyValue
     * @param String $global_oTable
     * @param Array(String) $global_value ex: ['one' => 1, 'two' => 2, 'three' => 3, ...]
     */
    public function setGlobalValue(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("global_oTable", "global_value"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
  
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
     
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName = $checkKey[0]->Column_name;
        
        if ( !(checkEmpty($request,array("global_oTableKeyValue"))) )
        {
            $col = DB::select("SHOW COLUMNS FROM `$global_oTable` WHERE `NULL` = 'NO' AND `Key` != 'PRI'");
            $ins_col = "";
            $ins_data = "";
            for($i=0; $i<count($col); $i++) {
                    $col_name = $col[$i]->Field;
                    $ins_col = $ins_col . "`" . $col_name . "`,";
                    if( substr($col[$i]->Type, 0, 3) == "int" 
                        || substr($col[$i]->Type, 0, 7) == "tinyint" 
                        || substr($col[$i]->Type, 0, 5) == "float" 
                        || substr($col[$i]->Type, 0, 6) == "double") {
                            $ins_data =  array_key_exists($col_name, $request['global_value']) ? $ins_data . $request['global_value'][$col_name] . "," : $ins_data . "0,";
                    } else if( substr($col[$i]->Type, 0, 4) == "date" ) {
                            $ins_data =  array_key_exists($col_name, $request['global_value']) ? $ins_data . "'". $request['global_value'][$col_name] . "'," : $ins_data . "'". date('Y-m-d H:i:s') . "',";
                    } else {
                            $ins_data =  array_key_exists($col_name, $request['global_value']) ? $ins_data . "'". $request['global_value'][$col_name] . "'," : $ins_data . "'',";
                    }
            }
            $ins_col = substr($ins_col, 0, -1);
            $ins_data = substr($ins_data, 0, -1);
            $DBCMD = DB::select("INSERT INTO `$global_oTable`($ins_col) SELECT $ins_data");
            
            $id = DB::select("SELECT MAX($global_oTableKeyName) as `id` FROM ".$global_oTable)[0]->id;
            if(count($DBCMD)===0) {
                $callback["data"]=$id;
                $callback["success"]=true;
            } else {
                $callback["msg"]="$global_oTable 新增失敗";
                $callback["success"]=false;
            }
            return $this->jsonResponse($callback);            
        }
        
        // cannot update primary key value 
        if(array_key_exists($global_oTableKeyName , $request['global_value']))
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot update id column"));
        }
        
        $updateStatus = DB::table($global_oTable)
            ->where($global_oTableKeyName, $request['global_oTableKeyValue'])
            ->update($request['global_value']);
        
        if ($updateStatus) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Credentials:true');
        header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
        header('Content-Type:text/html; charset=utf-8');
        
        return $this->jsonResponse($callback);
    }
    
    /*
     * @param String $global_oTable
     */
    public function listGlobalValue(Request $request) {
            
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        
        $check = DB::table($global_oTable)
                ->get();
            //->orderBy('uco_order', 'asc')
            
        if ($check->count()===0) {
            // when there is no data in this table $global_oTable
            $callback["msg"]="no result";
            $callback["success"]=false;
        }
        else{
            // wrap result
            $callback["data"] = json_decode(json_encode($check), true);
            $callback["success"]=true;
        }
        
        
        return $this->jsonResponse($callback);
    }
    
    public function getPermissions(Request $request) {
        
        $permissions = array();
//        $final = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0000","activity"=>"0000","unit"=>"0000");
//        
//        $usersInfo = $request["token_info"];
//        $userinfo_InExUser = json_decode($usersInfo[0]["userinfo_InExUser"],true);
//        foreach ($userinfo_InExUser as $key => $value) {
//            switch ($value) {
//                case "Administrator":
//                    $permissions[] = array("user"=>"1110","employee"=>"1110","customer"=>"1110","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
//                break;
//                case "Business":
//                    $permissions[] = array("user"=>"0000","employee"=>"0010","customer"=>"0010","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
//                break;
//                case "Operator":
//                    $permissions[] = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0010","activity"=>"1110","unit"=>"0000");
//                break;
//                case "Accounting":
//                    $permissions[] = array("user"=>"0010","employee"=>"1110","customer"=>"1110","jobsite"=>"0000","workorder"=>"1110","activity"=>"1110","unit"=>"1110");
//                break;
//                case "Sales":
//                    $permissions[] = array("user"=>"0000","employee"=>"0010","customer"=>"1110","jobsite"=>"1110","workorder"=>"1110","activity"=>"1110","unit"=>"0000");
//                break;
//                default:
//                    $permissions[] = array("user"=>"0000","employee"=>"0000","customer"=>"0000","jobsite"=>"0000","workorder"=>"0000","activity"=>"0000","unit"=>"0000");
//                break;
//            }
//        }
//        
//        $callback = array();
//        if(count($permissions)===0){
//            $callback['data'] = $final;
//        }
//        else if(count($permissions)===1){
//            $callback['data'] = $permissions[0];
//        }
//        else{
//            foreach ($permissions as $key => $value) {
//                foreach ($value as $key2 => $value2) {
//                    $final_value = $final[$key2];
//                    $first_word = (int)substr($final_value, 0, 1) || (int)substr($value2, 0, 1)?1:0;
//                    $second_word = (int)substr($final_value, 1, 1) || (int)substr($value2, 1, 1)?1:0;
//                    $third_word = (int)substr($final_value, 2, 1) || (int)substr($value2, 2, 1)?1:0;
//                    $fourth_word = (int)substr($final_value, 3, 1) || (int)substr($value2, 3, 1)?1:0;
//                    $final[$key2]=$first_word.$second_word.$third_word.$fourth_word;
//                }
//            }
//            $callback['data'] = $final;
//        }
        
        $callback['success'] = true;
        return $this->jsonResponse($callback);
        
    }
    
    public function userLogout(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        $DBCMD = DB::table('authentication')->where('authentication_token', '=', $usersInfo[0]["authentication_token"])->delete();
//        if ($DBCMD) {
                $callback["success"]=true;
//        } else {
//                $callback["success"]=false;
//                $callback["msg"]="Clear cache fail";
//        }
        return $this->jsonResponse( $callback);
        
    }
    
    public function checkToken(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
        $userinfoData = $usersInfo[0];
        
        $check = DB::table('userinfo_point')
            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
            ->get();
        if ($check->count()>0) {
            $check = json_decode(json_encode($check), true);
            $userinfoData["userinfo_point"] = $check[0]["userinfo_point"];
        }
        else{
            $userinfoData["userinfo_point"] = 0;
        }
        
        $check = DB::table('userinfo_evaluation')
            ->select(DB::raw('AVG(userinfo_evaluation) as average_evaluation'))
            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
            ->groupBy('userinfo_ID')
            ->get();
        if ($check->count()>0) {
            $check = json_decode(json_encode($check), true);
            $userinfoData["average_evaluation"] = $check[0]["average_evaluation"];
        }
        else{
            $userinfoData["average_evaluation"] = 0;
        }
        
        
        $callback["data"]=$userinfoData;
        $callback["success"]=true;

        return $this->jsonResponse( $callback);
        
    }
    
    public function editMyName(Request $request) {
        
        $callback = array();
        
        if ( !checkEmpty($request,array("userinfo_UserName")) )
        {
            return $this->parameterErrorResponse();
        }
        
        $usersInfo = $request["token_info"];
        //未修改
        if($usersInfo[0]['userinfo_UserName']===$request['userinfo_UserName']){
            $callback["success"]=true;
            return $this->jsonResponse( $callback);
        }
        
        
        $check = DB::table('userinfo')
            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
            ->get();
        if ($check->count()===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_UserName', $request['userinfo_UserName'])
            ->get();
        if ($check->count()>0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"UserName已存在"));
        }
        
        $updateStatus = $this->updateMysql( 'userinfo'
                , 'userinfo_ID' , $usersInfo[0]['userinfo_ID']
                , ['userinfo_UserName' => $request['userinfo_UserName']
                    , 'userinfo_UpdateDateTime' => date('Y-m-d H:i:s')
                    , 'userinfo_UpdateByID' => $usersInfo[0]['userinfo_ID'] ] );
//        ['userinfo_LastLoginTime' => $datatime, 'userinfo_LastLoginIp' => $_SERVER['REMOTE_ADDR']]
        
        if ($updateStatus) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse( $callback);
        
    }
    
    public function editMyEmail(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("userinfo_Email","userinfo_Password"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $usersInfo = $request["token_info"];
        //密碼驗證
        if($usersInfo[0]['userinfo_Password']!==$request['userinfo_Password']){
            $callback["success"]=false;
            $callback["msg"]="password is error";
            return $this->jsonResponse( $callback);
        }
        //未修改
        if($usersInfo[0]['userinfo_Email']===$request['userinfo_Email']){
            $callback["success"]=true;
            return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
            ->get();
        if ($check->count()===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_Email', $request['userinfo_Email'])
            ->get();
        if ($check->count()>0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"UserEmail已存在"));
        }
        
        
        $token = getRandom(20);
        
        $updateStatus = $this->updateMysql( 'userinfo'
                , 'userinfo_ID' , $usersInfo[0]['userinfo_ID']
                , ['userinfo_Email' => $request['userinfo_Email']
                    , 'userinfo_UpdateDateTime' => date('Y-m-d H:i:s')
                    , 'userinfo_UpdateByID' => $usersInfo[0]['userinfo_ID']
                    , 'userinfo_email_auth' => $token ] );
//        ['userinfo_LastLoginTime' => $datatime, 'userinfo_LastLoginIp' => $_SERVER['REMOTE_ADDR']]
        
        if ($updateStatus) {
                $callback["success"]=true;
		// *** global function ***
                $callback = $this -> send("authenticate",$token,"http://www.fooddy.co/iuning/public",'[IUNING] Email 地址驗證',$request["userinfo_Email"]);
                
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse( $callback);
        
        
    }
    
    public function editMyPassword(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("userinfo_old_Password","userinfo_new_Password"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $usersInfo = $request["token_info"];
        //密碼驗證
        if($usersInfo[0]['userinfo_Password']!==$request['userinfo_old_Password']){
            $callback["success"]=false;
            $callback["msg"]="old password is error";
            return $this->jsonResponse( $callback);
        }
        //未修改
        if($request['userinfo_old_Password']===$request['userinfo_new_Password']){
            $callback["success"]=true;
            return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
            ->get();
        if ($check->count()===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"userinfo_ID不存在"));
        }
          
        $updateStatus = $this->updateMysql( 'userinfo'
                , 'userinfo_ID' , $usersInfo[0]['userinfo_ID']
                , ['userinfo_Password' => $request['userinfo_new_Password']
                    , 'userinfo_UpdateDateTime' => date('Y-m-d H:i:s')
                    , 'userinfo_UpdateByID' => $usersInfo[0]['userinfo_ID'] ] );
        if ($updateStatus) {
                $callback["success"]=true;
                
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        
        return $this->jsonResponse( $callback);
    }
        
    
    public function reSendEmailForAuth(Request $request) {
        $callback = array();
        $usersInfo = $request["token_info"];
        $token = getRandom(20);
        
        $updateStatus = $this->updateMysql( 'userinfo'
                , 'userinfo_ID', $usersInfo[0]['userinfo_ID']
                , ['userinfo_email_auth' => $token ] );
        
        if ($updateStatus) {
            
                $callback = $this -> send("authenticate",$token,"http://www.fooddy.co/iuning/public",'[IUNING] Email 地址驗證',$usersInfo[0]["userinfo_Email"]);
                if($callback["success"]){
                        $callback["success"]=true;
                        $callback["msg"]="Send EMAIL to ".$usersInfo[0]["userinfo_Email"];
                }
                else {
                        $callback["success"]=false;
                        $callback["msg"]="Send EMAIL fail";
                }
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        return $this->jsonResponse( $callback);
    }
    
    public function usersList(Request $request) {
        $callback = array();
        $usersInfo = $request["token_info"];
        
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo')->get();
        if ($check->count()===0) {
            $callback["data"] = [];
        }
        else{
            $callback["data"] = json_decode(json_encode($check), true);
        }
        $callback["column"] = DB::getSchemaBuilder()->getColumnListing('userinfo');
        
        $callback["success"]=true;
        return $this->jsonResponse( $callback);
    }
    
    public function getUsersTableColumn(Request $request) {
        $callback = array();
        $usersInfo = $request["token_info"];
        
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo')->get();
        if ($check->count()===0) {
            $callback["data"] = [];
        }
        else{
            $callback["data"] = json_decode(json_encode($check), true);
        }
        $callback["column"] = DB::getSchemaBuilder()->getColumnListing('userinfo');
        
        $callback["success"]=true;
        return $this->jsonResponse( $callback);
    }
    
    public function addMember(Request $request) {
        
        if ( !(checkEmpty($request,array("userinfo_UserPlatform","userinfo_UserName","userinfo_UserPhone","userinfo_UserAge"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
//        $check = DB::table('userinfo')
//            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
//            ->get();
//        if ($check->count()===0) {
//            $callback["data"] = [];
//        }
//        else{
//            $callback["data"] = json_decode(json_encode($check), true);
//        }
//        $callback["column"] = DB::getSchemaBuilder()->getColumnListing('userinfo');
        $DBCMD = DB::table('userinfo')->insert(
            [   'userinfo_InExUser' => '["Accounting"]', 
                'userinfo_UserName' => $request['userinfo_UserName'], 
                'userinfo_UserPhone' => $request['userinfo_UserPhone'], 
                'userinfo_UserAge' => $request['userinfo_UserAge'], 
                'userinfo_UserPlatform' => $request['userinfo_UserPlatform'], 
//                'userinfo_Password' => $request['userinfo_Password'], 
//                'userinfo_Email' => $request['userinfo_Email'], 
                'userinfo_Status' => 'active', 
//                'userinfo_email_auth' => $token,
                'userinfo_CreateDateTime' => date('Y-m-d H:i:s') ]
        );
        
        if ($DBCMD) {
            $callback["success"] = true;
            $callback["data"] = $userinfo_array ;
            
            $tables = DB::select("SHOW TABLES like 'userinfo_%'");
            foreach($tables as $key => $table)
            {
                $table = json_decode(json_encode($table),true);
                $tableName = $table["Tables_in_iun (userinfo_%)"];
                if($tableName!=="userinfo_column_order"){
                    
                        $DBCMD = DB::table($tableName)->insert(
                            [   'userinfo_ID' => $userinfo_ID ,
                                $tableName => "" ]
                        );
                        // ++ jack
                        if( $tableName === 'userinfo_lineid' )
                        {
                                $updateStatus = $this->updateMysql( $tableName
                                        , 'userinfo_ID' , $userinfo_ID
                                        , [ 'userinfo_lineid' => $request['userinfo_lineid'] ] );
                        }
                        if( $tableName === 'userinfo_online' )
                        {
                                $updateStatus = $this->updateMysql( $tableName
                                        , 'userinfo_ID' , $userinfo_ID
                                        , [ 'userinfo_online' => $request['userinfo_online'] ] );
                        }
                        // --
                            
                }
            }
            
        }
        else{
            $callback["success"]=false;
            $callback["msg"]="新增 data fail";
        }
        
        return $this->jsonResponse( $callback);
    }
    
    public function usersColumnListByOrder(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo_column_order')
            ->orderBy('uco_order', 'asc')
            ->get();
        if ($check->count()===0) {
            $callback["data"] = [];
        }
        else{
            $callback["data"] = json_decode(json_encode($check), true);
        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    // Vincent
    public function usersColumnList(Request $request) {
        
        $callback = array();
//        $usersInfo = $request["token_info"];
//        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        $check = DB::table('userinfo_column_order')
            ->orderBy('uco_order', 'asc')
            ->get();
        if ($check->count()===0) {
            $callback["data"] = [];
        }
        else{
            $callback["data"] = json_decode(json_encode($check), true);
        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    public function updateUsersColumnOrder(Request $request) {
        
        if ( !(checkEmpty($request,array("orderArr"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
                $callback["success"]=false;
                $callback["msg"]="you are not Administrator.";
                return $this->jsonResponse( $callback);
        }
        
        foreach ($request["orderArr"] as $key => $value) {
            
            $uco_order = ((int)$key+7);
            switch ($value) {
                case 'userinfo_CreateDateTime':
                    $uco_order = 0;
                    break;
                case 'userinfo_Business':
                    $uco_order = 1;
                    break;
                case 'userinfo_UserPlatform':
                    $uco_order = 2;
                    break;
                case 'userinfo_UserName':
                    $uco_order = 3;
                    break;
                case 'userinfo_UserPhone':
                    $uco_order = 4;
                    break;
                case 'userinfo_UserAge':
                    $uco_order = 5;
                    break;
                case 'userinfo_Schedule':
                    $uco_order = 6;
                    break;
                default:
                    break;
            }
            $DBCMD = DB::table('userinfo_column_order')->where('uco_id', $value)->update(['uco_order' => $uco_order ]);
            
            //沒有變更過的會出現錯誤
//            if ($DBCMD) {
//                
//            }
//            else {
////                    $callback["success"]=false;
////                    $callback["msg"]="update fail uco_id=".$value.",uco_order=".(((int)$key));
////                    return $this->jsonResponse( $callback);
//            }
        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    public function editUsersColumnInfo(Request $request) {
        
        if ( !(checkEmpty($request,array("uco_name","uco_id"))) )
        {
            return $this->parameterErrorResponse();
        }
        if ( !(checkIsset($request,array("uco_type","uco_level","uco_display"))) )
        {
            return $this->parameterErrorResponse();
        }
        $uco_select_value = isset($request["uco_select_value"]) ? json_encode($request["uco_select_value"]) : json_encode(array());
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        $updateStatus = $this->updateMysql( 'userinfo_column_order' , 'uco_id' , $request["uco_id"] , ['uco_name' => $request["uco_name"] , 'uco_type' => $request["uco_type"] , 'uco_level' => $request["uco_level"] , 'uco_display' => $request["uco_display"] , 'uco_select_value' => $uco_select_value ] );

        if ($updateStatus) {
                $callback["success"]=true;
        }
        else {
                $callback["success"]=false;
                $callback["msg"]="update fail";
        }
        return $this->jsonResponse( $callback);
    }
    
    public function getUsersListAndColumn(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }

        $DBCMD = DB::table('userinfo_column_order')
                ->where([['uco_display', 0]])//,['uco_level', 1]
                ->orderBy('uco_order', 'asc')
                ->get();
        
        if ($DBCMD->count()===0) {
                $callback["column"] = [];
                $callback["success"]=true;
        }
        else {
                $callback["success"]=true;
                $callback["column"] = json_decode(json_encode($DBCMD), true);
        }
        
        
        $leftJoinString = "";
        $tables = DB::select("SHOW TABLES like 'userinfo_%'");
//        $tables = json_encode(json_decode($tables));
        
        foreach($tables as $key => $table)
        {
            $table = json_decode(json_encode($table),true);
            $tableName = $table["Tables_in_iun (userinfo_%)"];
            if($tableName!=="userinfo_column_order"){
                $leftJoinString .= "->leftJoin('$tableName', '$tableName'.'.userinfo_ID', '=', 'userinfo.userinfo_ID')";
            }
        }
        
        $funcString = "\$DBCMD = DB::table('userinfo')";
        $funcString .= $leftJoinString;
        $funcString .= "-> get();";
        $funcString .= "return \$DBCMD;";
//        echo $funcString;
        $newfunc = create_function('', $funcString);
        
        $DBCMD = $newfunc();
        
        if ($DBCMD->count()===0) {
                $callback["data"] = [];
                $callback["success"]=true;
        }
        else {
//            $select
//            foreach (json_decode(json_encode($DBCMD), true) as $key => $value) {
//                
//            }
                $callback["success"]=true;
                $callback["data"] = json_decode(json_encode($DBCMD), true);
        }
        
        
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    public function searchUserData(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        
        if ( !(checkEmpty($request,array("StartDate","EndDate"))) )
        {
            return $this->parameterErrorResponse();
        }
        if ( !(checkIsset($request,array("StartBirthday","EndBirthday"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $advanceColumn = isset($request["advanceColumn"]) ? $request["advanceColumn"] : array();
        $userinfo_column_order = isset($request["userinfo_column_order"]) ? $request["userinfo_column_order"] : "userinfo_column_order";
        $keyword = isset($request["keyword"]) ? $request["keyword"] : "";

        
        $DBCMD = DB::table($userinfo_column_order)
                ->where([['uco_display', 0]])//,['uco_level', 1]
                ->orderBy('uco_order', 'asc')
                ->get();
        
        if ($DBCMD->count()===0) {
                $callback["column"] = [];
                $callback["success"]=true;
        }
        else {
                $callback["success"]=true;
                $callback["column"] = json_decode(json_encode($DBCMD), true);
        }
        
        $DBCMD = DB::table("userinfo")
                ->select('userinfo_UserPhone')
                ->groupBy('userinfo_UserPhone')
                ->get();
        if ($DBCMD->count()===0) {
                $callback["userinfo_UserPhone"] = [];
                $callback["success"]=true;
        }
        else {
                $callback["success"]=true;
                $callback["userinfo_UserPhone"] = json_decode(json_encode($DBCMD), true);
        }
        
        $DBCMD = DB::table("userinfo")
                ->select('userinfo_UserName')
                ->groupBy('userinfo_UserName')
                ->get();
        if ($DBCMD->count()===0) {
                $callback["userinfo_UserName"] = [];
                $callback["success"]=true;
        }
        else {
                $callback["success"]=true;
                $callback["userinfo_UserName"] = json_decode(json_encode($DBCMD), true);
        }
        
//        $leftJoinString = "";
//        $tables = DB::select("SHOW TABLES like 'userinfo_%'");
////        $tables = json_encode(json_decode($tables));
//        
//        foreach($tables as $key => $table)
//        {
//            $table = json_decode(json_encode($table),true);
//            $tableName = $table["Tables_in_iun (userinfo_%)"];
//            if($tableName!=="userinfo_column_order"){
//                $leftJoinString .= "->leftJoin('$tableName', '$tableName'.'.userinfo_ID', '=', 'userinfo.userinfo_ID')";
//            }
//        }
//        
//        $funcString = "\$DBCMD = DB::table('userinfo')";
//        $funcString .= $leftJoinString;
//        $funcString .= "->whereBetween('userinfo.userinfo_CreateDateTime', array('".$request["StartDate"]."', '".$request["EndDate"]."'))";
//        
//        if( $request["StartBirthday"] !== null && $request["EndBirthday"] !== null )
//        {
//            $funcString .= "->whereBetween('userinfo.userinfo_UserAge', array('".$request["StartBirthday"]."', '".$request["EndBirthday"]."'))";
//        }
//        
//        foreach ($advanceColumn as $key => $value) {
//            $arrayString = "";
//            foreach ($value["keywork"] as $key2 => $value2) {
//                $arrayString .= "'".$value2."',";
//            }
//            $arrayString = substr($arrayString, 0, strlen($arrayString)-1);
//            $funcString .= "->whereIn('".$value["column"]."', array(".$arrayString."))";
//        }
//        $funcString .= "->get();";
//        $funcString .= "return \$DBCMD;";
////        echo $funcString;
//        $newfunc = create_function('', $funcString);
//        
//        $DBCMD = $newfunc();
//        
//        if ($DBCMD->count()===0) {
//                $callback["data"] = [];
//                $callback["success"]=true;
//        }
//        else {
//                $callback["data"] = json_decode(json_encode($DBCMD), true);
//                $callback["success"]=true;
//        }
        
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    public function searchUserDataDatatableFormat(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        
        if ( !(checkEmpty($request,array("StartDate","EndDate","length","order","search"))) )
        {
            return $this->parameterErrorResponse();
        }
        if ( !(checkIsset($request,array("StartBirthday","EndBirthday","start"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $keyword = isset($request["keyword"]) ? $request["keyword"] : "";
        $keywordID = isset($request["keywordID"]) ? $request["keywordID"] : array();
        
        $length = $request['length'];
        $order = $request['order'];
        $search = $request['search'];
        $start = $request['start'];
        $columns = $request['columns'];
        
        $columns[(int)$order[0]["column"]]["data"];
        $orderColumn = $columns[(int)$order[0]["column"]]["data"];

        $advanceColumn = isset($request["advanceColumn"]) ? $request["advanceColumn"] : array();
        
        $leftJoinString = "";
        $tables = DB::select("SHOW TABLES like 'userinfo_%'");
//        $tables = json_encode(json_decode($tables));
        $keywordColumnString = "";
        
        foreach($tables as $key => $table)
        {
            $table = json_decode(json_encode($table),true);
            $tableName = $table["Tables_in_iun (userinfo_%)"];
            if($tableName!=="userinfo_column_order"){
                $leftJoinString .= "->leftJoin('$tableName', '$tableName'.'.userinfo_ID', '=', 'userinfo.userinfo_ID')";
                if($orderColumn===$tableName){
                    $orderColumn = $orderColumn.".".$orderColumn;
                }
                $keywordColumnString .= ",`".$tableName."`.`".$tableName."`";
            }
        }
        
        if( strpos($orderColumn,".")===-1 ){
            $orderColumn = "userinfo.".$orderColumn;
        }

//        "DB::setFetchMode(\PDO::FETCH_ASSOC);"
        $funcString = "\$DBCMD = DB::table('userinfo')";
        $funcString .= "->select([DB::raw('SQL_CALC_FOUND_ROWS *')])";
        $funcString .= $leftJoinString;
        $columnssss = DB::connection()->getSchemaBuilder()->getColumnListing("userinfo");
        foreach ($columnssss as $key => $value) {
            $columnssss[$key] = "`userinfo`.`".$value."`";
        }
        $columnssss = implode(",", $columnssss);
        
        if($keyword!=="")
            $funcString .= "->where( DB::raw('CONCAT_WS(\" \", $columnssss $keywordColumnString)') ,'like','%$keyword%')";
        if(!empty($keywordID))
            $funcString .= "->whereIn('userinfo.userinfo_ID', array(".$keywordID."))";
        
        $funcString .= "->whereBetween('userinfo.userinfo_CreateDateTime', array('".$request["StartDate"]."', '".$request["EndDate"]."'))";
        
        if( $request["StartBirthday"] !== null && $request["EndBirthday"] !== null )
        {
            $funcString .= "->whereBetween('userinfo.userinfo_UserAge', array('".$request["StartBirthday"]."', '".$request["EndBirthday"]."'))";
        }
        
        foreach ($advanceColumn as $key => $value) {
            $arrayString = "";
            foreach ($value["keywork"] as $key2 => $value2) {
                $arrayString .= "'".$value2."',";
            }
            $arrayString = substr($arrayString, 0, strlen($arrayString)-1);
            $funcString .= "->whereIn('".$value["column"]."', array(".$arrayString."))";
        }
        
        $funcString .= "->orderBy('$orderColumn', '".$order[0]["dir"]."')";
        $funcString .= "->offset((int)'$start')->limit((int)'$length')";
        $funcString .= "->get();";
        $funcString .= "return \$DBCMD;";
//        echo $funcString;
        $newfunc = create_function('', $funcString);
        
        $DBCMD = $newfunc();
        $count = DB::select("SELECT FOUND_ROWS() as `row_count`")[0]->row_count;
        
        if ($DBCMD->count()===0) {
                $callback["data"] = [];
                $callback["success"]=true;
                $callback["recordsTotal"] = 0;
                $callback["recordsFiltered"] = 0;
        }
        else {
                $callback["success"]=true;
                $callback["data"] = json_decode(json_encode($DBCMD), true);
                $callback["recordsTotal"] = $count;
                $callback["recordsFiltered"] = $count;
        }
        $callback["funcString"] = $funcString;
        
        
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    public function setUserinfoValue(Request $request) {
        
        if ( !(checkEmpty($request,array("userinfo_oTableColumn","userinfo_value"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//            
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        // User need permission: Administrator 
        if(strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")===false)
        {
                $userinfo_id = $usersInfo[0]['userinfo_ID'];
        } 
        else  {
                if ( !(checkEmpty($request, "userinfo_id")) ) {
                     $userinfo_id = $usersInfo[0]['userinfo_ID'];
                } else {
                     $userinfo_id = $request['userinfo_id'];
                }
        }
        
        $userinfo_oTableColumn = explode(".", $request['userinfo_oTableColumn']);
        
        $columnName = $userinfo_oTableColumn[0] === "userinfo" ? 'userinfo_ID' : 'userinfo_ID';
        
        $updateStatus = $this->updateMysql( $userinfo_oTableColumn[0] , $columnName , $userinfo_id , [$userinfo_oTableColumn[1] => $request['userinfo_value'] ] );
        
        if ($updateStatus) {
                $callback["success"]=true;
        } else {
                //$callback["success"]=false;
                //$callback["msg"]="save fail";
                $DBCMD = DB::table($userinfo_oTableColumn[0])->insert(
                    [   $columnName => $userinfo_id, 
                        $userinfo_oTableColumn[1] => $request['userinfo_value'] ]
                );

                if ($DBCMD) {
                    $callback["success"]=true;
                }
                else{
                    $callback["success"]=false;
                    $callback["msg"]="Insert data fail";
                }
                
        }
        
        
        
        
        
        return $this->jsonResponse( $callback);
    }
    
    public function getBusinessList(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_InExUser','like','%Business%')
            ->get();
        if ($check->count()===0) {
            $callback["data"] = [];
        }
        else{
            $callback["data"] = json_decode(json_encode($check), true);
        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
        
    }
    
    public function editUsersBusinessName(Request $request) {
        
        
        if ( !(checkEmpty($request,array("userinfo_UserName","userinfo_ID"))) )
        {
            return $this->parameterErrorResponse();
        }
        $callback = array();
        $callback["success"]=true;
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        $updateStatus = $this->updateMysql( 'userinfo' , 'userinfo_ID' , $request["userinfo_ID"] , ['userinfo_UserName' => $request["userinfo_UserName"] ] );

        if ($updateStatus) {
                $callback["success"]=true;
        }
        else {
                $callback["success"]=false;
                $callback["msg"]="update fail";
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_InExUser','like','%Business%')
            ->get();
        if ($check->count()!==0) {
            $busnissList = json_decode(json_encode($check), true);
            $busnissListArr = array();
            foreach ($busnissList as $key => $value) {
                $busnissListArr[]=$value["userinfo_UserName"];
            }
            $busnissListArr = json_encode($busnissListArr);
            
            $updateStatus = $this->updateMysql( 'userinfo_column_order' , 'uco_id' , 'userinfo_Business' , ['uco_select_value' => $busnissListArr ] );
            
            if ($updateStatus) {
                    $callback["success"]=true;
            } else {
                    $callback["success"]=false;
                    $callback["msg"]="update fail";
            }
        }
        
        return $this->jsonResponse( $callback);
        
    }
    
    public function addBusiness(Request $request) {
        
        $callback = array();
        $callback["success"]=true;
        $usersInfo = $request["token_info"];
        
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
        $userinfo_UserName = "業務";
        $i = 1;
        while (1) {
            $check = DB::table('userinfo')
            ->where('userinfo_UserName', $userinfo_UserName.$i)
            ->get();
            if ($check->count()===0) {
                break;
            }
            $i++;
        }
        
        
        $DBCMD = DB::table('userinfo')->insert(
            [   'userinfo_InExUser' => '["Business"]', 
                'userinfo_UserName' => $userinfo_UserName.$i, 
                'userinfo_Password' => "ZTEwYWRjMzk0OWJhNTlhYmJlNTZlMDU3ZjIwZjg4M2U=", 
                'userinfo_Status' => 'active', 
                'userinfo_CreateDateTime' => date('Y-m-d H:i:s') ]
        );
        
        if (!$DBCMD) {
            $callback["success"]=false;
            $callback["msg"]="Insert data fail";
            return $this->jsonResponse( $callback);
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_InExUser','like','%Business%')
            ->get();
        if ($check->count()!==0) {
            $busnissList = json_decode(json_encode($check), true);
            $busnissListArr = array();
            foreach ($busnissList as $key => $value) {
                $busnissListArr[]=$value["userinfo_UserName"];
            }
            $busnissListArr = json_encode($busnissListArr);
            
            $updateStatus = $this->updateMysql( 'userinfo_column_order' , 'uco_id' , 'userinfo_Business' , ['uco_select_value' => $busnissListArr ] );
            if ($updateStatus) {
                    $callback["success"]=true;
            } else {
                    $callback["success"]=false;
                    $callback["msg"]="update fail";
            }
        }
        
        return $this->jsonResponse( $callback);
        
    }
    
    public function forgotPassword(Request $request) {
        $callback = array();
        
        if ( !(checkEmpty($request,array("userinfo_Email"))) )
        {
            return $this->parameterErrorResponse();
        }
        // check email format
        if (!filter_var($request["userinfo_Email"], FILTER_VALIDATE_EMAIL)) {
            $callback['msg'] = "email is not valid";
            $callback['success'] = false;
            return $this->jsonResponse( $callback);
        }
        
        // generate token
        $rand = getRandom(20);
        // send email to user
        $callback = $this -> send("forgot",$rand,"http://www.fooddy.co/iuning/public",'[IUNING] 忘記密碼通知信',$request["userinfo_Email"]);
        
        // update user forgot token
        if($callback["success"]){
            
            $updateStatus = $this->updateMysql( 'userinfo' , 'userinfo_Email' , $request['userinfo_Email'] , ['userinfo_forgot_token' => $rand ] );
            if ($updateStatus) {
                    $callback["success"]=true;
            } else {
                    $callback["success"]=false;
                    $callback["msg"]="SQL fail";
            }
            
        }
        
        return $this->jsonResponse( $callback);
    }
    
    // check whether forgot token is still valid
    public function checkForgotToken(Request $request) {
        $callback = array();
        
        if ( !(checkEmpty($request,array("token"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_forgot_token', $request['token'])
            ->get();
        if ($check->count()===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"此認證碼已經失效或不存在"));
        }
        
        $callback["success"]=true;
        return $this->jsonResponse( $callback);
    }
    
    // after check token, we can update the password for the user
    public function editPasswordByForgotToken(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("token","userinfo_Password"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $check = DB::table('userinfo')
            ->where('userinfo_forgot_token', $request['token'])
            ->get();
        if ($check->count()===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"此認證碼已經失效或不存在"));
        }
        
        $check = json_decode(json_encode($check), true);
        
        $updateStatus = $this->updateMysql( 'userinfo' , 'userinfo_ID' , $check[0]['userinfo_ID'] , ['userinfo_forgot_token' => '' , 'userinfo_Password' => $request['userinfo_Password'] ] );
        if ($updateStatus) {
                $callback["success"]=true;
        } else {
                $callback["success"]=false;
                $callback["msg"]="Update data fail";
        }
        return $this->jsonResponse( $callback);
        
    }
    
    
    
    
    
    public function send($mail_type,$rand,$http_default_path,$title,$a_email,$extra_html=""){

            $callback = array();

//            if( !check_empty( array( "mail_type","rand","http_default_path","title","a_email" ) ) ){
//                    $callback['msg'] = "輸入資料不完整";
//                    $callback['success'] = false;
//                    return $callback;
//            }

//            $mail_type = $_REQUEST["mail_type"];
//            $rand = $_REQUEST["rand"];
//            $http_default_path = $_REQUEST["http_default_path"];
//            $title = $_REQUEST["title"];
//            $a_email = $_REQUEST["a_email"];

            if( !in_array($mail_type, array("forgot","authenticate")) ){
                    $callback['msg'] = "輸入資料錯誤";
                    $callback['success'] = false;
                    return $callback;
            }

            if( $mail_type === "forgot" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                     </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                    </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">重要！</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                    ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10.5pt;font-family:新細明體,serif">如果您沒有提交密碼重置的請求或不是</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif;color:rgb(55,96,146)">IUNING</span><span style="font-size:10.5pt;font-family:新細明體,serif">的註冊用戶，<wbr>請立即忽略</span><span style="font-size:10.5pt;font-family:Arial,sans-serif"> </span><span style="font-size:10.5pt;font-family:新細明體,serif">並刪除這封郵件。只有在您確認需要重置密碼的情況下，<wbr>才需要繼續閱讀下面的內容。</span><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif">------------------------------<wbr>------------------------------<wbr>----------<br>
                    </span><b><span style="font-size:10.5pt;font-family:新細明體,serif">密碼重置說明</span></b><span lang="EN-US" style="font-size:10.5pt;font-family:Arial,sans-serif"><br>
                    ------------------------------<wbr>------------------------------<wbr>----------</span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的修改密碼網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <a target="_blank" href="http://www.fooddy.co/R0/vincent/iuning/public/forgot_password?token='.$rand.'&mail='.$a_email.'">http://www.fooddy.co/iuning/public/forgot_password?token='.$rand.'&mail='.$a_email.'</a>
                    <br>
                    </span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">感謝您的訪問，祝您使用愉快！</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                     </span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                     </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
            }
            else if( $mail_type === "authenticate" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您收到這封郵件，是由於在</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">進行了新用戶註冊，或用戶修改</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">使用</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">了這個郵箱地址。如果您並沒有訪問過</span><span style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">，或沒有進行上述操作，請忽略這封郵件。<wbr>您不需要退訂或進行其他進一步的操作。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                       <br> </span><b><span style="font-size:10pt;font-family:新細明體,serif;color:black;background-image:initial;background-repeat:initial">帳號激活說明</span></b><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    <span style="background-image:initial;background-repeat:initial">------------------------------<wbr>------------------------------<wbr>----------</span>
                       <br>
                       <br> </span><span lang="EN-US" style="font-family:新細明體,serif"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是</span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">的新用戶，或在修改您的註冊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> Email </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">時使用了本地址，<wbr>我們需要對您的地址有效性進行驗證以避免垃圾郵件或地址被濫用。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">您的認證網址為：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                   <a target="_blank" href="http://www.fooddy.co/iuning/public/register?status=authenticate&token='.$rand.'">http://www.fooddy.co/iuning/public/register?status=authenticate&token='.$rand.'</a>
                   <br>
                   </span> </p>
                   '.$extra_html.'
                   <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">此致</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    </span><span lang="EN-US" style="color:rgb(55,96,146)">IUNING</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">.&nbsp;
                    </span><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
                   <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                   <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
            }

            require '../service/send_mail/gmailsystem/gmail.php';
            mb_internal_encoding('UTF-8');
            $callback = mstart( $a_email , $html , $title );

            return $callback;

    }
    
    
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
        echo json_encode($data);
        exit;
    }
    
    private function updateMysql($tableName,$condiColumn,$condiValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumn , $condiValue ];
        
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
        
        $DBCMD = DB::table($tableName)
                ->where($condiColumn,$condiValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    
    // table('zipcode') 全台縣市及其鄉鎮區
    /* @param String $userinfo_InExUser
     * @param String $userinfo_Password
     * @param String $userinfo_Email
     */
    public function gerCityDistrict(Request $request) {
        
        $callback = array();
        $usersInfo = $request["token_info"];
//        var_dump($usersInfo);exit;
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse( $callback);
//        }
        
                
        $area = DB::table('zipcode')
            ->select('zipcode_City')
            ->orderBy('zipcode_ID', 'asc')
            ->groupBy('zipcode_City')
            ->get();
        if ($area->count()===0) {
            $callback["area"] = [];
        } else {
            $callback["area"] = json_decode(json_encode($area), true);
        }
        
//        $district = DB::table('zipcode')
//            ->select('zipcode_Country')
//            ->where('zipcode_City', $usersInfo[0]["userinfo_Area"])
//            ->orderBy('zipcode_ID', 'asc')
//            ->get();
//        if ($district->count()===0) {
//            $callback["district"] = [];
//        } else {
//            $callback["district"] = json_decode(json_encode($district), true);
//        }
        
        $districtAll = DB::table('zipcode')
            ->select('zipcode_City', 'zipcode_Country')
            ->orderBy('zipcode_ID', 'asc')
            ->get();
//        if ($district->count()===0) {
//            $callback["district"] = [];
//        } else {
            $callback["districtAll"] = json_decode(json_encode($districtAll), true);
//        }
        
        $callback["success"]=true;
//            $callback["success"]=false;
//            $callback["msg"]="新增 data fail";
        return $this->jsonResponse( $callback);
    }
    
    
    public function uploadImg(Request $request) {
            
            $callback = array();
                // check empty
    //        if ( !(checkEmpty($request,array("userinfo_id","userinfo_oTableColumn"))) )
    //        {
    //            return $this->parameterErrorResponse();
    //        }

            // User need permission: Administrator 
            $usersInfo = $request["token_info"];
    //        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
    //                $callback["success"]=false;
    //                $callback["msg"]="you are not Administrator.";
    //                return $this->jsonResponse($callback);
    //        }

            $FileName = $_FILES["file"]['name'];
            $FileSub = explode( "." , $FileName );
            $FileSub = $FileSub[count($FileSub)-1];

//            define('upload_profile', "C:\\xampp\\htdocs\\arod\\iuning\\resources\\data\\profile\\" );
//            define('http_upload_profile', "http://www.fooddy.co/arod/iuning/resources/data/profile/" );

            if( !file_exists(upload_profile . $usersInfo[0]['userinfo_ID'] . "\\") ){
                mkdir(upload_profile . $usersInfo[0]['userinfo_ID'] . "\\", 0777, true);
            }

            $filepath = upload_profile . $usersInfo[0]['userinfo_ID'] . '\\' . $request["imgName"] . '_tmp.' . $FileSub;

            move_uploaded_file( $_FILES["file"]["tmp_name"] , $filepath );
            if( !file_exists($filepath) ) {
                $callback['msg'] = "Upload fail filepath: $filepath";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            $callback['data'] = array( "file" => $request["imgName"] . '_tmp.' . $FileSub ,
                                       "ori_file" => $_FILES["file"]['name'] ,
                                       "path" => http_upload_profile . $usersInfo[0]['userinfo_ID'] . '/' . $request["imgName"] . '_tmp.' . $FileSub );
            $callback['success'] = true;

            return $this->jsonResponse($callback);
            
    }
    
    
    public function releaseImg(Request $request) {
            $callback = array();
            
            $usersInfo = $request["token_info"];
            $dir = upload_profile . $usersInfo[0]['userinfo_ID'] . "\\";
            
            if ( ! is_dir($dir) ){
                    $callback['msg'] = "is_dir fail: $dir";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
            }
            
            if ( ! $dh = opendir($dir)){
                $callback['msg'] = "opendir fail";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            while( $file = readdir($dh) ){
                if( strpos($file, "_tmp") !== false ){
                    $file_new = str_replace('_tmp', '', $file);//移除檔名之_tmp成為正式檔名
                    rename( $dir . $file , $dir . $file_new );
                    
                    
                    ////DB存取更新時間////
                    //table名稱為檔案名稱拿掉附檔名
                    $table = 'userinfo_' . explode('.', $file_new)[0] . 'Img';
                    
                    $check = DB::table($table)
                        ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
                        ->get();
                    if ($check->count()>0) {
                            //UPDATE
                            $DBCMD = DB::table($table)->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])->update( array($table => date('Y-m-d')) );
                    } else {
                            //INSERT
                            $DBCMD = DB::table($table)->insert(
                                [
                                    'userinfo_ID' => $usersInfo[0]['userinfo_ID'], 
                                    $table => date('Y-m-d')
                                ]
                            );
                    }
                }
            }

            closedir($dh);
            
            $callback['success'] = true;
            return $this->jsonResponse($callback);
    }
    
    
    public function removeTmpImg(Request $request) {
            $usersInfo = $request["token_info"];
            $dir = upload_profile . $usersInfo[0]['userinfo_ID'] . "\\";
            
            if ( ! is_dir($dir) ){
                    $callback['msg'] = "is_dir fail: $dir";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
            }
            
            if ( ! $dh = opendir($dir)){
                $callback['msg'] = "opendir fail";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            
            while( $file = readdir($dh) ){
                if( strpos($file, "_tmp") !== false ){
                    unlink( $dir . $file );
                }
            }

            closedir($dh);
            
            $callback['success'] = true;

            return $this->jsonResponse($callback);
            
    }
    
    public function getUserinfoImage(Request $request) {
        
        // check empty
        if ( !(checkEmpty($request,array("userinfo_oTableColumn"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        
        // User need permission: Administrator 
        $usersInfo = $request["token_info"];
//        if(!strpos($usersInfo[0]["userinfo_InExUser"],"Administrator")){
//                $callback["success"]=false;
//                $callback["msg"]="you are not Administrator.";
//                return $this->jsonResponse($callback);
//        }
        
        $TableColumnName = $request['userinfo_oTableColumn'];
        
        $check = DB::table($TableColumnName)
                            ->select('userinfo_ID', $TableColumnName)
                            ->where('userinfo_ID', $usersInfo[0]['userinfo_ID'])
//                ->toSql();
//                echo $check;exit;
                            ->get();
        if ($check->count() <> 1) {
            $callback["success"]=false;
        }
        else{
            $img_name = explode('_', $TableColumnName)[1];
            $img_name = str_replace('Img', '', $img_name);
            // add another element(userinfo_oTableColumn) in an array
            $obj_to_array = array();
//            var_dump($check[0]->$TableColumnName);exit;
//            var_dump($check->item[0]->$TableColumnName);exit;
            $check = (array)$check[0];
            $obj_to_array['date'] = $check[$TableColumnName];
            $obj_to_array['imgUrl'] = 'http://www.fooddy.co/arod/iuning/resources/data/profile/' . $usersInfo[0]['userinfo_ID'] . '/' . $img_name . '.jpg';
            // wrap result
            $callback["data"] = json_decode(json_encode($obj_to_array), true);
            $callback["success"]=true;
        }
       
        return $this->jsonResponse($callback);
    }

    public function setCollection(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable", "global_value"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
  
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
     
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName = $checkKey[0]->Column_name;
        $global_oTableKeyName2 = $checkKey[1]->Column_name;
        
        $checkData = DB::select("SELECT * FROM $global_oTable "
                             . "WHERE $global_oTableKeyName = ".$request['global_value']["$global_oTableKeyName"]
                              . " AND $global_oTableKeyName2 = ".$request['global_value']["$global_oTableKeyName2"]);
        
        if (count($checkData)===0) {
                $DBCMD = DB::table($global_oTable)->insert([$request['global_value']]);
                        $callback["data"] = $global_oTable;

                if ($DBCMD) {
                        $callback["success"] = true;
                } else {
                        $callback["success"] = false;
                        $callback["msg"] = "Insert collection fail";
                }
        } else {
                $callback["success"] = true;
        }
        
        return $this->jsonResponse($callback);
    }    
                
    public function delCollection(Request $request) {
        
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTableKeyValue2", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // avoid from userinfo table 
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        // get primary key
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName  = $checkKey[0]->Column_name;
        $global_oTableKeyName2 = $checkKey[1]->Column_name;
        
        $DBCMD = DB::table($global_oTable)
            ->where([[$global_oTableKeyName, $request['global_oTableKeyValue']],
                    [$global_oTableKeyName2, $request['global_oTableKeyValue2']]])
            ->delete();
        if ($DBCMD) {
                $callback["success"] = true;
        } else {
                $callback["success"] = false;
                $callback["msg"] = "Delete collection fail";
        }

        return $this->jsonResponse($callback);   
    }    

    public function delHistory(Request $request) {
        
        $callback = array();
        
        // check empty
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        // avoid from userinfo table 
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
        // check table exist
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
        // get primary key
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName  = $checkKey[0]->Column_name;
        
        $DBCMD = DB::table($global_oTable)
            ->where($global_oTableKeyName, $request['global_oTableKeyValue'])
            ->delete();
        if ($DBCMD) {
                $callback["success"] = true;
        } else {
                $callback["success"] = false;
                $callback["msg"] = "Delete history fail";
        }

        return $this->jsonResponse($callback);   
    }     

    public function removeGlobalValue(Request $request) {
        
        $callback = array();
        
        if ( !(checkEmpty($request,array("global_oTableKeyValue", "global_oTable"))) )
        {
            return $this->parameterErrorResponse();
        }
        
        $global_oTable = $request['global_oTable'];
        if(strpos($global_oTable, 'userinfo')!==false) 
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"cannot access this table"));
        }
  
        $checkTable = DB::select("SHOW TABLES LIKE '".$global_oTable."'");
        if (count($checkTable)===0)
        {
            return $this->jsonResponse( array("success"=>false,"msg"=>"table doesn't exist"));
        }
     
        $checkKey = DB::select("SHOW KEYS FROM ".$global_oTable." WHERE Key_name = 'PRIMARY'");
        if (count($checkKey)===0) {
            return $this->jsonResponse( array("success"=>false,"msg"=>"no primary key"));
        }
        
        $global_oTableKeyName = $checkKey[0]->Column_name;
        $DBCMD = DB::table($global_oTable)
            ->where($global_oTableKeyName, "=", $request['global_oTableKeyValue'])
            ->delete();
        if ($DBCMD) {
                $callback["success"] = true;
        } else {
                $callback["success"] = false;
                $callback["msg"] = "$global_oTable delete fail";
        }
        return $this->jsonResponse($callback);
    }
}

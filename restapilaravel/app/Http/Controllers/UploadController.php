<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;

class UploadController extends Controller
{
    // User Register
    /* @param String $userinfo_UserName
     * @param String $userinfo_Password
     * @param String $userinfo_Email
     */
    public function tempFile(Request $request) {
        
        $callback = array();
        //$upload_transient_file = "data/transient/";
        $data_file = "C:\\xampp\\htdocs\\go2taiwan\\data\\";
        $upload_transient_file = "C:\\xampp\\htdocs\\go2taiwan\\data\\transient\\";
        $http_upload_transient_file = "http://".$_SERVER['SERVER_NAME']."/go2taiwan/data/transient/";
        
        $FileName = $_FILES["file"]['name'];
        $FileSub = explode( "." , $FileName );
        $FileSub = $FileSub[count($FileSub)-1];

        if( !file_exists($data_file) ){
            mkdir($data_file, 0777, true);
        }
        if( !file_exists($upload_transient_file) ){
            mkdir($upload_transient_file, 0777, true);
        }
        $filepath_o = $upload_transient_file.$_FILES["file"]['name'];
        if( file_exists($filepath_o) ) {
            unlink($filepath_o);
        }
        $time = $this->udate('YmdHisu');
        $filepath = $upload_transient_file.$time.".".$FileSub;
        //$httppath = "http://".$_SERVER["SERVER_NAME"]."/ttshow/transient_file/".$time.".".$_REQUEST['subname'];

        move_uploaded_file( $_FILES["file"]["tmp_name"] , $filepath );
        if( file_exists($filepath) ) {
            $callback['data'] = array( "path" => $http_upload_transient_file ,
                                       "file" => $time.".".$FileSub );//$_FILES["file"]['name']
            $callback['success'] = true;
        } else {
            $callback['msg'] = "Upload fail";
            $callback['success'] = false;
        }
        
        
        return $this->jsonResponse($callback);
    }
    
    
    
    private function udate($format = 'u', $utimestamp = null) {
            if (is_null($utimestamp))
                $utimestamp = microtime(true);

            $timestamp = floor($utimestamp);
            $milliseconds = round(($utimestamp - $timestamp) * 1000000);

            return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }
    
    
    
    public function send($mail_type,$rand,$http_default_path,$title,$a_email,$extra_html=""){

            $callback = array();

//            if( !check_empty( array( "mail_type","rand","http_default_path","title","a_email" ) ) ){
//                    $callback['msg'] = "輸入資料不完整";
//                    $callback['success'] = false;
//                    return $callback;
//            }

//            $mail_type = $_REQUEST["mail_type"];
//            $rand = $_REQUEST["rand"];
//            $http_default_path = $_REQUEST["http_default_path"];
//            $title = $_REQUEST["title"];
//            $a_email = $_REQUEST["a_email"];

            if( !in_array($mail_type, array("forgot","authenticate")) ){
                    $callback['msg'] = "輸入資料錯誤";
                    $callback['success'] = false;
                    return $callback;
            }

            if( $mail_type === "forgot" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                    <span style="font-size:10pt;font-family:新細明體,serif;color:black">親愛的<span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">AROFLY</span>會員，您好：</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">(Dear AROFLY member, )</span>
                    </p>
                    <br>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span style="font-size:10pt;font-family:新細明體,serif;color:black">系統收到您要求重設密碼。請點擊<a target="_blank" href="http://www.arofly.com.tw/arofly/public/forgot_password?token='.$rand.'" style="color:red; text-decoration:none">此連結</a>來重設密碼。或是至以下網址進行變更。</span>
                        <span lang="EN-US" style="font-size:10pt;font-family:Arial,sans-serif">(System received your request of password reset. Please click <a target="_blank" href="http://www.arofly.com.tw/arofly/public/forgot_password?token='.$rand.'" style="color:red; text-decoration:none">HERE</a> for password reset. Or you can go to following site for resetting.)</span>
                    </p>
                    <a target="_blank" href="http://www.arofly.com.tw/arofly/public/forgot_password?token='.$rand.'" style="color:red">http://www.arofly.com.tw/arofly/public/forgot_password?token='.$rand.'</a>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span style="font-size:10pt;font-family:新細明體,serif;color:black">※注意：此信件為系統發出信件，若有問題請聯繫客服中心，謝謝！</span>
                        <br>
                        <span lang="EN-US" style="font-size:10pt;font-family:Arial,sans-serif">(Attention: This email is sent by system. If you have any questions, please contact Service Center. Thank you.)</span>
                    </p>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span lang="EN-US" style="">AROFLY </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊 敬上</span>
                        <span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">&nbsp;(Sincerely, AROFLY team)</span>
                    </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
                    //<p><span style="font-family:新細明體,serif;color:rgb(55,96,146)">'.$http_default_path.'</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"></span> </p>
            }
            else if( $mail_type === "authenticate" ){
                    $html = '<p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><br>
                    </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">這封信是由</span><span lang="EN-US" style="">Arofly</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">發送的。</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">(This email is sent by AROFLY)</span> </p>
                    <br>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial"><span style="font-size:10pt;font-family:新細明體,serif;color:black">如果您是<a href="http://www.arofly.com.tw/arofly/public/"><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">arofly.com</span></a>的新會員，我們需要進行驗証以確保您的權益。</span><span lang="EN-US">(If you are new member or the member modifying email, we need Email verification to protect your rights.)</span><span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> </span></p> <br>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span style="font-size:10pt;font-family:新細明體,serif;color:black">請前往此連結：<span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"><a target="_blank" href="http://www.arofly.com.tw/arofly/public/register?status=authenticate&token='.$rand.'">http://www.arofly.com.tw/arofly/public/register?status=authenticate&token='.$rand.'</a></span>
                        <br>
                        <span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">(Please go to this site for connection : <a target="_blank" href="http://www.arofly.com.tw/arofly/public/register?status=authenticate&token='.$rand.'">http://www.arofly.com.tw/arofly/public/register?status=authenticate&token='.$rand.'</a>)</span>
                    </p>
                    <br>
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span style="font-size:10pt;font-family:新細明體,serif;color:black">※注意：此信件為系統發出信件，若有問題請聯繫客服中心。若您並沒有登錄過，或沒有進行上述操作，請忽略這封信。您不需退訂或進行其他進一步的操作。</span>
                        <br>
                        <span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black"> (Attention: This email is sent by system. If you have any questions, please contact Service Center. If you didn\'t log in or did the above operation, please ignore this email. No need further action.)</span>
                    </p>
                   '.$extra_html.'
                    <p class="MsoNormal" style="background-image:initial;background-repeat:initial">
                        <span lang="EN-US" style="">AROFLY </span><span style="font-size:10pt;font-family:新細明體,serif;color:black">管理團隊 敬上</span>
                        <span lang="EN-US" style="font-size:10pt;font-family:\'Segoe UI\',sans-serif;color:black">&nbsp;(Sincerely, AROFLY team)</span>
                    </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>
                    <p class="MsoNormal"><span lang="EN-US">&nbsp;</span> </p>';
                    // .$http_default_path.
            }

            header("Access-Control-Allow-Origin:*");
            header('Access-Control-Allow-Credentials:true');
            header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
            header('Content-Type:text/html; charset=utf-8');
            require '../service/send_mail/gmailsystem/gmail.php';
            mb_internal_encoding('UTF-8');
            $callback = mstart( $a_email , $html , $title );

            return $callback;

    }
    
    
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        return json_encode($callback);
    }
    
    public function jsonResponse($data) {
        return json_encode($data);
    }
    
    private function updateMysql($tableName,$condiColumn,$condiValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumn , $condiValue ];
        
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
        
        $DBCMD = DB::table($tableName)
                ->where($condiColumn,$condiValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
}

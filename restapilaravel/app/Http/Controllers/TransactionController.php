<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Schema\Grammars;
use PDO;

class TransactionController extends Controller
{
    public function buyPoint(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"point") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            
            $check = DB::table('userinfo_point')
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->get();
            if ($check->count() == 0) {
                $DBCMD = DB::table('userinfo_point')->insert(
                    [   'userinfo_ID' => $usersInfo[0]["userinfo_ID"], 
                        'userinfo_point' => $request["point"]  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="購買失敗";
                    return $this->jsonResponse($callback);
                }
            }
            else{
                $DBCMD = DB::table("userinfo_point")
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->increment('userinfo_point',$request["point"]);
                if($DBCMD==0){
                    $callback["success"]=false;
                    $callback["msg"]="購買失敗";
                    return $this->jsonResponse($callback);
                }
            }
            
            
            $check = DB::table('userinfo_point')
                    ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                    ->get();
            if ($check->count() == 0) {
                $userinfo_point = 0;
            }
            else{
                $check = json_decode(json_encode($check),true)[0];
                $userinfo_point = $check["userinfo_point"];
            }
            
            $callback["data"]=$userinfo_point;
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function buyAdPublish(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"days") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
//            echo $request["days"];
            switch ($request["days"]) {
                case "1":
                    $costPoint = 50;
                    break;
                case "2":
                    $costPoint = 100;
                    break;
                case "3":
                    $costPoint = 150;
                    break;
                case "4":
                    $costPoint = 200;
                    break;
                case "5":
                    $costPoint = 250;
                    break;
                default:
                    return $this->parameterErrorResponse();
                    break;
            }
            
            $check = DB::table('userinfo_point')
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->get();
            if ($check->count() == 0) {
                $DBCMD = DB::table('userinfo_point')->insert(
                    [   'userinfo_ID' => $usersInfo[0]["userinfo_ID"], 
                        'userinfo_point' => 0  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="餘額不足";
                    return $this->jsonResponse($callback);
                }
                $callback["success"]=false;
                $callback["msg"]="餘額不足";
                return $this->jsonResponse($callback);
            }
            else{
                $check = json_decode(json_encode($check),true)[0];
                $userinfo_point = $check["userinfo_point"];
                if( $userinfo_point < $costPoint ){
                    $callback["success"]=false;
                    $callback["msg"]="餘額不足";
                    return $this->jsonResponse($callback);
                }
                
                $DBCMD = DB::table("userinfo_point")
                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                        ->decrement('userinfo_point',$costPoint);
                if($DBCMD==0){
                    $callback["success"]=false;
                    $callback["msg"]="購買失敗";
                    return $this->jsonResponse($callback);
                }
                
                if( $usersInfo[0]["userinfo_ad_publish_datetime"]==="0000-00-00 00:00:00" || strtotime($usersInfo[0]["userinfo_ad_publish_datetime"]) < strtotime("now") ){
                    $date = date_create('now');
                    date_add($date, date_interval_create_from_date_string($request["days"].' days'));
                    $returnAdPublish = date_format($date, 'Y-m-d H:i:s');
                    $DBCMD = DB::table("userinfo")
                            ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                            ->update(array('userinfo_ad_publish_datetime'=>$returnAdPublish));
                    if($DBCMD==0){
                        $callback["success"]=false;
                        $callback["msg"]="購買失敗";
                        return $this->jsonResponse($callback);
                    }
                }
                else{
                    $date = date_create($usersInfo[0]["userinfo_ad_publish_datetime"]);
                    date_add($date, date_interval_create_from_date_string($request["days"].' days'));
                    $returnAdPublish = date_format($date, 'Y-m-d H:i:s');
                    $DBCMD = DB::table("userinfo")
                            ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
                            ->update(array('userinfo_ad_publish_datetime'=>$returnAdPublish));
                    if($DBCMD==0){
                        $callback["success"]=false;
                        $callback["msg"]="購買失敗";
                        return $this->jsonResponse($callback);
                    }
                }
                //userinfo_ad_publish_datetime
//                $DBCMD = DB::table("userinfo")
//                        ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
//                        ->decrement('userinfo_ad_publish_datetime',$costPoint);
//                if($DBCMD==0){
//                    $callback["success"]=false;
//                    $callback["msg"]="購買失敗";
//                    return $this->jsonResponse($callback);
//                }
                
            }
            
            
//            $check = DB::table('userinfo_point')
//                    ->where('userinfo_ID', $usersInfo[0]["userinfo_ID"])
//                    ->get();
//            if ($check->count() == 0) {
//                $userinfo_point = 0;
//            }
//            else{
//                $check = json_decode(json_encode($check),true)[0];
//                $userinfo_point = $check["userinfo_point"];
//            }
            
            $callback["adPublish"]=$returnAdPublish;
            $callback["point"]=$userinfo_point-$costPoint;
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function sendMoneyAsk(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            if ( !checkEmpty($request,"money") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $check = DB::table('chat_room')
                            ->where('cr_investor_user_id', $request["Investor"])
                            ->where('cr_borrower_user_id', $usersInfo[0]['userinfo_ID'])
                            ->where('cr_status', 3)
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "錯誤";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $updateStatus = array("cr_ask_money"=>$request["money"],"cr_ask_datetime"=>date('Y-m-d H:i:s'));
                $updateResult = $this->updateMysql("chat_room",array("cr_investor_user_id"=>$request["Investor"],"cr_borrower_user_id"=>$usersInfo[0]['userinfo_ID']),$updateStatus);
                if ($updateResult) {
                        $callback["success"]=true;
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="寄送失敗";
                }
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $check = DB::table('chat_room')
                            ->where('cr_investor_user_id', $usersInfo[0]['userinfo_ID'])
                            ->where('cr_borrower_user_id', $request["Borrower"])
                            ->where('cr_status', 3)
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "錯誤";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $updateStatus = array("cr_ask_money"=>$request["money"],"cr_ask_datetime"=>date('Y-m-d H:i:s'));
                $updateResult = $this->updateMysql("chat_room",array("cr_investor_user_id"=>$usersInfo[0]['userinfo_ID'],"cr_borrower_user_id"=>$request["Borrower"]),$updateStatus);
                if ($updateResult) {
                        $callback["success"]=true;
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="寄送失敗";
                }
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $result["success"] = true;
            
            return $result;
            
    }
    
    public function sendTransactionRes(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"Investor") && !checkEmpty($request,"Borrower") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if(checkEmpty($request,"Investor")&&$usersInfo[0]['userinfo_InExUser']==="Borrower"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Investor"])
                            ->where('userinfo_InExUser', "Investor")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $check = DB::table('chat_room')
                            ->where('cr_investor_user_id', $request["Investor"])
                            ->where('cr_borrower_user_id', $usersInfo[0]['userinfo_ID'])
                            ->where('cr_status', 3)
                            ->where('cr_ask_money', ">", 0)
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "錯誤";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $updateStatus = array("cr_ask_money"=>0);
                $updateResult = $this->updateMysql("chat_room",array("cr_investor_user_id"=>$request["Investor"],"cr_borrower_user_id"=>$usersInfo[0]['userinfo_ID']),$updateStatus);
                if ($updateResult) {
                        $callback["success"]=true;
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="寄送失敗";
                }
            }
            else if(checkEmpty($request,"Borrower")&&$usersInfo[0]['userinfo_InExUser']==="Investor"){
                $check = DB::table('userinfo')
                            ->where('userinfo_ID', $request["Borrower"])
                            ->where('userinfo_InExUser', "Borrower")
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "對象不存在";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                else{
                    $check = json_decode(json_encode($check),true);
                }
                $check = DB::table('chat_room')
                            ->where('cr_investor_user_id', $usersInfo[0]['userinfo_ID'])
                            ->where('cr_borrower_user_id', $request["Borrower"])
                            ->where('cr_status', 3)
                            ->where('cr_ask_money', ">", 0)
                            ->get();
                if ($check->count() == 0) {
                    $callback['msg'] = "錯誤";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                $updateStatus = array("cr_ask_money"=>0);
                $updateResult = $this->updateMysql("chat_room",array("cr_investor_user_id"=>$usersInfo[0]['userinfo_ID'],"cr_borrower_user_id"=>$request["Borrower"]),$updateStatus);
                if ($updateResult) {
                        $callback["success"]=true;
                } else {
                        $callback["success"]=false;
                        $callback["msg"]="寄送失敗";
                }
            }
            else{
                return $this->parameterErrorResponse();
            }
            
            $result["success"] = true;
            
            return $result;
            
    }
    
    public function borrowNewBid(Request $request) {
            
            $callback = array();
            if ( !checkEmpty($request,"bidMoney") )
            {
                return $this->parameterErrorResponse();
            }
            
            $usersInfo = $request["token_info"];
            
            if($usersInfo[0]['userinfo_InExUser']!=="Borrower"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            if( $usersInfo[0]['userinfo_Status'] === "pause" ){
                $date = date('Y-m-d H:i:s', strtotime('+30 day', strtotime($usersInfo[0]['userinfo_last_transaction_datetime'])));
//                date_add($date, date_interval_create_from_date_string('30 days'));
                $callback["success"]=false;
                $callback["msg"]="您已經交易並評分，必須在".$date."才可開始交易";
                return $this->jsonResponse($callback);
            }
            else if ( $usersInfo[0]['userinfo_Status'] === "pause2" ){
                $date = date('Y-m-d H:i:s', strtotime('+90 day', strtotime($usersInfo[0]['userinfo_last_transaction_datetime'])));
                $callback["success"]=false;
                $callback["msg"]="您已經交易並未評分，必須在".$date."才可開始交易";
                return $this->jsonResponse($callback);
            }
//            $min = $this->minDiff(, date('Y-m-d H:i:s'));
            
            $DBCMD = DB::table('userinfo_project')->insert(
                [   'up_investor_uid' => 0, 
                    'up_borrower_uid' => $usersInfo[0]['userinfo_ID'], 
                    'up_money' => $request["bidMoney"], 
                    'up_create_datetime' => date('Y-m-d H:i:s'), 
                    'up_datetime' => date('Y-m-d H:i:s'), 
                    'up_status' => 0  ]
            );
            if (!$DBCMD) {
                $callback["success"]=false;
                $callback["msg"]="新增失敗";
                return $this->jsonResponse($callback);
            }
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function borrowbidList(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if($usersInfo[0]['userinfo_InExUser']!=="Borrower"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('userinfo_project as up')
                    ->join('userinfo as borrow', 'borrow.userinfo_ID', '=', 'up.up_borrower_uid')
                    ->leftjoin('userinfo as investor', 'investor.userinfo_ID', '=', 'up.up_investor_uid')
                    ->select('up.*','investor.userinfo_UserName as investorUserName','investor.userinfo_Area as investorArea')
                    ->where('borrow.userinfo_ID', $usersInfo[0]['userinfo_ID'])
//                    ->where('chat_room.cr_status', "3")
                    ->get();
            
            $callback["data"]=$check;
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function borrowbidListVersion2(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if($usersInfo[0]['userinfo_InExUser']!=="Borrower"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('userinfo_project as up')
                    ->join('userinfo as borrow', 'borrow.userinfo_ID', '=', 'up.up_borrower_uid')
                    ->leftjoin('userinfo as investor', 'investor.userinfo_ID', '=', 'up.up_investor_uid')
                    ->select('up.*','investor.userinfo_UserName as investorUserName','investor.userinfo_Area as investorArea')
                    ->where('borrow.userinfo_ID', $usersInfo[0]['userinfo_ID'])
                    ->where('up.up_status',">", 1)
                    ->get();
            
            $callback["data"]=$check;
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function bidListbyBorrowId(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,"borrowId") )
            {
                return $this->parameterErrorResponse();
            }
            if($usersInfo[0]['userinfo_InExUser']!=="Investor"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('chat_room')
                            ->where('cr_investor_user_id', $usersInfo[0]["userinfo_ID"])
                            ->where('cr_borrower_user_id', $request["borrowId"])
                            ->get();
            if ($check->count() == 0) {
                $callback['msg'] = "對象不存在";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            else{
                $check = json_decode(json_encode($check),true);
//                if($check[0]["cr_status"]!==3){
//                    $callback['msg'] = "對方未接受您的觀看請求";
//                    $callback['success'] = false;
//                    return $this->jsonResponse($callback);
//                }
            }
            
            $data = array();
            
            $check = DB::table('userinfo_project as up')
                    ->join('userinfo as borrow', 'borrow.userinfo_ID', '=', 'up.up_borrower_uid')
                    ->leftjoin('userinfo as investor', 'investor.userinfo_ID', '=', 'up.up_investor_uid')
                    ->select('up.*','borrow.userinfo_UserName as borrowUserName','borrow.userinfo_Area as borrowArea')
                    ->where('borrow.userinfo_ID', $request["borrowId"])
                    ->whereIn('up.up_status', [0,1])
                    ->get();
            $data["biding"] = $check;
            
            $check = DB::table('userinfo_project as up')
                    ->join('userinfo as borrow', 'borrow.userinfo_ID', '=', 'up.up_borrower_uid')
                    ->leftjoin('userinfo as investor', 'investor.userinfo_ID', '=', 'up.up_investor_uid')
                    ->select('up.*','borrow.userinfo_UserName as borrowUserName','borrow.userinfo_Area as borrowArea')
                    ->where('borrow.userinfo_ID', $request["borrowId"])
                    ->where('up.up_investor_uid', $usersInfo[0]["userinfo_ID"])
                    ->where('up.up_status',">", 1)
//                    ->where('chat_room.cr_status', "3")
                    ->get();
            $data["prepare"] = $check;
            
            $callback["data"]=$data;
            $callback["success"]=true;
            return $callback;
            
    }
    
    public function InvestorBid(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if ( !checkEmpty($request,array("up_id","up_bid_money")) )
            {
                return $this->parameterErrorResponse();
            }
            if($usersInfo[0]['userinfo_InExUser']!=="Investor"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('chat_room')
                            ->join('userinfo_project', 'userinfo_project.up_borrower_uid', '=', 'chat_room.cr_borrower_user_id')
                            ->where('cr_investor_user_id', $usersInfo[0]["userinfo_ID"])
                            ->where('up_id', $request["up_id"])
                            ->get();
            if ($check->count() == 0) {
                $callback['msg'] = "錯誤";
                $callback['success'] = false;
                return $this->jsonResponse($callback);
            }
            else{
                $check = json_decode(json_encode($check),true);
//                if($check[0]["cr_status"]!==3){
//                    $callback['msg'] = "您對方未接受您的觀看請求";
//                    $callback['success'] = false;
//                    return $this->jsonResponse($callback);
//                }
                if($check[0]["up_status"]!==0&&$check[0]["up_status"]!==1){
                    $callback['msg'] = "現在未能競標";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                if($check[0]["up_status"]===1){
                    $min = $this->minDiff($check[0]["up_datetime"], date('Y-m-d H:i:s'));
                    if( $min >= 3 ){
                        $callback['msg'] = "競標時間已過";
                        $callback['success'] = false;
                        return $this->jsonResponse($callback);
                    }
                }
                if($check[0]["up_investor_uid"]===$usersInfo[0]["userinfo_ID"]){
                    $callback['msg'] = "您已經是上個競標者";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
                if((int)$request["up_bid_money"]<=$check[0]["up_bid_money"]){
                    $callback['msg'] = "金額小於或等於上一次競標金額";
                    $callback['success'] = false;
                    return $this->jsonResponse($callback);
                }
            }
            
            $updateStatus = array( "up_investor_uid" => $usersInfo[0]["userinfo_ID"]
                                   , "up_bid_money" => (int)$request["up_bid_money"]
                                   , "up_status" => 1
                                   , "up_datetime" => date('Y-m-d H:i:s') );
            
            $updateResult = $this->updateMysql("userinfo_project"
                                        ,array("up_id"=>$request["up_id"])
                                        ,$updateStatus);
            if ($updateResult) {
                    $callback["success"]=true;
            } else {
                    $callback["success"]=false;
                    $callback["msg"]="競標失敗";
            }
            
            return $callback;
            
    }
    
    public function scheduleChangeStatus(Request $request) {
            
            $callback = array();
            $callback["success"] = 0;
            $callback["updateDatetimeSuccess"] = 0;
            $callback["updateDatetimeFail"] = 0;
            $callback["addCommand3Success"] = 0;
            $callback["addCommand3Fail"] = 0;
            $callback["addCommand4Success"] = 0;
            $callback["addCommand4Fail"] = 0;
            $callback["fail"] = 0;
            $callback["nothing"] = 0;
            
            $usersInfo = $request["token_info"];
            
            $check = DB::table('userinfo_project as up')
                            ->join('userinfo as borrow', 'borrow.userinfo_ID', '=', 'up.up_borrower_uid')
                            ->leftjoin('userinfo as investor', 'investor.userinfo_ID', '=', 'up.up_investor_uid')
                            ->select('up.*','investor.userinfo_UserName as investorUserName','borrow.userinfo_UserName as borrowUserName')
                            ->where('up.up_status', "!=", 0)
                            ->get();
            if ($check->count() !== 0) {
                
                $check = json_decode(json_encode($check),true);
                
                foreach ($check as $key => $value) {
                    
                    $min = $this->minDiff($value["up_datetime"], date('Y-m-d H:i:s'));
                    
                    if( $min >= 3 ){
                        if( $min >= 15 && $value["up_status"] < 6 ){
                            $up_status = 6;
                        }
                        else if( $min >= 12 && $value["up_status"] < 5 ){
                            $up_status = 5;
                        }
                        else if( $min >= 9 && $value["up_status"] < 4 ){
                            $up_status = 4;
                        }
                        else if( $min >= 6 && $value["up_status"] < 3 ){
                            $up_status = 3;
                        }
                        else if( $min >= 3 && $value["up_status"] < 2 ){
                            $up_status = 2;
                        }
                        else{
                            $callback["nothing"]++;
                            continue;
                        }
                    }
                    else{
                        continue;
                    }
                    
                    $updateStatus = array( "up_status" => $up_status );

                    $updateResult = $this->updateMysql("userinfo_project"
                                                 ,array("up_id"=>$value["up_id"])
                                                 ,$updateStatus);
                     if ($updateResult) {
                            $callback["success"]++;
                            
                            $insertAPICallback = $this->insertAPI( $value["up_investor_uid"], 0, 4, 0 );
                            if( $insertAPICallback["success"] ){
                                    $callback["addCommand4Success"]++;
                            }
                            else{
                                    $callback["addCommand4Fail"]++;
                            }
                            
                            if( $up_status >= 2 ){
                                
                                $updateArr = array( "userinfo_last_transaction_datetime" => date('Y-m-d H:i:s') );
                                $updateResult = $this->updateMysql("userinfo"
                                                            ,array("userinfo_ID"=>$value["up_borrower_uid"])
                                                            ,$updateArr);
                                if ($updateResult) {
                                       $callback["updateDatetimeSuccess"]++;
                                } else {
                                       $callback["updateDatetimeFail"]++;
                                }
                                
                                $changeCanTalk = $this->searchConvid($value["up_investor_uid"],$value["up_borrower_uid"],$value["investorUserName"],$value["borrowUserName"]);
                                if( $changeCanTalk["success"] ){
                                        $insertAPICallback = $this->insertAPI( $value["up_investor_uid"], 0, 3, 0 );
                                        if( $insertAPICallback["success"] ){
                                                $callback["addCommand3Success"]++;
                                        }
                                        else{
                                                $callback["addCommand3Fail"]++;
                                        }
                                        $insertAPICallback = $this->insertAPI( $value["up_borrower_uid"], 0, 3, 0 );
                                        if( $insertAPICallback["success"] ){
                                                $callback["addCommand3Success"]++;
                                        }
                                        else{
                                                $callback["addCommand3Fail"]++;
                                        }
                                }
                                
                            }
                     } else {
                            $callback["fail"]++;
                     }
                    
                }
                
            }
            
            return $callback;
            
    }
    
    public function getAllFinishByInvestor(Request $request) {
            
            $callback = array();
            
            $usersInfo = $request["token_info"];
            
            if($usersInfo[0]['userinfo_InExUser']!=="Investor"){
                $callback["success"]=false;
                $callback["msg"]="您不是借方";
                return $this->jsonResponse($callback);
            }
            
            $check = DB::table('userinfo_project')
                            ->leftjoin('userinfo', 'userinfo_project.up_borrower_uid', '=', 'userinfo.userinfo_ID')
                            ->where('up_investor_uid', $usersInfo[0]["userinfo_ID"])
//                            ->where('up_id', $request["up_id"])
                            ->get();
            if ($check->count() == 0) {
//                $callback['msg'] = "錯誤";
//                $callback['success'] = false;
//                return $this->jsonResponse($callback);
                $callback["data"]=array();
            }
            else{
                $callback["data"]=$check;
            }
            $callback["success"]=true;
            
            return $callback;
            
    }
    
    
    
    
    
    private function minDiff($startTime, $endTime) {
        $start = strtotime($startTime);
        $end = strtotime($endTime);
        $timeDiff = $end - $start;
        return floor($timeDiff / 60);
    }
    
    public function udate($format = 'u', $utimestamp = null) {
            if (is_null($utimestamp))
                $utimestamp = microtime(true);

            $timestamp = floor($utimestamp);
            $milliseconds = round(($utimestamp - $timestamp) * 1000000);

            return date(preg_replace('`(?<!\\\\)u`', $milliseconds, $format), $timestamp);
    }

    private function callCurl($Url,$Type,$Json=""){
            
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $Url);//"https://leancloud.cn/1.1/classes/className"
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            if($Type==="POST"){
                curl_setopt($ch, CURLOPT_POST, TRUE);
            }
            else if($Type==="PUT"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
            else if($Type==="GET"){
                
            }
            else if($Type==="DELETE"){
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            }
            
            if(!empty($Json))
                curl_setopt($ch, CURLOPT_POSTFIELDS, $Json);/*"{
                \"title\": \"工程师周会\",
                \"content\": \"每周工程师会议，周一下午2点\"
              }"*/

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              "Content-Type: application/json",
              "X-LC-Id: MamNNjzE9Qb9UBohRJ05hSFm-gzGzoHsz",
              "X-LC-Key: VaIUCWxS03vG3wX707yfbYxg,master"
            ));

            $response = curl_exec($ch);
            curl_close($ch);

            //var_dump($response);
            return $response;
    }
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
            echo json_encode($data);
            exit;
    }
    
    private function updateMysql($tableName,$condiColumnArr,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    private function updateMysqlIncludeIn($tableName,$condiColumnArr,$condiInColumn,$condiInValue,$updateArr){
        
        $checkArr = array();
        foreach ($updateArr as $key => $value) {
            $checkArr[count($checkArr)] = [ $key , $value ];
        }
        $checkArr[count($checkArr)] = [ $condiColumnArr ];
//        print_r($checkArr);
        $check = DB::table($tableName)
            ->where($checkArr)
            ->whereIn($condiInColumn,$condiInValue)//('chat_room.cr_status', [1,2])
            ->get();
//        echo $condiColumn." ".$condiValue;
//        print_r($checkArr);
        
        if ($check->count()>0) {
            return true;//完全不用更動
        }
//        print_r($condiColumnArr);
        $DBCMD = DB::table($tableName)
                ->where($condiColumnArr)
                ->whereIn($condiInColumn,$condiInValue)
                ->update( $updateArr );
        
        if($DBCMD==0){
            return false;
        }
        return true;
        
    }
    
    ///////////////////chat
    private function searchConvid($Investor,$Borrower,$InvestorName,$BorrowerName) {
            
            $callback = array();
            $check = DB::table('chat_room')
                    ->select('cr_convid','cr_status','cr_ask_money')
                    ->where('cr_investor_user_id', $Investor)
                    ->where('cr_borrower_user_id', $Borrower)
                    ->get();
            if ($check->count() == 0) {
                $Json=array("name"=>"$InvestorName&$BorrowerName Room",
                        "m"=> [$InvestorName, $BorrowerName],
                        "tr"=> true,
                        "sys"=> true);
                $Json = json_encode($Json);
                $data = $this->callCurl('https://api.leancloud.cn/1.1/classes/_Conversation','POST',$Json);
                $data = json_decode($data,true);
                $insert_id = $data["objectId"];
                
                $DBCMD = DB::table('chat_room')->insert(
                    [   'cr_investor_user_id' => $Investor, 
                        'cr_borrower_user_id' => $Borrower, 
                        'cr_convid' => $insert_id, 
                        'cr_create_datetime' => date('Y-m-d H:i:s'), 
                        'cr_response_datetime' => date('Y-m-d H:i:s'), 
                        'cr_status' => 3  ]
                );
                if (!$DBCMD) {
                    $callback["success"]=false;
                    $callback["msg"]="create chat room db fail";
                    return $this->jsonResponse($callback);
                }
                $chat_room = array('cr_convid'=>$insert_id,'cr_status'=>0,'cr_ask_money'=>0);
                $callback["success"]=true;
            }
            else{
                $chat_room = json_decode(json_encode($check),true)[0];
                
                if( $chat_room["cr_status"] !== 3 ){
                    $updateStatus = array("cr_status"=>3,"cr_response_datetime"=>date('Y-m-d H:i:s'));
                    $updateResult = $this->updateMysql("chat_room",array("cr_convid"=>$chat_room["cr_convid"]),$updateStatus);
                    if ($updateResult) {
                            $callback["success"]=true;
                    } else {
                            $callback["success"]=false;
                            $callback["msg"]="寄送失敗";
                    }
                }
                else{
                    $callback["success"]=false;
                }
            }
            return $callback;
    }
    
    private function insertAPI( $cca_user_id, $cca_call_user_id, $cca_command_id, $cca_command_user ){
            
            $callback = array();
            $DBCMD = DB::table('chat_command_api')->insert(
                [   'cca_user_id' => $cca_user_id, 
                    'cca_call_user_id' => $cca_call_user_id, 
                    'cca_command_id' => $cca_command_id, 
                    'cca_command_user' => $cca_command_user,
                    'cca_call_time' => date('Y-m-d H:i:s'), 
                    'cca_state' => 0 ]
            );
            if (!$DBCMD) {
                $callback["success"]=false;
                $callback["msg"]="create api fail";
            }
            
            $callback["success"]=true;
            return $callback;
    }
}

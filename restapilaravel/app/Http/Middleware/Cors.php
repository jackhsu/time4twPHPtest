<?php

namespace App\Http\Middleware;
use Closure;

class Cors {
    public function handle($request, Closure $next)
    {
            header("Access-Control-Allow-Origin:*");
            header('Access-Control-Allow-Credentials:true');
            header('Access-Control-Allow-Methods:GET, POST, PUT, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers:Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
            header('Content-Type:text/html; charset=utf-8');
        return $next($request);
    }
}
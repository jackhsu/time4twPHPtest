<?php

namespace App\Http\Middleware;
use Closure;

class HandleCors {
    
    public function handle($request, Closure $next)
    {
//        echo 1231231231;
        return $next($request)
          ->header('Access-Control-Allow-Origin', '*')
          ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
    
}
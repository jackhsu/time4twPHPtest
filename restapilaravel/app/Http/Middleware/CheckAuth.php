<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Http\Response;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    
    public function handle($request, Closure $next)
    {
        
        $callback = array();
//        $header = $request->headers->all();
//        $authString = $header["authorization"][0];
        if(empty($_COOKIE['lnbCookie'])){
            return new Response(view('register'));
        }
        else
            $authString = 'Token '. $_COOKIE['lnbCookie'];//$request->cookie('lnbCookie');//Cookie::get('lnbCookie')
        $token_info = $this->verifyTokenAuthentication($authString);
        if (strpos($authString, 'Token') !== false && $token_info !== false ) {
            if( $token_info[0]["userinfo_Status"] === "inactive" ){
                $callback["success"] = false;
                $callback["msg"] = "your account has been deleted";
                return $this->jsonResponse( $callback);
            }
            $request["token_info"] = $token_info;
            return $next($request);
        }

//        $newResponse = $response->withStatus(401);
//        return $newResponse;
        $callback["success"] = false;
        $callback["msg"] = "TOKEN Error";
        return $this->jsonResponse( $callback);
        
        
        
        if (strpos($authString, 'Basic') !== false) {
                $this->verifyBasicAuthentication($request,$authString);
        }
        
        return $next($request);
    }
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
        echo json_encode($data);
        exit;
    }
    
    private function verifyTokenAuthentication($authString) {
        $pos = strpos($authString, "Token");
        if ( $pos === false ) {
            return false;
        }        
        $token = trim(substr($authString, $pos+5));
        return $this->verifyToken($token);
    }
    
    public function verifyToken($token) {
        $token_info = $this->findTokenInDB($token);
        if ( $token_info !== false ) {
            return $token_info;
        }
        return false;
    }
    
    private function findTokenInDB($token) {
        $authentication = DB::table('authentication')
            ->join('userinfo', 'userinfo.userinfo_ID', '=', 'authentication.userinfo_ID')
//            ->select('users.*', 'contacts.phone', 'orders.price')
            ->where('authentication.authentication_token', $token)
            ->get();
        if ($authentication->count()===0) {
            return false;
        }
        return json_decode(json_encode($authentication), true);
        
        return false;
    }
    
}

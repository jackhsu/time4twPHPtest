<?php

namespace App\Http\Middleware;

require_once ( __DIR__ . "/../Lib/DatabaseHelper.php");
use Closure;
use Illuminate\Support\Facades\DB;

class BaseAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $header = $request->headers->all();
        $authString = $header["authorization"][0];//"Basic ODExYzUyZGQxMGFlNmJhMGM5NDY1ZWJjNGE0YzlkYmQ=";//
        
        if (strpos($authString, 'Basic') !== false) {
                $this->verifyBasicAuthentication($request,$authString);
                return $next($request);
        }
        
    }
    
    public function verifyBasicAuthentication($request,$authString)
    {
        
        $userinfo_UserName = $request->userinfo_UserName;
        if ( empty($request->userinfo_UserName) )
        {
            return $this->parameterErrorResponse();
        }
        
        $pwd = trim(substr($authString, 5));
        
//        $searchData = array();
//        $searchData["userinfo_UserName"] = $userinfo_UserName;
//        $searchData["userinfo_Password"] = $pwd;
        
        // Email matches User Name
        // escape from '@' at 
        $users = DB::table('userinfo')->where([
            ['userinfo_Email', '=',  urldecode($userinfo_UserName)],
            ['userinfo_Password', '=', $pwd],
        ])->get();
        
        //print_r($users);
        
        if ($users->count()===0) {
            $callback["success"] = false;
            $callback["msg"] = "Email or Password Error";//"帳號或密碼錯誤";
            return $this->jsonResponse($callback);
        }
	
/*
        $users = DB::table('userinfo')->where([
            ['userinfo_UserName', '=', $userinfo_UserName],
            ['userinfo_Password', '=', $pwd],
        ])->get();
        
//        print_r($users);
        
        if ($users->count()===0) {
            $callback["success"] = false;
            $callback["msg"] = "Account or Password Error";//"帳號或密碼錯誤";
            return $this->jsonResponse($callback);
        }
	*/
        
        $user = json_decode(json_encode($users), true)[0];
        
        if( $user["userinfo_Status"] === "inactive" ){
            $callback["success"] = false;
            $callback["msg"] = "your account has been deleted";
            return $this->jsonResponse($callback);
        }
        
        if ( !empty($user["userinfo_ID"]) )
        {
            $token = $this->createToken($user["userinfo_ID"]);
            if ($token === false) {
                return $this->parameterErrorResponse();
            }

            $callback['data'] = $token;
            $callback['success'] = true;
            return $this->jsonResponse($callback);            
        }
        
        return $this->parameterErrorResponse();
        
    }
    
    public function parameterErrorResponse() {
        $callback = array();
        $callback['msg'] = "parameter error.";
        $callback['success'] = false;
        echo json_encode($callback);
        exit;
    }
    
    public function jsonResponse($data) {
        echo json_encode($data);
        exit;
    }
    
    public function createToken($userinfo_ID) {
        return $this->setRandomToken($userinfo_ID);
    }

    // build & set token
    private function setRandomToken($userinfo_ID) {
        $md5_token = $this->forLoopBuildRandomToken();
        
        $authentication = DB::table('authentication')
            ->where('userinfo_ID', $userinfo_ID)
            ->get();
        if ($authentication->count()>=env('DB_MEMBER_COUNT',10)) {
                $DBCMD = DB::table('authentication')
                        ->where('userinfo_ID', $userinfo_ID)
                        ->take($authentication->count()-env('DB_MEMBER_COUNT',10)+1)
                        ->orderBy('authentication_CreateDateTime', 'asc')
                        ->delete();
                if (!$DBCMD) {
                        $callback["success"]=false;
                        $callback["msg"]="Clear cache fail";
                        echo json_encode($callback);
                        exit;
                }
        }
        
        // Insert to authentication table
        $DBCMD = DB::table('authentication')->insert(
            ['userinfo_ID' => $userinfo_ID, 'authentication_token' => $md5_token, 'authentication_CreateDateTime' => date('Y-m-d H:i:s')]
        );
        
        $datatime = date('Y-m-d H:i:s');
        
        if ($DBCMD) {
            //updata last login time
//            $sql = "UPDATE userinfo SET userinfo_LastLoginTime='$datatime',userinfo_LastLoginIp='".$_SERVER['REMOTE_ADDR']."' WHERE userinfo_ID='$userinfo_ID'";
//            $stmt = $this->db->prepare($sql);
//            $stmt->execute();
            DB::table('userinfo')
                ->where('userinfo_ID', $userinfo_ID)
                ->update(['userinfo_LastLoginTime' => $datatime, 'userinfo_LastLoginIp' => $_SERVER['REMOTE_ADDR']]);
            
            return $md5_token;
        }

        return false;
    }

    // build token
    private function forLoopBuildRandomToken() {
        while (1) {
            $token = getRandom(20);
            $md5_token = md5($token);
            if ( $this->findTokenInDB($md5_token) === false ) {
                break;
            }
        }
        return $md5_token;
    }

    private function findTokenInDB($token) {
        $authentication = DB::table('authentication')
            ->join('userinfo', 'userinfo.userinfo_ID', '=', 'authentication.userinfo_ID')
//            ->select('users.*', 'contacts.phone', 'orders.price')
            ->where('authentication.authentication_token', $token)
            ->get();
        if ($authentication->count()===0) {
            return false;
        }
        return json_decode(json_encode($authentication), true);
        
//        $sql = "SELECT * FROM " . $this->tableName . " au JOIN userinfo u on u.userinfo_ID=au.userinfo_ID WHERE au.authentication_token = '" . $token . "'";
//        $stmt = $this->db->prepare($sql);
//        if ($stmt->execute()) {
//            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
//            $data = array_filter($data);
//            if ( !empty($data) ) {
//                return $data;
//            }
//        }
        return false;
    }
    
}

<?php
//define('DEFAULT_JACK_AROFLY_EXCEL_PATH', "C:\\xampp\\htdocs\\jack\\arofly\\excel\\" );
define('DEFAULT_JACK_AROFLY_EXCEL_PATH', "C:\\xampp\\htdocs\\arofly\\excel\\" );
define('DEFAULT_AROFLY_EXCEL_PATH', "C:\\xampp\\htdocs\\arofly\\excel\\" );

$func = $_REQUEST['func'];

switch ($func) {
    case 'upload_csv':
        upload_csv();
        break;
}    

    /**
     * 2017-05-26 justin
     *
     * 上傳"Arofly CSV"
     * URL: Arofly/excel/excel.php?func=upload_csv
     */
    function upload_csv() {
        $callback = array();
        $token = $_REQUEST['token'];
        
        $con=mysqli_connect('localhost','root','520520op','arofly');
        $con->query("SET NAMES utf8");
        if (mysqli_connect_errno()) {
                $callback['msg'] = "MySQL connect fail";
                $callback['success'] = false;
                return $callback;
        }

        //token換userinfo_id
        $res = mysqli_query($con, "SELECT u.userinfo_id "
                . "FROM authentication as auth "
                . "JOIN userinfo as u on u.userinfo_ID = auth.userinfo_ID "
                . "WHERE auth.authentication_token = '" . $token . "'");
        if( mysqli_num_rows($res) == false ) {
                mysqli_close($con);
                $callback['msg'] = "user not found";
                $callback['success'] = false;
                return $callback;
        }
        $userinfo_ID = (int)mysqli_fetch_array($res,MYSQLI_NUM)[0];
                
        $date4filename = date('Y-m-d_His');
        
        //會員資料夾路徑, false=>新開會員資料夾
        $jack_user_path = DEFAULT_JACK_AROFLY_EXCEL_PATH . $userinfo_ID;
        if(!is_dir($jack_user_path)) mkdir($jack_user_path, 0777, true);
        
        //副檔名
        $FileName = $_FILES["file"]['name'];
        $FileMainSub = explode( "." , $FileName );
        $ext = $FileMainSub[count($FileMainSub)-1];
        
        if( move_uploaded_file( $_FILES["file"]["tmp_name"] , $jack_user_path . "\\$date4filename.$ext" )  ) {
                
                if( $ext == 'afr' ){
                        $cmd = "Transfer.exe " . $jack_user_path . "\\$date4filename.$ext";
                        exec($cmd, $arr, $output);
                        $ext = 'csv';
                }
                
                $callback_csv_import_db = csv_import_db($con, $userinfo_ID, "$userinfo_ID\\$date4filename.$ext");
                if($callback_csv_import_db['success'] == true)
                {
                    $callback['data_record_date'] = $callback_csv_import_db['data_record_date'];
                    $callback['success'] = true;
                }
                else
                {
                    $callback['msg'] = $callback_csv_import_db['msg'];
                    $callback['success'] = false;
                }
        } else {
                $callback['msg'] = "Upload fail";
                $callback['success'] = false;
        }

        echo json_encode($callback);
    }

    function csv_import_db($con, $userinfo_ID, $relative_path) {
        include 'Classes/PHPExcel.php';
        ini_set('memory_limit',-1);
        set_time_limit(0);    

        $file = $relative_path;
//        $file = '2017-05-08-Arofly.xlsx';
        $is_overwrite = 1;
        $input = array(
            
                        ['B1', 'userinfo_format'],
                        ['B2', 'userinfo_date_time'],
                        ['B3', 'userinfo_device_id'],
                        ['B4', 'userinfo_console_id'],
            
                        ['B5', 'userinfo_appv'],/*0526新增(資料表 及此欄位)*/
                        ['B6', 'userinfo_cfv'],/*0526新增(資料表 及此欄位)*/
            
                        ['B7', 'userinfo_email'],
                        ['B8', 'userinfo_birth'],
                        ['B9', 'userinfo_weight'],
                        ['B10', 'userinfo_unit'],//欄位格式text 預備未來存json
                        ['B11', 'userinfo_bike_mode'],//欄位格式text 預備未來存json
                        ['B12', 'userinfo_training_mode'],//欄位格式text 預備未來存json
                        ['B13', 'userinfo_tire_size'],//疑似沒建表
                        ['B14', 'userinfo_indoor']//欄位格式text 預備未來存json
                        
                        /*['B15', 'userinfo_calorie']0613刪除(資料表 及此欄位)*/
            
            /* 0526刪除(資料表 及此欄位)
                        ['B13', 'userinfo_version'],
                        ['B14', 'userinfo_firmware_version'],
             */
                      );
        
                $oneTimeInfo = array();
                $data = array();

                try {
                        $objPHPExcel = PHPExcel_IOFactory::load($file);
                } catch(Exception $e) {
                        die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
                }

//var_dump( $input); exit;
                $num = count($input);
                for($i=0; $i<$num; $i++) {
                        $oneTimeInfo[$input[$i][1]] /*表名稱 (=欄位名稱)*/ = $objPHPExcel->getActiveSheet()->getCell($input[$i][0])->getFormattedValue() /*.csv 該B直欄的值*/;
                }

                $objPHPExcel->setActiveSheetIndex(0);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,null,true,true);

                $time_s = array(); $pam_s = array(); $ahz_s = array(); $phz_s = array(); $paz_s = array(); 
                $speed_s = array(); $cadence_s = array(); $power_s = array(); $psensor_s = array(); $distance_s = array(); 
                $longitude_s = array(); $latitude_s = array(); $altitude_s = array(); $HRM = array(); $calorie = array(); 
                $loop_pm = array(); $status = array();
                $otherValue = array();
    //            var_dump($sheetData); exit;
                $i = 0;
                foreach($sheetData as $key/*1開始*/ => $col)
                {
                                if( $key <= 15 ){//起點: 第15列, 以上不讀
                                    $otherValue[] = $col['B'];
                                    continue;
                                }
                                
                                if( strlen($sheetData[$key]["A"]) != 19 ){//終點: A行之時間資料(如: 5/23/2017 8:28,長度為14), 不為時間的長度時結束
                                    /* $col["A"] ="" OR $col["B"] ="" OR $col["C"] ="" OR $col["D"] ="" OR $col["E"] ="" OR 
                                    $col["F"] ="" OR $col["G"] ="" OR $col["H"] ="" OR $col["I"] ="" OR $col["J"] ="" OR 
                                    $col["K"] ="" OR $col["L"] ="" OR $col["M"] ="" OR $col["N"] ="" OR $col["O"] ="" */
                                    break;
                                }

                                foreach ($col as $colkey/*A*/ => $colvalue /*value*/ ) 
                                {
                                        $colvalue = (string)$colvalue;

                                        if( $colkey == 'A' ){
                                                $time_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'B' ){
                                                $pam_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'C' ){
                                                $ahz_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'D' ){
                                                $phz_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'E' ){
                                                $paz_s[$i] = $colvalue;
                                        }

                                        
                                        if( $colkey == 'F' ){
                                                $longitude_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'G' ){
                                                $latitude_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'H' ){
                                                $altitude_s[$i] = $colvalue;
                                        }
										
                                        // 'I' GPS_LINK_QUALITY_SCORE

                                        // 'J' GPS_GCS_INFO

										
                                        if( $colkey == 'K' ){
                                                $status[$i] = $colvalue;
                                        }
                                        if( $colkey == 'L' ){
                                                $loop_pm[$i] = $colvalue;
                                        }
										
                                        
                                        if( $colkey == 'M' ){
                                                $speed_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'N' ){
                                                $cadence_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'O' ){
                                                $power_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'P' ){
                                                $psensor_s[$i] = $colvalue;
                                        }
                                        if( $colkey == 'Q' ){
                                                $distance_s[$i] = $colvalue;
                                        }
                                        
                                        
                                        if( $colkey == 'R' ){
                                                $HRM[$i] = $colvalue;
                                        }
                                        if( $colkey == 'S' ){
                                                $calorie[$i] = $colvalue;
                                        }
                                        if( $colkey == 'T' ){//R(含)開始即無資料
                                                break;
                                        }
                                }
                                $i++;
                }            

                //------------------------------------------------------------------
                
                //email換userinfo_id
//                $res = mysqli_query($con, "SELECT userinfo_id FROM userinfo WHERE userinfo_Email = '" . $oneTimeInfo['userinfo_email'] . "'");
//                if( mysqli_num_rows($res) <= 0 ) {
//                        mysqli_rollback($con);
//                        mysqli_close($con);
//                        $callback['msg'] = "user's email not found";
//                        $callback['success'] = false;
//                        return $callback;
//                }
//                $userinfo_id = (int)mysqli_fetch_array($res,MYSQLI_NUM)[0];

                // echo "SELECT console_id FROM console WHERE time_s = '" . $oneTimeInfo['userinfo_date_time'] . "' AND userinfo_id=$userinfo_ID" ;
                //阻擋重複上傳
                $res = mysqli_query($con, "SELECT console_id FROM console WHERE time_s = '" . $oneTimeInfo['userinfo_date_time'] . "' AND userinfo_id=$userinfo_ID");
                if( mysqli_num_rows($res) > 0 ) {
                        mysqli_close($con);
                        $callback['msg'] = "file exist ," . $oneTimeInfo['userinfo_date_time'] ; // userinfo_id, time_s exist
                        $callback['success'] = false;
                        return $callback;
                }

                $sql_arr = array();
                
                //console表 INSERT
                $sql_arr[count($sql_arr)] = "INSERT INTO `console` ( userinfo_id "
                                . ", FORMAT, DATETIME, DEVICEID, CONSOLEID, APPV, CFV, EMAIL"
                                . ", BIRTH, WEIGHT, UNIT, BIKE, TRAINING, TIRE, INDOOR"
                                . ", time_s, pam_s, ahz_s, phz_s, paz_s"
                                . ", speed_s, cadence_s, power_s, psensor_s, distance_s"
                                . ", longitude_s, latitude_s, altitude_s, status, loop_pm"
                                . ", HRM, calorie) VALUES ( $userinfo_ID, "
                            ."'". $otherValue[0] ."', '". $otherValue[1] ."', '". $otherValue[2] ."', '". $otherValue[3] ."', '". $otherValue[4] ."', '". $otherValue[5] ."', '". $otherValue[6] ."', "
                            ."'". $otherValue[7] ."', '". $otherValue[8] ."', '". $otherValue[9] ."', '". $otherValue[10] ."', '". $otherValue[11] ."', '". $otherValue[12] ."', '". $otherValue[13] ."', "
                            ."'". $oneTimeInfo['userinfo_date_time'] ."', '". json_encode($pam_s) ."', '". json_encode($ahz_s) ."', '". json_encode($phz_s) ."', '". json_encode($paz_s) ."', "
                            . "'". json_encode($speed_s) ."', '". json_encode($cadence_s) ."', '". json_encode($power_s) ."', '". json_encode($psensor_s) ."', '". json_encode($distance_s) ."', "
                            . "'". json_encode($longitude_s) ."', '". json_encode($latitude_s) ."', '". json_encode($altitude_s) ."', '". json_encode($status) ."', '". json_encode($loop_pm) ."', "
                            . "'". json_encode($HRM) ."', '". json_encode($calorie) ."')";


                //$oneTimeInfo->key表 INSERT / UPDATE
                foreach( $oneTimeInfo as $key/*表名稱 (=欄位名稱)*/ => $value ){
                        if( $key == 'userinfo_email' ){ continue; } //此為帳號, 不再多存表
                    
                        $res = mysqli_query($con, "SELECT * FROM `$key` WHERE userinfo_id=$userinfo_ID");
                        if( mysqli_num_rows($res) > 0 ) {
                                $sql_arr[count($sql_arr)] = "UPDATE `$key` SET `$key`='$value' WHERE userinfo_id =$userinfo_ID";
                        } else {
                                $sql_arr[count($sql_arr)] = "INSERT INTO `$key` SELECT $userinfo_ID, '$value'";
                        }
                }

                $statements = implode( "; ", $sql_arr);
                mysqli_autocommit($con, false);
                $j = 0;
                if (mysqli_multi_query($con, $statements)) {
                        do {
                                $j++;
                        } while (mysqli_next_result($con));
                }

                if (mysqli_errno($con)) {
                        mysqli_rollback($con);//mysqli_multi_query, mysqli_next_result  所有query皆取消
                        $callback['msg'] = $sql_arr[$j];//"ended on: $sql_arr[$j] ". mysqli_error($con);
                        $callback['success'] = false;
                        return $callback;
                }

                mysqli_commit($con);
                mysqli_close($con);


                $callback['data_record_date'] = $oneTimeInfo['userinfo_date_time'];
                $callback['success'] = true;
                return $callback;

        }

                    
    function getServerMemoryUsage($getPercentage=true)
    {
        $memoryTotal = null;
        $memoryFree = null;

        if (stristr(PHP_OS, "win")) {
            // Get total physical memory (this is in bytes)
            $cmd = "wmic ComputerSystem get TotalPhysicalMemory";
            @exec($cmd, $outputTotalPhysicalMemory);

            // Get free physical memory (this is in kibibytes!)
            $cmd = "wmic OS get FreePhysicalMemory";
            @exec($cmd, $outputFreePhysicalMemory);

            if ($outputTotalPhysicalMemory && $outputFreePhysicalMemory) {
                // Find total value
                foreach ($outputTotalPhysicalMemory as $line) {
                    if ($line && preg_match("/^[0-9]+\$/", $line)) {
                        $memoryTotal = $line;
                        break;
                    }
                }

                // Find free value
                foreach ($outputFreePhysicalMemory as $line) {
                    if ($line && preg_match("/^[0-9]+\$/", $line)) {
                        $memoryFree = $line;
                        $memoryFree *= 1024;  // convert from kibibytes to bytes
                        break;
                    }
                }
            }
        }
        else
        {
            if (is_readable("/proc/meminfo"))
            {
                $stats = @file_get_contents("/proc/meminfo");

                if ($stats !== false) {
                    // Separate lines
                    $stats = str_replace(array("\r\n", "\n\r", "\r"), "\n", $stats);
                    $stats = explode("\n", $stats);

                    // Separate values and find correct lines for total and free mem
                    foreach ($stats as $statLine) {
                        $statLineData = explode(":", trim($statLine));

                        // Total memory
                        if (count($statLineData) == 2 && trim($statLineData[0]) == "MemTotal") {
                            $memoryTotal = trim($statLineData[1]);
                            $memoryTotal = explode(" ", $memoryTotal);
                            $memoryTotal = $memoryTotal[0];
                            $memoryTotal *= 1024;  // convert from kibibytes to bytes
                        }

                        // Free memory
                        if (count($statLineData) == 2 && trim($statLineData[0]) == "MemFree") {
                            $memoryFree = trim($statLineData[1]);
                            $memoryFree = explode(" ", $memoryFree);
                            $memoryFree = $memoryFree[0];
                            $memoryFree *= 1024;  // convert from kibibytes to bytes
                        }
                    }
                }
            }
        }
    }

    function insert_sql_mysql( $con , $table , $insert_array )
    {
            $bool = false;
            $sql_column = "";
            $sql_value = "";                   
            foreach ($insert_array as $key => $value) {

                $sql_column = $sql_column."`".$key."` ,";

                if( gettype($value) == "NULL" ) {
                    $sql_value = $sql_value."'NULL' ,";
                } else if ( gettype($value) == "integer"){
                    $sql_value = $sql_value.$value." ,";
                } else if ( $value == ""){
                    $sql_value = $sql_value."'' ,";
                }  else {
                    $sql_value = $sql_value."'".$value."' ,";
                }
            }
            $sql_column = substr( $sql_column ,0 ,-1);
            $sql_value = substr( $sql_value   ,0 ,-1);

            $sql = "INSERT INTO `$table` ( $sql_column ) VALUES ( $sql_value )";
            if ( mysqli_query($con, $sql) ) {
                $bool = true; 
            }  
            return $bool;
    }         

    function update_sql( $con , $table , $json , $keyword ) 
    {
            $bool = true;
            $set_value = "";   
            $key_string = "";

            foreach ($json as $key => $value) {
                if(gettype($value) == "string") {
                    $set_value = $set_value." `$key`='$value',";
                } else {
                    $set_value = $set_value." `$key`=$value,";
                }
            }
            $set_value = substr( $set_value ,0 ,-1);

            foreach ($keyword as $key => $value) {
                if(gettype($value) == "string") {
                    $key_string = $key_string." `$key`= '$value' AND";
                } else if( gettype($value) == "integer" ) {
                    $key_string = $key_string." `$key`=$value AND";
                } else if ( $value == ""){
                    $key_string = $key_string." `$key`= '' AND";
                } else {
                    $value = (string) $value;
                    $key_string = $key_string." `$key`= $value AND";
                }
            } 

            $key_string = substr( $key_string ,0 ,-3);
            $sql = "UPDATE `$table` SET  $set_value WHERE $key_string";

            if ( mysqli_query( $con , $sql ) ) {
                $bool = true;
            } else {
                $bool = false;
            }
            return $bool;
    }    
    
?>
    
    

<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>It's Time for Taiwan Now</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    <div id="fb-root"></div>
    <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
</head>

<body>
    <?php include_once("incl/googleTagBody.php"); ?>

    <?php include_once("incl/nav.php"); ?>
    <!-- share buttons -->
    <ul class="share hidden-sm hidden-xs">

        <!-- like button code -->
        <li class="" data-href="http://ec2-54-69-68-125.us-west-2.compute.amazonaws.com/time4twPHP/" data-layout="button" data-size="small" data-mobile-iframe="true">
            <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.go2taiwan.net%2Fjack%2Fgo2taiwan%2Ffrontend%2F&amp;src=sdkpreparse">
                <span class="ti-facebook"></span>
            </a>
        </li>
        <!-- tweet button code -->
        <li class="">
            <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=http://www.go2taiwan.net/#" data-size="large" target="_blank">
                <span class="ti-twitter-alt"></span></a>
        </li>
    </ul>
    <!-- end of share buttons -->

    <div class="main-container" id="main">
        <!-- bg-Hero -->
        <section class="kenburns cover fullscreen image-slider slider-all-controls mb30">
            <ul class="slides">
                <!-- <li class="vid-bg image-bg overlay">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
                    </div>
                    <div class="fs-vid-background">
                        <video muted="" loop="">
                            <source src="video/video.webm" type="video/webm">
                            <source src="video/video.mp4" type="video/mp4">
                            <source src="video/video.ogv" type="video/ogg">
                        </video>
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>
                            </div>
                        </div>
                    </div>
                </li> -->
                <li class="image-bg overlay">
<!--                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" style="zIndex: 2; opacity: 1;" src="img/Main-Hero.jpg" >
                    </div>-->
                    <div class="background-image-holder" v-bind:style="{ 'background-image': 'url(' + imgPath + detail11.image + ')' }">
                           <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>
                    <!--<div class="background-image-holder" >
                        <img alt="Background Image" class="background-image">
                    </div>-->
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/Main-Hero-logo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section><!-- end bg-Hero -->

        <!-- Places To Go -->
        <section class="main-pad">

            <div class="container">
                <!--                 <div class="row masonry-loader">
                    <div class="col-sm-12 text-center">
                        <div class="spinner"></div>
                    </div>
                </div> -->
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold">Places To Go</h1>

                <div class="row">
                    <!-- <div class="row masonry masonryFlyIn"> -->
                    <div class="col-md-8 project zoomin p2go">
                        <div class="image-tile inner-title">
                            <a href="place-to-go.php#northern">
                                <img alt="Pic" v-bind:src="imgPath + detail12.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="detail12.title" ></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6  masonry-item project zoomin" v-for="main13 in searched_table_data13">
                        <div class="image-tile inner-title">
                            <a href="place-to-go.php#southern">
                                <img alt="Pic" v-bind:src="imgPath + main13.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main13.title"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>                        
                    </div>

                    <!-- <div class="col-md-4 col-sm-6  masonry-item project zoomin">
                        <div class="image-tile inner-title">
                            <a href="place-to-go.php#eastern">
                                <img alt="Pic" src="img/place-main-3.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Eastern Taiwan</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>-->

                </div>

            </div>
        </section><!-- end of Places To Go -->

        <!-- Things to do -->
        <section class="main-pad ">

            <div class="container">

                <!--<div class="row masonry-loader">
                   <div class="col-sm-12 text-center">
                       <div class="spinner"></div>
                   </div>
               </div> -->
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold">Things to do</h1>

                <div class="row masonry masonryFlyIn">
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 1" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 2" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 3" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="temples.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 4" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="cuisine.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 5" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="outdoor.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 6" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="wellness.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 7" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="ecotourism.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 masonry-item project zoomin" v-show="main2.id == 8" v-for="main2 in searched_table_data2">
                        <div class="image-tile inner-title ">
                            <a href="festivals.php">
                                <img alt="Pic" v-bind:src="imgPath + main2.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="main2.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- end of Things to do -->


        <!-- What's Happening -->
        <section class="main-pad ">
            <div class="container " >
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold">What's Happening</h1>
                <div class="image-tile inner-title zoomin" v-for="main3 in searched_table_data3">
                    <a href="event.php">
                        <img alt="Pic" v-bind:src="imgPath + main3.image">
                        <div class="title">
                            <h5 class="uppercase mb0"  v-html="main3.title"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
        </section><!-- end of What's Happening -->

        <!-- Deals -->
        <section class="main-pad ">

            <div class="container">

                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold">DEALS</h1>

                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/enchanting-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic"  v-for="main4 in searched_table_data4" v-show="main4.id == 2" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div  v-for="main4 in searched_table_data4" v-show="main4.id == 2" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Enchanting Taiwan</h5><h6 class="bookNowPrice">From USD $961</h6><p class="bookNowText">This 8 day tour of Taiwan highlights the must see attractions efficiently! You will visit Taipei, Sun Moon Lake, Kaohsiung, Kenting National Park, Hualien, and the Taroko Gorge</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/enchanting-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 3" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 3" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Enchanting Taiwan</h5><h6 class="bookNowPrice">From USD $961</h6><p class="bookNowText">This 8 day tour of Taiwan highlights the must see attractions efficiently! You will visit Taipei, Sun Moon Lake, Kaohsiung, Kenting National Park, Hualien, and the Taroko Gorge</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/taste-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 4" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 4" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taste of Taiwan</h5><h6 class="bookNowPrice">From USD $2,830</h6><p class="bookNowText">On this 7-day culinary tour of Taiwan, You will take a cooking class and sample foods at a famous Taiwanese night market. You will also ride Taiwan's high-speed bullet train to Sun Moon Lake, Taiwan’s largest body of water.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/zjsthU" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 5" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 5" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">8 Days Taiwan Island Classic Tour</h5><h6 class="bookNowPrice">From USD $1,239</h6><p class="bookNowText">Start this 8-day tour in Taipei at the National Palace Museum and one of Taiwan's famous night markets! On this tour, you will visit Sun Moon Lake, Alishan National Scenic Area, Lukang Town, and have extra free time in Taipei!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.supervaluetours.com/type_tourtype.do?tid=50&fr=supusa" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 6" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 6" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taiwan Island Tour 10 days</h5><h6 class="bookNowPrice">From USD $2,049</h6><p class="bookNowText">This island tour will feature the best of Taiwan! From soup dumplings to the largest collection of Chinese artifacts in Taipei to contemplating a rose-gold sunset at Sun Moon Lake, Taiwan is sure to surprise with this tour!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw8" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 7" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 7" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">8 Days Enchanting Taiwan Tour</h5><h6 class="bookNowPrice">From USD $950</h6><p class="bookNowText">On this 8-day tour around Taiwan, you will be visiting Taipei, Sun Moon Lake, Fo Guang Shan Buddhist Monastery, Kaohsiung, Kenting National Park, East Coast & Taroko National Park.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw5" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 8" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 8" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">5 Days Around Taiwan Tour</h5><h6 class="bookNowPrice">From USD $590</h6><p class="bookNowText">During this 5-day tour, you will be visiting Sun Moon Lake, Kaohsiung, Kenting, Taitung, Hualien, and the Taroko Gorge.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.intrepidtravel.com/us/taiwan/explore-taiwan-100826" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 9" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 9" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">10% OFF Intrepid Travel's Explore Taiwan tour</h5><h6 class="bookNowPrice">From USD $2,415</h6><p class="bookNowText">This small group tour will see Taiwan's exciting food culture, hike, ride and kayak around Sun Moon Lake and visit both the Qingshui Cliffs and Taroko Gorge. Book until August 20th with promo code "50146" for 10% off your booking.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/LXzALk" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 10" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 10" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Vancouver to Hong Kong, plus 3-day stopover in Taipei</h5><h6 class="bookNowPrice">From CAD $1,088</h6><p class="bookNowText">This direct flight from Vancouver to Hong Kong includes an optional 3-day stopover in Taipei! Enjoy 3 nights stay in Taipei with breakfast included. Subway cards for Taipei and Hong Kong will also be provided.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.bicycleadventures.com/destinations/taiwan-bike-tours/Taiwan-Bike-Tour" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 11" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 11" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taiwan Bicycle Tour with Silks Spa Experience</h5><h6 class="bookNowPrice">From USD $5,498</h6><p class="bookNowText">This 11-day bike tour offers days of riding through Taiwan's sparsely populated south and east, filled with national parks, rice paddies, palm trees, aboriginal villages, colorful temples and incredibly friendly people.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://campbelltravel.bc.ca/en/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 12" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 12" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taipei Free and Easy </h5><h6 class="bookNowPrice">From CAD $738</h6><p class="bookNowText">3 nights stopover in Taipei with breakfast, include round trip air to one of the following destinations: Hong Kong, Seoul, Osaka or Nagoya. </p>-->
                                </div>
                            </a>
                        </div>
                    </div> 
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.panagoetour.com/colourful-north-date-and-price" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 13" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 13" v-html="main4.description2"></div>
                                    <!--<h5 class="bookNowTitle">Colorful North</h5><h6 class="bookNowPrice">From USD $945</h6><p class="bookNowText">This 4-day package tours Northern Taiwan and features extinct volcanoes, hot springs, and extraordinary rock formations along the coast! Explore the nature of Taiwan on this short but exciting trip!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>  
<!--                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.sonictour.com/Formosa%20Star%20Train%20First%20Class%20Experiences%20%28TWR86%29%20-935.html#.WU2pSRIrKV5" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="main4 in searched_table_data4" v-show="main4.id == 14" v-bind:src="imgPath + main4.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="main4 in searched_table_data4" v-show="main4.id == 14" v-html="main4.description2"></div>
                                    <h5 class="bookNowTitle">Formosa Star Train First Class Experiences</h5><h6 class="bookNowPrice">Please contact Tour Operator for pricing</h6><p class="bookNowText">Discover the magic of train travel and tour Taiwan to the fullest with this 8-day tour! This tour includes Taipei, Sun Moon Lake, Kaohsiung, Zhiben hot springs, Hualien, and the Taroko Gorge. </p>
                                </div>
                            </a>
                        </div>
                    </div>-->
                </div>
            </div>
        </section><!-- end of Deals -->

        <!-- social media -->
        <section class="bg-primary">
            <div class="container">
                <div class="col-md-6">
                    <div class="social-container social-facebook">
                        <h3 class="social">
                            <a href="https://www.facebook.com/itstimefortaiwan/" target="_blank">
                                <span class="ti-facebook icon-sm popout"></span>
                                Facebook
                                <span class="small text-gray text-sans-serif text-regular">#TimeforTaiwan</span>
                            </a>
                        </h3>
                        <div class="social-content">
                            <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fitstimefortaiwan%2Fposts%2F654372571433106%3A0&amp;width=500" width="500" height="486" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="social-container social-instagram">
                        <h3 class="social">
                            <a href="https://www.instagram.com/ttb_na/" target="_blank">
                                <span class="ti-instagram icon-sm popout"></span>
                                Instagram
                                <span class="small text-gray text-sans-serif text-regular">@ttb_na</span>
                            </a>
                        </h3>
                        <div class="social-content widget">
                            <ul class="instafeed">
                                <?php include_once("incl/instaFeed.php"); ?> 
                            </ul>
                            <!--                                    <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-1.jpg" nopin="nopin" src="./img/insta-1.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-2.jpg" nopin="nopin" src="./img/insta-2.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-3.jpg" nopin="nopin" src="./img/insta-3.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-4.jpg" nopin="nopin" src="./img/insta-4.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-5.jpg" nopin="nopin" src="./img/insta-5.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-6.jpg" nopin="nopin" src="./img/insta-6.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-7.jpg" nopin="nopin" src="./img/insta-7.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-8.jpg" nopin="nopin" src="./img/insta-8.jpg" style="display: block;">
                                                               </a>
                                                               <a class="social-instagram-image" href="#" target="_blank">
                                                                   <img alt="description" class="img-responsive  lazy" data-original="./img/insta-9.jpg" nopin="nopin" src="./img/insta-9.jpg" style="display: block;">
                                                               </a> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end of social media -->

        <section class="main-pad bg-subscribe">
            <div class="container">
                <div class="row mb32 mt32 mb-xs-24">
                    <div class="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 text-center">
                        <h1 class="uppercase">SUBSCRIBE HERE</h1>
                    </div>
                </div>

                <div class="row mb0 mb-xs-24">
                    <div class="col-sm-6 col-sm-offset-3">
                        <form class="form-newsletter halves" data-success="Thanks for your registration, we will be in touch shortly." data-error="Please fill all fields correctly.">
                            <input type="text" name="email" class="mb0 transparent validate-email validate-required signup-email-field" placeholder="Email Address">
                            <button type="submit" class="btn mb0">S E N D</button>
                            <iframe srcdoc="<div id="mc_embed_signup"><form action="//fooddy.us16.list-manage.com/subscribe/post?u=61fb25e95b5617a2c51547afb&amp;id=6801a9c635" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>    <div id="mc_embed_signup_scroll">    <h2>Subscribe to our mailing list</h2><div class="indicates-required"><span class="asterisk">*</span> indicates required</div><div class="mc-field-group">    <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span></label>    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"></div><div class="mc-field-group">    <label for="mce-FNAME">First Name </label>    <input type="text" value="" name="FNAME" class="" id="mce-FNAME"></div><div class="mc-field-group">    <label for="mce-LNAME">Last Name </label>    <input type="text" value="" name="LNAME" class="" id="mce-LNAME"></div>    <div id="mce-responses" class="clear">        <div class="response" id="mce-error-response" style="display:none"></div>        <div class="response" id="mce-success-response" style="display:none"></div>    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->    <div style="position: absolute; left: -5000px;"><input type="text" name="b_61fb25e95b5617a2c51547afb_6801a9c635" tabindex="-1" value=""></div>    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>    </div></form></div><script type='text/javascript' src='https://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script><!--End mc_embed_signup-->" class="mail-list-form">
                            </iframe>
                        </form>
                    </div>
                </div>

                <!--   <div class="row">
                      <div class="col-sm-12 text-center">
                          <p class="fade-half lead mb0">*Newsletters are sent bi-monthly</p>
                      </div>
                  </div> -->

            </div>

        </section> <!-- end of subscribe -->

<?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <div class="modal-strip bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 overflow-hidden">
                        <p class="mb0 pull-left"><strong>Cookie Disclaim</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. "</p>
                    </div>
                </div>
            </div>
        </div>
        <!--end Cookie-->
    </div>




    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        $(document).ready(function () {

            var vm = new Vue({
                el: "#main",
                data: {
                    imgPath: $.config.imgPath,
                    detail11: {},
                    detail12: {},
                    detail13: [],
                    detail2: [],
                    detail3: [],
                    detail4: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    //            searched_table_data11() {
                    //                let clone = JSON.parse(JSON.stringify(this.detail11));
                    //                let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    //                return temp
                    //            },
                    //            searched_table_data12() {
                    //                let clone = JSON.parse(JSON.stringify(this.detail12));
                    //                let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    //                return temp
                    //            },
                    searched_table_data13() {
                        let clone = JSON.parse(JSON.stringify(this.detail13));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + "" /*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    },
                    searched_table_data2() {
                        let clone = JSON.parse(JSON.stringify(this.detail2));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + "" /*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    },
                    searched_table_data3() {
                        let clone = JSON.parse(JSON.stringify(this.detail3));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + "" /*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    },
                    searched_table_data4() {
                        let clone = JSON.parse(JSON.stringify(this.detail4));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {

                    var ajax1 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'place_to_go'}});
                    var ajax2 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level1'}});
                    var ajax3 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'whats_happening'}});
                    var ajax4 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'deals'}});
                    $.when(ajax1, ajax2, ajax3, ajax4).then(function (res1, res2, res3, res4) {
                        //console.log(res1);
                        //var res = {res1, res2, res3, res4};
                        //console.log(res);

                        //---place_to_go---
                        $.each(JSON.parse(res1[0]).data, function (k, v) {
                            //console.log(v);
                            if (v.id == '1') {
                                console.log('id=1');
                                vm.detail11 = v;
                            } else if (v.id == '2') {
                                console.log('id=2');
                                vm.detail12 = v;
                            }
                        });
                        //console.log(vm.detail11);
                        //console.log(vm.detail12);

                        var tmp_arr1 = [];
                        $.each(JSON.parse(res1[0]), function (k, v) {
                            var reslen = v.length - 2;
                            console.log(reslen);
                            for (var i = 2; i < reslen; i++) {
                                console.log(v[i]);
                                tmp_arr1[ tmp_arr1.length ] = v[i];
                            }
                        });
                        vm.detail13 = tmp_arr1;
                        console.log(vm.detail13);


                        //---things_to_do_level1---
                        var tmp_arr2 = [];
                        $.each(JSON.parse(res2[0]).data, function (k, v) {
                            console.log(v);
                            tmp_arr2[ tmp_arr2.length ] = v;
                        });
                        vm.detail2 = tmp_arr2;
                        vm.imgPath = vm.imgPath;
                        console.log(vm.detail2);


                        //---whats_happening---
                        var tmp_arr3 = [];
                        $.each(JSON.parse(res3[0]).data, function (k, v) {
                            console.log(v);
                            if (tmp_arr3.length == 0)
                                if (v.title != "" && v.image != "")
                                {
                                    tmp_arr3[ tmp_arr3.length ] = v;
                                }
                        });
                        vm.detail3 = tmp_arr3;
                        vm.imgPath = vm.imgPath;
                        console.log(vm.detail3);


                        //---deals---
                        var tmp_arr4 = [];
                        $.each(JSON.parse(res4[0]).data, function (k, v) {
                            tmp_arr4[ tmp_arr4.length ] = v;
                            //                    var reslen = v.length;
                            //                    console.log(reslen);
                            //                    for (var i = 1; i < reslen; i++) {
                            //                        console.log(v[i]);
                            //                        tmp_arr4[ tmp_arr4.length ] = v[i];
                            //                    }
                        });
                        vm.detail4 = tmp_arr4;
                        vm.imgPath = vm.imgPath;
                        console.log(vm.detail4);


                        js_initail();


                    }).fail(function () {
                        alert("fail");
                    });

                }
            });

        });
    </script>
</body>

</html>
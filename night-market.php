<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Night Market - Culture & Heritage | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="thingslevel2">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                      <!--<img alt="Background Image" class="background-image" src="img/nightMarketHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="#main">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                       <!--<img alt="Background Image" class="background-image" src="img/fineDiningHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="fine-dining.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-bind:style="{ 'background-image': 'url(' + thingslevel2.image + ')' }">
                       <!--<img alt="Background Image" class="background-image" src="img/taiwanSpecialtyHero.jpg">-->
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="taiwan-specialty.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>


                <!-- level1輪播 -->
                <!--<li class="image-bg">
                    <div class="background-image-holder"  v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 4" v-bind:style="{ 'background-image': 'url(' + thingslevel1.image + ')' }">
                        <img alt="Background Image" class="background-image" src="img/culinaryHero.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="culinary.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
            </ul>
        </section>

        <section class="bg-primary" id="main" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold" v-html="thingslevel2.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="thingslevel2.description">
                        <!--<h2>NIGHT MARKETS</h2>
                            <p>The lights, sounds, and aromas of some 300 markets fill the night across Taiwan each week. Taiwan’s night markets have their roots in the hawking and mingling activities around temples – the sales of foodstuffs and sundries, and the random street shows that kept ordinary folk sated and happy in a bygone era. Even today, some night markets are like mini-carnivals where music, merchandise, fairground games, and food vie for attention. </p>
                            <p>The array of food at these nocturnal bazaars is bewildering. Dozens of tiny stalls and holes-in-the-wall hawk Taiwan-style braises, fermented ‘stinky’ tofu, fritters savory and sugared, skewers galore, and all manner of fruity and caffeine-laced concoctions to wash them down with. Fluffy scallion pancakes, gua bao, a soft bun stuffed with braised pork and pickled greens, and red bean-filled pastries are fixtures at many markets. Japanese street fare like grilled octopus balls and okonomiyaki pancakes are common. Some operators up the game with flash-seared steak, pepper-and-pork buns made from scratch, even freshly shucked oysters. </p>
                            <p>Markets located in fishing ports and southern Taiwan have the best seafood options. At the famed Miaokou Market in Keelung port, and the wildly popular Ruifeng Market in Kaohsiung, visitors gorge on shellfish and squid; while foodies flock to Donggang Market in Pingtung County for mud crab and local shrimp. Zhongxiao Market in Taichung has fantastic vegetarian selections because it is close to a few temples. In Hualien on the east coast, charming Fuding Market offers indigenous flavors, food-wise and also in the nightly music performances.</p>
                            <p>Tainan’s reputation as Taiwan’s snack food capital draws fans to the southern city in search of native nibbles like braised eel noodles, bean curd dessert, and of course, milkfish, a creamy, unctuous fish eaten pan-fried, braised, or in broth, and often for breakfast in Tainan. These delicacies have long made their way to the rest of Taiwan, but many still insist they are tastiest on their home turf.</p>-->

                    </div>
                </div>
            </div>
        </section>

        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">

            <div class="container">
                <div><h3>Features</h3></div>
                <div class="row">
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li class="feature-pad" >
                                <a href="taiwan-specialty.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 10" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="night-market.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 11" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                            <li class="feature-pad">
                                <a href="fine-dining.php">
                                    <img v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-bind:src="thingslevel2.image2" />
                                    <p class="flex-caption" v-for="thingslevel2 in searched_table_data" v-show="thingslevel2.id == 12" v-html="thingslevel2.title2"></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">
                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project" >
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 1">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 2">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 3">
                            <a href="temples.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 4">
                            <a href="culinary.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 5">
                            <a href="outdoor.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 6">
                            <a href="wellness.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data2" v-show="thingslevel1.id == 7">
                            <a href="ecotourism.php">
                                <img alt="Pic" v-bind:src="thingslevel1.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end of Things to do  -->

        <?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <!--         <div class="modal-strip bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 overflow-hidden">
                                <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                                <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                                <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });

        var vm = new Vue({
            el: "#thingslevel2",
            data: {
                things: [],
                things2: [],
                search_key: ''
            },
            methods: {
                edit: function (id) {
                    alert(id);
                },
                Get: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'things_to_do_level1',
                        },
                        success: function (res2) {
                            console.log(res2);
                            res2 = JSON.parse(res2);

                            if (res2.success) {
                                vm.things2 = res2.data;

                                //settimeout show img
                                setTimeout(function () {
                                    console.log('ReFresh masonry');
                                    $('.masonry').masonry();

                                }, 1000);
                            }
                        },
                        error: function (res2) {
                            console.log(res2);
                        }
                    });
                }
            },
            computed: {
                searched_table_data() {
                    let clone = JSON.parse(JSON.stringify(this.things));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                },
                searched_table_data2() {
                    let clone = JSON.parse(JSON.stringify(this.things2));
                    let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                    return temp
                }
            },
            created: function () {
                $.ajax({
                    type: "POST",
                    url: $.config.WebApiUrl,
                    data: {
                        'global_oTable': 'things_to_do_level2'
                    },
                    success: function (res) {
                        console.log(res);
                        res = JSON.parse(res);

                        if (res.success) {
                            vm.things = res.data;
                            vm.Get();

                        } else {
                            alert(res.msg);
                            //show_remind( res.msg , "error" );
                        }
                    },
                    error: function (res) {
                        console.log(res);
                    }
                });
            }
        });
    </script>
</body>

</html>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Museums - Culture & Heritage | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
    </head>

    <body>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/thingsHero-2.1.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="#main">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                    
                <li class="image-bg">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/thingsHero-2.1.1.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="national-palace-museum.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/thingsHero-2.1.2.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="">    <p class="lead">
                                    <a class="btn btn-sm mt104" href="national-taiwan-museum-of-fine-arts.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>                   
                <li class="image-bg">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/thingsHero-2.1.3.jpg">
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                                <p class="lead">
                                    <a class="btn btn-sm mt104" href="national-museum-of-taiwan-literature.php">MORE</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>


        <section class="bg-primary" id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold">CULTRURE<br>&<br>HERITAGE</h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8">
                        <strong class="article">Taiwan has inherited the entire Chinese tradition of religion and philosophy, in addition to its vast literary and aesthetic canon. The country is also a proud heir of the Austronesian culture of its indigenous people. The influences of the Japanese, and to a lesser extent, the Dutch and the Spaniards are not hard to find either. </strong>
                        <strong class="article">These rich and varied legacies are what captivate about Taiwan, but even more so, is how it succeeded in turning them into a modern identity as distinctly Taiwan as a sculpture by Ju Ming or Taroko Gorge. </strong><br><br>

                        <h2>MUSEUMS</h2>

                        <p>Dozens of museums showcase the multifarious facets of Taiwanese culture. They range from big names like the National Palace Museum and the many fine history and art museums, to quaintly specialized institutions devoted to puppetry (Taipei), wood sculpture (Sanyi, Miaoli), ceramics (Yingge, New Taipei City), even paper and drinking water (both in Taipei). 
                            Moreover, many of them are located in attractive buildings of historical value. For instance, the excellent The National Museum of Taiwan Literature in Tainan calls home a riveting Japanese colonial structure that once served as the Tainan District Hall; while the enjoyable Lukang Folk Arts Museum occupies the former mansion of a wealthy local family, also built during the Japanese era. 
                        </p>

                    </div>
                </div>
            </div>
        </section>


        <!-- Things to do FlexSlider -->
        <section class="pt120 pb120 bg-features">

            <div class="container">
                <div><h3>Features</h3></div>
                <div class="row">
                    <!-- Place somewhere in the <body> of your page -->
                    <div class="flexslider">
                        <ul class="slides">
                            <li class="feature-pad">
                                <!-- <a href="national-palace-museum.php"> -->
                                <img src="img/NATIONAL-PALACE-MUSEUM-Features.jpg" /><p class="flex-caption">NATIONAL PALACE MUSEUM</p>
                            </li>
                            <li class="feature-pad">
                                <img src="img/NATIONAL-TAIWAN-MUSEUM-OF-FINE-ARTS-Features.jpg" /><p class="flex-caption">NATIONAL TAIWAN MUSEUM OF FINE ARTS</p>
                            </li>
                            <li class="feature-pad">
                                <img src="img/NATIONAL-MUSEUM-OF-TAIWAN-LITERATURE-Features.jpg" /><p class="flex-caption">NATIONAL MUSEUM OF TAIWAN LITERATURE</p>
                            </li>						    <li class="feature-pad">
                                <img src="img/NATIONAL-PALACE-MUSEUM-Features.jpg" /><p class="flex-caption">NATIONAL PALACE MUSEUM</p>
                            </li>
                            <li class="feature-pad">
                                <img src="img/NATIONAL-TAIWAN-MUSEUM-OF-FINE-ARTS-Features.jpg" /><p class="flex-caption">NATIONAL TAIWAN MUSEUM OF FINE ARTS</p>
                            </li>
                            <li class="feature-pad">
                                <img src="img/NATIONAL-MUSEUM-OF-TAIWAN-LITERATURE-Features.jpg" /><p class="flex-caption">NATIONAL MUSEUM OF TAIWAN LITERATURE</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <!-- Things to do  -->
        <section class="">

            <div class="container">

                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="culture-and-heritage.php">
                                <img alt="Pic" src="img/things2do-1.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Culture and Heritage</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="indigenous-culture.php">
                                <img alt="Pic" src="img/things2do-2.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Indigenous Culture</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="temples.php">
                                <img alt="Pic" src="img/things2do-3.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Temples</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="culinary.php">
                                <img alt="Pic" src="img/things2do-4.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Culinary</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="outdoor.php">
                                <img alt="Pic" src="img/things2do-5.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Adventure/Outdoor</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="wellness.php">
                                <img alt="Pic" src="img/things2do-6.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Wellness</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin">
                            <a href="ecotourism.php">
                                <img alt="Pic" src="img/things2do-7.jpg">
                                <div class="title">
                                    <h5 class="uppercase mb0">Ecotourism</h5><i class="ti-plus grid-plus"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- end of Things to do  -->

        <?php include_once("incl/footer.php"); ?>
        <!-- Cookie Disclaimer -->
        <!--         <div class="modal-strip bg-white">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 overflow-hidden">
                                <i class="ti-announcement icon icon-sm pull-left color-primary"></i>
                                <p class="mb0 pull-left"><strong>Cookie Disclaimer</strong> "This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. Find out more here."</p>
                                <a class="btn btn-sm btn-filled mb0" href="#">See More</a>
                            </div>
                        </div>
                    </div>
                </div> -->
        <!--end Cookie-->
    </div>

    <?php include( "js/all_js.php"); ?>
        
    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
    </script>
</body>

</html>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>FESTIVAL CALENDAR - Event | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="event">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <!--<li class="vid-bg image-bg overlay">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
                    </div>
                    <div class="fs-vid-background">
                        <video muted="" loop="">
                            <source src="video/video.webm" type="video/webm">
                            <source src="video/video.mp4" type="video/mp4">
                            <source src="video/video.ogv" type="video/ogg">
                        </video>
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="event in searched_table_data" v-show="event.id == 2" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + event.image + ')' }">
                    <!--<img alt="Background Image" class="background-image" src="img/eventHero.jpg">-->
                    </div>

                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/celebrateEventLogo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>
        <section class="bg-primary" v-for="event in searched_table_data"  v-show="event.id == 2">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold italic" v-html="event.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="event.description">
			<!--<h2>Festival Calendar</h2>
			<ul class="calMonth">Jan-Mar</ul>
			<li>Lunar New Year      </li>
			<li>Taipei International Book Exhibition    </li>    
			<li>Taiwan Lantern Festival & Yanshui Beehive Fireworks </li>
			<li>Festival  </li>      
			<li>Pingxi Sky Lantern Festival  </li>       
			<li>Megaport Music Festival</li>
			<li>Kaohsiung Spring Arts Festival</li>
			<div class="pb24"></div>
			<ul class="calMonth">April-May</ul>
			<li>Mazu Pilgrimage      </li>  
			<li>Spring Scream    </li>   
			<li>Bao'an Folk Arts Festival   </li>     
			<li>Penghu Fireworks Festival    </li>   
			<li>Kaohsiung Spring Arts Festival</li>
			<div class="pb24"></div>
			<ul class="calMonth">June-Aug</ul>
			<li>Welcoming the City God  </li>    
			<li>Dragon Boat Festival  </li>      
			<li>Taipei Film Festival </li>       
			<li>Taiwan International Balloon Festival</li>  
			<li>Indigenous Festivals    </li>    
			<li>Chinese Ghost Festival  </li>
			<div class="pb24"></div>
			<ul class="calMonth">Sept-Dec</ul>
			<li>Taipei Arts Festival    </li>    
			<li>Confucius’ Birthday  </li>    
			<li>Penghu Triathlon   </li>      
			<li>Burning of the Wangye Boats  </li>    
			<li>Taichung Jazz Festival   </li>    
			<li>Taipei Jazz Festival  </li>       
			<li>Kaohsiung Lion Dance Competition   </li>      
			<li>Art Kaohsiung </li>
			<div class="pb32"></div>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Grid of Event  -->
        <section class="pt0">

            <div class="container">

                <div class="row masonry masonryFlyIn">
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 3">
                            <a href="https://www.eventaiwan.tw/cal_en/cal_19955" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 4">
                            <a href="https://eventaiwan.tw/cal_en/cal_20020" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 5">
                            <a href="https://www.eventaiwan.tw/cal_en/cal_19955" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 6">
                            <a href="https://www.eventaiwan.tw/cal_en/cal_20071" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 7">
                            <a href="https://eventaiwan.tw/cal_en/cal_20186" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 8">
                            <a href="https://www.eventaiwan.tw/cal_en/cal_20078" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 9">
                            <a href="http://theme.taiwanbike.tw/event/2017/en/" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 10">
                            <a href="https://eventaiwan.tw/cal_en/cal_20169" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project">
                        <div class="image-tile inner-title zoomin" v-for="event in searched_table_data" v-show="event.id == 11">
                            <a href="https://eventaiwan.tw/cal_en/cal_20115" target="_blank">
                                <img alt="Pic" v-bind:src="imgPath + event.image2">
                                <div class="title">
                                    <h5 class="uppercase mb0" v-html="event.title2"></h5><!-- <i class="ti-plus grid-plus"></i> -->
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- end of Grid of Event  -->

        <?php include_once("incl/footer.php"); ?>

    </div>

    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        
        $(document).ready(function() {

            var vm = new Vue({
                el: "#event",
                data: {
                    imgPath: $.config.imgPath,
                    happening: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.happening));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'whats_happening'
                        },
                        success: function (res) {
                            console.log(res);
                            res = JSON.parse(res);

                            if (res.success) {
                                vm.happening = res.data;
                                vm.imgPath = vm.imgPath;

                                js_initail();

                            } else {
                                alert(res.msg);
                                //show_remind( res.msg , "error" );
                            }
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                }
            });
        
        });
    </script>
</body>

</html>
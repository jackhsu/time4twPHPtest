<!-- Cuisine Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="taiwan-specialty.php">
					      <img src="img/taiwanSpecialtyFeatures.jpg" /><p class="flex-caption">Taiwanese Specialities</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="night-market.php">
					      <img src="img/nightMarketFeatures.jpg" /><p class="flex-caption">Night Markets</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="fine-dining.php">
						  <img src="img/fineDiningFeatures.jpg" /><p class="flex-caption">Fine Dining</p>
				      </a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
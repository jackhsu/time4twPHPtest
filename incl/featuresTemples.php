<!-- Temples Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="baoan-temple.php">
					      <img src="img/baoanFeatures.jpg" /><p class="flex-caption">Bao'an Temple</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="tainan-confucius-temple.php">
					      <img src="img/tainanConfuciusFeatures.jpg" /><p class="flex-caption">Tainan Confucius Temple</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="lugang-longshan-temple.php">
						  <img src="img/lugangFeatures.jpg" /><p class="flex-caption">Lugang Longshan Temple</p>
				      </a>
				    </li>				    
				    <li class="feature-pad">
				      <a href="penghu-tianhou-temple.php">
						  <img src="img/tianhouFeatures.jpg" /><p class="flex-caption">Penghu Tianhou Temple</p>
				      </a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
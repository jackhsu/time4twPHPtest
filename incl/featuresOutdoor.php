<!-- Outdoor Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="water-sports.php">
			            <img src="img/waterSportsFeatures.jpg" /><p class="flex-caption">Water Sports</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="hiking.php">
				        <img src="img/hikingFeatures.jpg" /><p class="flex-caption">Hiking</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="cycling.php">
					    <img src="img/cyclingFeatures.jpg" /><p class="flex-caption">Cycling</p>
				      </a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
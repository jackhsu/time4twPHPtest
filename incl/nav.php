<!-- Nav -->
<div class="nav-container">

    <nav>

        <div class="nav-bar text-center">
            <div class="col-md-2 col-md-push-5 col-sm-12 text-center">
                <a href="index.php">
                    <img alt="logo" class="image-xs" src="img/logo-dark.png">
                </a>
            </div>

            <div class="col-sm-12 col-md-5 col-md-pull-2 overflow-hidden-xs">
                <ul class="menu inline-block pull-right">
                    <li><a href="place-to-go.php">Places To Go</a></li>

                    <li class="has-dropdown">
                        <a href="things-to-do.php">THINGS TO DO</a>
                        <ul id="nav-things-to-do-menu" >
                            <li class="has-dropdown">
                                <a href="things-to-do.php?page=culture-and-heritage">CULTURE & HERITAGE</a>
                                <ul>
                                    <li><a href="museums.php" class="title">MUSEUMS</a></li>
                                    <li><a href="national-taiwan-museum-of-fine-arts.php">NATIONAL TAIWAN MUSEUM OF FINE ARTS</a></li>
                                    <li><a href="national-museum-of-taiwan-literature.php">NATIONAL MUSEUM OF TAIWAN LITERATURE</a></li>
                                    <li><a href="national-palace-museum.php">NATIONAL PALACE MUSEUM</a></li>
                                </ul>
                            </li>                                
                            <li class="has-dropdown">
                                <a href="indigenous-culture.php">Indigenous Culture</a>
                                <ul>
                                    <li><a href="lanyu-island.php">Lanyu (Orchid) Island</a></li>
                                    <li><a href="east-coast.php">East Coast</a></li>
                                    <li><a href="museums-for-indigenous-art-and-history.php">MUSEUMS OF INDIGENOUS ART AND HISTORY</a></li>
                                </ul>
                            </li>                                
                            <li class="has-dropdown">
                                <a href="temples.php">Temples</a>
                                <ul>
                                    <li><a href="baoan-temple.php">Bao'an Temple</a></li>
                                    <li><a href="lugang-longshan-temple.php">lugang Longshan Temple</a></li>
                                    <li><a href="tainan-confucius-temple.php">Tainan Confucius Temple</a></li>
                                    <li><a href="penghu-tianhou-temple.php">Penghu Tianhou Temple</a></li>
                                </ul>
                            </li>                                  
                            <li class="has-dropdown">
                                <a href="cuisine.php">Cuisine</a>
                                <ul>
                                    <li><a href="taiwan-specialty.php">Taiwanese Specialities</a></li>
                                    <li><a href="night-market.php">Night Markets</a></li>
                                    <li><a href="fine-dining.php">Fine Dining</a></li>
                                </ul>
                            </li>                                   
                            <li class="has-dropdown"><a href="outdoor.php">Adventure/Outdoor</a>
                                <ul>
                                    <li><a href="cycling.php">Cycling</a></li>
                                    <li><a href="water-sports.php">Water Sports</a></li>
                                    <li><a href="hiking.php">Hiking</a></li>
                                </ul>
                            </li>                              
                            <li class="has-dropdown"><a href="wellness.php">WELLNESS</a>
                                <ul>
                                    <li><a href="spa.php">SPA</a></li>
                                    <li><a href="hot-spring.php">Hot Springs</a></li>
                                </ul>
                            </li>                               
                            <li class="has-dropdown"><a href="ecotourism.php">Ecotourism</a>
                                <ul>
                                    <li><a href="butterfly-watching.php">Butterfly watching</a></li>
                                    <li><a href="bird-watching.php">Bird watching</a></li>
                                </ul>
                            </li>                                
                            <li class="has-dropdown"><a href="festivals.php">Festivals</a>
                                <ul>
                                    <li><a href="mazu-pilgrimage.php">Mazu Pilgrimage</a></li>
                                    <li><a href="burning-of-the-wangye-boats.php">Burning of the Wangye Boats</a></li>
                                    <li><a href="yanshui-beehive-fireworks-festival.php">Yanshui Beehive Fireworks Festival</a></li>
                                    <li><a href="outdoor-music-festivals.php">Outdoor Music Festivals</a></li>
                                </ul>
                            </li>                                
                        </ul>
                    </li> 
                    <li class="has-dropdown">
                        <a href="before-you-go.php">BEFORE YOU GO</a>
                        <ul>
                            <li><a href="getting-there.php">Getting There</a></li> 
                            <li><a href="travel-tips.php">Travel Tips</a></li>
                            <li><a href="getting-around.php">Getting Around</a></li>
                            <li><a href="visas.php">Visa</a></li>
                            <li><a href="language.php">Language</a></li>                                      
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="col-sm-12 col-md-5 pb-xs-24">
                <ul class="menu">
                    <li class="has-dropdown"><a href="event.php">WHATS HAPPENING</a>
                        <ul>
                            <li><a href="event.php">CURRENT EVENT</a></li>
                            <li><a href="calendar.php">FESTIVAL CALENDAR</a></li>
                        </ul>
                    </li>
                    <li><a href="deals.php">DEALS</a>
                    </li>
                    <li><a href="http://taiwanspecialistprogram.com/" target="_blank">TRADE</a></li>

                    <li class="module widget-handle search-widget-handle left"><i class="ti-search icon-xs"></i>
                        <div class="search">
                            <span class="title">Search Site</span>
                        </div>
                        <div class="function">
                            <form class="search-form">
                                <input type="text" id="search" name="text1" value="" placeholder="Search..." />
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="module widget-handle mobile-toggle right visible-sm visible-xs absolute-xs">
            <i class="ti-menu"></i>
        </div>
    </nav>

    <script>

        $(document).ready(function() {

                var focus_nav = $( "#nav-things-to-do-menu" ) ;
                var ajax1 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level1'}});
                var ajax2 = $.ajax({type: "POST", url: $.config.WebApiUrl, data: {'global_oTable': 'things_to_do_level2'}});
                $.when(ajax1, ajax2 ).then(function (res1, res2) {
                    //console.log(res1);
                    var res = [ res1, res2 ];
                    console.log(res);
                    
                    var tmp_html = "" ;
                    $.each( JSON.parse( res[0][0] ).data , function (k, v) {
                            tmp_html +=
                            '<li class="has-dropdown">' +
                                '<a href="things-to-do.php?page=' + v.title2.replace( / /g , "-" ).toLowerCase() + '">' + v.title2 + '</a>' +
                                '<ul>' ;
                            var tmp_level_id = v.id ;
                            $.each( JSON.parse( res[1][0] ).data , function (k, v) {
                                    if( tmp_level_id == v.level1_id )
                                    {
                                    tmp_html += '<li><a href="things-to-do.php?page=' + v.title2.replace( / /g , "-" ).toLowerCase() + '" class="title">' + v.title2 + '</a></li>' ;
                                    }
                                    
                            });
                            
                            tmp_html +=
                                '</ul>' +
                            '</li>' ;
                    });                      
                    
                    focus_nav.html( tmp_html );
                    
                }).fail(function () {
                    alert("fail");
                });
        });
        
        window.addEventListener("keydown", function (event) {
            console.log(event.keyCode);
            if (event.keyCode == 13)
            {
                // window.open( 'http://www.fooddy.co/el/go2taiwan/frontend/results.php' , '_self' );
                if (!(location.pathname.split("results.php").length > 1))
                    location.href = "results.php?search=" + $('input[name="text1"]').val();


                $("#search").trigger("magic");
                // Suppress "double action" if event handled
                event.preventDefault();
            }

        }, true);

    </script>

</div>

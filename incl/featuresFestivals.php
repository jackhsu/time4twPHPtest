<!-- Festivals Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="mazu-pilgrimage.php">
			            <img src="img/mazuFeatures.jpg" /><p class="flex-caption">Mazu Pilgrimage</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="wangye-boats.php">
				        <img src="img/wanyeFeatures.jpg" /><p class="flex-caption">Burning of the Wangye Boats</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="yanshui-festival.php">
					    <img src="img/yanshuiFeatures.jpg" /><p class="flex-caption">Yanshui Beehive Fireworks Festival</p>
				      </a>
				    </li>				    
				    <li class="feature-pad">
				      <a href="outdoor-music.php">
					    <img src="img/musicFestivalsFeatures.jpg" /><p class="flex-caption">Outdoor Music Festivals</p>
				      </a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>

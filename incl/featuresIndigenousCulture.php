<!-- Indigenous Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="east-coast.php">
					      <img src="img/eastCoastFeatures.jpg" /><p class="flex-caption">East Coast</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="museums-for-indigenous-art-and-history.php">
					      <img src="img/indigenousMuseumsFeatures.jpg" /><p class="flex-caption">MUSEUMS OF INDIGENOUS ART AND HISTORY</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="lanyu-island.php">
						  <img src="img/lanyuIslandFeatures.jpg" /><p class="flex-caption">Lanyu (Orchid) Island</p>
				      </a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
<!-- Wellness Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="spa.php">
			            <img src="img/spaFeatures.jpg" /><p class="flex-caption">SPA</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="hot-spring.php">
				        <img src="img/hotSpringFeatures.jpg" /><p class="flex-caption">Hot Springs</p>
				      </a>
				    </li>
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
        <footer class="footer-1 bg-dark">
            <div class="container">
                <div class="row">

                    <div class="col-md-2 col-xs-6">
                        <div class="widget">
                            <h6 class="title">PLACE TO GO</h6>
                            <ul class="link-list recent-posts">
                                <li><a href="place-to-go.php">Northern Taiwan</a></li>
                                <li><a href="place-to-go.php">Central Taiwan</a></li>
                                <li><a href="place-to-go.php">Eastern Taiwan</a></li>
                                <li><a href="place-to-go.php">Southern Taiwan</a></li>
                                <li><a href="place-to-go.php">Offshore Islands</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="widget">
                            <h6 class="title">THINGS TO DO</h6>
                            <ul class="link-list recent-posts">
                                <li><a href="culture-and-heritage.php">Culture and Heritage</a></li>
                                <li><a href="indigenous-culture.php">Indigenous Culture</a></li>
                                <li><a href="temples.php">Temples</a></li>
                                <li><a href="cuisine.php">Cuisine</a></li>
                                <li><a href="outdoor.php">Adventure/Outdoor</a></li>
                                <li><a href="wellness.php">Wellness</a></li>
                                <li><a href="ecotourism.php">Ecotourism</a></li>
                                <li><a href="festivals.php">Festivals</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2 col-xs-6">
                        <div class="widget">
                            <h6 class="title">BEFORE YOU GO</h6>
                            <ul class="link-list recent-posts">
                                <li><a href="travel-tips.php">Travel Tips</a></li>
                                <li><a href="getting-there.php">Getting There</a></li>
                                <li><a href="getting-around.php">Getting Around</a></li>
                                <li><a href="visas.php">Visa</a></li>
                                <li><a href="language.php">Language</a></li>
                            </ul>
                        </div>
                    </div>                    
                    <div class="col-md-2 col-xs-6 border-right-line">
                        <div class="widget">
                            <a href="event.php"><h6 class="title">EVENT</h6></a>
                            <a href="deals.php"><h6 class="title">DEALS</h6></a>
                            <a href="trade.php"><h6 class="title">TRADE</h6></a>
                            <br>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-10">
                        <a href="#"><img alt="Logo" class="logo" src="img/logo-light.png"></a>
                            <ul class="list-inline social-list">
                                <li>
                                    <a href="https://www.facebook.com/TourTaiwan/" target="_blank">
                                        <i class="ti-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/TaiwanTourism" target="_blank">
                                        <i class="ti-twitter-alt"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="https://www.youtube.com/user/taiwanbesttrip" target="_blank">
                                        <i class="ti-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/ttb_na/" target="_blank">
                                        <i class="ti-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        <span class="sub">© 2017-2018 go2taiwan.net</span>
                    </div>
                </div>
            </div>

            <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
        </footer>
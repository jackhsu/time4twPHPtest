
<!-- Things to do Grid -->
<section class="">

    <div class="container">

        <div class="row masonry masonryFlyIn">
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 1">
                    <a href="culture-and-heritage.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 2">
                    <a href="indigenous-culture.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 3">
                    <a href="temples.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 4">
                    <a href="cuisine.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 5">
                    <a href="outdoor.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>   
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 6">
                    <a href="wellness.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 7">
                    <a href="ecotourism.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
            
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 8">
                    <a href="festivals.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section><!-- end of Things to do  -->
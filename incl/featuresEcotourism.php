<!-- Ecotourism Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="butterfly-watching.php">
			            <img src="img/butterflyWatchingFeatures.jpg" /><p class="flex-caption">Butterfly Watching</p>
				      </a>
				    </li>
				    <li class="feature-pad">
				      <a href="bird-watching.php">
				        <img src="img/birdWatchingFeatures.jpg" /><p class="flex-caption">Bird Watching</p>
				      </a>
				    </li>
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
<!-- Culture Heritage Featrures FlexSlider -->
<section class="pt120 pb120 bg-features">
        <div class="container">

        	<div><h3>Features</h3></div>
            
            <div class="row">
				<div class="flexslider">
				  <ul class="slides">
					<li class="feature-pad">
				      <a href="national-palace-museum.php"><img src="img/NATIONAL-PALACE-MUSEUM-Features.jpg" />
                      <p class="flex-caption">NATIONAL PALACE MUSEUM</p></a>
				    </li>
				    <li class="feature-pad">
				      <a href="national-taiwan-museum-of-fine-arts.php"><img src="img/NATIONAL-TAIWAN-MUSEUM-OF-FINE-ARTS-Features.jpg" /><p class="flex-caption">NATIONAL TAIWAN MUSEUM OF FINE ARTS</p></a>
				    </li>
				    <li class="feature-pad">
				      <a href="national-museum-of-taiwan-literature.php"><img src="img/NATIONAL-MUSEUM-OF-TAIWAN-LITERATURE-Features.jpg" /><p class="flex-caption">NATIONAL MUSEUM OF TAIWAN LITERATURE</p></a>
				    </li>						    
				  </ul>
				</div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
</section>
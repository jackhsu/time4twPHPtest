<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Places To Go | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <?php include_once("incl/nav.php"); ?>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="placetogo">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="placetogo in searched_table_data" v-show="placetogo.id == 1" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + placetogo.image + ')' }">
                        <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
                    </div>

                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/explore-Hero-logo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>
        <!-- Spacer for small devices -->
        <div class="visible-xs visible-sm spacer"></div>


        <section class="image-square left" v-for="placetogo in searched_table_data" v-show="placetogo.id == 2">
            <div class="col-md-6 image">
                <div class="imgPlace2go zoomin">
                    <img alt="image" class="" v-bind:src="imgPath + placetogo.image2">
                </div>
            </div>
            <div data-as="true" class="col-md-6 col-md-offset-1 content anime-start-1" data-as-animation="anime-end-1">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="placetogo.title"></h1>
                <p class="mb0" v-html="placetogo.description"></p>
                <!--<p class="mb0">Taipei, the gateway to northern Taiwan, is the political and cultural heart of the nation. The capital offers world-class museums, an impressive Cuisine repertoire, and great culture. New Taipei City is a mosaic of towns, some celebrated for ceramics, some for tea, some for their European links, some for a mining heritage. In addition, sightseeing can be easily combined with hiking, hot springing, even bird-watching. Cycling routes hug the northern coastline, whizzing through rural townships and over highways. If you just want to chill, there are sweet surfable waves and buoyant music festivals.</p>-->
            </div>
        </section>
        <section class="image-square right" v-for="placetogo in searched_table_data" v-show="placetogo.id == 3">
            <div class="col-md-6 image ">
                <div class="imgPlace2go zoomin">
                    <img alt="image" class="" v-bind:src="imgPath + placetogo.image2">
                </div>
            </div>
            <div data-as="true" class="col-md-6 content anime-start-1" data-as-animation="anime-end-1">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="placetogo.title"></h1>
                <p class="mb0" v-html="placetogo.description"></p>
                <!--<p class="mb0">Central Taiwan is defined by the mountain ranges down its eastern edge and the historical towns in the foothills and scattered along the west coast.  Opportunities for adventure here are exceptional. Alishan lures with old-growth forests and firefly-lit villages. Mt. Jade is the highest peak in Northeast Asia, while Sun Moon Lake beckons with dappled pathways, temples and tea farms. For the truly adventurous, roads less-travelled lead to the rugged heart of wild Taiwan. Towns like Lugang and Chiayi are hubs of history and folk art, where old shop-lined streets radiate from magnificent temples. Central Taiwan is the site of the Mazu Pilgrimage, a dazzling procession spanning nine days, four counties and 60 temples. Taichung has all the latest modern luxuries, in addition to top-notch museums, gourmet restaurants, and lovely urban parks.</p>-->
            </div>
        </section>
        <section class="image-square left" v-for="placetogo in searched_table_data" v-show="placetogo.id == 4">
            <div class="col-md-6 image ">
                <div class="imgPlace2go zoomin">
                    <img alt="image" class="" v-bind:src="imgPath + placetogo.image2">
                </div>
            </div>
            <div data-as="true" class="col-md-6 col-md-offset-1 content anime-start-1" data-as-animation="anime-end-1">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="placetogo.title"></h1>
                <p class="mb0" v-html="placetogo.description"></p>
                <!--<p class="mb0">It is always warm in Southern Taiwan. This chunk of the island is known for a tropical climate, bubbling mud volcanoes, and warm hospitality. Southern Taiwan refers to the counties of Tainan, Kaohsiung and Pingtung. Folk culture thrives here, infusing streets with enchanting sights, smells and sounds—maybe an ornate, incense-filled temple or one of the most spectacular rituals in the world. Tainan, being the oldest part of Taiwan, is a trove of history. Old-world charm permeates walkable streets, with relics at every turn and as many snacks to remind you of the city’s proud heritage. Kaohsiung capitalizes well on its maritime past. The city offers some of Taiwan’s most stunning coastal architecture, cool contemporary art, and inspired takes on southern cuisine. Pingtung is best known for the coraled splendor of Little Liuqiu Island and the beach town of Kending, but, like the rest of the region, it has hiking trails and biking routes galore. </p>-->
            </div>
        </section>
        <section class="image-square right" v-for="placetogo in searched_table_data" v-show="placetogo.id == 5">
            <div class="col-md-6 image ">
                <div class="imgPlace2go zoomin">
                    <img alt="image" class="" v-bind:src="imgPath + placetogo.image2">
                </div>
            </div>
            <div data-as="true" class="col-md-6 content anime-start-1" data-as-animation="anime-end-1">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="placetogo.title"></h1>
                <p class="mb0" v-html="placetogo.description"></p>
                <!--<p class="mb0">Many consider Taiwan’s east coast landscape to be the country’s most beautiful. If you’ve seen how the cliffs plunge into the ocean and the blue waters caress grey-pebbled beaches, you might agree. Cycling here is one of life’s greatest pleasures. The scenery is awesome, the roads excellent, and there’s no shortage of fine B&Bs. Town-hopping along the coast by car is wonderful too. There are plenty of attractions to stop for – marble gorges, ancient hunting trails, beaches, prehistoric sites, or simply great coffee. The east coast is a stronghold of indigenous culture, with exuberant festivals in the summer and opportunities to experience the tribal way of life all year round.</p>-->
            </div>
        </section>
        <section class="image-square left" v-for="placetogo in searched_table_data" v-show="placetogo.id == 6">
            <div class="col-md-6 image ">
                <div class="imgPlace2go zoomin">
                    <img alt="image" class="" v-bind:src="imgPath + placetogo.image2">
                </div>
            </div>
            <div data-as="true" class="col-md-6 col-md-offset-1 content anime-start-1" data-as-animation="anime-end-1">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="placetogo.title"></h1>
                <p class="mb0" v-html="placetogo.description"></p>
                <!--<p class="mb0">Taiwan’s outlying islands contribute to a rich and many-layered experience of the country. Gusty Penghu Archipelago is about maritime history, graceful temples, and fine old houses. It also promises stellar windsurfing and fresh seafood. Green Island draws with clear waters, lively reefs, and a rare seawater hot spring. Kinmen and Mazu, lying between Taiwan and China’s Fujian province, offer handsome villages and Cold War relics. Between April and September, Mazu’s waters turn luminous from glowing algae. Dramatic rocks, jungled mountains, and vibrant tribal culture characterize Lanyu (Orchid) Island, the gorgeous home of the seafaring Tao (Yami) tribe.</p>-->
            </div>
        </section>


        <?php include_once("incl/footer.php"); ?>

    </div>



    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });


        //el_20170901
        $(document).ready(function() {
        
            var vm = new Vue({
                el: "#placetogo",
                data: {
                    imgPath: $.config.imgPath,
                    places: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.places));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'place_to_go'
                        },
                        success: function (res) {
                            console.log(res);
                            res = JSON.parse(res);

                            if (res.success) {
                                vm.places = res.data;
                                vm.imgPath = vm.imgPath;
                            } else {
                                alert(res.msg);
                                //show_remind( res.msg , "error" );
                            }
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                }
            });

        });
    </script>
</body>

</html>
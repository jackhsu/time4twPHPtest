<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Deals - Things to do | Time for Taiwan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet" type="text/css">
        <link href="css/font-opensans.css" rel="stylesheet" type="text/css">
        <?php include_once("incl/googleTagHead.php"); ?><?php include( "js/all_js.php"); ?>
    </head>

    <body>
        <?php include_once("incl/googleTagBody.php"); ?>

        <div class="nav-container">

            <?php include_once("incl/nav.php"); ?>

        </div>

        <ul class="share hidden-sm hidden-xs">
            <li class="">
                <a href="#">
                    <span class="ti-facebook"></span>
                </a>
            </li>
            <li class="">
                <a href="#">
                    <span class="ti-twitter-alt"></span>
                </a>
            </li>
        </li>
    </ul>

    <div class="main-container" id="deals">
        <section class="kenburns cover fullscreen image-slider slider-all-controls">
            <ul class="slides">
                <!--<li class="vid-bg image-bg overlay">
                    <div class="background-image-holder">
                        <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
                    </div>
                    <div class="fs-vid-background">
                        <video muted="" loop="">
                            <source src="video/video.webm" type="video/webm">
                            <source src="video/video.mp4" type="video/mp4">
                            <source src="video/video.ogv" type="video/ogg">
                        </video>
                    </div>
                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>
                            </div>
                        </div>
                    </div>
                </li>-->
                <li class="image-bg">
                    <div class="background-image-holder"  v-for="deals in searched_table_data" v-show="deals.id == 1" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + deals.image + ')' }">
                    <!--<img alt="Background Image" class="background-image" src="img/dealsHero.jpg">-->
                    </div>

                    <div class="container v-align-transform">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <img src="img/bookDealsLogo.png" alt="" class="text-center">
                                <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                                <p class="lead">
                                    IT'S TIME FOR TAIWAN NOW
                                </p>-->
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </section>

        <section class="bg-primary" v-for="deals in searched_table_data"  v-show="deals.id == 1">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4">
                        <span class="ti-plus prime-plus"></span>
                        <h1 class="uppercase mb24 bold italic" v-html="deals.title"></h1>
                    </div>
                    <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="deals.description">
                        <!--<h2>Are you ready for Taiwan？</h2>
                        <p>American Airlines commenced their non-stop service from LAX to Taipei in June 2016 and United Airlines launched their flight from SFO in July 2016. Air Taiwan continues to fly non-stop to Taipei from Los Angeles, San Francisco, Houston and Honolulu.</p>
                        <h2>Closer than you think</h2>
                        <p>The average flight time is 12 hours, about the same time it takes to get to Europe from the West Coast. Hop on a plane around 9:30pm, have dinner, some Taiwan wine, and watch a movie before you fall asleep. Wake up to the smell of breakfast being served, and you’re here in Taiwan!</p>-->
                    </div>
                </div>
            </div>
        </section>

        <!-- Deals -->
        <section class="main-pad ">

            <div class="container pb88">
                <div class="row masonry masonryFlyIn">

                    <div class="col-md-4 col-sm-6 masonry-item project " >
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/enchanting-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 2" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 2" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Enchanting Taiwan</h5><h6 class="bookNowPrice">From USD $961</h6><p class="bookNowText">This 8 day tour of Taiwan highlights the must see attractions efficiently! You will visit Taipei, Sun Moon Lake, Kaohsiung, Kenting National Park, Hualien, and the Taroko Gorge</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project " >
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/enchanting-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 3" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 3" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Enchanting Taiwan</h5><h6 class="bookNowPrice">From USD $961</h6><p class="bookNowText">This 8 day tour of Taiwan highlights the must see attractions efficiently! You will visit Taipei, Sun Moon Lake, Kaohsiung, Kenting National Park, Hualien, and the Taroko Gorge</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.goway.com/trip/asia/taste-taiwan/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 4" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 4" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taste of Taiwan</h5><h6 class="bookNowPrice">From USD $2,830</h6><p class="bookNowText">On this 7-day culinary tour of Taiwan, You will take a cooking class and sample foods at a famous Taiwanese night market. You will also ride Taiwan's high-speed bullet train to Sun Moon Lake, Taiwan’s largest body of water.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/zjsthU" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 5" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 5" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">8 Days Taiwan Island Classic Tour</h5><h6 class="bookNowPrice">From USD $1,239</h6><p class="bookNowText">Start this 8-day tour in Taipei at the National Palace Museum and one of Taiwan's famous night markets! On this tour, you will visit Sun Moon Lake, Alishan National Scenic Area, Lukang Town, and have extra free time in Taipei!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.supervaluetours.com/type_tourtype.do?tid=50&fr=supusa" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 6" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 6" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taiwan Island Tour 10 days</h5><h6 class="bookNowPrice">From USD $2,049</h6><p class="bookNowText">This island tour will feature the best of Taiwan! From soup dumplings to the largest collection of Chinese artifacts in Taipei to contemplating a rose-gold sunset at Sun Moon Lake, Taiwan is sure to surprise with this tour!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>                    
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw8" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 7" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 7" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">8 Days Enchanting Taiwan Tour</h5><h6 class="bookNowPrice">From USD $950</h6><p class="bookNowText">On this 8-day tour around Taiwan, you will be visiting Taipei, Sun Moon Lake, Fo Guang Shan Buddhist Monastery, Kaohsiung, Kenting National Park, East Coast & Taroko National Park.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.chinaasiatour.com/asia-tours/taiwan-tours/#etw5" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 8" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 8" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">5 Days Around Taiwan Tour</h5><h6 class="bookNowPrice">From USD $590</h6><p class="bookNowText">During this 5-day tour, you will be visiting Sun Moon Lake, Kaohsiung, Kenting, Taitung, Hualien, and the Taroko Gorge.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.intrepidtravel.com/us/taiwan/explore-taiwan-100826" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 9" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 9" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">10% OFF Intrepid Travel's Explore Taiwan tour</h5><h6 class="bookNowPrice">From USD $2,415</h6><p class="bookNowText">This small group tour will see Taiwan's exciting food culture, hike, ride and kayak around Sun Moon Lake and visit both the Qingshui Cliffs and Taroko Gorge. Book until August 20th with promo code "50146" for 10% off your booking.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://goo.gl/LXzALk" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 10" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 10" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Vancouver to Hong Kong, plus 3-day stopover in Taipei</h5><h6 class="bookNowPrice">From CAD $1,088</h6><p class="bookNowText">This direct flight from Vancouver to Hong Kong includes an optional 3-day stopover in Taipei! Enjoy 3 nights stay in Taipei with breakfast included. Subway cards for Taipei and Hong Kong will also be provided.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.bicycleadventures.com/destinations/taiwan-bike-tours/Taiwan-Bike-Tour" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 11" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 11" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taiwan Bicycle Tour with Silks Spa Experience</h5><h6 class="bookNowPrice">From USD $5,498</h6><p class="bookNowText">This 11-day bike tour offers days of riding through Taiwan's sparsely populated south and east, filled with national parks, rice paddies, palm trees, aboriginal villages, colorful temples and incredibly friendly people.</p>-->
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://campbelltravel.bc.ca/en/" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 12" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 12" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Taipei Free and Easy </h5><h6 class="bookNowPrice">From CAD $738</h6><p class="bookNowText">3 nights stopover in Taipei with breakfast, include round trip air to one of the following destinations: Hong Kong, Seoul, Osaka or Nagoya. </p>-->
                                </div>
                            </a>
                        </div>
                    </div>                     
                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="https://www.panagoetour.com/colourful-north-date-and-price" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 13" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 13" v-html="deals.description2"></div>
                                    <!--<h5 class="bookNowTitle">Colorful North</h5><h6 class="bookNowPrice">From USD $945</h6><p class="bookNowText">This 4-day package tours Northern Taiwan and features extinct volcanoes, hot springs, and extraordinary rock formations along the coast! Explore the nature of Taiwan on this short but exciting trip!</p>-->
                                </div>
                            </a>
                        </div>
                    </div>   
<!--                    <div class="col-md-4 col-sm-6 masonry-item project ">
                        <div class="image-tile inner-title">
                            <a href="http://www.sonictour.com/Formosa%20Star%20Train%20First%20Class%20Experiences%20%28TWR86%29%20-935.html#.WU2pSRIrKV5" target="_blank">
                                <img class="bookHover" alt="Pic" v-for="deals in searched_table_data" v-show="deals.id == 14" v-bind:src="imgPath + deals.image2">
                                <div class="bookNow fixedH">
                                    <div class="bookNowBtn">Booking Now</div>
                                    <div v-for="deals in searched_table_data" v-show="deals.id == 14" v-html="deals.description2"></div>
                                    <h5 class="bookNowTitle">Formosa Star Train First Class Experiences</h5><h6 class="bookNowPrice">Please contact Tour Operator for pricing</h6><p class="bookNowText">Discover the magic of train travel and tour Taiwan to the fullest with this 8-day tour! This tour includes Taipei, Sun Moon Lake, Kaohsiung, Zhiben hot springs, Hualien, and the Taroko Gorge. </p>
                                </div>
                            </a>
                        </div>
                    </div>-->
                </div>
            </div>
        </section><!-- end of Deals -->

        <?php include_once("incl/footer.php"); ?>

    </div>

    

    <script>
        $(window).load(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                animationLoop: false,
                itemWidth: 210,
                itemMargin: 5,
                minItems: 2,
                maxItems: 4
            });
        });
        
        $(document).ready(function() {

            var vm = new Vue({
                el: "#deals",
                data: {
                    imgPath: $.config.imgPath,
                    deal: [],
                    search_key: ''
                },
                methods: {
                    edit: function (id) {
                        alert(id);
                    }
                },
                computed: {
                    searched_table_data() {
                        let clone = JSON.parse(JSON.stringify(this.deal));
                        let temp = clone.filter(o => Object.values(o).reduce((result, b) => result || (b + ""/*轉字串*/).indexOf(this.search_key) != -1, false))
                        return temp
                    }
                },
                created: function () {
                    $.ajax({
                        type: "POST",
                        url: $.config.WebApiUrl,
                        data: {
                            'global_oTable': 'deals'
                        },
                        success: function (res) {
                            console.log(res);
                            res = JSON.parse(res);

                            if (res.success) {
                                vm.deal = res.data;
                                vm.imgPath = vm.imgPath;

                                js_initail();

                            } else {
                                alert(res.msg);
                                //show_remind( res.msg , "error" );
                            }
                        },
                        error: function (res) {
                            console.log(res);
                        }
                    });
                }
            });
        
        });
    </script>
</body>

</html>

<section class="kenburns cover fullscreen image-slider slider-all-controls">
    <ul class="slides">
        <!--<li class="vid-bg image-bg overlay">
            <div class="background-image-holder">
                <img alt="Background Image" class="background-image" src="img/Main-Hero.jpg">
            </div>
            <div class="fs-vid-background">
                <video muted="" loop="">
                    <source src="video/video.webm" type="video/webm">
                    <source src="video/video.mp4" type="video/mp4">
                    <source src="video/video.ogv" type="video/ogg">
                </video>
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h1 class="large">TAIWAN BIG LOGO</h1>
                        <p class="lead">
                            IT'S TIME FOR TAIWAN NOW
                        </p>
                    </div>
                </div>
            </div>
        </li>-->

        <li class="image-bg">
            <div class="background-image-holder"  v-for="thingstodo in searched_table_data" v-show="thingstodo.id == 1" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + thingstodo.image + ')' }">
                <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                        <!--<h1 class="large">TAIWAN BIG LOGO</h1>
                        <p class="lead">
                            IT'S TIME FOR TAIWAN NOW
                        </p>-->
                    </div>
                </div>
            </div>
        </li>
    </ul>
</section>

<section class="bg-primary" v-for="thingstodo in searched_table_data"  v-show="thingstodo.id == 1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold italic" v-html="thingstodo.title"></h1>
            </div>
            <div class="col-md-8 col-md-offset-1 col-sm-8"  v-html="thingstodo.description">
            <!--<strong>Area: 36,000 km2 <br>
                    Population: 23 million<br>
                    Language: Mandarin and Taiwanese<br>
                    Area code: 886<br>
                    Time: GMT +8<br>
                    Climate: Subtropical in the north, tropical in the south</strong>
                <div class="pb24"></div>
                <p>With a stunning landscape of soaring mountains, sweeping flatlands, and pristine waters, Taiwan offers adventure fans great opportunities to flex their brawn. For other travelers, the same sights could fuel inspiration for a poem. Culture enthusiasts will have a blast here. Museums specialize in everything from Chinese dynastic relics to drinking water. Not to mention Taiwan is home to 13,000 temples and some of the world’s most extravagant folk festivals. Food wise, you will be spoilt by a spectrum of excellent Chinese, outstanding Japanese, and a host of delectable local specialties. Taiwan, like its feted night markets, is a timeless feast. </p>-->
            </div>
        </div>
    </div>
</section>

<!-- Things to do Grid -->
<section class="">

    <div class="container">

        <div class="row masonry masonryFlyIn">
            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 1">
                    <a href="things-to-do.php?page=culture-and-heritage.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 2">
                    <a href="things-to-do.php?page=indigenous-culture.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 3">
                    <a href="things-to-do.php?page=temples.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 4">
                    <a href="things-to-do.php?page=cuisine.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 5">
                    <a href="things-to-do.php?page=outdoor.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>   

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 6">
                    <a href="things-to-do.php?page=wellness.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 7">
                    <a href="things-to-do.php?page=ecotourism.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 masonry-item project">
                <div class="image-tile inner-title zoomin" v-for="thingslevel1 in searched_table_data_level1" v-show="thingslevel1.id == 8">
                    <a href="things-to-do.php?page=festivals.php">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section><!-- end of Things to do  -->
<footer class="footer-1 bg-dark">
    <div class="container">
        <div class="row">

            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">PLACE TO GO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="place-to-go.php">Northern Taiwan</a></li>
                        <li><a href="place-to-go.php">Central Taiwan</a></li>
                        <li><a href="place-to-go.php">Eastern Taiwan</a></li>
                        <li><a href="place-to-go.php">Southern Taiwan</a></li>
                        <li><a href="place-to-go.php">Offshore Islands</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">THINGS TO DO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="culture-and-heritage.php">Culture and Heritage</a></li>
                        <li><a href="indigenous-culture.php">Indigenous Culture</a></li>
                        <li><a href="temples.php">Temples</a></li>
                        <li><a href="cuisine.php">Cuisine</a></li>
                        <li><a href="outdoor.php">Adventure/Outdoor</a></li>
                        <li><a href="wellness.php">Wellness</a></li>
                        <li><a href="ecotourism.php">Ecotourism</a></li>
                        <li><a href="festivals.php">Festivals</a></li>
                    </ul>
                </div>

            </div>
            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">BEFORE YOU GO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="travel-tips.php">Travel Tips</a></li>
                        <li><a href="getting-there.php">Getting There</a></li>
                        <li><a href="getting-around.php">Getting Around</a></li>
                        <li><a href="visas.php">Visa</a></li>
                        <li><a href="language.php">Language</a></li>
                    </ul>
                </div>
            </div>                    
            <div class="col-md-2 col-xs-6 border-right-line">
                <div class="widget">
                    <a href="event.php"><h6 class="title">EVENT</h6></a>
                    <a href="deals.php"><h6 class="title">DEALS</h6></a>
                    <a href="trade.php"><h6 class="title">TRADE</h6></a>
                    <br>
                    <br>
                </div>
            </div>
            <div class="col-md-3 col-xs-10">
                <a href="#"><img alt="Logo" class="logo" src="img/logo-light.png"></a>
                <ul class="list-inline social-list">
                    <li>
                        <a href="https://www.facebook.com/TourTaiwan/" target="_blank">
                            <i class="ti-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/TaiwanTourism" target="_blank">
                            <i class="ti-twitter-alt"></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://www.youtube.com/user/taiwanbesttrip" target="_blank">
                            <i class="ti-youtube"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/ttb_na/" target="_blank">
                            <i class="ti-instagram"></i>
                        </a>
                    </li>
                </ul>
                <span class="sub">© 2017-2018 go2taiwan.net</span>
            </div>
        </div>
    </div>

    <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
</footer>



<section class="kenburns cover fullscreen image-slider slider-all-controls">
    <ul class="slides">
        <li class="image-bg">
            <div class="background-image-holder" v-bind:style="{ zIndex: 2, opacity: 1, backgroundImage: 'url(' + imgPath + things.image + ')' }">
                <!--<img alt="Background Image" class="background-image" v-bind:src="place.image" >-->
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                        <p class="lead">
                            <a class="btn btn-sm mt104" href="#main">MORE</a>
                        </p>
                    </div>
                </div>
            </div>
        </li>

        <!-- level1輪播 -->
        <!--<li class="image-bg" v-for="thingslevel1 in searched_table_data_level1">
            <div class="background-image-holder" v-bind:style="{ 'background-image': 'url(' + imgPath + thingslevel1.image + ')' }">
                <img alt="Background Image" class="background-image" v-bind:src="place.image" >
            </div>
            <div class="container v-align-transform">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <img src="img/imagine-Hero-logo.png" alt="" class="text-center">
                        <p class="lead">
                            <a class="btn btn-sm mt104" v-bind:href="indigenous-culture.php">MORE</a>
                        </p>
                    </div>
                </div>
            </div>
        </li>-->
    </ul>
</section>

<section class="bg-primary" id="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4">
                <span class="ti-plus prime-plus"></span>
                <h1 class="uppercase mb24 bold" v-html="things.title"></h1>
            </div>
            <div class="col-md-8 col-md-offset-1 col-sm-8" v-html="things.description">
                <!--<strong class="article">Taiwan has inherited Chinese traditions of religion and philosophy, as well as that country’s vast literary and aesthetic canon. The island is also a proud heir of the Austronesian culture of its indigenous people. The influences of the Japanese, and to a lesser extent the Dutch and the Spanish are not hard to find either. These rich and varied legacies are what captivate about Taiwan, but even more so is how it succeeded in turning them into an identity as distinctly Taiwanese as Taroko Gorge or a sculpture by Ju Ming.</strong>-->
            </div>
        </div>
    </div>
</section>

<!-- Things to do FlexSlider -->
<section class="pt120 pb120 bg-features" id="features">

    <div class="container">
        <div><h3>Features</h3></div>
        <div class="row">
            <!-- Place somewhere in the <body> of your page -->
            <div class="flexslider">
                <ul class="slides">
                    <li class="feature-pad" v-for="thingslevel2 in searched_table_data_level2">
                        <a v-bind:href="thingslevel2.href">
                            <img v-bind:src="imgPath + thingslevel2.image2" />
                            <p class="flex-caption" v-html="thingslevel2.title2"></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    
</section>


<!-- Things to do Grid -->
<section class="">

    <div class="container">
        
        <div class="row masonry masonryFlyIn">
            <div class="col-md-4 col-sm-6 masonry-item project" v-for="thingslevel1 in searched_table_data_level1feature">
                <div class="image-tile inner-title zoomin">
                    <a v-bind:href="thingslevel1.href">
                        <img alt="Pic" v-bind:src="imgPath + thingslevel1.image2">
                        <div class="title">
                            <h5 class="uppercase mb0" v-html="thingslevel1.title2"></h5><i class="ti-plus grid-plus"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</section><!-- end of Things to do  -->


<footer class="footer-1 bg-dark">
    <div class="container">
        <div class="row">

            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">PLACE TO GO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="place-to-go.php">Northern Taiwan</a></li>
                        <li><a href="place-to-go.php">Central Taiwan</a></li>
                        <li><a href="place-to-go.php">Eastern Taiwan</a></li>
                        <li><a href="place-to-go.php">Southern Taiwan</a></li>
                        <li><a href="place-to-go.php">Offshore Islands</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">THINGS TO DO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="things-to-do.php?page=culture-and-heritage">Culture and Heritage</a></li>
                        <li><a href="indigenous-culture.php">Indigenous Culture</a></li>
                        <li><a href="temples.php">Temples</a></li>
                        <li><a href="cuisine.php">Cuisine</a></li>
                        <li><a href="outdoor.php">Adventure/Outdoor</a></li>
                        <li><a href="wellness.php">Wellness</a></li>
                        <li><a href="ecotourism.php">Ecotourism</a></li>
                        <li><a href="festivals.php">Festivals</a></li>
                    </ul>
                </div>

            </div>
            <div class="col-md-2 col-xs-6">
                <div class="widget">
                    <h6 class="title">BEFORE YOU GO</h6>
                    <ul class="link-list recent-posts">
                        <li><a href="travel-tips.php">Travel Tips</a></li>
                        <li><a href="getting-there.php">Getting There</a></li>
                        <li><a href="getting-around.php">Getting Around</a></li>
                        <li><a href="visas.php">Visa</a></li>
                        <li><a href="language.php">Language</a></li>
                    </ul>
                </div>
            </div>                    
            <div class="col-md-2 col-xs-6 border-right-line">
                <div class="widget">
                    <a href="event.php"><h6 class="title">EVENT</h6></a>
                    <a href="deals.php"><h6 class="title">DEALS</h6></a>
                    <a href="trade.php"><h6 class="title">TRADE</h6></a>
                    <br>
                    <br>
                </div>
            </div>
            <div class="col-md-3 col-xs-10">
                <a href="#"><img alt="Logo" class="logo" src="img/logo-light.png"></a>
                <ul class="list-inline social-list">
                    <li>
                        <a href="https://www.facebook.com/TourTaiwan/" target="_blank">
                            <i class="ti-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/TaiwanTourism" target="_blank">
                            <i class="ti-twitter-alt"></i>
                        </a>
                    </li>

                    <li>
                        <a href="https://www.youtube.com/user/taiwanbesttrip" target="_blank">
                            <i class="ti-youtube"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/ttb_na/" target="_blank">
                            <i class="ti-instagram"></i>
                        </a>
                    </li>
                </ul>
                <span class="sub">© 2017-2018 go2taiwan.net</span>
            </div>
        </div>
    </div>

    <a class="btn btn-sm fade-half back-to-top inner-link" href="#top">Top</a>
</footer>